package ar.com.gestionit.dao;

import java.io.Serializable;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PagedComboRepository<T, ID extends Serializable> {
	Set<T> findByIdIn(String[] id);
	Page<T> findByOrderByNombre(Pageable pageable);
	Page<T> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, String nombre);
}
