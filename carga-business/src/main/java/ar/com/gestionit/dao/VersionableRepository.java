package ar.com.gestionit.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

@NoRepositoryBean
public interface VersionableRepository<ID extends Serializable> {
	@Query("select e.version from #{#entityName} e where (e.id = :id)")
	Long findLastVersion(@Param("id") ID id);
}
