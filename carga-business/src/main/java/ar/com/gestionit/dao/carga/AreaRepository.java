package ar.com.gestionit.dao.carga;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.carga.Area;

public interface AreaRepository extends PagingAndSortingRepository<Area, Long>, PagedComboRepository<Area, Long>{
	
	/** Defecto para grillas **/
	/** Parametros agregar los where con los parametros que se deseen filtrar **/ 
	@Query("select e from Area e where (convert(upper(e.descripcion),'US7ASCII') like convert(:descripcion,'US7ASCII') or :descripcion is null)")
	Page<Area> findBySearch(Pageable pageRequest, @Param("descripcion") String descripcion);

	@Query("select e from Area e where rtrim(lower(e.descripcion)) = rtrim(lower(:descripcion))")
	Page<Area> findByDescripcion(Pageable pageRequest, @Param("descripcion") String descripcion);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 areas con el mismo nombre
	@Query("select count(e) from Area e where e.descripcion = :descripcion ")
	Long countExisteDescripcion(@Param("descripcion") String descripcion);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 areas con el mismo nombre
	@Query("select count(e) from Area e where e.descripcion = :descripcion and e.id <> :id")
	Long countExisteDescripcion(@Param("id") Long id, @Param("descripcion") String descripcion);
	
	@Override
	@Query("select e from Area e order by e.descripcion")
	Page<Area> findByOrderByNombre(Pageable pageable);
	
	@Override
	@Query("select e from Area e where convert(upper(e.descripcion),'US7ASCII') like convert(upper(:search),'US7ASCII')")
	Page<Area> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("search") String search);

}
