package ar.com.gestionit.dao.carga;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.carga.Cliente;

public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long>, PagedComboRepository<Cliente, Long> {

	@Query("select e from Cliente e where (convert(upper(e.razonSocial),'US7ASCII') like convert(:razonSocial,'US7ASCII') or :razonSocial is null) and (:cuit is null  or e.cuit = :cuit) ")
	Page<Cliente> findByCuitAndRazonSocial(Pageable pageRequest, @Param("razonSocial") String razonSocial, @Param("cuit") String cuit);

	@Query("select e from Cliente e where e.cuit = :cuit")
	Cliente findByCuit(@Param("cuit") Long cuit);

//	@Query("select e from Cliente e where rtrim(lower(e.numeroCliente)) = rtrim(lower(:numeroCliente)) ")
//	Cliente findByNumeroCliente(@Param("numeroCliente") String numeroCliente);
	
	@Query("select count(e) from Cliente e where e.cuit = :cuit ")
	Long countExisteCuit(@Param("cuit") String cuit);
	
	@Query("select count(e) from Cliente e where e.cuit = :cuit and e.id <> :id")
	Long countExisteCuit(@Param("id") Long id, @Param("cuit") String cuit);

	@Override
	@Query("select e from Cliente e order by e.cuit")
	Page<Cliente> findByOrderByNombre(Pageable pageable);

	@Override
	@Query("select e from Cliente e where (lower(e.cuit) like :nombre or lower(e.razonSocial) like :nombre) order by e.cuit")
	Page<Cliente> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("nombre") String nombre);
}
