package ar.com.gestionit.dao.carga;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.carga.Contrato;

public interface ContratoRepository extends PagingAndSortingRepository<Contrato, Long>, PagedComboRepository<Contrato, Long> {

	
//	
//	En vez de findByNumeroAndRazonSocial, defino findBySearch porque me parece que es el nombre genérico que RA le dio a los filtros
//	
	
	/** Defecto para grillas **/
	/** Parametros agregar los where con los parametros que se deseen filtrar **/ 
	@Query("select e from Contrato e where (convert(upper(e.cliente.razonSocial),'US7ASCII') like convert(:clienteRazonSocial,'US7ASCII') or :clienteRazonSocial is null) and (convert(upper(e.nombre),'US7ASCII') like convert(:nombre,'US7ASCII') or :nombre is null)")
	Page<Contrato> findBySearch(Pageable pageRequest, @Param("clienteRazonSocial") String clienteRazonSocial, @Param("nombre") String nombre);

	/** Defecto para grillas -- Sin paginado **/
	@Query("select e from Contrato e where (convert(upper(e.descripcion),'US7ASCII') like convert(:descripcion,'US7ASCII') or :descripcion is null)")
	List<Contrato> findBySearch(@Param("descripcion") String descripcion);

	//Seteo para los combos
	@Override
	@Query("select e from Contrato e order by e.descripcion")
	Page<Contrato> findByOrderByNombre(Pageable pageable);

	@Override
	@Query("select e from Contrato e where convert(upper(e.descripcion),'US7ASCII') like convert(upper(:search),'US7ASCII')")
	Page<Contrato> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("search") String search);

	/** Busqueda para el Combo ClienteContrato **/
	@Query("select e from Contrato e where (convert(upper(e.nombre),'US7ASCII') like convert(:search,'US7ASCII') or :search is null) or (convert(upper(e.cliente.razonSocial),'US7ASCII') like convert(upper(:search),'US7ASCII') or :search is null)")
	List<Contrato> findClienteContrato(@Param("search") String search);


}
