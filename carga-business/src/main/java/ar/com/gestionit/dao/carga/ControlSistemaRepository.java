package ar.com.gestionit.dao.carga;

import org.springframework.data.repository.PagingAndSortingRepository;

import ar.com.gestionit.model.carga.ControlSistema;

public interface ControlSistemaRepository extends PagingAndSortingRepository<ControlSistema, Long> {

	ControlSistema findFirstByOrderById();

}
