package ar.com.gestionit.dao.carga;

import org.springframework.data.repository.PagingAndSortingRepository;

import ar.com.gestionit.model.carga.LogProceso;

public interface LogProcesoRepository  extends PagingAndSortingRepository<LogProceso, Long>{
	
}
