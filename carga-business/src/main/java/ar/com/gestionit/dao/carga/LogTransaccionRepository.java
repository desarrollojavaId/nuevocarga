package ar.com.gestionit.dao.carga;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.model.carga.LogTransaccion;

public interface LogTransaccionRepository  extends PagingAndSortingRepository<LogTransaccion, Long>{

	@Query("Select e From LogTransaccion e where (:tipoTransaccion is null or lower(e.tipoTransaccion) = :tipoTransaccion) and (:tabla is null or lower(e.tabla) = :tabla) and (:campo is null or lower(e.campo) = :campo) and (:usuario is null or lower(e.usuario) = :usuario) and e.fechaTransaccion between :fechaDesde and :fechaHasta order by e.id")
	List<LogTransaccion> findTransaccionesByFechas(@Param("tipoTransaccion") String tipoTransaccion,@Param("tabla") String tabla,@Param("campo") String campo,@Param("usuario") String usuario,
			@Param("fechaDesde") Date fechaDesdeConsulta, @Param("fechaHasta") Date fechaHastaConsulta);

	@Query("Select e From LogTransaccion e where (:tipoTransaccion is null or e.tipoTransaccion = :tipoTransaccion) and (:tabla is null or lower(e.tabla) = :tabla) and (:campo is null or lower(e.campo) = :campo) and (:usuario is null or lower(e.usuario) = :usuario) order by e.id")
	List<LogTransaccion> findTransacciones(@Param("tipoTransaccion") Integer tipoTransaccion,@Param("tabla") String tabla,@Param("campo") String campo,@Param("usuario") String usuario);
	
}
