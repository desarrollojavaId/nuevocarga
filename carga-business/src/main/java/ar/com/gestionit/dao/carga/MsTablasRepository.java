package ar.com.gestionit.dao.carga;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.model.carga.MsTablas;
import ar.com.gestionit.model.carga.MsTablasPK;

public interface MsTablasRepository extends PagingAndSortingRepository<MsTablas, Long> {
	
	
	@Query("select e from MsTablas e where e.id.tabla = :tabla and (e.id.codigo = :codigo or :codigo is null) and (e.id.subCodigo <> 0) and (e.id.subCodigo = :subCodigo or :subCodigo is null) and (upper(e.descripcion) like :descripcion or :descripcion is null) order by e.id.subCodigo")
	Page<MsTablas> pageByTablaCodigoNotSubCodigo(Pageable pageRequest, @Param("tabla") Integer tabla,
			@Param("codigo") Integer codigo, @Param("subCodigo") Integer subCodigo, @Param("descripcion") String descripcion);
	
	@Query("select e from MsTablas e ")
	Page<MsTablas> findBySearch(Pageable pageRequest);	
	
	@Query("select e from MsTablas e where e.id = :id")
	MsTablas findById(@Param("id") MsTablasPK id);
	
	@Query("select e from MsTablas e where e.id.tabla = :tabla and e.id.codigo = :codigo and (e.id.subCodigo = :subCodigo or :subCodigo is null)")
	MsTablas findByTablaCodigoSubCodigo(@Param("tabla") Integer tabla, @Param("codigo") Integer codigo,
			@Param("subCodigo") Integer subCodigo);
	
	@Query("select e from MsTablas e where e.id.tabla = :tabla and e.descripcionReducida = :descripcionReducida and (e.id.subCodigo = :subCodigo or :subCodigo is null)")
	MsTablas findByTablaDescripcionSubCodigo(@Param("tabla") Integer tabla, @Param("descripcionReducida") String descripcionReducida,
			@Param("subCodigo") Integer subCodigo);	
	

	@Query("select e from MsTablas e where e.id.tabla = :tabla and e.id.codigo = :codigo and e.id.subCodigo <> :subCodigo")
	MsTablas findByTablaCodigoNotSubCodigo(@Param("tabla") Integer tabla, @Param("codigo") Integer codigo,
			@Param("subCodigo") Integer subCodigo);

	@Query("select e from MsTablas e where e.id.tabla = :tabla and (e.id.codigo = :codigo or :codigo is null) and (e.id.subCodigo = :subCodigo or :subCodigo is null) and (upper(e.descripcion) like :descripcion or :descripcion is null) order by e.id.codigo")
	Page<MsTablas> pageByTablaCodigoSubCodigo(Pageable pageRequest, @Param("tabla") Integer tabla,
			@Param("codigo") Integer codigo, @Param("subCodigo") Integer subCodigo, @Param("descripcion") String descripcion);

	@Query("select e from MsTablas e where e.id.tabla = :tabla and e.id.codigo = :codigo order by e.id.codigo")
	List<MsTablas> findAllByTablaAndCodigo(@Param("tabla") Integer tabla, @Param("codigo") Integer codigo);


}
