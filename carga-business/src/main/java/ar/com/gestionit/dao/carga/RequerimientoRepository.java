package ar.com.gestionit.dao.carga;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.carga.Requerimiento;

public interface RequerimientoRepository extends PagingAndSortingRepository<Requerimiento, Long>, PagedComboRepository<Requerimiento, Long> {
	
	/** Defecto para grillas **/
	/** Parametros agregar los where con los parametros que se deseen filtrar **/ 
	
	@Query("select e from Requerimiento e where (convert(upper(e.titulo),'US7ASCII') like convert(:titulo,'US7ASCII') or :titulo is null) and (:nuss is null  or e.nuss = :nuss)")
	Page<Requerimiento> findBySearch(Pageable pageRequest, @Param("nuss") Long nuss, @Param("titulo") String titulo);
	
	
	/*
	@Query("select e from Sector e where rtrim(lower(e.descripcion)) = rtrim(lower(:descripcion))")
	Page<Sector> findByDescripcion(Pageable pageRequest, @Param("descripcion") String descripcion);
*/	
	// Copiado desde Cliente, serviría para no dar de alta 2 sector con el mismo nuss
	@Query("select count(e) from Requerimiento e where e.nuss = :nuss ")
	Long countExisteNuss(@Param("nuss") Long nuss);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 sector con el mismo nuss
	@Query("select count(e) from Requerimiento e where e.nuss = :nuss and e.id <> :id")
	Long countExisteNuss(@Param("id") Long id, @Param("nuss") Long descripcion);

	@Override
	@Query("select e from Requerimiento e order by e.nuss")
	Page<Requerimiento> findByOrderByNombre(Pageable pageable);

	@Override
	@Query("select e from Requerimiento e where (lower(e.nuss) like :nombre or lower(e.titulo) like :nombre) order by e.nuss")
	Page<Requerimiento> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("nombre") String nombre);
}
