package ar.com.gestionit.dao.carga;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.carga.Sector;

public interface SectorRepository extends PagingAndSortingRepository<Sector, Long>, PagedComboRepository<Sector, Long> {
	
	/** Defecto para grillas **/
	/** Parametros agregar los where con los parametros que se deseen filtrar **/ 
	@Query("select e from Sector e where (convert(upper(e.area.descripcion),'US7ASCII') like convert(:areaDescripcion,'US7ASCII') or :areaDescripcion is null) and (convert(upper(e.descripcion),'US7ASCII') like convert(:sectorDescripcion,'US7ASCII') or :sectorDescripcion is null)")
	Page<Sector> findBySearch(Pageable pageRequest, @Param("areaDescripcion") String areaDescripcion, @Param("sectorDescripcion") String sectorDescripcion);

	@Query("select e from Sector e where rtrim(lower(e.descripcion)) = rtrim(lower(:descripcion))")
	Page<Sector> findByDescripcion(Pageable pageRequest, @Param("descripcion") String descripcion);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 sector con el mismo nombre
	@Query("select count(e) from Sector e where e.descripcion = :descripcion ")
	Long countExisteDescripcion(@Param("descripcion") String descripcion);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 sector con el mismo nombre
	@Query("select count(e) from Sector e where e.descripcion = :descripcion and e.id <> :id")
	Long countExisteDescripcion(@Param("id") Long id, @Param("descripcion") String descripcion);
	

	@Override
	@Query("select e from Sector e order by e.descripcion")
	Page<Sector> findByOrderByNombre(Pageable pageable);
	
	@Override
	@Query("select e from Sector e where convert(upper(e.descripcion),'US7ASCII') like convert(upper(:search),'US7ASCII')")
	Page<Sector> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("search") String search);
	

}
