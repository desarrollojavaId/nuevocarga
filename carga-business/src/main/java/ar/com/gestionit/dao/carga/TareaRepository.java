package ar.com.gestionit.dao.carga;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.model.carga.Tarea;

public interface TareaRepository extends PagingAndSortingRepository<Tarea, Long> {
	
	/** Defecto para grillas **/
	/** Parametros agregar los where con los parametros que se deseen filtrar **/ 
	
	@Query("select e from Tarea e where (:titulo is null or (convert(upper(e.titulo),'US7ASCII') like convert(:titulo,'US7ASCII'))) and (:nuss is null  or e.requerimiento.nuss = :nuss)")
	Page<Tarea> findBySearch(Pageable pageRequest, @Param("nuss") Long nuss, @Param("titulo") String titulo);
	
	
	/*
	// Copiado desde Cliente, serviría para no dar de alta 2 sector con el mismo nuss
	@Query("select count(e) from Tarea e where e.nuss = :nuss ")
	Long countExisteNuss(@Param("nuss") Long nuss);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 sector con el mismo nuss
	@Query("select count(e) from Tarea e where e.nuss = :nuss and e.id <> :id")
	Long countExisteNuss(@Param("id") Long id, @Param("nuss") Long descripcion);

*/	


}
