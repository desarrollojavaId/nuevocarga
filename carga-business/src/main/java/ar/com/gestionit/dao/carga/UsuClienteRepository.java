package ar.com.gestionit.dao.carga;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.carga.UsuCliente;

public interface UsuClienteRepository extends PagingAndSortingRepository<UsuCliente, Long>, PagedComboRepository<UsuCliente, Long>{

	/** Defecto para grillas **/
	/** Parametros agregar los where con los parametros que se deseen filtrar **/ 
	@Query("select e from UsuCliente e where (convert(upper(e.cliente.razonSocial),'US7ASCII') like convert(:razonSocial,'US7ASCII') or :razonSocial is null) and (convert(upper(e.usuario.nombre),'US7ASCII') like convert(:nombre,'US7ASCII') or :nombre is null) and (convert(upper(e.usuario.apellido),'US7ASCII') like convert(:apellido,'US7ASCII') or :apellido is null)")
	Page<UsuCliente> findBySearch(Pageable pageRequest, @Param("razonSocial") String razonSocial, @Param("nombre") String nombre, @Param("apellido") String apellido);

	/** Defecto para grillas -- Sin paginado **/
	@Query("select e from UsuCliente e where (convert(upper(e.usuario.nombre),'US7ASCII') like convert(:nombre,'US7ASCII') or :nombre is null) and (convert(upper(e.usuario.apellido),'US7ASCII') like convert(:apellido,'US7ASCII') or :apellido is null)")
	List<UsuCliente> findBySearch(@Param("nombre") String nombre, @Param("apellido") String apellido);

	@Query("select count(e) from UsuCliente e where e.usuario.id = :usuarioId and e.id <> :usuClienteId")
	Long countExisteUsuClienteId(@Param("usuClienteId") Long usuClienteId, @Param("usuarioId") Long usuarioId);

	@Query("select count(e) from UsuCliente e where e.usuario.id = :usuarioId ")
	Long countExisteUsuCliente(@Param("usuarioId") Long usuarioId);
	
	//Busqueda por combos
	@Override
	@Query("select e from UsuCliente e order by e.usuario.nombre, e.usuario.apellido")
	Page<UsuCliente> findByOrderByNombre(Pageable pageable);

	@Override
	@Query("select e from UsuCliente e where (convert(upper(e.usuario.nombre),'US7ASCII') like convert(upper(:search),'US7ASCII')) or (convert(upper(e.usuario.apellido),'US7ASCII') like convert(upper(:search),'US7ASCII'))")
	Page<UsuCliente> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("search") String search);
	

}
