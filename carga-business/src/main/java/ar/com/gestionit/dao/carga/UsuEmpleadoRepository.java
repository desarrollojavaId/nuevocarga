package ar.com.gestionit.dao.carga;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.carga.UsuEmpleado;

public interface UsuEmpleadoRepository extends PagingAndSortingRepository<UsuEmpleado, Long>, PagedComboRepository<UsuEmpleado, Long> {

	
//	
//	En vez de findByNumeroAndRazonSocial, defino findBySearch porque me parece que es el nombre genérico que RA le dio a los filtros
//	
	
	/** Defecto para grillas **/
	/** Parametros agregar los where con los parametros que se deseen filtrar **/ 
	@Query("select e from UsuEmpleado e where (convert(upper(e.usuario.nombre),'US7ASCII') like convert(:nombre,'US7ASCII') or :nombre is null) and (convert(upper(e.usuario.apellido),'US7ASCII') like convert(:apellido,'US7ASCII') or :apellido is null)")
	Page<UsuEmpleado> findBySearch(Pageable pageRequest, @Param("nombre") String nombre, @Param("apellido") String apellido);

	/** Defecto para grillas -- Sin paginado **/
	@Query("select e from UsuEmpleado e where (convert(upper(e.usuario.nombre),'US7ASCII') like convert(:nombre,'US7ASCII') or :nombre is null) and (convert(upper(e.usuario.apellido),'US7ASCII') like convert(:apellido,'US7ASCII') or :apellido is null)")
	List<UsuEmpleado> findBySearch(@Param("nombre") String nombre, @Param("apellido") String apellido);

	/** Busqueda según sea o no lider **/
//	@Query("select e from UsuEmpleado e where (esLider = :esLider or :esLider is null) and ((convert(upper(e.usuario.nombre),'US7ASCII') like convert(upper(:search),'US7ASCII') or :search is null) or (convert(upper(e.usuario.apellido),'US7ASCII') like convert(upper(:search),'US7ASCII') or :search is null))")
	@Query("select e from UsuEmpleado e where esLider = :esLider and ((convert(upper(e.usuario.nombre),'US7ASCII') like convert(upper(:search),'US7ASCII') or :search is null) or (convert(upper(e.usuario.apellido),'US7ASCII') like convert(upper(:search),'US7ASCII') or :search is null))")
	List<UsuEmpleado> findByLider(@Param("esLider") String esLider, @Param("search") String search);

	@Override
	@Query("select e from UsuEmpleado e order by e.usuario.nombre, e.usuario.apellido")
	Page<UsuEmpleado> findByOrderByNombre(Pageable pageable);

	@Override
	@Query("select e from UsuEmpleado e where (convert(upper(e.usuario.nombre),'US7ASCII') like convert(upper(:search),'US7ASCII')) or (convert(upper(e.usuario.apellido),'US7ASCII') like convert(upper(:search),'US7ASCII'))")
	Page<UsuEmpleado> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("search") String search);

	@Query("select count(e) from UsuEmpleado e where e.usuario.id = :usuarioId and e.id <> :usuEmpleadoId")
	Long countExisteUsuEmpleadoId(@Param("usuEmpleadoId") Long usuEmpleadoId, @Param("usuarioId") Long usuarioId);

	@Query("select count(e) from UsuEmpleado e where e.usuario.id = :usuarioId ")
	Long countExisteUsuEmpleado(@Param("usuarioId") Long usuarioId);


}
