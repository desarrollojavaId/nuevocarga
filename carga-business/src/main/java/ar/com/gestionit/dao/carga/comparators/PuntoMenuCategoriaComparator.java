package ar.com.gestionit.dao.carga.comparators;

import java.util.Comparator;

import ar.com.gestionit.model.seguridad.PuntoMenuCategoria;

public class PuntoMenuCategoriaComparator implements Comparator<Object>{

	@Override
	public int compare(Object o1, Object o2) {
		PuntoMenuCategoria pm1 = (PuntoMenuCategoria) o1;
		PuntoMenuCategoria pm2 = (PuntoMenuCategoria) o2;
		if (pm1.getOrden() < pm2.getOrden()) {
            return -1;
        }
		if (pm1.getOrden() > pm2.getOrden()) {
            return 1;
        }
        return 0;
	}

}
