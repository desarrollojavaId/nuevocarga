package ar.com.gestionit.dao.seguridad;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.model.seguridad.GrupoPuntoMenu;
import ar.com.gestionit.model.seguridad.PuntoMenuCategoria;

public interface GrupoPuntoMenuRepository extends PagingAndSortingRepository<GrupoPuntoMenu, Long> {

	
	@Query("select e from GrupoPuntoMenu e where e.grupo.id = :id order by e.puntoMenuDetalle.categoriaPuntoMenuId.orden, e.puntoMenuDetalle.orden")
	List<GrupoPuntoMenu> findAllGrupoPuntoMenuByGrupoId(@Param("id") Long id);
	
	@Query("select distinct e.puntoMenuDetalle.categoriaPuntoMenuId from GrupoPuntoMenu e where e.grupo.nombre in (:nombres) order by e.puntoMenuDetalle.categoriaPuntoMenuId.orden")
	List<PuntoMenuCategoria> findAllGrupoPuntoMenuByNombreGrupoList(@Param("nombres") Collection<String> ids);
	
	@Query("select count(e) from GrupoPuntoMenu e where e.puntoMenuDetalle.id in (:id)")
	Long findExistPuntoMenu(@Param("id") List<Long> id);
	
	@Query("select e from GrupoPuntoMenu e where e.puntoMenuDetalle.id = :id")
	GrupoPuntoMenu findByPuntoMenu(@Param("id") Long id);
	
}

