package ar.com.gestionit.dao.seguridad;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.seguridad.Grupo;

public interface GrupoRepository extends PagingAndSortingRepository<Grupo, Long>, PagedComboRepository<Grupo, Long> {
//	Grupo findByNombreIgnoreCaseAndEnabledTrueOrderByNombre(String nombre);
	
//	@Query("select p.nombre from Grupo p where p.nombre = :nombre")
//	List<String> findPermisosByNombreGrupo(@Param("nombre") String nombre);
	
	@Query("select e from Grupo e where ((convert(upper(e.nombre),'US7ASCII') like convert(:nombre,'US7ASCII')) or :nombre is null)")
	Page<Grupo> findBySearch(Pageable pageRequest, @Param("nombre") String nombre);
	
	@Query("select e from Grupo e where e.nombre = :nombre")
	List<Grupo> findByNombre(@Param("nombre") String nombre);
	
	@Query("select count(e) from Grupo e where e.nombre = :nombre")
	Long countExisteGrupo(@Param("nombre") String nombre);
	
	@Query("select count(e) from Grupo e where e.nombre = :nombre and e.id <> :id")
	Long countExisteGrupoById(@Param("nombre") String nombre, @Param("id") Long id);
	
}