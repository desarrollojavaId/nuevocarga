package ar.com.gestionit.dao.seguridad;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.seguridad.PuntoMenu;
import ar.com.gestionit.model.seguridad.PuntoMenuCategoria;
import ar.com.gestionit.model.seguridad.PuntoMenuDetalle;

public interface PuntoMenuRepository extends PagingAndSortingRepository<PuntoMenu, Long>, PagedComboRepository<PuntoMenu, Long> {

	/**Metodos de categoria**/
	
	@Query("select e from PuntoMenuCategoria e where (:id is null  or e.id = :id) ")
	Page<PuntoMenuCategoria> findByCategoria(Pageable pageRequest, @Param("id") Long id);
	
	
	/**Metodos de Detalle**/
	
	@Query("select e from PuntoMenuDetalle e where e.categoriaPuntoMenuId.id = :categoria")
	Page<PuntoMenuDetalle> findAllPuntoMenuByCategoria(Pageable pageRequest,  @Param("categoria") Long categoria);
	
	@Query("select e from PuntoMenuCategoria e where e.id = :id")
	PuntoMenuCategoria findCategoriaById(@Param("id") Long id);
	
	@Query("select e from PuntoMenuDetalle e where e.id = :id")
	PuntoMenuDetalle findDetalleById(@Param("id") Long id);
	
	@Query("select e from PuntoMenuDetalle e where e.categoriaPuntoMenuId.id = :categoria and (:detalleId is null or e.id = :detalleId)")
	Page<PuntoMenuDetalle> findAllPuntoMenuByCategoriaDetalle(Pageable pageRequest,  @Param("categoria") Long categoria, @Param("detalleId") Long detalleId);
	
	
	@Query("select count(e) from PuntoMenu e where e.orden = :orden")
	Long findExistOrder(@Param("orden") Long orden);
	
	@Query("select e from PuntoMenuDetalle e where e.categoriaPuntoMenuId.id = :categoria order by e.orden")
	List<PuntoMenuDetalle> findAllPuntoMenuByCategoriaList(@Param("categoria") Long categoria);
	
	//Se sobreescriben estos dos métodos ya que el objeto PuntoMenu no tiene el atributo nombre y el mismo se requiere para bindear el combo
	@Override
	@Query("select e from PuntoMenu e where e.class = 'Categoria' order by e.descripcionPuntoMenu")
	Page<PuntoMenu> findByOrderByNombre(Pageable pageable);
	
	@Override
	@Query("select e from PuntoMenu e where e.class = 'Categoria' and e.descripcionPuntoMenu = :descPuntoMenu")
	Page<PuntoMenu> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("descPuntoMenu") String descPuntoMenu);
	
	/**/
	@Query("select distinct pto from Grupo e inner join e.grupoPuntoMenu gp inner join gp.puntoMenuDetalle pto where e.nombre in (:nombreGrupos) order by pto.categoriaPuntoMenuId.id, pto.orden")
	List<PuntoMenuDetalle> findAllGrupoPuntoMenuByGrupoNombre(@Param("nombreGrupos") Collection<String> nombreGrupos);

	@Query("select count(e) from PuntoMenu e where e.orden = :orden and e.id <> :id")
	Long findAllGrupoPuntoMenuByGrupoNombre(@Param("orden") Long orden,@Param("id") Long id);
	
}

