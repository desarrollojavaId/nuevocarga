package ar.com.gestionit.dao.seguridad;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.seguridad.PuntoMenuUrl;

public interface PuntoMenuUrlRepository extends PagingAndSortingRepository<PuntoMenuUrl, Long>, PagedComboRepository<PuntoMenuUrl, Long> {

	
	//Se sobreescriben estos dos métodos ya que el objeto PuntoMenu no tiene el atributo nombre y el mismo se requiere para bindear el combo
	@Override
	@Query("select e from PuntoMenuUrl e order by e.id")
	Page<PuntoMenuUrl> findByOrderByNombre(Pageable pageable);
	@Override
	@Query("select e from PuntoMenuUrl e where e.url = :url")
	Page<PuntoMenuUrl> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("url") String url);

}

