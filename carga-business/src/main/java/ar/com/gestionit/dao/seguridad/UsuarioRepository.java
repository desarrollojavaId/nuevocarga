package ar.com.gestionit.dao.seguridad;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.model.seguridad.Usuario;

public interface UsuarioRepository extends PagingAndSortingRepository<Usuario, Long>, PagedComboRepository<Usuario, Long> {

	
//	
//	En vez de findByNumeroAndRazonSocial, defino findBySearch porque me parece que es el nombre genérico que RA le dio a los filtros
//	
	
	/** Defecto para grillas **/
	/** Parametros agregar los where con los parametros que se deseen filtrar **/ 
	@Query("select e from Usuario e where (convert(upper(e.nombreUsuario),'US7ASCII') like convert(:nombreUsuario,'US7ASCII') or :nombreUsuario is null) and (convert(upper(e.nombre),'US7ASCII') like convert(:nombre,'US7ASCII') or :nombre is null) and (convert(upper(e.apellido),'US7ASCII') like convert(:apellido,'US7ASCII') or :apellido is null)")
	Page<Usuario> findBySearch(Pageable pageRequest, @Param("nombreUsuario") String nombreUsuario, @Param("nombre") String nombre, @Param("apellido") String apellido);
	
	/** Defecto para grillas -- Sin paginado **/
	@Query("select e from Usuario e where (convert(upper(e.nombreUsuario),'US7ASCII') like convert(:nombreUsuario,'US7ASCII') or :nombreUsuario is null) and (convert(upper(e.nombre),'US7ASCII') like convert(:nombre,'US7ASCII') or :nombre is null) and (convert(upper(e.apellido),'US7ASCII') like convert(:apellido,'US7ASCII') or :apellido is null)")
	List<Usuario> findBySearch(@Param("nombreUsuario") String nombreUsuario, @Param("nombre") String nombre, @Param("apellido") String apellido);

	@Query("select e from Usuario e where rtrim(lower(e.nombreUsuario)) = rtrim(lower(:nombreUsuario))")
	Page<Usuario> findByNombreUsuario(Pageable pageRequest, @Param("nombreUsuario") String nombreUsuario);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 usuarios con el mismo nombre
	@Query("select count(e) from Usuario e where e.nombreUsuario = :nombreUsuario ")
	Long countExisteNombreUsuario(@Param("nombreUsuario") String nombreUsuario);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 usuarios con el mismo nombre
	@Query("select count(e) from Usuario e where e.nombreUsuario = :nombreUsuario and e.id <> :id")
	Long countExisteNombreUsuario(@Param("id") Long id, @Param("nombreUsuario") String nombreUsuario);

	@Override
	@Query("select e from Usuario e order by e.nombre, e.apellido")
	Page<Usuario> findByOrderByNombre(Pageable pageable);

	@Override
	@Query("select e from Usuario e where (convert(upper(e.nombre),'US7ASCII') like convert(upper(:search),'US7ASCII')) or (convert(upper(e.apellido),'US7ASCII') like convert(upper(:search),'US7ASCII'))")
	Page<Usuario> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageable, @Param("search") String search);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 usuarios con el mismo nombre
	@Query("select count(e) from Usuario e where e.iniciales = :iniciales")
	Long countExisteInicialesUsuario(@Param("iniciales") String iniciales);
	
	// Copiado desde Cliente, serviría para no dar de alta 2 usuarios con el mismo nombre
	@Query("select count(e) from Usuario e where e.iniciales = :iniciales and e.id <> :id")
	Long countExisteInicialesUsuario(@Param("id") Long id, @Param("iniciales") String iniciales);

	//Obtengo los registros para el uso del combo buscando tanto por nombre como por apellido
	@Query("select e from Usuario e where (convert(upper(e.nombre),'US7ASCII') like convert(upper(:search),'US7ASCII')) or (convert(upper(e.apellido),'US7ASCII') like convert(upper(:search),'US7ASCII'))")
	Page<Usuario> findAllByNombreAndApellido(Pageable pageRequest, @Param("search") String search);
	
}
