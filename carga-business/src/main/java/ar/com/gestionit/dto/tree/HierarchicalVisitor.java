package ar.com.gestionit.dto.tree;

public interface HierarchicalVisitor {
    boolean visitEnter(NodeTreeDto node);
    boolean visitExit(NodeTreeDto node); 
}
