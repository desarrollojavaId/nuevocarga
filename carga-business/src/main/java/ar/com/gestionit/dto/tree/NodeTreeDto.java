package ar.com.gestionit.dto.tree;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NodeTreeDto {
	private String key;
	private String type;
	private String title;
	private boolean folder;
	private List<NodeTreeDto> children = new ArrayList<NodeTreeDto>();

	public boolean accept(HierarchicalVisitor v) {
		boolean visitEnter = v.visitEnter(this);
		if(visitEnter && children != null) {
			for (NodeTreeDto cn : children) {
				boolean success = cn.accept(v);
				if (!success) {
					break;
				}
			}
		}
		return v.visitExit(this) && visitEnter;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<NodeTreeDto> getChildren() {
		return children;
	}

	public void setChildren(List<NodeTreeDto> children) {
		this.children = children;
	}

	public boolean isFolder() {
		return folder;
	}

	public void setFolder(boolean folder) {
		this.folder = folder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeTreeDto other = (NodeTreeDto) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
