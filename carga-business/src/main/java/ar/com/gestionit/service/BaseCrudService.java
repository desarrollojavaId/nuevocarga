package ar.com.gestionit.service;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import ar.com.gestionit.utils.BeanUtils;

public abstract class BaseCrudService<T, ID extends Serializable> {

	protected PagingAndSortingRepository<T, ID> repository;
	
	private static final String usuarioBajaLog = "usuarioBajaLog";
	
	private static final String horaBajaLog = "horaBajaLog";
	
	private static final String fechaBajaLog = "fechaBajaLog";
	
	private static final String usuarioUltimaActualizacion = "usuarioUltimaActualizacion";
	
	private static final String fechaUltimaActualizacion = "fechaUltimaActualizacion";
	
	private static final String usuarioAlta = "usuarioAlta";
	
	private static final String fechaAlta = "fechaAlta";
	
	private static final String horaAlta = "horaAlta";
	
	private static final String horaUltAct = "horaUltAct";
	
	public T save(T entity) {
		setCreatedData(entity);
		setUpdatedData(entity);
		return repository.save(entity);
	}
	
	public T bajaLogica(T entity) {
		setBajaLogicaData(entity);
		setUpdatedData(entity);
		return repository.save(entity);
	}

	private void setBajaLogicaData(T entity) {
		if (BeanUtils.propertyExists(entity, usuarioBajaLog)) {
			BeanUtils.setProperty(entity, usuarioBajaLog, getUsername());
		}
		if (BeanUtils.propertyExists(entity, fechaBajaLog)) {
			BeanUtils.setProperty(entity, fechaBajaLog, new Date());
		}
		if (BeanUtils.propertyExists(entity, horaBajaLog)) {
			BeanUtils.setProperty(entity, horaBajaLog, new Date());
		}
	}

	private void setUpdatedData(T entity) {
		if (BeanUtils.propertyExists(entity, usuarioUltimaActualizacion)) {
			BeanUtils.setProperty(entity, usuarioUltimaActualizacion, getUsername());
		}
		if (BeanUtils.propertyExists(entity, fechaUltimaActualizacion)) {
			BeanUtils.setProperty(entity, fechaUltimaActualizacion, new Date());
		}
		if (BeanUtils.propertyExists(entity, horaUltAct) && BeanUtils.isNull(entity, horaUltAct)) {
			BeanUtils.setProperty(entity, horaUltAct, new Date());
		}
	}

	private void setCreatedData(T entity) {
		if (BeanUtils.propertyExists(entity, usuarioAlta) && BeanUtils.isNull(entity, usuarioAlta)) {
			BeanUtils.setProperty(entity, usuarioAlta, getUsername());
		}
		if (BeanUtils.propertyExists(entity, fechaAlta) && BeanUtils.isNull(entity, fechaAlta)) {
			BeanUtils.setProperty(entity, fechaAlta, new Date());
		}
		
		if (BeanUtils.propertyExists(entity, horaAlta) && BeanUtils.isNull(entity, horaAlta)) {
			BeanUtils.setProperty(entity, horaAlta, new Date());
		}

	}

	public void remove(ID id) {
		repository.delete(id);
	}

	/**
	 * Obtiene la entidad a partir de su id.
	 * 
	 * @param id
	 * @return La entidad correspondiente al id.
	 */
	public T findById(ID id) {
		return repository.findOne(id);
	}

	/**
	 * Obtiene una lista con todos los registros para esa entidad. Si entonces
	 * incluye las eliminadas.
	 * 
	 * @param includeDisabled
	 * @return Lista con todas las entidades.
	 */
	public Iterable<T> findAll() {
		return repository.findAll();
	}

	/**
	 * Obtiene una lista con todos los registros para esa entidad. Si
	 * includeDisabled es true y la entidad implementa entonces incluye las
	 * eliminadas. Utiliza los datos de paginado para paginar el resultado.
	 * 
	 * @param pageRequest
	 * @param includeDisabled
	 * @return La pagina requerida con la lista de entidades correspondientes a
	 *         la pagina
	 * @see PageRequest
	 * @see PageData
	 */
	public Page<T> findAll(Pageable pageRequest) {
		return repository.findAll(pageRequest);
	}

	public void setRepository(PagingAndSortingRepository<T, ID> repository) {
		this.repository = repository;
	}

	protected String getUsername() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			return ((UserDetails) principal).getUsername();
		} else {
			return principal.toString();
		}
	}
	
	public void removeAll() {
		repository.deleteAll();
	}
	
	public String getUsuarioLogeado() {
		return getUsername();
	}
}
