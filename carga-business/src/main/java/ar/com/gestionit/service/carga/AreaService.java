package ar.com.gestionit.service.carga;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.AreaRepository;
import ar.com.gestionit.model.carga.Area;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class AreaService extends BaseCrudService<Area, Long> {

	@Autowired
	public AreaService(AreaRepository repository) {
		setRepository(repository);
	}
	
	private AreaRepository getRepository() {
		return (AreaRepository) this.repository;
	}
	
	public Page<Area> findBySearch(Pageable pageRequest, String descripcion) {
		return getRepository().findBySearch(pageRequest, descripcion);
	}
	
//	public List<Area> findBySearch(String nombreArea, String nombre, String apellido) {
//		return getRepository().findBySearch(nombreArea, nombre, apellido);
//	}
//	
	public Page<Area> findByDescripcion(Pageable pageRequest, String descripcion) {
		return getRepository().findByDescripcion(pageRequest, descripcion);
	}
	
	// Utilizado para no dar de alta 2 areas con el mismo nombre
	public Long countExisteDescripcion(String descripcion) {
		return getRepository().countExisteDescripcion(descripcion);
	}
	
	// Utilizado para no dar de alta 2 areas con el mismo nombre
	public Long countExisteDescripcion(Long id, String descripcion) {
		return getRepository().countExisteDescripcion(id, descripcion);
	}
/*
	public Page<Area> findByOrderByNombre(Pageable pageRequest) {
		return getRepository().findByOrderByNombre(pageRequest);
	}

	public Page<Area> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageRequest, String nombreArea) {
		return getRepository().findByNombreLikeIgnoreCaseOrderByNombre(pageRequest, nombreArea);
	}
	*/

}