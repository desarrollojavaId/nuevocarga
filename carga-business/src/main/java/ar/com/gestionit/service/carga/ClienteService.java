package ar.com.gestionit.service.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.ClienteRepository;
import ar.com.gestionit.model.carga.Cliente;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class ClienteService extends BaseCrudService<Cliente, Long> {

	@Autowired
	public ClienteService(ClienteRepository repository) {
		setRepository(repository);
	}

	private ClienteRepository getRepository() {
		return (ClienteRepository) this.repository;
	}

	public Page<Cliente> findByCuitAndRazonSocial(PageRequest pageRequest, String razonSocial, String cuit) {
		return getRepository().findByCuitAndRazonSocial(pageRequest, razonSocial, cuit);
	}

//	public Cliente findByNumeroCliente(String numeroCliente) {
//		return getRepository().findByNumeroCliente(numeroCliente);
//	}
	
	public Long countExisteCuit(String cuit) {
		return getRepository().countExisteCuit(cuit);
	}
	public Long countExisteCuit(Long id, String cuit) {
		return getRepository().countExisteCuit(id, cuit);
	}

	public Cliente findByCuit(Long cuit) {
		return getRepository().findByCuit(cuit);
	}
	
	
}