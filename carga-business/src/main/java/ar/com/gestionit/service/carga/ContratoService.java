package ar.com.gestionit.service.carga;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.ContratoRepository;
import ar.com.gestionit.model.carga.Contrato;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class ContratoService extends BaseCrudService<Contrato, Long> {

	@Autowired
	public ContratoService(ContratoRepository repository) {
		setRepository(repository);
	}
	
	private ContratoRepository getRepository() {
		return (ContratoRepository) this.repository;
	}
	
	public Page<Contrato> findBySearch(Pageable pageRequest, String clienteRazonSocial, String nombre) {
		return getRepository().findBySearch(pageRequest, clienteRazonSocial, nombre);
	}
	
	public List<Contrato> findBySearch(String descripcion) {
		return getRepository().findBySearch(descripcion);
	}
	
	public List<Contrato> findClienteContrato(String search) {
		return getRepository().findClienteContrato(search);
	}

	
	//	
//	public Long countExisteContratoId(Long contratoId, Long usuarioId) {
//		return getRepository().countExisteContratoId(contratoId, usuarioId);
//	}

//	public Long countExisteContrato(Long usuarioId) {
//		return getRepository().countExisteContrato(usuarioId);
//	}	
	

}