package ar.com.gestionit.service.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.ControlSistemaRepository;
import ar.com.gestionit.model.carga.ControlSistema;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class ControlSistemaService extends BaseCrudService<ControlSistema, Long> {

	@Autowired
	public ControlSistemaService (ControlSistemaRepository repository) {
		setRepository(repository);
	}

	private ControlSistemaRepository getRepository() {
		return (ControlSistemaRepository) this.repository;
	}

	public ControlSistema findFirstByOrderById() {
		return getRepository().findFirstByOrderById();
	}
	
	
}