package ar.com.gestionit.service.carga;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.LogProcesoRepository;
import ar.com.gestionit.model.carga.LogProceso;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class LogProcesoService extends BaseCrudService<LogProceso, Long> {

	@Autowired
	public LogProcesoService(LogProcesoRepository repository) {
		setRepository(repository);
	}

	@SuppressWarnings("unused")
	private LogProcesoRepository getRepository() {
		return (LogProcesoRepository) this.repository;
	}

	public void guardarComienzo(Integer nroDeProceso, String grabadoDesde, String usuario) {
		guardar(nroDeProceso, 100, "Inicio.", grabadoDesde, null, usuario);
	}

	public void guardarComienzoPasoAccion(Integer nroDeProceso, String pasoAccion, String grabadoDesde, String usuario) {
		guardar(nroDeProceso, 200, pasoAccion, grabadoDesde, null, usuario);
	}

	public void guardarFinDeProceso(Integer nroDeProceso, String grabadoDesde, String usuario) {
		guardar(nroDeProceso, 900, "Fin.", grabadoDesde, null, usuario);
	}

	public void guardarErrorProceso(Integer nroDeProceso, String grabadoDesde, String datosError, String usuario) {
		guardar(nroDeProceso, 300, "Error.", grabadoDesde, datosError, usuario);
	}
	
	private void guardar(Integer nroDeProceso, Integer estadoDeProceso, String pasoAccion, String grabadoDesde, String datosError,
			String usuario) {		
		LogProceso logProceso = new LogProceso();
		datosError = datosError == null ? "" : datosError;
		logProceso.setNumeroProceso(nroDeProceso);
		logProceso.setEstadoProceso(estadoDeProceso);
		logProceso.setPasoAccion(pasoAccion);
		logProceso.setDatosError(datosError);
		logProceso.setGrabadoDesde(grabadoDesde);
		logProceso.setNodo("DEFAULT POR AHORA");
		logProceso.setFecha(new Date());
        logProceso.setUsuario(usuario);
		save(logProceso);
	}
}