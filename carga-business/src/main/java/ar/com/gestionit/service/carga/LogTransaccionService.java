package ar.com.gestionit.service.carga;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.LogTransaccionRepository;
import ar.com.gestionit.model.carga.LogTransaccion;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class LogTransaccionService extends BaseCrudService<LogTransaccion, Long> {

	@Autowired
	public LogTransaccionService(LogTransaccionRepository repository) {
		setRepository(repository);
	}

	private LogTransaccionRepository getRepository() {
		return (LogTransaccionRepository) this.repository;
	}

	public List<LogTransaccion> findTransaccionesByFechas(String tipoTransaccion, String tabla, String campo,
			String usuario, Date fechaDesdeConsulta, Date fechaHastaConsulta) {
		return this.getRepository().findTransaccionesByFechas(tipoTransaccion, tabla,campo, usuario, fechaDesdeConsulta,fechaHastaConsulta);
	}

	public List<LogTransaccion> findTransacciones(Integer tipoTransaccion, String tabla, String campo, String usuario) {
		return this.getRepository().findTransacciones(tipoTransaccion, tabla,campo, usuario);
		}
}