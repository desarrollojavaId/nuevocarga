package ar.com.gestionit.service.carga;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.MsTablasRepository;
import ar.com.gestionit.model.carga.MsTablas;
import ar.com.gestionit.model.carga.MsTablasPK;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class MsTablasService extends BaseCrudService<MsTablas, Long> {

//	private static final Logger logger = LogManager.getLogger(MsTablasService.class);
	
	@Autowired
	public MsTablasService(MsTablasRepository repository) {
		setRepository(repository);
	}

	private MsTablasRepository getRepository() {
		return (MsTablasRepository) this.repository;
	}

	public Page<MsTablas> findBySearch(PageRequest pageRequest) {
		return getRepository().findBySearch(pageRequest);
	}
	
	public MsTablas findById(MsTablasPK mstablasPk) {
		return getRepository().findById(mstablasPk);
	}
	
	@Cacheable(value="TablaCodigoSubCodigo")
	public MsTablas findByTablaCodigoSubCodigo(Integer tabla, Integer codigo, Integer subCodigo) {
		return getRepository().findByTablaCodigoSubCodigo(tabla, codigo, subCodigo);
	}
	
	
	@Cacheable(value="TablaDescripcionSubCodigo")
	public MsTablas findByTablaDescripcionSubCodigo(Integer tabla, String descripcionReducida, Integer subCodigo) {
		return getRepository().findByTablaDescripcionSubCodigo(tabla, descripcionReducida, subCodigo);
	}

	@Cacheable(value="TablaCodigoNotSubCodigo")
	public MsTablas findByTablaCodigoNotSubCodigo(Integer tabla, Integer codigo, Integer subCodigo) {
		return getRepository().findByTablaCodigoNotSubCodigo(tabla, codigo, subCodigo);
	}

	public Page<MsTablas> pageByTablaCodigoSubCodigo(PageRequest page, Integer tabla, Integer codigo,
			Integer subCodigo, String search) {
		return getRepository().pageByTablaCodigoSubCodigo(page, tabla, codigo, subCodigo, search);
	}

	@Cacheable(value="TablaCodigoNotSubCodigoPage")
	public Page<MsTablas> pageByTablaCodigoNotSubCodigo(PageRequest page, Integer tabla, Integer codigo,
			Integer subCodigo, String search) {
		return getRepository().pageByTablaCodigoNotSubCodigo(page, tabla, codigo, subCodigo, search);
	}
	
	public List<MsTablas> findAllByTablaAndCodigo(Integer tablaCodigo, Integer campoCodigo) {
		return getRepository().findAllByTablaAndCodigo(tablaCodigo, campoCodigo);
	}
	
	public void remove(MsTablasPK id) {
		MsTablas mstablas = getRepository().findById(id);
		getRepository().delete(mstablas);
	}


}
