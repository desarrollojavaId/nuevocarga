package ar.com.gestionit.service.carga;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.RequerimientoRepository;
import ar.com.gestionit.model.carga.Requerimiento;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class RequerimientoService extends BaseCrudService<Requerimiento, Long> {

	@Autowired
	public RequerimientoService(RequerimientoRepository repository) {
		setRepository(repository);
	}
	
	private RequerimientoRepository getRepository() {
		return (RequerimientoRepository) this.repository;
	}
	
	public Page<Requerimiento> findBySearch(Pageable pageRequest, Long nuss, String titulo) {
		return getRepository().findBySearch(pageRequest, nuss, titulo);
	}
	/*
//	public List<Requerimiento> findBySearch(String nombreRequerimiento, String nombre, String apellido) {
//		return getRepository().findBySearch(nombreRequerimiento, nombre, apellido);
//	}
//	
	public Page<Requerimiento> findByDescripcion(Pageable pageRequest, String descripcion) {
		return getRepository().findByDescripcion(pageRequest, descripcion);
	}
	**/
	// Utilizado para no dar de alta 2 requerimientos con el mismo nuss
	public Long countExisteNuss(Long nuss) {
		return getRepository().countExisteNuss(nuss);
	}
	
	// Utilizado para no dar de alta 2 requerimientos con el mismo nombre
	public Long countExisteNuss(Long id, Long nuss) {
		return getRepository().countExisteNuss(id, nuss);
	}
	

}