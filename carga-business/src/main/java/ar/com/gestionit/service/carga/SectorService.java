package ar.com.gestionit.service.carga;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.SectorRepository;
import ar.com.gestionit.model.carga.Sector;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class SectorService extends BaseCrudService<Sector, Long> {

	@Autowired
	public SectorService(SectorRepository repository) {
		setRepository(repository);
	}
	
	private SectorRepository getRepository() {
		return (SectorRepository) this.repository;
	}
	
	public Page<Sector> findBySearch(Pageable pageRequest, String areaDescripcion, String sectorDescripcion) {
		return getRepository().findBySearch(pageRequest, areaDescripcion, sectorDescripcion);
	}
	
//	public List<Sector> findBySearch(String nombreSector, String nombre, String apellido) {
//		return getRepository().findBySearch(nombreSector, nombre, apellido);
//	}
//	
	public Page<Sector> findByDescripcion(Pageable pageRequest, String descripcion) {
		return getRepository().findByDescripcion(pageRequest, descripcion);
	}
	
	// Utilizado para no dar de alta 2 sectors con el mismo nombre
	public Long countExisteDescripcion(String descripcion) {
		return getRepository().countExisteDescripcion(descripcion);
	}
	
	// Utilizado para no dar de alta 2 sectors con el mismo nombre
	public Long countExisteDescripcion(Long id, String descripcion) {
		return getRepository().countExisteDescripcion(id, descripcion);
	}
/*
	public Page<Sector> findByOrderByNombre(Pageable pageRequest) {
		return getRepository().findByOrderByNombre(pageRequest);
	}

	public Page<Sector> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageRequest, String nombreSector) {
		return getRepository().findByNombreLikeIgnoreCaseOrderByNombre(pageRequest, nombreSector);
	}
	*/

}