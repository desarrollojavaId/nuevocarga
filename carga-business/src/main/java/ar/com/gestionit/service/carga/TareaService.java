package ar.com.gestionit.service.carga;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.TareaRepository;
import ar.com.gestionit.model.carga.Tarea;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class TareaService extends BaseCrudService<Tarea, Long> {

	@Autowired
	public TareaService(TareaRepository repository) {
		setRepository(repository);
	}
	
	private TareaRepository getRepository() {
		return (TareaRepository) this.repository;
	}
	
	public Page<Tarea> findBySearch(Pageable pageRequest, Long nuss, String titulo) {
		return getRepository().findBySearch(pageRequest, nuss, titulo);
	}
	/*
	// Utilizado para no dar de alta 2 requerimientos con el mismo nuss
	public Long countExisteNuss(Long nuss) {
		return getRepository().countExisteNuss(nuss);
	}
	
	// Utilizado para no dar de alta 2 requerimientos con el mismo nombre
	public Long countExisteNuss(Long id, Long nuss) {
		return getRepository().countExisteNuss(id, nuss);
	}
	**/
	

}