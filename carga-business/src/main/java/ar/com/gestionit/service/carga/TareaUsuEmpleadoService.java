package ar.com.gestionit.service.carga;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.TareaUsuEmpleadoRepository;
import ar.com.gestionit.model.carga.TareaUsuEmpleado;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class TareaUsuEmpleadoService extends BaseCrudService<TareaUsuEmpleado, Long> {

	@Autowired
	public TareaUsuEmpleadoService(TareaUsuEmpleadoRepository repository) {
		setRepository(repository);
	}
	
	private TareaUsuEmpleadoRepository getRepository() {
		return (TareaUsuEmpleadoRepository) this.repository;
	}
	
	public Page<TareaUsuEmpleado> findBySearch(Pageable pageRequest, Long tareaId) {
		return getRepository().findBySearch(pageRequest, tareaId);
	}
	/*
	// Utilizado para no dar de alta 2 requerimientos con el mismo nuss
	public Long countExisteNuss(Long nuss) {
		return getRepository().countExisteNuss(nuss);
	}
	
	// Utilizado para no dar de alta 2 requerimientos con el mismo nombre
	public Long countExisteNuss(Long id, Long nuss) {
		return getRepository().countExisteNuss(id, nuss);
	}
	**/
	

}