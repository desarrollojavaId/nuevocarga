package ar.com.gestionit.service.carga;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.UsuClienteRepository;
import ar.com.gestionit.model.carga.UsuCliente;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class UsuClienteService extends BaseCrudService<UsuCliente, Long> {

	@Autowired
	public UsuClienteService(UsuClienteRepository repository) {
		setRepository(repository);
	}
	
	private UsuClienteRepository getRepository() {
		return (UsuClienteRepository) this.repository;
	}
	
	public Page<UsuCliente> findBySearch(Pageable pageRequest, String razonSocial, String nombre, String apellido) {
		return getRepository().findBySearch(pageRequest, razonSocial, nombre, apellido);
	}
	
	public List<UsuCliente> findBySearch(String nombre, String apellido) {
		return getRepository().findBySearch(nombre, apellido);
	}
	
	public Long countExisteUsuClienteId(Long usuClienteId, Long usuarioId) {
		return getRepository().countExisteUsuClienteId(usuClienteId, usuarioId);
	}

	public Long countExisteUsuCliente(Long usuarioId) {
		return getRepository().countExisteUsuCliente(usuarioId);
	}	
	

}