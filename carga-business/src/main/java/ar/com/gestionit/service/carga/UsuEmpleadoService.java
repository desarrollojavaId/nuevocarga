package ar.com.gestionit.service.carga;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.UsuEmpleadoRepository;
import ar.com.gestionit.model.carga.UsuEmpleado;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class UsuEmpleadoService extends BaseCrudService<UsuEmpleado, Long> {

	@Autowired
	public UsuEmpleadoService(UsuEmpleadoRepository repository) {
		setRepository(repository);
	}
	
	private UsuEmpleadoRepository getRepository() {
		return (UsuEmpleadoRepository) this.repository;
	}
	
	public Page<UsuEmpleado> findBySearch(Pageable pageRequest, String nombre, String apellido) {
		return getRepository().findBySearch(pageRequest, nombre, apellido);
	}
	
	public List<UsuEmpleado> findBySearch(String nombre, String apellido) {
		return getRepository().findBySearch(nombre, apellido);
	}
	
	public List<UsuEmpleado> findByLider(String esLider, String search) {
		return getRepository().findByLider(esLider, search);
	}
	
	public Long countExisteUsuEmpleadoId(Long usuEmpleadoId, Long usuarioId) {
		return getRepository().countExisteUsuEmpleadoId(usuEmpleadoId, usuarioId);
	}

	public Long countExisteUsuEmpleado(Long usuarioId) {
		return getRepository().countExisteUsuEmpleado(usuarioId);
	}	
	

}