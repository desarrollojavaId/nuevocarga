package ar.com.gestionit.service.seguridad;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.carga.comparators.PuntoMenuCategoriaComparator;
import ar.com.gestionit.dao.seguridad.GrupoPuntoMenuRepository;
import ar.com.gestionit.model.seguridad.GrupoPuntoMenu;
import ar.com.gestionit.model.seguridad.PuntoMenuCategoria;
import ar.com.gestionit.model.seguridad.PuntoMenuDetalle;
import ar.com.gestionit.service.BaseCrudService;


@Service
public class GrupoPuntoMenuService extends BaseCrudService<GrupoPuntoMenu, Long> {
	
	@Autowired
	PuntoMenuService servicePuntoMenu;

	@Autowired
	public GrupoPuntoMenuService(GrupoPuntoMenuRepository repository) {
		setRepository(repository);
	}
	
	private GrupoPuntoMenuRepository getRepository() {
		return (GrupoPuntoMenuRepository) this.repository;
	}
	
	public List<GrupoPuntoMenu> findAllGrupoPuntoMenuByGrupoId(Long id) {
		return getRepository().findAllGrupoPuntoMenuByGrupoId(id);
	}
	public HashMap<String, Object> findAllGrupoPuntoMenuByNombreGrupoList(Collection<String>  lstNombres) {
		List<PuntoMenuDetalle> puntosEncontrados = servicePuntoMenu.findAllGrupoPuntoMenuByGrupoNombre(lstNombres);
		return calcularNombresAlternativos(puntosEncontrados);
	}
	
	public Long findExistPuntoMenu(List<Long> puntoMenuId) {
		return getRepository().findExistPuntoMenu(puntoMenuId);
	}
	
	public GrupoPuntoMenu findByPuntoMenu(Long puntoMenuId) {
		return getRepository().findByPuntoMenu(puntoMenuId);
	}
	
	public GrupoPuntoMenu prepararEntity() {
		return  new GrupoPuntoMenu();
	}
	
	public HashMap<String, Object> calcularNombresAlternativos(List<PuntoMenuDetalle> puntosEncontrados){
		PuntoMenuCategoriaComparator comparator = new PuntoMenuCategoriaComparator(); 
		HashMap<String,String> mapaNombres = new HashMap<String, String>();
		HashMap<String,Object> mapaObj = new HashMap<String, Object>();
		List<PuntoMenuCategoria> lst = new ArrayList<PuntoMenuCategoria>();
		List<PuntoMenuDetalle> puntoMenuDetalles = new ArrayList<PuntoMenuDetalle>(); 
		PuntoMenuCategoria categoriaOld = null;
		for(PuntoMenuDetalle detalle : puntosEncontrados){
			List<GrupoPuntoMenu> lstGrupoPuntoMenu = detalle.getGrupoPuntoMenu();
			GrupoPuntoMenu grupoPuntoMenu = null;
			if(lstGrupoPuntoMenu.size() == 1) {
				grupoPuntoMenu = lstGrupoPuntoMenu.get(0); 
			}
				 
			//En caso de ser la primera vez se toma la categoria del detalle y solo se vuelve a reiniciar cuando se cambia la categoria
			if(categoriaOld == null) {
				categoriaOld = detalle.getCategoriaPuntoMenuId();
			}else if(categoriaOld != detalle.getCategoriaPuntoMenuId()){
				categoriaOld = detalle.getCategoriaPuntoMenuId();
				puntoMenuDetalles = new ArrayList<PuntoMenuDetalle>();
			}
			//
			if(grupoPuntoMenu != null && grupoPuntoMenu.getNombrePuntoMenuAlternativo() != null && !"".equals(grupoPuntoMenu.getNombrePuntoMenuAlternativo())) {
				detalle.setNombreDetalleParaMostrar(grupoPuntoMenu.getNombrePuntoMenuAlternativo());					
			}
			//Se agrega la categoria a lista de categorias para mostrarse en el menu
			if(!lst.contains(categoriaOld)) {
				if(grupoPuntoMenu != null && grupoPuntoMenu.getNombreCategoriaAlternativo() != null && !"".equals(grupoPuntoMenu.getNombreCategoriaAlternativo())) {
					categoriaOld.setNombreCategoriaParaMostrar(grupoPuntoMenu.getNombreCategoriaAlternativo());
				}
				lst.add(categoriaOld);	
			}
			puntoMenuDetalles.add(detalle);
			categoriaOld.setPuntoMenuDetalles(puntoMenuDetalles);
			mapaNombres.put(detalle.getPuntoMenuUrl().getUrl().substring(1, detalle.getPuntoMenuUrl().getUrl().length()), detalle.getNombreDetalleParaMostrar());
		}
		//Se ordena la lista de categoria segun el orden establecido
		Collections.sort(lst, comparator);
		mapaObj.put("puntoMenuNombres", mapaNombres);
		mapaObj.put("lista", lst);
		return mapaObj;
	}
	
	
}