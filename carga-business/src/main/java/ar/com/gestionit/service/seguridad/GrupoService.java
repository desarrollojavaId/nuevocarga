package ar.com.gestionit.service.seguridad;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.seguridad.GrupoRepository;
import ar.com.gestionit.model.seguridad.Grupo;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class GrupoService extends BaseCrudService<Grupo, Long> {

	@Autowired
	public GrupoService(GrupoRepository repository) {
		setRepository(repository);
	}

	private GrupoRepository getRepository() {
		return (GrupoRepository) this.repository;
	}
	
	public Page<Grupo> findBySearch(PageRequest pageRequest, String nombre) {
		return getRepository().findBySearch(pageRequest, nombre);
	}

	public List<Grupo> findByNombre(String grupoNombre){
		return getRepository().findByNombre(grupoNombre);
	}

	public Long countExisteGrupo(String nombreGrupo) {
		return getRepository().countExisteGrupo(nombreGrupo);
	}
	
	public Long countExisteGrupoById(String nombreGrupo, Long id) {
		return getRepository().countExisteGrupoById(nombreGrupo, id);
	}
	
}