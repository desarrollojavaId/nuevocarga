package ar.com.gestionit.service.seguridad;


import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.seguridad.PuntoMenuRepository;
import ar.com.gestionit.model.seguridad.PuntoMenu;
import ar.com.gestionit.model.seguridad.PuntoMenuCategoria;
import ar.com.gestionit.model.seguridad.PuntoMenuDetalle;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class PuntoMenuService extends BaseCrudService<PuntoMenu, Long> {

	@Autowired
	public PuntoMenuService(PuntoMenuRepository repository) {
		setRepository(repository);
	}
	
	private PuntoMenuRepository getRepository() {
		return (PuntoMenuRepository) this.repository;
	}
	
	public PuntoMenuCategoria findCategoriaById(Long id) {
		return getRepository().findCategoriaById(id);
	}
	
	public Page<PuntoMenuCategoria> findByCategoria(PageRequest pageRequest, Long categoria) {
		return getRepository().findByCategoria(pageRequest, categoria);
	}
	
	//Detalle 
	
	public Page<PuntoMenuDetalle> findAllPuntoMenuByCategoria(PageRequest pageRequest, Long categoria) {
		return getRepository().findAllPuntoMenuByCategoria(pageRequest, categoria);
	}
	
	public PuntoMenuDetalle findDetalleById(Long id) {
		return getRepository().findDetalleById(id);
	}
	
	public Page<PuntoMenuDetalle> findAllPuntoMenuByCategoriaDetalle(PageRequest pageRequest, Long categoria, Long detalleId) {
		return getRepository().findAllPuntoMenuByCategoriaDetalle(pageRequest, categoria, detalleId);
	}
	
	public List<PuntoMenuDetalle> findAllPuntoMenuByCategoriaList( Long categoria) {
		return getRepository().findAllPuntoMenuByCategoriaList( categoria);
	}
	
	
	public Long findExistOrder(Long orden) {
		return getRepository().findExistOrder(orden);
	}
	
	public List<PuntoMenuDetalle> findAllGrupoPuntoMenuByGrupoNombre(Collection<String> nombreGrupo) {
		return getRepository().findAllGrupoPuntoMenuByGrupoNombre( nombreGrupo);
	}

	public Long findExistOrder(Long orden, Long id) {
		return getRepository().findAllGrupoPuntoMenuByGrupoNombre( orden, id);
	}
	
	
	
}