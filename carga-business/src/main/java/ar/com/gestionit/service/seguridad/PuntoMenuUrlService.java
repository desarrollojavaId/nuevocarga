package ar.com.gestionit.service.seguridad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.seguridad.PuntoMenuUrlRepository;
import ar.com.gestionit.model.seguridad.PuntoMenuUrl;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class PuntoMenuUrlService extends BaseCrudService<PuntoMenuUrl, Long> {

	@Autowired
	public PuntoMenuUrlService(PuntoMenuUrlRepository repository) {
		setRepository(repository);
	}
	
	@SuppressWarnings("unused")
	private PuntoMenuUrlRepository getRepository() {
		return (PuntoMenuUrlRepository) this.repository;
	}
	
}