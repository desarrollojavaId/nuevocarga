package ar.com.gestionit.service.seguridad;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ar.com.gestionit.dao.seguridad.UsuarioRepository;
import ar.com.gestionit.model.seguridad.Usuario;
import ar.com.gestionit.service.BaseCrudService;

@Service
public class UsuarioService extends BaseCrudService<Usuario, Long> {

	@Autowired
	public UsuarioService(UsuarioRepository repository) {
		setRepository(repository);
	}
	
	private UsuarioRepository getRepository() {
		return (UsuarioRepository) this.repository;
	}
	
	public Page<Usuario> findBySearch(Pageable pageRequest, String nombreUsuario, String nombre, String apellido) {
		return getRepository().findBySearch(pageRequest, nombreUsuario, nombre, apellido);
	}
	
	public List<Usuario> findBySearch(String nombreUsuario, String nombre, String apellido) {
		return getRepository().findBySearch(nombreUsuario, nombre, apellido);
	}
	
	public Page<Usuario> findByNombreUsuario(Pageable pageRequest, String nombreUsuario) {
		return getRepository().findByNombreUsuario(pageRequest, nombreUsuario);
	}
	
	// Utilizado para no dar de alta 2 usuarios con el mismo nombre
	public Long countExisteNombreUsuario(String nombreUsuario) {
		return getRepository().countExisteNombreUsuario(nombreUsuario);
	}
	
	// Utilizado para no dar de alta 2 usuarios con el mismo nombre
	public Long countExisteNombreUsuario(Long id, String nombreUsuario) {
		return getRepository().countExisteNombreUsuario(id, nombreUsuario);
	}

	public Page<Usuario> findByOrderByNombre(Pageable pageRequest) {
		return getRepository().findByOrderByNombre(pageRequest);
	}

	public Page<Usuario> findByNombreLikeIgnoreCaseOrderByNombre(Pageable pageRequest, String nombreUsuario) {
		return getRepository().findByNombreLikeIgnoreCaseOrderByNombre(pageRequest, nombreUsuario);
	}
	// Utilizado para no dar de alta 2 usuarios con las mismas iniciales	
	public Long countExisteInicialesUsuario(String iniciales) {
		return getRepository().countExisteInicialesUsuario(iniciales);
	}
	// Utilizado para no dar de alta 2 usuarios con las mismas iniciales
	public Long countExisteInicialesUsuario(Long id, String iniciales) {
		return getRepository().countExisteInicialesUsuario(id, iniciales);
	}
	
	public Page<Usuario> findAllByNombreAndApellido(Pageable pague, String search) {
		return getRepository().findAllByNombreAndApellido(pague,search);
	}
}