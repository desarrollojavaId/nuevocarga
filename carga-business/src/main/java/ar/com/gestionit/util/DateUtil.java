package ar.com.gestionit.util;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.Days;


/**
 * This class is used for date management 
 * @author jdigruttola
 */
public class DateUtil {
	
	/**
	 * Constants for internal usage
	 */
	private static final SimpleDateFormat SDF_yyyy_MM_dd_HH_mm_ss = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss");
	private static final String SEPARATOR_BY_DEFAULT = "/";
	
	public static final long WEEK_IN_MINUTES = 10080;
	public static final long WEEK_IN_DAYS = WEEK_IN_MINUTES / 60 / 24;
	public static final long DAY_IN_MINUTES = 1440;

	/**
	 * This method converts a given date to XMLGregorianCalendar object 
	 * @param date
	 * @return XMLGregorianCalendar
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar getAsXMLGregorianCalendar() 
			throws DatatypeConfigurationException {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	}

	/**
	 * Devuelve un string con la fecha formateada para los campos de "fecha" especificados.
	 * @param date
	 * @param input
	 * @param output
	 * @return
	 */
	public static String dateForOutput(String date, SimpleDateFormat input, SimpleDateFormat output) {
		if(StringUtil.isEmpty(date) || input == null || output == null)
			return "";

		String out;
		try{
			Date convertedCurrentDate = input.parse(date);
			out = output.format(convertedCurrentDate);	
		} catch(Exception e){
			return "";
		}
		
		return 	out;
	}
	
	/**
	 * Devuelve un string con la fecha formateada segun el SimpleDateFormat especificado
	 * @param date
	 * @param format
	 * @return
	 */
	public static String dateToString(Date date, SimpleDateFormat format) {
		if(date == null || format == null)
			return "";
		return format.format(date);
	}
	
	/**
	 * Devuelve un string con la fecha formateada segun el formato especificado
	 * @param date
	 * @param format
	 * @return
	 */
	public static String dateToString(Date date, String format) {
		return dateToString(date, new SimpleDateFormat(format));
	}
	
	/**
	 * This method converts a hour (HH:mm:ss) to a number according
	 * to the format (HHmmss).
	 * For example: given hour 14:02:33, the method returns the number 140233
	 * @param date
	 * @return
	 */
	public static long hourToLong(Date date) {
		if(date == null)
			return 0;
		return Long.parseLong(new SimpleDateFormat("HHmmss").format(date));
	}
	
	/**
	 * This method compares two hours (format HH:ss:mm).
	 * - If the first date given by argument (Date d1) is greater than the second
	 * date (Date d2), the method will return a positive number.
	 * - If the first date given by argument (Date d2) is less than the second date
	 * (Date d2), the method will return a negative number.
	 * - If both two arguments are equal, so the methor will return 0 (zero)
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static long compareHours(Date d1, Date d2) {
		if(d1 == null || d2 == null)
			return 0;
		return hourToLong(d1) - hourToLong(d2);
	}

	/**
	 * Devuelve un string con la fecha formateada para los campos de "fecha" 
	 * según el formato de parametros de entrada y salida recibidos en el método.
	 * @param date
	 * @param patternInput
	 * @param patternOutput
	 * @return
	 */
	public static String changeFormat(String date, String patternInput, String patternOutput) {
		String out;
		try{
			SimpleDateFormat sdfInput = new SimpleDateFormat(patternInput);
			SimpleDateFormat sdfOutput = new SimpleDateFormat(patternOutput);
			Date convertedCurrentDate = sdfInput.parse(date);
			out =sdfOutput.format(convertedCurrentDate);	
		} catch(Exception e){
			return "";
		}
		return 	out;
	}

	/**
	 * Devuelve un string con la hora formateada para los campos "Horas" especificados
	 * @param hour
	 * @param input
	 * @param output
	 * @return
	 */
	public static String hourForOutput(String hour, SimpleDateFormat input, SimpleDateFormat output) {
		String out;
		try{
			Date convertedCurrentDate = input.parse(hour);
			out = output.format(convertedCurrentDate);
		} catch(Exception e){
			return "";
		}
		return 	out;
	}

	/**
	 * Devuelve un string con la hora formateada para los campos de "Hora" 
	 * según el formato de parametros de entrada y salida recibidos en el método.
	 * @param hour
	 * @param patternInput
	 * @param patternOutput
	 * @return
	 */
	public static String hourForOutput(String hour, String patternInput, String patternOutput) {
		String out;
		try{
			SimpleDateFormat sdfInput = new SimpleDateFormat(patternInput);
			SimpleDateFormat sdfOutput = new SimpleDateFormat(patternOutput);
			Date convertedCurrentDate = sdfInput.parse(hour);
			out =sdfOutput.format(convertedCurrentDate);
		}
		catch(Exception e){
			return "";
		}
		return 	out;
	}

	/**
	 * This static method validates if the string given by argument is a date
	 * according to the format loaded in the specified SimpleDateFormat given.
	 * If the string is NULL, the method also returns TRUE because of this
	 * is used by non mandatory inputs
	 * @param date
	 * @param sdf
	 * @return TRUE if the string is a date or NULL and FALSE if not
	 */
	public static boolean isDateOrNull(String date, SimpleDateFormat sdf) {
		if(date == null )
			return true;
		return isDate(date, sdf);
	}

	/**
	 * This static method validates if the string given by argument is a date
	 * according to the format loaded in the specified SimpleDateFormat given.
	 * @param date
	 * @param sdf
	 * @return TRUE if the string is a date and FALSE if not or if this is NULL
	 */
	public static boolean isDate(String date, SimpleDateFormat sdf) {
		if(date == null )
			return false;
		try {
			sdf.parse(date);
			return true;
		} catch(Exception e) {
			return false;
		}
	}

	/**
	 * This static method returns a date according to the input format
	 * @param date
	 * @param sdf
	 * @return
	 */
	public static Date getDate(String date, SimpleDateFormat sdf) {
		if(date == null )
			return null;
		try {
			String dateTest = date.substring(8);
			if (!NumberUtil.isNumeric(dateTest)){
				return null;}
			sdf.setLenient(false);
			return sdf.parse(date);
		} catch(Exception e) {
			return null;
		}
	}

	/**
	 * This method validates if the date interval is right
	 * @param from
	 * @param to
	 * @return
	 */
	public static boolean validateDateInterval(Date from, Date to) {
		if(from == null || to == null)
			return true;

		return !from.after(to);
	}
	
	/**
	 * This method returns a increased date the minutes given by argument
	 * @param date
	 * @param minutes
	 * @return
	 * @throws ParseException 
	 */
	public static Date increase(Date date, int minutes) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dt = sdf.format(date); 

		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(dt));
		} catch (ParseException e) {
			System.err.println(e.getMessage());
		}

		c.add(Calendar.MINUTE, minutes);
		dt = sdf.format(c.getTime());
		try {
			return sdf.parse(dt);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}
	
	/**
	 * This method returns a increased date the minutes given by argument
	 * @param date
	 * @param minutes
	 * @return
	 * @throws ParseException 
	 */
	public static int differenceBetweenDates(Date d1, Date d2) {
		int result = Days.daysBetween(new DateTime(d1), new DateTime(d2)).getDays();
		return result < 0 ? result*(-1) : result;
	}
	
	/**
	 * This method returns a increased date the days given by argument
	 * @param date
	 * @param minutes
	 * @return
	 * @throws ParseException 
	 */
	public static Date increaseDays(Date date, int days) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dt = sdf.format(date); 

		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(dt));
		} catch (ParseException e) {
			System.err.println(e.getMessage());
		}

		c.add(Calendar.DATE, days);
		dt = sdf.format(c.getTime());
		try {
			return sdf.parse(dt);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}
	
	/**
	 * Metodo que incrementa una fecha dado una cantidad de mes
	 * @param date
	 * @param Mes
	 * @return
	 * @throws ParseException 
	 */
	public static Date increaseMonth(Date date, int mes) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dt = sdf.format(date); 

		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(dt));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		c.add(Calendar.MONTH, mes);
		dt = sdf.format(c.getTime());
		try {
			return sdf.parse(dt);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}
	
	/**
	 * This method returns the date without hour, minutes, seconds and milliseconds
	 * @param date
	 * @return
	 */
	public static Date clearHours(Date date) {
		if(date == null) return null;
		
		Calendar cal = Calendar.getInstance(); // locale-specific
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		long time = cal.getTimeInMillis();
		
		return new Date(time);
	}
	
	/**
	 * This method returns the hour with the format HH:mm
	 * @param offset
	 * @return
	 */
	public static String hourWithFormat(long minutes) {
		long hour = hour(minutes);
		long minute = minute(minutes);
		
		String result = "";
		if(hour < 10) result += "0";
		result += String.valueOf(hour);
		result += ":";
		if(minute < 10) result += "0";
		result += String.valueOf(minute);
		
		return result;
	}
	
	/**
	 * This method returns the hour in a day without minutes
	 * @param offset
	 * @return
	 */
	public static long hour(long minutes) {
		return (minutes / 60) % 24;
	}
	
	/**
	 * This method returns the minutes in an hour
	 * @param offset
	 * @return
	 */
	public static long minute(long minutes) {
		return (minutes % 60);
	}
	
	/**
	 * This method transforms an hour to minutes
	 * @param hour
	 * @return
	 */
	public static long hourToMinutes(String hour) {
		String[] fields = hour.split(":");
		long h = Long.parseLong(fields[0]);
		long m = Long.parseLong(fields[1]);
		
		return (h * 60) + m;
		
	}
	
	/**
	 * This method transforms the minutes to days
	 * @param minutes
	 * @return
	 */
	public static Long minuteToHourWithoutMinutes(Long minutes) {
		return minutes / 60 / 24;
	}
	
	/**
	 * This method validates if the specific date is today
	 * @param date
	 * @return
	 */
	public static boolean isToday(Date date) {
		if(date == null) return false;
		
		SimpleDateFormat dayFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		if( dayFormat.format(today).equals(dayFormat.format(date)) )
			return true;
		return false;
	}
	
	/**
	 * This method converts the arguments to date
	 * @param year
	 * @param month
	 * @param day
	 * @param hour
	 * @param minute
	 * @param second
	 * @return
	 * @throws ParseException
	 */
	public static Date toDate(int year, int month, int day,
			int hour, int minute, int second) throws ParseException {
		StringBuffer sb = new StringBuffer();
		sb.append(year);
		sb.append(SEPARATOR_BY_DEFAULT);
		sb.append(month);
		sb.append(SEPARATOR_BY_DEFAULT);
		sb.append(day);
		sb.append(SEPARATOR_BY_DEFAULT);
		sb.append(hour);
		sb.append(SEPARATOR_BY_DEFAULT);
		sb.append(minute);
		sb.append(SEPARATOR_BY_DEFAULT);
		sb.append(second);
		return SDF_yyyy_MM_dd_HH_mm_ss.parse(sb.toString());
	}
	
	/**
	 * This method generates a Date object according to the data given by argument
	 * @param year (yyyy)
	 * @param month (mm)
	 * @param day (dd)
	 * @return
	 */
	public static Date generateDate(String year, String month, String day) throws Exception {
		if(!NumberUtil.isNumeric(year) || !NumberUtil.isNumeric(month) || !NumberUtil.isNumeric(day)) {
			throw new Exception("Data given by argument is not valid. All the parameters have to be numbers");
		}
		
		try {
			return new SimpleDateFormat("yyyy/MM/dd").parse(
					new StringBuffer(year)
					.append("/")
					.append(month)
					.append("/")
					.append(day)
					.toString());
		} catch (ParseException e) {
			return null;
		}
	}
	
	/**
	 * This method obtains the system date
	 * @return
	 */
	public static Date getSystemDate() {
		return new Date(System.currentTimeMillis());
	}
	
	/**
	 * This method converts a date to Timestamp object
	 * @param date
	 * @return
	 */
	public static Timestamp getTimestampByDate(Date date) {
		if(date == null)
			return null;
		SimpleDateFormat sdfHourFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return Timestamp.valueOf(sdfHourFormat.format(date));
	}
	
	/**
	 * This method converts a date to Time object
	 * @param date
	 * @return
	 */
	public static Time getTimeByDate(Date date) {
		if(date == null)
			return null;
		SimpleDateFormat sdfHourFormat = new SimpleDateFormat("HH:mm:ss");
		return Time.valueOf(sdfHourFormat.format(date));
	}
	
	/**
	 * This method compares both dates according to the format (SimpleDateFormat)
	 * given by argument, i.e., the dates can be compared only by the specified format
	 * (not by the total data of the date) 
	 * @param d1
	 * @param d2
	 * @param sdf
	 * @return
	 */
	public static boolean equalDateBySDF(Date d1, Date d2, SimpleDateFormat sdf) {
		if(d1 == null && d2 == null)
			return true;
		if(sdf == null || d1 == null || d2 == null)
			return false;
		try {
			d1 = sdf.parse(sdf.format(d1));
			d2 = sdf.parse(sdf.format(d2));
			return d1.compareTo(d2) == 0 ? true : false;
		} catch (ParseException e) {
			return false;
		} 
	}
	
	
	/**
	 * This method converts a date to Timestamp object
	 * @param date
	 * @return
	 */
	public static String getStringByDate(Date date, SimpleDateFormat sdf) {
		if(date == null)
			return null;
		return sdf.format(date);
	}
	
}