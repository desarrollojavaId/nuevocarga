package ar.com.gestionit.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * This class is used for number management 
 * @author jdigruttola
 */
	

	public class NumberUtil {
		
	public static boolean twoDecimalMax(String s){
	
		if (s == null) 
			return false;
		return s.matches("[0-9]+([.][0-9]{1,2})?");
		
	}
	
		
	/**
	 * This static method validates if the value given by argument
	 * is number or not according to this pattern
	 * @param s
	 * @return
	 */
	public static boolean isNumeric(String s) {
		if(s == null)
			return false;
	    return s.matches("[-+]?\\d*\\.?\\d+");  
	}
	
	/**
	 * This static method validates if the value given by argument
	 * is an integer (not decimal) or not according to this pattern
	 * @param s
	 * @return
	 */
	public static boolean isInteger(String s) {
		if(s == null)
			return false;
	    return s.matches("[-+]?\\d+$");  
	}
	
	/**
	 * This static method validates if the value given by argument
	 * is a unsigned integer (not decimal) or not according to this pattern
	 * @param s
	 * @return
	 */
	public static boolean isUnsignedInteger(String s) {
		if(s == null)
			return false;
	    return s.matches("\\d+$");  
	}
	
	/**
	 * This static method validates if the value given by argument
	 * is number or not according to this pattern
	 * @param s
	 * @return TRUE is the given argument is a number or NULL. FALSE if not
	 */
	public static boolean isNumericOrNull(String s) {
		if(s == null)
			return true;
	    return isNumeric(s);  
	} 
	
	/**
	 * This static method validates if the value given by argument
	 * is number or not according to this pattern
	 * @param s
	 * @return
	 */
	public static String getNumberOrZero(String s, String errorKey) throws Exception {
		if(StringUtil.isEmpty(s))
			return "0";
		if(!isNumeric(s))
			throw new Exception("The value is not a number");
		return s;
	}

	/**
	 * This method validates if the number interval is right
	 * @param f
	 * @param t
	 * @return TRUE if the limit "from" is less or equal than the limit "to"
	 */
	public static boolean validateNumberInterval(float f, float t) {
		return f <= t;
	}
	
	/**
	 * This method validates if the number interval is right
	 * @param f
	 * @param t
	 * @return TRUE if the limit "from" is less or equal than the limit "to"
	 */
	public static boolean validateNumberInterval(int f, int t) {
		return validateNumberInterval((float)f, (float)t);
	}

	/**
	 * This method validates if the number interval is right
	 * @param f
	 * @param t
	 * @return TRUE if the limit "from" is less or equal than the limit "to"
	 */
	public static boolean validateNumberInterval(long f, long t) {
		return validateNumberInterval((float)f, (float)t);
	}

	/**
	 * This method formats the number to #.NN
	 * @param n
	 * @return a string with format #.NN (always two decimals)
	 */
	public static String doubleFormat(double n) {
		NumberFormat df = NumberFormat.getInstance();			
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		return df.format(n);
	}
	
	/**
	 * This method formats the number to #.NN
	 * @param n
	 * @return a string with format #.NN (always two decimals)
	 */
	public static String doubleFormat(String s) {
		double n;
		try {
			n = Double.parseDouble(s);
		} catch (Exception e) {
			n = 0;
		}
		NumberFormat df = NumberFormat.getInstance();			
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		return df.format(n);
	}
	

	/**
	 * This method converts a string to BigDecimal object
	 * without losing accuracy.
	 * @param n
	 * @return
	 */
	public static BigDecimal stringToBigDecimal(String n) {
		//Validate argument
		if(StringUtil.isEmpty(n))
			return null;
		
		//Format argument
		n = n.trim();
		
		//Validate argument after the format it
		if(!isNumeric(n))
			return null;
		
		//Convert to BigDecimal object
		DecimalFormat df = new DecimalFormat("");
		df.setParseBigDecimal(true);
		try {
			return (BigDecimal) df.parse(n);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l + " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}
	
	public static long safeBigDecimalToLong(BigDecimal l) {
	    if (l.doubleValue() < Long.MIN_VALUE || l.doubleValue() > Long.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l + " cannot be cast to long without changing its value.");
	    }
	    return (long) l.doubleValue();
	}
}