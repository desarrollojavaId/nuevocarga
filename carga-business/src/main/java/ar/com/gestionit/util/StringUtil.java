package ar.com.gestionit.util;

/**
 * This class is used for string management 
 * @author jdigruttola
 */
public class StringUtil {
	
	/**
	 * This static validates if the string given by argument
	 * is interpreted as empty or not
	 * @param s
	 * @return
	 */
	public static boolean isEmpty(String s) {
		return s == null || s.trim().equals("");
	}
	
	/**
	 * This method is used to avoid NULL
	 * @param s
	 * @return
	 */
	public static String notNull(String s) {
		return s == null || s.trim().equalsIgnoreCase("null") ? "" : s;
	}

	/**
	 * This method autocompletes a given string with the specified value
	 * to the indicated size
	 * @param obj
	 * @param size
	 * @param value
	 * @return
	 */
	public static String autocomplete(String obj, int size, char value) {
		//Validate size
		if(size <= 0)
			return "";

		StringBuffer result = new StringBuffer();
		int difference;

		//Validate if the string is NULL
		if(obj == null) {
			difference = size;
		} else {
			difference = size - obj.length();
			
			if(difference <= 0)
				return obj;
		}
		
		//Autocomplete the string
		for(int i = 0; i < difference; i++)
			result.append(value);

		//Add the real string
		if(difference < size)
			result.append(obj);

		return result.toString();
	}
	
	/**
	 * This method returns a substring of the string given by argument if it is possible, obviously.
	 * @param s
	 * @param from
	 * @param to
	 * @return
	 * - If the object "s" is NULL or is empty, the method returns ""
	 * - If the length the object "s" is less than the variable "from" specified by argument, the method returns ""
	 * - If the length the object "s" is less than the variable "to" specified by argument, the method returns the substring from-length
	 */
	public static String getStringContent(String s, int from, int to) {
		if(isEmpty(s)) return "";
		if(s.length() < from) return "";
		if(s.length() < to) return s.substring(from).trim();
		return s.substring(from, to).trim();
	}
	
	/**
	 * This method returns a substring of the string (without trimming spaces) given by argument if it is possible, obviously.
	 * @param s
	 * @param from
	 * @param to
	 * @return
	 * - If the object "s" is NULL or is empty, the method returns ""
	 * - If the length the object "s" is less than the variable "from" specified by argument, the method returns ""
	 * - If the length the object "s" is less than the variable "to" specified by argument, the method returns the substring from-length
	 */
	public static String getStringContentWithSpaces(String s, int from, int to) {
		if(isEmpty(s)) return "";
		if(s.length() < from) return "";
		if(s.length() < to) return s.substring(from);
		return s.substring(from, to);
	}
}