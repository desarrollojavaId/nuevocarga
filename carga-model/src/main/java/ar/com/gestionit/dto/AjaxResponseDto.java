package ar.com.gestionit.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AjaxResponseDto {
	boolean error;
	List<String> errors;
	Map<String, Object> dataMap;

	public AjaxResponseDto() {
		errors = new ArrayList<String>();
		dataMap = new HashMap<>();
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public void addError(String error) {
		this.error = true;
		this.errors.add(error);
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

}
