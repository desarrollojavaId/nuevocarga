package ar.com.gestionit.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ValidationException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	List<String> errors = Collections.emptyList();
	
	public ValidationException(String...strings){
		errors = Arrays.asList(strings);
	}
	
	public ValidationException(Throwable t, String ...strings){
		super(t);
		errors = Arrays.asList(strings);
	}

	public ValidationException(Collection<String> validationErrors) {
		errors = new ArrayList<>(validationErrors);
	}

	public List<String> getErrors(){
		return this.errors;
	}
}
