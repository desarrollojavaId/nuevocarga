package ar.com.gestionit.exception;

public class VersionableException extends RuntimeException {

	public VersionableException(){
		
	}
	public VersionableException(String string) {
		super(string);
	}

	private static final long serialVersionUID = -6497698493878591436L;
	
}
