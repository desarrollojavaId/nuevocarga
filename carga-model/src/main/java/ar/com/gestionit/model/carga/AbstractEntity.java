package ar.com.gestionit.model.carga;

import javax.persistence.Transient;

public abstract class AbstractEntity {

	@Transient
	private String tituloPantalla;

	public String getTituloPantalla() {
		return tituloPantalla;
	}

	public void setTituloPantalla(String tituloPantalla) {
		String className = this.getClass().getSimpleName();

		if (tituloPantalla.contains(className)) {
			this.tituloPantalla = tituloPantalla;
		} else {
			String classNameLower = Character.toLowerCase(className.charAt(0))
					+ (className.length() > 1 ? className.substring(1) : "");
			this.tituloPantalla = classNameLower + "." + tituloPantalla + className;
			
		}

	}

}
