package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Cliente")

public class Cliente extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_cliente")
	@SequenceGenerator(name = "id_sequence_cliente", sequenceName = "id_seq_cliente")
	private Long id;	

	@Column(name = "razonSocial", nullable = false, length = 100)
	private String razonSocial;

	@Column(name = "cuit", unique=true, nullable = false, length = 11)
	private String cuit;

	@Column(name = "direccionReal", nullable = true, length = 255)
	private String direccionReal;

	@Column(name = "direccionFiscal", nullable = true, length = 255)
	private String direccionFiscal;

	@Column(name = "t02SituacionFiscal", nullable = true)
	private Integer t02SituacionFiscal;
    
	// Datos de auditoría	

	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaAlta;
	
	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta ;
	
	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta; 

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 
	
	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	
	//
	// Setter y getter
	//
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getDireccionReal() {
		return direccionReal;
	}

	public void setDireccionReal(String direccionReal) {
		this.direccionReal = direccionReal;
	}

	public String getDireccionFiscal() {
		return direccionFiscal;
	}

	public void setDireccionFiscal(String direccionFiscal) {
		this.direccionFiscal = direccionFiscal;
	}

	public Integer getT02SituacionFiscal() {
		return t02SituacionFiscal;
	}

	public void setT02SituacionFiscal(Integer t02SituacionFiscal) {
		this.t02SituacionFiscal = t02SituacionFiscal;
	}

	//
	// Auditoría
	//
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

}
