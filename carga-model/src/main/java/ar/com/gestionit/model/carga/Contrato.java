package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "contrato")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "usuario" })
public class Contrato extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_contrato")
	@SequenceGenerator(name = "id_sequence_contrato", sequenceName = "id_seq_contrato")
	private Long id;

	@Column(name = "nombre", nullable = false, length = 100)
	private String nombre;

	@Column(name = "descripcion", nullable = false, length = 255)
	private String descripcion;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;

	@Column(name = "contacto1", length = 255)
	private String contacto1;

	@Column(name = "contacto2", length = 255)
	private String contacto2;

	@Column(name = "t03Categoria", nullable = false)
	private Integer t03Categoria;

	@Column(name = "horasAbono")
	private Integer horasAbono;

	@Column(name = "renovable", nullable = false, length = 2)
	private String renovable;

	@Column(name = "puntoAviso")
	private Integer puntoAviso;

	@Column(name = "fecInicio", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecInicio;

	@Column(name = "fecFinal", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecFinal;

	@Column(name = "fecCargaHorasDesde", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecCargaHorasDesde;

	@Column(name = "fecCargaHorasHasta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecCargaHorasHasta;

	// Datos de auditoría	

	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaAlta;

	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta;

	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta;

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;

	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct;

	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	// Datos transient

	@Transient
	private String razonSocial;

	@Transient
	String descCategoria;

	//
	// Getters y Setters
	//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getContacto1() {
		return contacto1;
	}

	public void setContacto1(String contacto1) {
		this.contacto1 = contacto1;
	}

	public String getContacto2() {
		return contacto2;
	}

	public void setContacto2(String contacto2) {
		this.contacto2 = contacto2;
	}

	public Integer getT03Categoria() {
		return t03Categoria;
	}

	public void setT03Categoria(Integer t03Categoria) {
		this.t03Categoria = t03Categoria;
	}

	public String getDescCategoria() {
		return descCategoria;
	}

	public void setDescCategoria(String descCategoria) {
		this.descCategoria = descCategoria;
	}

	public Integer getHorasAbono() {
		return horasAbono;
	}

	public void setHorasAbono(Integer horasAbono) {
		this.horasAbono = horasAbono;
	}

	public String getRenovable() {
		return renovable;
	}

	public void setRenovable(String renovable) {
		this.renovable = renovable;
	}

	public Integer getPuntoAviso() {
		return puntoAviso;
	}

	public void setPuntoAviso(Integer puntoAviso) {
		this.puntoAviso = puntoAviso;
	}

	public Date getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(Date fecInicio) {
		this.fecInicio = fecInicio;
	}

	public Date getFecFinal() {
		return fecFinal;
	}

	public void setFecFinal(Date fecFinal) {
		this.fecFinal = fecFinal;
	}

	public Date getFecCargaHorasDesde() {
		return fecCargaHorasDesde;
	}

	public void setFecCargaHorasDesde(Date fecCargaHorasDesde) {
		this.fecCargaHorasDesde = fecCargaHorasDesde;
	}

	public Date getFecCargaHorasHasta() {
		return fecCargaHorasHasta;
	}

	public void setFecCargaHorasHasta(Date fecCargaHorasHasta) {
		this.fecCargaHorasHasta = fecCargaHorasHasta;
	}

	//
	// Getters y Setters
	//
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

}
