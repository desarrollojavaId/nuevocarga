package ar.com.gestionit.model.carga;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "mscontrolsistema")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler"})
public class ControlSistema {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_controlSistema")
	@SequenceGenerator(name = "id_sequence_controlSistema", sequenceName = "id_seq_controlSistema")
	private Long id;	
	
	@Column(name = "nroBcra", nullable = false)
	private Integer numeroBanco;
	
	@Column(name = "nomBco", nullable = false, length = 100)
	private String nombreBanco;
	
	@Column(name = "fecProceso", nullable = false)
	private Date fechaProceso;

	@Column(name = "fecDiaHabAnt", nullable = false)
	private Date fechaDiaHabilAnterior;
	
	@Column(name = "sistHabilitado", nullable = true, length = 1)
	private String sistemaHabilitado;

	@Column(name = "estadoIniCie", nullable = false)
	private Integer estadoInicioCierre;

	@Column(name = "maxRegWs", nullable = false)
	private BigInteger maximoRegistroWS;

	@Column(name = "contWsBco", nullable = true)
	private BigInteger contadorBancoWS;
	
	@Column(name = "logProcesoHab", nullable = true, length = 1)
	private String logProcesoHabilitado;

	@Column(name = "direccionAvisoEmail", nullable = true, length = 100)
	private String direccionAvisoEmail;

	@Column(name = "codSucursalCentral", nullable = false, length = 8)
	private String codigoSucursalCentral;
	
	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 
	
	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumeroBanco() {
		return numeroBanco;
	}

	public void setNumeroBanco(Integer numeroBanco) {
		this.numeroBanco = numeroBanco;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Date getFechaDiaHabilAnterior() {
		return fechaDiaHabilAnterior;
	}

	public void setFechaDiaHabilAnterior(Date fechaDiaHabilAnterior) {
		this.fechaDiaHabilAnterior = fechaDiaHabilAnterior;
	}

	public String getSistemaHabilitado() {
		return sistemaHabilitado;
	}

	public void setSistemaHabilitado(String sistemaHabilitado) {
		this.sistemaHabilitado = sistemaHabilitado;
	}

	public Integer getEstadoInicioCierre() {
		return estadoInicioCierre;
	}

	public void setEstadoInicioCierre(Integer estadoInicioCierre) {
		this.estadoInicioCierre = estadoInicioCierre;
	}

	public BigInteger getMaximoRegistroWS() {
		return maximoRegistroWS;
	}

	public void setMaximoRegistroWS(BigInteger maximoRegistroWS) {
		this.maximoRegistroWS = maximoRegistroWS;
	}

	public BigInteger getContadorBancoWS() {
		return contadorBancoWS;
	}

	public void setContadorBancoWS(BigInteger contadorBancoWS) {
		this.contadorBancoWS = contadorBancoWS;
	}

	public String getLogProcesoHabilitado() {
		return logProcesoHabilitado;
	}

	public void setLogProcesoHabilitado(String logProcesoHabilitado) {
		this.logProcesoHabilitado = logProcesoHabilitado;
	}

	public String getDireccionAvisoEmail() {
		return direccionAvisoEmail;
	}

	public void setDireccionAvisoEmail(String direccionAvisoEmail) {
		this.direccionAvisoEmail = direccionAvisoEmail;
	}

	public String getCodigoSucursalCentral() {
		return codigoSucursalCentral;
	}

	public void setCodigoSucursalCentral(String codigoSucursalCentral) {
		this.codigoSucursalCentral = codigoSucursalCentral;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}
}
