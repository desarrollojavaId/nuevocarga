package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "logprocesos")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class LogProceso {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_logProceso")
	@SequenceGenerator(name = "id_sequence_logProceso", sequenceName = "id_seq_logProceso")
	private Long id;

	@Column(name = "nroProceso", nullable = false)
	private Integer numeroProceso;

	@Column(name = "estadoProceso", nullable = false)
	private Integer estadoProceso;

	@Column(name = "pasoAccion", nullable = false, length = 200)
	private String pasoAccion;

	@Column(name = "datosError", nullable = true, length = 100)
	private String datosError;

	@Column(name = "grabadoDesde", nullable = false, length = 100)
	private String grabadoDesde;

	@Column(name = "nodo", nullable = false, length = 200)
	private String nodo;

	@Column(name = "fecha", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;

	@Column(name = "usuario", nullable = false)
	private String usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumeroProceso() {
		return numeroProceso;
	}

	public void setNumeroProceso(Integer numeroProceso) {
		this.numeroProceso = numeroProceso;
	}

	public Integer getEstadoProceso() {
		return estadoProceso;
	}

	public void setEstadoProceso(Integer estadoProceso) {
		this.estadoProceso = estadoProceso;
	}

	public String getPasoAccion() {
		return pasoAccion;
	}

	public void setPasoAccion(String pasoAccion) {
		this.pasoAccion = pasoAccion;
	}

	public String getDatosError() {
		return datosError;
	}

	public void setDatosError(String datosError) {
		this.datosError = datosError;
	}

	public String getGrabadoDesde() {
		return grabadoDesde;
	}

	public void setGrabadoDesde(String grabadoDesde) {
		this.grabadoDesde = grabadoDesde;
	}

	public String getNodo() {
		return nodo;
	}

	public void setNodo(String nodo) {
		this.nodo = nodo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
