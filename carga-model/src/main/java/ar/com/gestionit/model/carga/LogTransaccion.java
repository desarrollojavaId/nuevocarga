package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "logtransacciones")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class LogTransaccion {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_logTransacciones")
	@SequenceGenerator(name = "id_sequence_logTransacciones", sequenceName = "id_seq_logTransacciones")
	private Long id;

	@Column(name = "tipoTrn", nullable = false, length = 1)
	private String tipoTransaccion;

	@Column(name = "tabla", nullable = false, length = 50)
	private String tabla;

	@Column(name = "campo", nullable = false, length = 25)
	private String campo;

	@Column(name = "pk", nullable = false, length = 100)
	private String primaryKey;

	@Column(name = "valorOriginal", nullable = true, length = 100)
	private String valorOriginal;

	@Column(name = "valorNuevo", nullable = true, length = 100)
	private String valorNuevo;

	@Column(name = "fechaTrn", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaTransaccion;

	@Column(name = "usuario", nullable = false, length = 50)
	private String usuario;
	
	@Transient
	private String tipoTransaccionDescripcion;

	@Transient
	private String fechaTransaccionFormateada;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoTransaccion() {
		return tipoTransaccion;
	}

	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	public String getTabla() {
		return tabla;
	}

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getValorOriginal() {
		return valorOriginal;
	}

	public void setValorOriginal(String valorOriginal) {
		this.valorOriginal = valorOriginal;
	}

	public String getValorNuevo() {
		return valorNuevo;
	}

	public void setValorNuevo(String valorNuevo) {
		this.valorNuevo = valorNuevo;
	}

	public Date getFechaTransaccion() {
		return fechaTransaccion;
	}

	public void setFechaTransaccion(Date fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTipoTransaccionDescripcion() {
		return tipoTransaccionDescripcion;
	}

	public void setTipoTransaccionDescripcion(String tipoTransaccionDescripcion) {
		this.tipoTransaccionDescripcion = tipoTransaccionDescripcion;
	}

	public String getFechaTransaccionFormateada() {
		return fechaTransaccionFormateada;
	}

	public void setFechaTransaccionFormateada(String fechaTransaccionFormateada) {
		this.fechaTransaccionFormateada = fechaTransaccionFormateada;
	}

}
