package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "mstablas", uniqueConstraints = @UniqueConstraint(columnNames = { "tabla", "codigo", "subCodigo" }))
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MsTablas {


	@EmbeddedId
	public MsTablasPK id;

	@Column(name = "descripcion", nullable = false, length = 100)
	private String descripcion;

	@Column(name = "descRed", nullable = true, length = 30)
	private String descripcionReducida;

	@Column(name = "auxUsoInt1", nullable = true, length = 1)
	private String auxiliarUsoInterno1;

	@Column(name = "auxUsoInt2", nullable = true, length = 20)
	private String auxiliarUsoInterno2;

	@Column(name = "auxUsoBco", nullable = true, length = 20)
	private String auxiliarUsoBanco;

	@Column(name = "autMan", nullable = false, length = 1)
	private String automaticaMananual;

	@Column(name = "fecalta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaAlta;

	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta;

	@Column(name = "usrAlta", length = 50, nullable = false)
	private String usuarioAlta;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	@Column(name = "fecultact", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaUltimaActualizacion;

	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltimaActualizacion;

	@Column(name = "usrUltAct", length = 50, nullable = false)
	private String usuarioUltimaActualizacion;

	public MsTablasPK getId() {
		return id;
	}

	public void setId(MsTablasPK id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionReducida() {
		return descripcionReducida;
	}

	public void setDescripcionReducida(String descripcionReducida) {
		this.descripcionReducida = descripcionReducida;
	}

	public String getAuxiliarUsoInterno1() {
		return auxiliarUsoInterno1;
	}

	public void setAuxiliarUsoInterno1(String auxiliarUsoInterno1) {
		this.auxiliarUsoInterno1 = auxiliarUsoInterno1;
	}

	public String getAuxiliarUsoInterno2() {
		return auxiliarUsoInterno2;
	}

	public void setAuxiliarUsoInterno2(String auxiliarUsoInterno2) {
		this.auxiliarUsoInterno2 = auxiliarUsoInterno2;
	}

	public String getAuxiliarUsoBanco() {
		return auxiliarUsoBanco;
	}

	public void setAuxiliarUsoBanco(String auxiliarUsoBanco) {
		this.auxiliarUsoBanco = auxiliarUsoBanco;
	}

	public String getAutomaticaMananual() {
		return automaticaMananual;
	}

	public void setAutomaticaMananual(String automaticaMananual) {
		this.automaticaMananual = automaticaMananual;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltimaActualizacion() {
		return horaUltimaActualizacion;
	}

	public void setHoraUltimaActualizacion(Date horaUltimaActualizacion) {
		this.horaUltimaActualizacion = horaUltimaActualizacion;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}



}
