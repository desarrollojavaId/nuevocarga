package ar.com.gestionit.model.carga;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Index;
import org.springframework.beans.factory.annotation.Configurable;

@Embeddable
@Configurable
public class MsTablasPK implements Serializable {

	private static final long serialVersionUID = 1L;

	public MsTablasPK(Integer tabla, Integer codigo, Integer subCodigo) {
		super();
		this.tabla = tabla;
		this.codigo = codigo;
		this.subCodigo = subCodigo;
	}

	public MsTablasPK() {
		super();
	}

	@Index(name = "INDEX_tabla_codigo_subCodigo", columnNames = {   "tabla", "codigo", "subCodigo"  })
	@Column(name = "tabla", nullable = false)
	private Integer tabla;

	@Column(name = "codigo", nullable = false)
	private Integer codigo;

	@Column(name = "subCodigo", nullable = false)
	private Integer subCodigo;

	public Integer getTabla() {
		return tabla;
	}

	public void setTabla(Integer tabla) {
		this.tabla = tabla;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getSubCodigo() {
		return subCodigo;
	}

	public void setSubCodigo(Integer subCodigo) {
		this.subCodigo = subCodigo;
	}
}
