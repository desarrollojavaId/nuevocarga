package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "requerimiento")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "cliente", "usuEmpLider", "sector", "contrato",
		"usuCliente" })
public class Requerimiento extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_requerimiento")
	@SequenceGenerator(name = "id_sequence_requerimiento", sequenceName = "id_seq_requerimiento")
	private Long id;

	@Column(name = "nuss", unique = true, nullable = false)
	private Long nuss;

	@Column(name = "idCliente", nullable = false, length = 25)
	private String idCliente; // Clarity

	@Column(name = "titulo", nullable = false, length = 100)
	private String titulo; // 100

	@Column(name = "descripcion", nullable = false, length = 255)
	private String descripcion; // 255

	// Se elimina esta relación porque la relación natural es cliente --> contrato --> requerimiento 
	//@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	//@JoinColumn(name = "cliente_id")
	//private Cliente cliente;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "sector_id")
	private Sector sector;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "contrato_id")
	private Contrato contrato;

	// usuEmpleado
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "usuEmpLider_id")
	private UsuEmpleado usuEmpLider;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "usuCliente_id")
	private UsuCliente usuCliente;

	@Column(name = "t06Sistema", nullable = false)
	private Integer t06Sistema;

	@Column(name = "t05TipoRequerimiento", nullable = false)
	private Integer t05TipoRequerimiento;

	@Column(name = "t04TipoSolicitud", nullable = false)
	private Integer t04TipoSolicitud;

	@Column(name = "t04Estado", nullable = false)
	private Integer t04Estado;

	@Column(name = "t04EstadoAnterior", nullable = true)
	private Integer t04EstadoAnterior;

	@Column(name = "esfuerzoEstimado", nullable = true)
	private Integer esfuerzoEstimado;

	@Column(name = "esfuerzoAdicional", nullable = true)
	private Integer esfuerzoAdicional;

	@Column(name = "esfuerzoEstimadoUT", nullable = true)
	private Integer esfuerzoEstimadoUT;

	@Column(name = "esfuerzoAdicionalUT", nullable = true)
	private Integer esfuerzoAdicionalUT;

	@Column(name = "fecSolicitud", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecSolicitud;

	@Column(name = "fecInicioImputacion", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecInicioImputacion;

	@Column(name = "fecOriInicioEstimado", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecOriInicioEstimado;

	@Column(name = "fecUltInicioEstimado", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecUltInicioEstimado;

	@Column(name = "fecOriFinalEstimado", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecOriFinalEstimado;

	@Column(name = "fecUltFinalEstimado", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecUltFinalEstimado;

	@Column(name = "fecEstimadaFinCarga", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecEstimadaFinCarga;

	@Column(name = "fecFinCarga", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecFinCarga;

	@Column(name = "fecEntrega", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecEntrega;

	@Column(name = "fecImplementación", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecImplementación;

	@Column(name = "observaciones", nullable = true, length = 255)
	private String observaciones; // 255

	@Column(name = "tratamiento", nullable = true, length = 255)
	private String tratamiento;

	@Column(name = "linkDocumentacion", nullable = true, length = 255)
	private String linkDocumentacion;

	@Column(name = "nussConRetraso", nullable = true, length = 1)
	private String nussConRetraso;

	@Column(name = "t08MotivoRetraso1", nullable = true, length = 3) // t08MotivoRetraso1
	private Integer t08MotivoRetraso1;

	@Column(name = "t08MotivoRetraso2", nullable = true, length = 3)
	private Integer t08MotivoRetraso2;

	@Column(name = "t08MotivoRetraso3", nullable = true, length = 3)
	private Integer t08MotivoRetraso3;

	// Datos de auditoría

	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaAlta;

	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta;

	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta;

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;

	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct;

	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	// Datos transient

	@Transient
	String descEstado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNuss() {
		return nuss;
	}

	public void setNuss(Long nuss) {
		this.nuss = nuss;
	}

	public String getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public UsuEmpleado getUsuEmpLider() {
		return usuEmpLider;
	}

	public void setUsuEmpLider(UsuEmpleado usuEmpLider) {
		this.usuEmpLider = usuEmpLider;
	}

	public UsuCliente getUsuCliente() {
		return usuCliente;
	}

	public void setUsuCliente(UsuCliente usuCliente) {
		this.usuCliente = usuCliente;
	}

	public Integer getT06Sistema() {
		return t06Sistema;
	}

	public void setT06Sistema(Integer t06Sistema) {
		this.t06Sistema = t06Sistema;
	}

	public Integer getT05TipoRequerimiento() {
		return t05TipoRequerimiento;
	}

	public void setT05TipoRequerimiento(Integer t05TipoRequerimiento) {
		this.t05TipoRequerimiento = t05TipoRequerimiento;
	}

	public Integer getT04TipoSolicitud() {
		return t04TipoSolicitud;
	}

	public void setT04TipoSolicitud(Integer t04TipoSolicitud) {
		this.t04TipoSolicitud = t04TipoSolicitud;
	}

	public Integer getT04Estado() {
		return t04Estado;
	}

	public void setT04Estado(Integer t04Estado) {
		this.t04Estado = t04Estado;
	}

	public Integer getT04EstadoAnterior() {
		return t04EstadoAnterior;
	}

	public void setT04EstadoAnterior(Integer t04EstadoAnterior) {
		this.t04EstadoAnterior = t04EstadoAnterior;
	}

	public Integer getEsfuerzoEstimado() {
		return esfuerzoEstimado;
	}

	public void setEsfuerzoEstimado(Integer esfuerzoEstimado) {
		this.esfuerzoEstimado = esfuerzoEstimado;
	}

	public Integer getEsfuerzoAdicional() {
		return esfuerzoAdicional;
	}

	public void setEsfuerzoAdicional(Integer esfuerzoAdicional) {
		this.esfuerzoAdicional = esfuerzoAdicional;
	}

	public Integer getEsfuerzoEstimadoUT() {
		return esfuerzoEstimadoUT;
	}

	public void setEsfuerzoEstimadoUT(Integer esfuerzoEstimadoUT) {
		this.esfuerzoEstimadoUT = esfuerzoEstimadoUT;
	}

	public Integer getEsfuerzoAdicionalUT() {
		return esfuerzoAdicionalUT;
	}

	public void setEsfuerzoAdicionalUT(Integer esfuerzoAdicionalUT) {
		this.esfuerzoAdicionalUT = esfuerzoAdicionalUT;
	}

	public Date getFecSolicitud() {
		return fecSolicitud;
	}

	public void setFecSolicitud(Date fecSolicitud) {
		this.fecSolicitud = fecSolicitud;
	}

	public Date getFecInicioImputacion() {
		return fecInicioImputacion;
	}

	public void setFecInicioImputacion(Date fecInicioImputacion) {
		this.fecInicioImputacion = fecInicioImputacion;
	}

	public Date getFecOriInicioEstimado() {
		return fecOriInicioEstimado;
	}

	public void setFecOriInicioEstimado(Date fecOriInicioEstimado) {
		this.fecOriInicioEstimado = fecOriInicioEstimado;
	}

	public Date getFecUltInicioEstimado() {
		return fecUltInicioEstimado;
	}

	public void setFecUltInicioEstimado(Date fecUltInicioEstimado) {
		this.fecUltInicioEstimado = fecUltInicioEstimado;
	}

	public Date getFecOriFinalEstimado() {
		return fecOriFinalEstimado;
	}

	public void setFecOriFinalEstimado(Date fecOriFinalEstimado) {
		this.fecOriFinalEstimado = fecOriFinalEstimado;
	}

	public Date getFecUltFinalEstimado() {
		return fecUltFinalEstimado;
	}

	public void setFecUltFinalEstimado(Date fecUltFinalEstimado) {
		this.fecUltFinalEstimado = fecUltFinalEstimado;
	}

	public Date getFecEstimadaFinCarga() {
		return fecEstimadaFinCarga;
	}

	public void setFecEstimadaFinCarga(Date fecEstimadaFinCarga) {
		this.fecEstimadaFinCarga = fecEstimadaFinCarga;
	}

	public Date getFecFinCarga() {
		return fecFinCarga;
	}

	public void setFecFinCarga(Date fecFinCarga) {
		this.fecFinCarga = fecFinCarga;
	}

	public Date getFecEntrega() {
		return fecEntrega;
	}

	public void setFecEntrega(Date fecEntrega) {
		this.fecEntrega = fecEntrega;
	}

	public Date getFecImplementación() {
		return fecImplementación;
	}

	public void setFecImplementación(Date fecImplementación) {
		this.fecImplementación = fecImplementación;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

	public String getLinkDocumentacion() {
		return linkDocumentacion;
	}

	public void setLinkDocumentacion(String linkDocumentacion) {
		this.linkDocumentacion = linkDocumentacion;
	}

	public String getNussConRetraso() {
		return nussConRetraso;
	}

	public void setNussConRetraso(String nussConRetraso) {
		this.nussConRetraso = nussConRetraso;
	}

	public Integer getT08MotivoRetraso1() {
		return t08MotivoRetraso1;
	}

	public void setT08MotivoRetraso1(Integer t08MotivoRetraso1) {
		this.t08MotivoRetraso1 = t08MotivoRetraso1;
	}

	public Integer getT08MotivoRetraso2() {
		return t08MotivoRetraso2;
	}

	public void setT08MotivoRetraso2(Integer t08MotivoRetraso2) {
		this.t08MotivoRetraso2 = t08MotivoRetraso2;
	}

	public Integer getT08MotivoRetraso3() {
		return t08MotivoRetraso3;
	}

	public void setT08MotivoRetraso3(Integer t08MotivoRetraso3) {
		this.t08MotivoRetraso3 = t08MotivoRetraso3;
	}

	public String getDescEstado() {
		return descEstado;
	}

	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	//
	// Datos de auditoría
	//
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

}
