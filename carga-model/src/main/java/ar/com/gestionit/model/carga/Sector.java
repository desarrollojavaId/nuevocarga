package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "AreaSector")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "usuEmpLider" })
public class Sector extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_sector")
	@SequenceGenerator(name = "id_sequence_sector", sequenceName = "id_seq_sector")
	private Long id;	
	
	@Column(name = "descripcion", unique=true, nullable = false, length = 255)
	private String descripcion;

	  
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "area_id")
	private Area area;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "usuEmpLider_id")
	private UsuEmpleado usuEmpLider;
  
	// Datos de auditoría	

	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaAlta;
	
	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta ;
	
	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta; 

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 
	
	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	@Transient
	private boolean valida = true;
	
	@Transient 
	String nombreLider;


	//
	// Getters y Setters	
	//
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public UsuEmpleado getUsuEmpLider() {
		return usuEmpLider;
	}

	public void setUsuEmpLider(UsuEmpleado usuEmpLider) {
		this.usuEmpLider = usuEmpLider;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

	public boolean isValida() {
		return valida;
	}

	public void setValida(boolean valida) {
		this.valida = valida;
	}

	public String getNombreLider() {
		return usuEmpLider == null ? "" : usuEmpLider.getNombre() + ", " + usuEmpLider.getApellido();
	}

	public void setNombreLider(String nombreLider) {
		this.nombreLider = nombreLider;
	}
	
}
