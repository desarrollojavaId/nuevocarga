package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Tarea")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "requerimiento", "area" })
public class Tarea extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_tarea")
	@SequenceGenerator(name = "id_sequence_tarea", sequenceName = "id_seq_tarea")
	private Long id;
	  
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "requerimiento_id")
	private Requerimiento requerimiento;

	@Column(name = "titulo", nullable = false, length = 100)
	private String titulo;

	@Column(name = "descripcion", nullable = false, length = 255)
	private String descripcion;
	  
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "area_id")
	private Area area;

	@Column(name = "t07TipoTarea", nullable = false)
	private Integer t07TipoTarea;

	@Column(name = "t14Estado", nullable = false)
	private Integer t14Estado;

	@Column(name = "esfuerzoEstimado", nullable = true)
	private Integer esfuerzoEstimado;

	@Column(name = "esfuerzoEstimadoUT", nullable = true)
	private Integer esfuerzoEstimadoUT;

	@Column(name = "fecIniEjecucion", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecIniEjecucion;

	@Column(name = "fecIniTarea", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecIniTarea;

	@Column(name = "fecFinEjecucion", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecFinEjecucion;

	@Column(name = "fecFinTarea", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecFinTarea;

	// Datos de auditoría

	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaAlta;

	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta;

	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta;

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;

	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct;

	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	// Datos transient

	@Transient
	private String descTipoTarea;

	@Transient
	private String descEstado;

	@Transient 
	private Long nuss;

	@Transient
	private String areaDescripcion;
	
	
	//
	// Setter y getter
	//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Requerimiento getRequerimiento() {
		return requerimiento;
	}

	public void setRequerimiento(Requerimiento requerimiento) {
		this.requerimiento = requerimiento;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Integer getT07TipoTarea() {
		return t07TipoTarea;
	}

	public void setT07TipoTarea(Integer t07TipoTarea) {
		this.t07TipoTarea = t07TipoTarea;
	}

	public Integer getT14Estado() {
		return t14Estado;
	}

	public void setT14Estado(Integer t14Estado) {
		this.t14Estado = t14Estado;
	}

	public Integer getEsfuerzoEstimado() {
		return esfuerzoEstimado;
	}

	public void setEsfuerzoEstimado(Integer esfuerzoEstimado) {
		this.esfuerzoEstimado = esfuerzoEstimado;
	}

	public Integer getEsfuerzoEstimadoUT() {
		return esfuerzoEstimadoUT;
	}

	public void setEsfuerzoEstimadoUT(Integer esfuerzoEstimadoUT) {
		this.esfuerzoEstimadoUT = esfuerzoEstimadoUT;
	}

	public Date getFecIniEjecucion() {
		return fecIniEjecucion;
	}

	public void setFecIniEjecucion(Date fecIniEjecucion) {
		this.fecIniEjecucion = fecIniEjecucion;
	}

	public Date getFecIniTarea() {
		return fecIniTarea;
	}

	public void setFecIniTarea(Date fecIniTarea) {
		this.fecIniTarea = fecIniTarea;
	}

	public Date getFecFinEjecucion() {
		return fecFinEjecucion;
	}

	public void setFecFinEjecucion(Date fecFinEjecucion) {
		this.fecFinEjecucion = fecFinEjecucion;
	}

	public Date getFecFinTarea() {
		return fecFinTarea;
	}

	public void setFecFinTarea(Date fecFinTarea) {
		this.fecFinTarea = fecFinTarea;
	}

	public String getDescTipoTarea() {
		return descTipoTarea;
	}

	public void setDescTipoTarea(String descTipoTarea) {
		this.descTipoTarea = descTipoTarea;
	}

	public String getDescEstado() {
		return descEstado;
	}

	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	public Long getNuss() {
		return requerimiento == null ? null : requerimiento.getNuss();
	}

	public void setNuss(Long nuss) {
		this.nuss = nuss;
	}

	public String getAreaDescripcion() {
		return area == null ? null : area.getDescripcion();
	}

	public void setAreaDescripcion(String areaDescripcion) {
		this.areaDescripcion = areaDescripcion;
	}

	//
	// Datos de auditoría
	//
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

}
