package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ar.com.gestionit.model.seguridad.Usuario;

@Entity
@Table(name = "TareaUsuEmpleado")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "tarea", "usuEmpleado" })
public class TareaUsuEmpleado extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence_tarea")
	@SequenceGenerator(name = "id_sequence_tarea", sequenceName = "id_seq_tarea")
	private Long id;

	// tarea
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "tarea_id")
	private Tarea tarea;

	// usuEmpleado
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "usuEmpleado_id")
	private UsuEmpleado usuEmpleado;


	// Datos de auditoría

	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaAlta;

	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta;

	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta;

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;

	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct;

	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	//
	// Datos transient

	@Transient
	private String tareaTitulo;

	@Transient 
	private String nombreApellido;

	@Transient 
	private String esLider;

	@Transient 
	private Usuario usuario;
	
	
	//
	// Setter y getter
	//

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Tarea getTarea() {
		return tarea;
	}

	public void setTarea(Tarea tarea) {
		this.tarea = tarea;
	}

	public UsuEmpleado getUsuEmpleado() {
		return usuEmpleado;
	}

	public void setUsuEmpleado(UsuEmpleado usuEmpleado) {
		this.usuEmpleado = usuEmpleado;
	}

	public String getTareaTitulo() {
		return tarea == null ? null : tarea.getTitulo();
	}

	public void setTareaTitulo(String tareaTitulo) {
		this.tareaTitulo = tareaTitulo;
	}

	public String getNombreApellido() {
		return usuEmpleado == null ? null : usuEmpleado.getNombre() + ", " + usuEmpleado.getApellido();
	}

	public void setNombreApellido(String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}

	public String getEsLider() {
		return usuEmpleado == null ? null : usuEmpleado.getEsLider();
	}

	public void setEsLider(String esLider) {
		this.esLider = esLider;
	}

	public Usuario getUsuario() {
		return usuEmpleado == null ? null : usuEmpleado.getUsuario();
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	//
	// Datos de auditoría
	//
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

}
