package ar.com.gestionit.model.carga;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ar.com.gestionit.model.seguridad.Usuario;

@Entity
@Table(name = "usuCliente")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "usuario", "cliente"})
public class UsuCliente extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_usuCliente")
	@SequenceGenerator(name = "id_sequence_usuCliente", sequenceName = "id_seq_usuCliente")
	private Long id;	
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "usuario_id", referencedColumnName = "id")
	private Usuario usuario;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "cliente_id")
	private Cliente cliente;

	@Column(name = "sector", nullable = true, length = 100)
	private String sector;
	
	@Column(name = "nombreJefe", nullable = true, length = 100)
	private String nombreJefe;
	
//	@OneToMany(mappedBy = "cliente")
//	private Set<Requerimiento> requerimientos;
	
//	
// Datos de auditoría	
//
	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaAlta;
	
	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta ;
	
	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta; 

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 
	
	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

//	@Transient
//	private boolean valida = true;

	//Datos transient
	@Transient 
	private String nombre;
	
	@Transient 
	private String apellido;
	
	@Transient 
	private String razonSocial;

	//
	// Getters y Setters	
	//
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getNombreJefe() {
		return nombreJefe;
	}

	public void setNombreJefe(String nombreJefe) {
		this.nombreJefe = nombreJefe;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

	public String getNombre() {
		if(usuario!=null)
			return usuario.getNombre();
		else
			return null;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		if(usuario!=null)
			return usuario.getApellido();
		else
			return null;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getRazonSocial() {
			return cliente.getRazonSocial();
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

//	public Set<Requerimiento> getRequerimientos() {
//		return requerimientos;
//	}
//
//	public void setRequerimientos(Set<Requerimiento> requerimientos) {
//		this.requerimientos = requerimientos;
//	}
	
}
