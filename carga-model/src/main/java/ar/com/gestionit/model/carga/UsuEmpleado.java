package ar.com.gestionit.model.carga;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ar.com.gestionit.model.seguridad.Usuario;

@Entity
@Table(name = "usuEmpleado")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "usuario", "usuEmpLider", "sector", "cliente"})
public class UsuEmpleado extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_usuEmpleado")
	@SequenceGenerator(name = "id_sequence_usuEmpleado", sequenceName = "id_seq_usuEmpleado")
	private Long id;	
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "usuario_id", referencedColumnName = "id")
	private Usuario usuario;
	
	@Column(name = "acreditaHoras", nullable = false, length = 2)
	private String acreditaHoras;
	
	@Column(name = "horasDiarias", nullable = false, length = 2)
	private int horasDiarias;
	
	@Column(name = "registraIngreso", nullable = false, length = 2)
	private String registraIngreso;
	
	@Column(name = "esLider", nullable = false, length = 2)
	private String esLider;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "usuEmpLider_id")
	private UsuEmpleado usuEmpLider;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "sector_id")
	private Sector sector;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;
	
	@Column(name = "numeroLegajo", unique=true, nullable = false)
	private BigInteger numeroLegajo;
	
	@Column(name = "diasVacacionesLey", length = 2)
	private int diasVacacionesLey;
	
	@Column(name = "diasVacacionesAdic", length = 2)
	private int diasVacacionesAdic;
	
	@Column(name = "fecIngreso", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fecIngreso;
	
	@Column(name = "fecEgreso")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fecEgreso;
	
	@Column(name = "costoLaboral")
	private Long costoLaboral;	

//	
// Datos de auditoría	

	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaAlta;
	
	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta ;
	
	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta; 

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 
	
	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	//
	//Datos transient
	@Transient 
	private String nombre;
	
	@Transient 
	private String apellido;
	
	@Transient 
	private String razonSocial;
	
	@Transient 
	String nombreLider;

	//
	// Getters y Setters	
	//
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
/*
	public String getIniciales() {
		return iniciales;
	}

	public void setIniciales(String iniciales) {
		this.iniciales = iniciales;
	}
*/
	public String getAcreditaHoras() {
		return acreditaHoras;
	}

	public void setAcreditaHoras(String acreditaHoras) {
		this.acreditaHoras = acreditaHoras;
	}

	public int getHorasDiarias() {
		return horasDiarias;
	}

	public void setHorasDiarias(int horasDiarias) {
		this.horasDiarias = horasDiarias;
	}

	public String getRegistraIngreso() {
		return registraIngreso;
	}

	public void setRegistraIngreso(String registraIngreso) {
		this.registraIngreso = registraIngreso;
	}

	public String getEsLider() {
		return esLider;
	}

	public void setEsLider(String esLider) {
		this.esLider = esLider;
	}

	public UsuEmpleado getUsuEmpLider() {
		return usuEmpLider;
	}

	public void setUsuEmpLider(UsuEmpleado usuEmpLider) {
		this.usuEmpLider = usuEmpLider;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public BigInteger getNumeroLegajo() {
		return numeroLegajo;
	}

	public void setNumeroLegajo(BigInteger numeroLegajo) {
		this.numeroLegajo = numeroLegajo;
	}

	public int getDiasVacacionesLey() {
		return diasVacacionesLey;
	}

	public void setDiasVacacionesLey(int diasVacacionesLey) {
		this.diasVacacionesLey = diasVacacionesLey;
	}

	public int getDiasVacacionesAdic() {
		return diasVacacionesAdic;
	}

	public void setDiasVacacionesAdic(int diasVacacionesAdic) {
		this.diasVacacionesAdic = diasVacacionesAdic;
	}

	public Date getFecIngreso() {
		return fecIngreso;
	}

	public void setFecIngreso(Date fecIngreso) {
		this.fecIngreso = fecIngreso;
	}

	public Date getFecEgreso() {
		return fecEgreso;
	}

	public void setFecEgreso(Date fecEgreso) {
		this.fecEgreso = fecEgreso;
	}
	
	public Long getCostoLaboral() {
		return costoLaboral;
	}

	public void setCostoLaboral(Long costoLaboral) {
		this.costoLaboral = costoLaboral;
	}

	
	//
	// Datos de auditoría
	//
	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

	
/*
	public boolean isValida() {
		return valida;
	}

	public void setValida(boolean valida) {
		this.valida = valida;
	}
*/
	public String getNombre() {
		return usuario == null ? null : usuario.getNombre();
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return usuario == null ? null : usuario.getApellido();
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getRazonSocial() {
		return cliente == null ? null : cliente.getRazonSocial();
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getNombreLider() {
		
		return usuEmpLider == null ? "" : usuEmpLider.getNombre() + ", " + usuEmpLider.getApellido();
	}

	public void setNombreLider(String nombreLider) {
		this.nombreLider = nombreLider;
	}

}
