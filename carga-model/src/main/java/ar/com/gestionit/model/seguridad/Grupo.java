package ar.com.gestionit.model.seguridad;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import ar.com.gestionit.model.carga.AbstractEntity;

@Entity
@Table(name = "msgrupo")
//@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "permisos" })
public class Grupo extends AbstractEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_msgrupo")
	@SequenceGenerator(name = "id_sequence_msgrupo", sequenceName = "id_seq_msgrupo")

	private Long id;

	@Column(name = "nombre", length = 45, nullable = false) 
	private String nombre;

	@Column(name = "descripcion", nullable = false, length = 255)
	private String descripcion;

	@Column(name = "t01Categoria", nullable = false)
	private Integer t01Categoria;

	@Column(name = "ENABLED")
    private boolean enabled;
	
	@Column(name = "fecAlta", nullable = false)
	private Date fechaAlta;
	
	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta ;
	
	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta; 
	
	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion; 
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 
	
	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;

	
	@OneToMany(mappedBy = "grupo", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<GrupoPuntoMenu> grupoPuntoMenu;

	@Transient
	private String habilitado;
	
	@Transient 
	String descCategoria;

	
	public String getHabilitado() {
		if(this.isEnabled())
			return "S";
		else
			return "N";
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getT01Categoria() {
		return t01Categoria;
	}

	public void setT01Categoria(Integer t01Categoria) {
		this.t01Categoria = t01Categoria;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getDescCategoria() {
		return descCategoria;
	}


	public void setDescCategoria(String descCategoria) {
		this.descCategoria = descCategoria;
	}


	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

}
