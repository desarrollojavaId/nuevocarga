package ar.com.gestionit.model.seguridad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "msgrupopuntomenu")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "puntoMenuDetalle" })
public class GrupoPuntoMenu {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_grupopuntomenu")
	@SequenceGenerator(name = "id_sequence_grupopuntomenu", sequenceName = "id_seq_grupopuntomenu")
	private Long id;	
	
	@ManyToOne()
	@JoinColumn(name = "msgrupo_id")
	private Grupo grupo;
		
	@ManyToOne()
	@JoinColumn(name = "mspuntomenu_id")
	private PuntoMenuDetalle puntoMenuDetalle;
	
	@Column(name = "nombreCategoriaAlt", nullable = true, length = 50)
	private String nombreCategoriaAlternativo;
	
	@Column(name = "nombrePuntoMenuAlt", nullable = true, length = 50)
	private String nombrePuntoMenuAlternativo;
	
	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaAlta;

	@Column(name = "horaAlta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date horaAlta;

	@Column(name = "usrAlta ", nullable = false, length = 50)
	private String usuarioAlta;

	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;
	
	
	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion; 
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 
	
	@Transient 
	private String nombreCategoriaParaMostrar;
	
	@Transient 
	private String nombreItemParaMostrar;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}


	public String getNombreCategoriaAlternativo() {
		return nombreCategoriaAlternativo;
	}

	public void setNombreCategoriaAlternativo(String nombreCategoriaAlternativo) {
		this.nombreCategoriaAlternativo = nombreCategoriaAlternativo;
	}

	public String getNombrePuntoMenuAlternativo() {
		return nombrePuntoMenuAlternativo;
	}

	public void setNombrePuntoMenuAlternativo(String nombrePuntoMenuAlternativo) {
		this.nombrePuntoMenuAlternativo = nombrePuntoMenuAlternativo;
	}

	public PuntoMenuDetalle getPuntoMenuDetalle() {
		return puntoMenuDetalle;
	}

	public void setPuntoMenuDetalle(PuntoMenuDetalle puntoMenuDetalle) {
		this.puntoMenuDetalle = puntoMenuDetalle;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getNombreCategoriaParaMostrar() {
		if(getFechaAlta() != null && (getNombreCategoriaAlternativo() == null || "".equals(getNombreCategoriaAlternativo())))
			return getPuntoMenuDetalle().getDescripcionPuntoMenu();
		else
			return getNombreCategoriaAlternativo();
	}

	public String getNombreItemParaMostrar() {
		
		if(getFechaAlta() != null && (getNombrePuntoMenuAlternativo() == null || "".equals(getNombrePuntoMenuAlternativo())))
			return getPuntoMenuDetalle().getDescripcionPuntoMenu();
		else
			return getNombrePuntoMenuAlternativo();
	}

	
	}
