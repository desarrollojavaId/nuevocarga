package ar.com.gestionit.model.seguridad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ar.com.gestionit.model.carga.AbstractEntity;

@Entity
@Table(name = "mspuntomenu")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler"})
public class PuntoMenu extends AbstractEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_mspuntomenu")
	@SequenceGenerator(name = "id_sequence_mspuntomenu", sequenceName = "id_seq_mspuntomenu")
	private Long id;

	@Column(name = "descPuntoMenu", length = 50, nullable = false) 
	private String descripcionPuntoMenu;	
	
	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaAlta;

	@Column(name = "horaAlta", nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date horaAlta;

	@Column(name = "usrAlta ", nullable = false, length = 50)
	private String usuarioAlta;

	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;
	
	
	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion; 
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 

	@Column(name = "orden", nullable = false) 
	private Long orden;	
	
	@Transient
	private String descIdPuntoMenu;
	
	@Transient
	public String getDecriminatorValue() {
	    return this.getClass().getAnnotation(DiscriminatorValue.class).value();
	}
	@Transient 
	private String nombreCategoriaParaMostrar;
	
	@Transient 
	private String nombreDetalleParaMostrar;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public Long getOrden() {
		return orden;
	}

	public void setOrden(Long orden) {
		this.orden = orden;
	}

	public String getDescripcionPuntoMenu() {
		return descripcionPuntoMenu;
	}

	public void setDescripcionPuntoMenu(String descripcionPuntoMenu) {
		this.descripcionPuntoMenu = descripcionPuntoMenu;
	}

	public String getDescIdPuntoMenu() {
		return getId() + " - " + getDescripcionPuntoMenu();
	}

	public String getNombreCategoriaParaMostrar() {
		if(nombreCategoriaParaMostrar != null && !"".equals(nombreCategoriaParaMostrar))
			return nombreCategoriaParaMostrar;
		else
			return descripcionPuntoMenu;
			
	}

	public void setNombreCategoriaParaMostrar(String nombreCategoriaParaMostrar) {
		this.nombreCategoriaParaMostrar = nombreCategoriaParaMostrar;
	}

	public String getNombreDetalleParaMostrar() {
		if(nombreDetalleParaMostrar != null  && !"".equals(nombreDetalleParaMostrar))
			return nombreDetalleParaMostrar;			
		else
			return descripcionPuntoMenu;
	}

	public void setNombreDetalleParaMostrar(String nombreDetalleParaMostrar) {
		this.nombreDetalleParaMostrar = nombreDetalleParaMostrar;
	}


}
