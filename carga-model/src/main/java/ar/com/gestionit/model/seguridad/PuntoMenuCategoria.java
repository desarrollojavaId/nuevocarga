package ar.com.gestionit.model.seguridad;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@DiscriminatorValue(value="Categoria")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "puntoMenuDetalles" })
public class PuntoMenuCategoria extends PuntoMenu{

	
	@OneToMany(mappedBy = "categoriaPuntoMenuId")
	private List<PuntoMenuDetalle> puntoMenuDetalles;

	public List<PuntoMenuDetalle> getPuntoMenuDetalles() {
		return puntoMenuDetalles;
	}

	public void setPuntoMenuDetalles(List<PuntoMenuDetalle> puntoMenuDetalles) {
		this.puntoMenuDetalles = puntoMenuDetalles;
	}
	
	
}
