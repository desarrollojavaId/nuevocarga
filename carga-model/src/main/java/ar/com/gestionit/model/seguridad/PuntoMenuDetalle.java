package ar.com.gestionit.model.seguridad;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@DiscriminatorValue(value = "Detalle")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler"})
public class PuntoMenuDetalle extends PuntoMenu {

	@ManyToOne()
	@JoinColumn(name = "catPuntoMenu_ID", nullable = true)
	private PuntoMenuCategoria categoriaPuntoMenuId;

	@ManyToOne()
	@JoinColumn(name = "msurlpuntomenu_id")
	private PuntoMenuUrl puntoMenuUrl;
	
	@OneToMany(mappedBy = "puntoMenuDetalle")
	private List<GrupoPuntoMenu> grupoPuntoMenu;
	
	public PuntoMenuCategoria getCategoriaPuntoMenuId() {
		return categoriaPuntoMenuId;
	}

	public void setCategoriaPuntoMenuId(PuntoMenuCategoria categoriaPuntoMenuId) {
		this.categoriaPuntoMenuId = categoriaPuntoMenuId;
	}

	public PuntoMenuUrl getPuntoMenuUrl() {
		return puntoMenuUrl;
	}

	public void setPuntoMenuUrl(PuntoMenuUrl puntoMenuUrl) {
		this.puntoMenuUrl = puntoMenuUrl;
	}

	public List<GrupoPuntoMenu> getGrupoPuntoMenu() {
		return grupoPuntoMenu;
	}

	public void setGrupoPuntoMenu(List<GrupoPuntoMenu> grupoPuntoMenu) {
		this.grupoPuntoMenu = grupoPuntoMenu;
	}

	
	

}
