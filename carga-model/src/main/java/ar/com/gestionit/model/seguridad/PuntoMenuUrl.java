package ar.com.gestionit.model.seguridad;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "msurlpuntomenu")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "puntoMenuDetalle" })
public class PuntoMenuUrl {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_msurlpuntomenu")
	@SequenceGenerator(name = "id_sequence_msurlpuntomenu", sequenceName = "id_seq_msurlpuntomenu")
 
	private Long id;

	@Column(name = "url", length = 45, nullable = false) 
	private String url;
	
	@OneToMany(mappedBy = "puntoMenuUrl")
	private Set<PuntoMenuDetalle> puntoMenuDetalle;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Set<PuntoMenuDetalle> getPuntoMenuDetalle() {
		return puntoMenuDetalle;
	}

	public void setPuntoMenuDetalle(Set<PuntoMenuDetalle> puntoMenuDetalle) {
		this.puntoMenuDetalle = puntoMenuDetalle;
	}

	
	
}
