package ar.com.gestionit.model.seguridad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Index;
import org.springframework.format.annotation.DateTimeFormat;

import ar.com.gestionit.model.carga.AbstractEntity;
import ar.com.gestionit.model.carga.UsuEmpleado;

@Entity
@Table(name = "MSUsuario")
public class Usuario extends AbstractEntity {

	@Id
	@Index(name = "INDEX_id", columnNames = { "id" })
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_sequence_usuario")
	@SequenceGenerator(name = "id_sequence_usuario", sequenceName = "id_seq_usuarios")
	private Long id;	
	
	@Column(name = "nombreUsuario", unique=true, nullable = false, length = 50)
	private String nombreUsuario;

	@Column(name = "clave", nullable = true, length = 255)
	private String clave;
	
	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;
	
	@Column(name = "apellido", nullable = false, length = 50)
	private String apellido;
	
	@Column(name = "dni", nullable = false, length = 8)
	private int dni;

	@Column(name = "t01Categoria", nullable = false)
	private Integer t01Categoria;

	@Column(name = "mail", nullable = true, length = 255)
	private String mail;
	
	@Column(name = "telPrincipal", nullable = true, length = 100)
	private String telPrincipal;

	@Column(name = "telSecundario", nullable = true, length = 100)
	private String telSecundario;
	
    @OneToOne(mappedBy = "usuario")
	private UsuEmpleado usuEmpleado;
    
    @Column(name = "iniciales", unique=true, nullable = true, length = 8)
	private String iniciales;
    
    
    
// Datos de auditoría	

	@Column(name = "fecAlta", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaAlta;
	
	@Column(name = "horaAlta", nullable = false)
	private Date horaAlta ;
	
	@Column(name = "usrAlta", nullable = false, length = 50)
	private String usuarioAlta; 

	@Column(name = "fecUltAct", nullable = true)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date fechaUltimaActualizacion;
	
	@Column(name = "horaUltAct", nullable = true)
	private Date horaUltAct; 
	
	@Column(name = "usrUltAct", nullable = true, length = 50)
	private String usuarioUltimaActualizacion;

	@Column(name = "fecBajalog", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaBajaLog;

	@Column(name = "horabajalog", nullable = true)
	private Date horaBajaLog;

	@Column(name = "usrBajaLog", length = 50, nullable = true)
	private String usuarioBajaLog;
//
//	@Transient
//	private boolean valida = true;


	//
	// Getters y Setters	
	//
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public Integer getT01Categoria() {
		return t01Categoria;
	}

	public void setT01Categoria(Integer t01Categoria) {
		this.t01Categoria = t01Categoria;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelPrincipal() {
		return telPrincipal;
	}

	public void setTelPrincipal(String telPrincipal) {
		this.telPrincipal = telPrincipal;
	}

	public String getTelSecundario() {
		return telSecundario;
	}

	public void setTelSecundario(String telSecundario) {
		this.telSecundario = telSecundario;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getHoraAlta() {
		return horaAlta;
	}

	public void setHoraAlta(Date horaAlta) {
		this.horaAlta = horaAlta;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	public Date getHoraUltAct() {
		return horaUltAct;
	}

	public void setHoraUltAct(Date horaUltAct) {
		this.horaUltAct = horaUltAct;
	}

	public String getUsuarioUltimaActualizacion() {
		return usuarioUltimaActualizacion;
	}

	public void setUsuarioUltimaActualizacion(String usuarioUltimaActualizacion) {
		this.usuarioUltimaActualizacion = usuarioUltimaActualizacion;
	}

	public Date getFechaBajaLog() {
		return fechaBajaLog;
	}

	public void setFechaBajaLog(Date fechaBajaLog) {
		this.fechaBajaLog = fechaBajaLog;
	}

	public Date getHoraBajaLog() {
		return horaBajaLog;
	}

	public void setHoraBajaLog(Date horaBajaLog) {
		this.horaBajaLog = horaBajaLog;
	}

	public String getUsuarioBajaLog() {
		return usuarioBajaLog;
	}

	public void setUsuarioBajaLog(String usuarioBajaLog) {
		this.usuarioBajaLog = usuarioBajaLog;
	}

	
	
/*	
	public boolean isValida() {
		return valida;
	}

	public void setValida(boolean valida) {
		this.valida = valida;
	}
*/
	public UsuEmpleado getUsuEmpleado() {
		return usuEmpleado;
	}

	public void setUsuEmpleado(UsuEmpleado usuEmpleado) {
		this.usuEmpleado = usuEmpleado;
	}

	public String getIniciales() {
		return iniciales;
	}

	public void setIniciales(String iniciales) {
		this.iniciales = iniciales;
	}

	
	
}
