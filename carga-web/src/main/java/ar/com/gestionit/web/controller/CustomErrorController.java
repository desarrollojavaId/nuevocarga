package ar.com.gestionit.web.controller;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
class CustomErrorController {
	private static final Logger logger = LogManager.getLogger(CustomErrorController.class);

	@RequestMapping(value="/error")
	public String customError(HttpServletRequest request, HttpServletResponse response, Model model) {
		// retrieve some useful information from the request
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		logger.warn("ERROR_SERVER " + statusCode, throwable);

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
		if (requestUri == null) {
			requestUri = "Unknown";
		}

		String message = MessageFormat.format("Codigo de error {0} al intentar acceder a la url {1}.", statusCode, requestUri);

		model.addAttribute("errorMessage", message);
		return "customError";
	}
}