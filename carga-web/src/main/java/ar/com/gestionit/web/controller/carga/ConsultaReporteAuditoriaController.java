package ar.com.gestionit.web.controller.carga;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.model.carga.LogTransaccion;
import ar.com.gestionit.model.carga.MsTablas;
import ar.com.gestionit.service.carga.LogTransaccionService;
import ar.com.gestionit.service.carga.MsTablasService;

@Controller
@RequestMapping("/reporteAuditoria")
public class ConsultaReporteAuditoriaController {

	private static Logger logger = LogManager.getLogger(ConsultaReporteAuditoriaController.class);

	@Autowired
	LogTransaccionService service;

	@Autowired
	MsTablasService serviceMstablas;

	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(@RequestParam(name = "sinRegistro", required = false) boolean sinRegistro,
			@RequestParam(name = "error", required = false) boolean error, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/reporteAuditoria/reporteAuditoria_list");
		modelAndView.addObject("sinRegistro", sinRegistro);
		modelAndView.addObject("error", error);
		return modelAndView;
	}

	/**
	 * 
	 * @param tipoTransaccion
	 * @param tablaCodigo
	 * @param campoCodigo
	 * @param usuario
	 * @param fechaHasta
	 * @param fechaDesde
	 * @param request
	 * @param httpServletResponse
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/exportar")
	public void exportarPrueba(@RequestParam(name = "tipoTransaccion", required = false) Integer tipoTransaccion,
			@RequestParam(name = "tabla", required = false) Integer tablaCodigo,
			@RequestParam(name = "campo", required = false) Integer campoCodigo,
			@RequestParam(name = "usuario", required = false) String usuario,
			@RequestParam(name = "fechaHasta", required = false) String fechaHasta,
			@RequestParam(name = "fechaDesde", required = false) String fechaDesde, HttpServletRequest request,
			HttpServletResponse httpServletResponse) {
		HashMap<String, Object> datosRetornados  = buscarDatosAuditor(tipoTransaccion, tablaCodigo, campoCodigo, usuario, fechaHasta, fechaDesde);
		List<LogTransaccion> logTransacciones = (List<LogTransaccion>) datosRetornados.get("logTranscciones");
		String descripcionTipoTransaccion = (String) datosRetornados.get("descripcionTipoTransaccion");
		String descrReducidaTipoTransaccion = (String) datosRetornados.get("descrReducidaTipoTransaccion");
		Date fechaDesdeConsulta = (Date) datosRetornados.get("fechaDesdeConsulta");
		Date fechaHastaConsulta = (Date) datosRetornados.get("fechaHastaConsulta");
		SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
		MsTablas mstablas;
		SimpleDateFormat formatNombreReporte = new SimpleDateFormat("yyyyMMdd");
		for (LogTransaccion logTransaccion : logTransacciones) {
			//
			if (descrReducidaTipoTransaccion == null) {
				mstablas = serviceMstablas.findByTablaDescripcionSubCodigo(406, logTransaccion.getTipoTransaccion(),null);
				if (mstablas != null) {
					logTransaccion.setTipoTransaccionDescripcion(mstablas.getDescripcion());
				} else {
					logTransaccion.setTipoTransaccionDescripcion(logTransaccion.getTipoTransaccion());
				}
			} else {
				logTransaccion.setTipoTransaccionDescripcion(descripcionTipoTransaccion);
			}
			logTransaccion.setFechaTransaccionFormateada(myFormat.format(logTransaccion.getFechaTransaccion()));
		}
		Context context = new Context();
		String nombreReporte = "reporteAuditoria" + formatNombreReporte.format(new Date()) + ".xls";
		context.putVar("fecha", myFormat.format(new Date()));
		context.putVar("fechaDesde", myFormat.format(fechaDesdeConsulta));
		context.putVar("fechaHasta", myFormat.format(fechaHastaConsulta));
		context.putVar("tipoTransacion", descripcionTipoTransaccion);
		context.putVar("logTransacciones", logTransacciones);
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("jxls-template/auditoria_template.xls");
		try {
			logger.info("Generando reporte de consulta de auditoria");
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			JxlsHelper.getInstance().processTemplate(is, os, context);
			httpServletResponse.setHeader("Content-Disposition", "attachment;filename=" + nombreReporte);
			httpServletResponse.getOutputStream().write(os.toByteArray());
			httpServletResponse.flushBuffer();
		} catch (Exception e) {
			logger.error("Error al crear reporte de auditoría" + e.getMessage());
		}
	}

	/***
	 * Funcion de validar si se posee datos para exportar
	 * 
	 * @return
	 * @param request
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/validarDatosExistente")
	public @ResponseBody boolean validarDatosExistente(
			@RequestParam(name = "tipoTransaccion", required = false) Integer tipoTransaccion,
			@RequestParam(name = "tabla", required = false) Integer tablaCodigo,
			@RequestParam(name = "campo", required = false) Integer campoCodigo,
			@RequestParam(name = "usuario", required = false) String usuario,
			@RequestParam(name = "fechaHasta", required = false) String fechaHasta,
			@RequestParam(name = "fechaDesde", required = false) String fechaDesde, HttpServletRequest request,
			HttpServletResponse httpServletResponse) {
		HashMap<String, Object> datosRetornados = buscarDatosAuditor(tipoTransaccion, tablaCodigo, campoCodigo, usuario, fechaHasta, fechaDesde);
		List<LogTransaccion> logTranscciones = (List<LogTransaccion>) datosRetornados.get("logTranscciones");
		return logTranscciones.isEmpty();
	}

	/***
	 * 
	 * @param tipoTransaccion
	 * @param tablaCodigo
	 * @param campoCodigo
	 * @param usuario
	 * @param fechaHasta
	 * @param fechaDesde
	 * @return
	 */
	private HashMap<String,Object> buscarDatosAuditor(Integer tipoTransaccion, Integer tablaCodigo, Integer campoCodigo,
			String usuario, String fechaHasta, String fechaDesde) {
		HashMap<String, Object> datosRetornar = new HashMap<String,Object>();
		// Dado que la tabla de tranccciones tengo son la descripcion reducida, y por
		// pantalla llegan los codigo de MSTABLAS
		// Busco todo los datos de esa tabla y codigo (esto para hacer solo una consulta a la BD), y luego los recorro para obtener el Indicado
		List<MsTablas> mstablasFiltros = serviceMstablas.findAllByTablaAndCodigo(459, tablaCodigo);
		String tabla = "";
		String campo = "";
		for (MsTablas mstabla : mstablasFiltros) {
			if (mstabla.getId().getSubCodigo().equals(0)) {
				tabla = mstabla.getDescripcionReducida();
			} else if (mstabla.getId().getSubCodigo().equals(campoCodigo)) {
				campo = mstabla.getDescripcionReducida();
				break;
			}
		}
		Date fechaDesdeConsulta;
		Date fechaHastaConsulta;
		if (!"".equals(tabla) && tabla != null) {
			tabla = tabla.toLowerCase();
		}
		if (!"".equals(campo) && campo != null) {
			campo = campo.toLowerCase();
		}
		if (!"".equals(usuario) && usuario != null) {
			usuario = usuario.toLowerCase();
		}
		String descripcionTipoTransaccion = "Todas";
		String descrReducidaTipoTransaccion = null;
		//Se busca la descripcion segun el tipo de transaccion
		if(tipoTransaccion != null && tipoTransaccion != 0) {
			MsTablas mstablasTipoTransaccion = serviceMstablas.findByTablaCodigoSubCodigo(406, tipoTransaccion, null);
			descrReducidaTipoTransaccion = mstablasTipoTransaccion.getDescripcionReducida().toLowerCase();
			descripcionTipoTransaccion = mstablasTipoTransaccion.getDescripcion();
		}
		SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(myFormat.parse(fechaDesde));
			fechaDesdeConsulta = calendar.getTime();
			calendar.setTime(myFormat.parse(fechaHasta));
			fechaHastaConsulta = calendar.getTime();
		} catch (ParseException e) {
			fechaDesdeConsulta = new Date();
			fechaHastaConsulta = new Date();
		}
		List<LogTransaccion> logTranscciones = service.findTransaccionesByFechas(descrReducidaTipoTransaccion, tabla, campo, usuario, fechaDesdeConsulta, fechaHastaConsulta);
		datosRetornar.put("logTranscciones", logTranscciones);
		datosRetornar.put("fechaDesdeConsulta", fechaDesdeConsulta);
		datosRetornar.put("fechaHastaConsulta", fechaHastaConsulta);
		datosRetornar.put("descripcionTipoTransaccion", descripcionTipoTransaccion);
		datosRetornar.put("descrReducidaTipoTransaccion", descrReducidaTipoTransaccion);
		return datosRetornar;
	}

}
