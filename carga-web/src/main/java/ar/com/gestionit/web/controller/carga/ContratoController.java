package ar.com.gestionit.web.controller.carga;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dao.carga.ClienteRepository;
import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.carga.Cliente;
import ar.com.gestionit.model.carga.Contrato;
import ar.com.gestionit.model.carga.MsTablas;
import ar.com.gestionit.service.carga.ContratoService;
import ar.com.gestionit.service.carga.MsTablasService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.editor.GenericEditor;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.ContratoValidator;

@Controller
@RequestMapping("/contrato")
public class ContratoController {
	private static Logger logger = LogManager.getLogger(ContratoController.class);

	
	@Autowired
	ContratoService service;
	
	@Autowired
	ContratoValidator validator;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	MsTablasService mstablasService;

	
	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("descripcionSession");
		ModelAndView modelAndView = new ModelAndView("carga/contrato/contrato_list");
		modelAndView.addObject("tituloPantalla", "contrato");
		modelAndView.addObject("tomarFiltrosSession", false);
		return modelAndView;
	}

	/***
	 * Funcion para retornar los datos sin limpiar los filtros de la session
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/filtros")
	public ModelAndView listByFiltros(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/contrato/contrato_list");
		modelAndView.addObject("tituloPantalla", "contrato");
		modelAndView.addObject("tomarFiltrosSession", true);
		return modelAndView;
	}	
	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @param nombre
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
//			@RequestParam(name = "sort", required = false, defaultValue = "cliente.razonSocial, nombre") String sort,
			@RequestParam(name = "sort", required = false, defaultValue = "cliente.razonSocial") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "clienteRazonSocial", required = false) String clienteRazonSocial,
			@RequestParam(name = "nombre", required = false) String nombre,
			@RequestParam(name = "mantenerfiltros", required = false) boolean mantenerfiltros,
			@RequestParam(name = "tomarFiltrosSession", required = false) boolean tomarFiltrosSession,
			HttpServletRequest request) {
		logger.info(Strings.scnt(limit, offset, sort, order));
		HttpSession session = request.getSession();
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		if(mantenerfiltros) {
			session.setAttribute("clienteRazonSocialSession", clienteRazonSocial);
			session.setAttribute("descripcionSession", nombre);
		}
		if(tomarFiltrosSession) {
			clienteRazonSocial = (String) SpringUtils.coalesce(clienteRazonSocial, session.getAttribute("clienteRazonSocial"));			
			nombre = (String) SpringUtils.coalesce(nombre, session.getAttribute("nombreSession"));			
		}
		Page<Contrato> result = null;
		
		if(("").equals(clienteRazonSocial)) {
			clienteRazonSocial = null;
		}
		
		if(("").equals(nombre)) {
			nombre = null;
		}
		clienteRazonSocial = clienteRazonSocial == null ? null : "%" + clienteRazonSocial.toUpperCase() + "%";
		nombre = nombre == null ? null : "%" + nombre.toUpperCase() + "%";
		
		result = service.findBySearch(pageRequest, clienteRazonSocial, nombre);

		// obtiene la descripción de contrato.T03Categoria
		MsTablas mstablas=null;
		int categoria=0;
		String descCategoria = "";
		for(Contrato contrato : result) {
			
			categoria = contrato.getT03Categoria();
			mstablas = mstablasService.findByTablaCodigoSubCodigo(3, categoria, null);
			descCategoria = categoria + " - " + mstablas.getDescripcion(); 

			contrato.setDescCategoria(descCategoria);
	}	

		
		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") Contrato entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/contrato/contrato_add");
	
		if (result.hasErrors()) {
			modelAndView.addObject("create", true);
			return modelAndView;
		}
		
		try {
			service.save(entity);
			return new ModelAndView("redirect:/contrato/");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un usuario");
			modelAndView.addObject("evtEjecutar", "error");
			modelAndView.addObject("create", true);
			return modelAndView;
		}
	}
	
	/***
	 * Funcion para devolver la pantalla de crear
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request) {
		Contrato entity = new Contrato();		
		ModelAndView modelAndView = new ModelAndView("carga/contrato/contrato_add");
		entity.setTituloPantalla("add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para devolver la pantalla de editar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, HttpServletRequest request) {
		Contrato entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/contrato/contrato_add");
		entity = entity == null ? new Contrato() : entity;
		entity.setTituloPantalla("edit");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para de volver la pantalla de eliminar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/remove/{id}")
	public ModelAndView remove(@PathVariable("id") Long id,HttpServletRequest request) {
		Contrato entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/contrato/contrato_add");
		entity = entity == null ? new Contrato() : entity;
		entity.setTituloPantalla("del");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de borrar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/eliminar/{id}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("id") Long id, HttpServletRequest request) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
			service.remove(id);			
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}
	
	/***
	 * Funcion para de volver la pantalla de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{id}")
	public ModelAndView show(@PathVariable("id") Long id, HttpServletRequest request) {
		Contrato entity = service.findById(id);
		entity = entity == null ? new Contrato() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/contrato/contrato_add");
		entity.setTituloPantalla("show");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}
	
	/**
	 * Busqueda del contrato con los datos del Cliente relacionado
	 * @return
	 */
	@RequestMapping(value = "/findClienteContrato")
	public @ResponseBody Object getByClienteContrato(@RequestParam(required = false) String search) {
		String criteria = null;		
		if (!StringUtils.isEmpty(search)) {
			criteria = Strings.wrap(search.toLowerCase(), "%");	
		}		
		
		
		List<Contrato> result = null;
		result = service.findClienteContrato(criteria);

		// obtiene la descripción de contrato.T03Categoria
		MsTablas mstablas=null;
		int categoria=0;
		String descCategoria = "";
		for(Contrato contrato : result) {
			
			categoria = contrato.getT03Categoria();
			mstablas = mstablasService.findByTablaCodigoSubCodigo(3, categoria, null);
			descCategoria = mstablas.getDescripcion(); 

			contrato.setDescCategoria(descCategoria);
	}	
		
		return result;	
	}
	
	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(Cliente.class, "cliente", new GenericEditor(clienteRepository));

	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public Contrato getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new Contrato();
		} else {
			return service.findById(id);
		}
	}


}