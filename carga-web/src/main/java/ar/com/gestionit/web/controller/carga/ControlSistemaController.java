package ar.com.gestionit.web.controller.carga;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.model.carga.ControlSistema;
import ar.com.gestionit.service.carga.ControlSistemaService;
import ar.com.gestionit.web.validator.carga.ControlSistemaValidator;

@Controller
@RequestMapping("/controlSistema")
public class ControlSistemaController {
	private static Logger logger = LogManager.getLogger(ControlSistemaController.class);
	
	@Autowired
	ControlSistemaService service;
	
	@Autowired
	ControlSistemaValidator validator;
	
	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView productoList(HttpServletRequest request) {
		ControlSistema entity = service.findFirstByOrderById();
		ModelAndView modelAndView = new ModelAndView("carga/controlSistema/controlSistema_add");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") ControlSistema entity, BindingResult result,
			HttpServletRequest request) {
		//
		ModelAndView modelAndView = new ModelAndView("carga/controlSistema/controlSistema_add");
		if (result.hasErrors()) {
			modelAndView.addObject("error", true);
			return modelAndView;
		}
		//
		try {
			if(entity.getId() != null) {
				entity.setFechaUltimaActualizacion(new Date());
			}
			service.save(entity);
			return new ModelAndView("redirect:/controlSistema");	
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta una bonificacion");
			modelAndView.addObject("error", true);
			return modelAndView;
		}
	}
	
	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public ControlSistema getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new ControlSistema();
		} else {
			return service.findFirstByOrderById();
		}
	}
}
