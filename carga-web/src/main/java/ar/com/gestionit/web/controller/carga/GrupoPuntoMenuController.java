package ar.com.gestionit.web.controller.carga;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ar.com.gestionit.dao.seguridad.GrupoRepository;
import ar.com.gestionit.model.seguridad.Grupo;
import ar.com.gestionit.model.seguridad.GrupoPuntoMenu;
import ar.com.gestionit.model.seguridad.PuntoMenuCategoria;
import ar.com.gestionit.model.seguridad.PuntoMenuDetalle;
import ar.com.gestionit.service.seguridad.GrupoPuntoMenuService;
import ar.com.gestionit.service.seguridad.GrupoService;
import ar.com.gestionit.service.seguridad.PuntoMenuService;
import ar.com.gestionit.web.dto.carga.GruposPuntoMenuDto;
import ar.com.gestionit.web.editor.GenericEditor;
import ar.com.gestionit.web.validator.carga.GrupoPuntoMenuValidator;

@Controller
@RequestMapping("/grupoPuntoMenu")
public class GrupoPuntoMenuController {

	private static Logger logger = LogManager.getLogger(GrupoPuntoMenuController.class);
	
	private static final Gson GSON = new Gson();
	
	@Autowired
	GrupoPuntoMenuService service;
	
	@Autowired
	GrupoService serviceGrupos;
	
	@Autowired
	GrupoPuntoMenuValidator validator;

	@Autowired
	GrupoRepository grupoRepository;
	
	@Autowired
	PuntoMenuService servicePuntoMenu;
	
	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		GruposPuntoMenuDto entity = new GruposPuntoMenuDto();
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/gruposPuntoMenu_add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("mostrarBackUrl", false);
		return modelAndView;
	}
	/***
	 * Función para visualizar los puntos de menú de un grupo
	 * 
	 * @return
	 * @param request
	 */
	
	@RequestMapping("/showByGroup/{id}")
	public ModelAndView showByEvento(@PathVariable("id") Long id, HttpServletRequest request) {
		GruposPuntoMenuDto entity = new GruposPuntoMenuDto();
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/gruposPuntoMenu_add");
		entity.setGrupo(serviceGrupos.findById(id));	
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("mostrarBackUrl", true);
		return modelAndView;
	}

	
	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") GruposPuntoMenuDto entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/gruposPuntoMenu_add");
		if (result.hasErrors()) {
			modelAndView.addObject("evtEjecutar", "error");
			//guardo los datos en el model 
			return modelAndView;
		}
		try {
			persistirPuntosMenu(entity);
			return new ModelAndView("redirect:/grupoPuntoMenu");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.err.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un Grupo Punto de menu");
			modelAndView.addObject("evtEjecutar", "error");
			return modelAndView;
		}
	}
	
	/**
	 * Funcion para guardar los puntos de menu referenciados a un Grupo
	 * @param entity
	 */
	private void persistirPuntosMenu(GruposPuntoMenuDto entity) {
		// Se transforma el json recibido del formulario a una lista de tipo
		// PasoDTO
		String obj = entity.getCamposDinamicos();
		Type type = new TypeToken<ArrayList<GruposPuntoMenuDto>>() {}.getType();
		List<GruposPuntoMenuDto> lstDto = GSON.fromJson(obj, type);
        Long puntoMenu = null;
		for (GruposPuntoMenuDto dto : lstDto) {
			GrupoPuntoMenu grupoPuntoMenu= new GrupoPuntoMenu();
			if(dto.isBorrarRegistro()) {
				service.remove(dto.getId());
			}
			else {
				if(puntoMenu == null || !dto.getPuntoMenu().equals(puntoMenu)) {
					puntoMenu = dto.getPuntoMenu();
					//Le agrego el id para que realice el update en los registros previamente existentes
					grupoPuntoMenu.setId(dto.getId());
					grupoPuntoMenu.setGrupo(entity.getGrupo());			
					grupoPuntoMenu.setPuntoMenuDetalle(servicePuntoMenu.findDetalleById(dto.getPuntoMenu()));
					grupoPuntoMenu.setNombreCategoriaAlternativo(dto.getCabeceraNomAlternativo());
					grupoPuntoMenu.setNombrePuntoMenuAlternativo(dto.getPuntoMenuNomAlternativo());				
					service.save(grupoPuntoMenu);	
				}
			}	
		}
	}

	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Grupo.class, "grupo", new GenericEditor(grupoRepository));
	}

	/***
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object getById(@RequestParam(name="grupo", required = true) Long id) {
		List<GruposPuntoMenuDto> lstDto = new ArrayList<>();
		List<GrupoPuntoMenu> lst = service.findAllGrupoPuntoMenuByGrupoId(id);
		for(GrupoPuntoMenu grupoPuntoMenu : lst) {
			GruposPuntoMenuDto dto = new GruposPuntoMenuDto();
			PuntoMenuDetalle puntoMenuDetalle = grupoPuntoMenu.getPuntoMenuDetalle();
			PuntoMenuCategoria categoriaPuntoMenuId = puntoMenuDetalle.getCategoriaPuntoMenuId();
			dto.setCabeceraNomAlternativo(grupoPuntoMenu.getNombreCategoriaAlternativo());
			dto.setPuntoMenuNomAlternativo(grupoPuntoMenu.getNombrePuntoMenuAlternativo());
			dto.setPuntoMenuCategoria(categoriaPuntoMenuId.getId());
			dto.setPuntoMenu(puntoMenuDetalle.getId());
			dto.setId(grupoPuntoMenu.getId());
			dto.setOrden(categoriaPuntoMenuId.getOrden());
			dto.setOrdenDetalle(puntoMenuDetalle.getOrden());
			lstDto.add(dto);
			
		}
	    return GSON.toJson(lstDto);
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getPuntoMenu")
	public ModelAndView getPuntoMenu(HttpServletRequest request) {
		List<PuntoMenuCategoria> lst = new ArrayList<>();
		try {
			//armo la lista de grupos para ir a buscar los puntos de menu
			Collection<String> lstGrupos = CollectionUtils.collect(SecurityContextHolder.getContext().getAuthentication().getAuthorities(), new BeanToPropertyValueTransformer("authority"));
			HashMap<String,Object> mapaObj = new HashMap<String, Object>();
			//busco los puntos de menu para asignar al usuario
			mapaObj = service.findAllGrupoPuntoMenuByNombreGrupoList(lstGrupos);
			lst = (List<PuntoMenuCategoria>) mapaObj.get("lista");
			String strPuntoMenuNombres = GSON.toJson(mapaObj.get("puntoMenuNombres"));
			request.getSession().setAttribute("puntoMenuNombres", strPuntoMenuNombres);
		}
		catch(Exception e) {
			System.err.println(e.getMessage());
		}	 
		ModelAndView modelAndView = new ModelAndView("carga/menu/menu");
		modelAndView.addObject("lst", lst);
		return modelAndView;
	}
	
}
