package ar.com.gestionit.web.controller.carga;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.carga.MsTablas;
import ar.com.gestionit.model.carga.MsTablasPK;
import ar.com.gestionit.service.carga.MsTablasService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.MsTablasValidator;

@Controller
@RequestMapping("/mstablas")
public class MstablasController {

	private static Logger logger = LogManager.getLogger(MstablasController.class);
	
	@Autowired
	MsTablasService service;
	
	@Autowired
	MsTablasValidator validator;
	
	@RequestMapping("")
	public String list() {
		return "carga/mstablas/mstablas_list";
	}
	
	@RequestMapping("/check")
	public String check() {
		return "carga/mstablas/checkboxes";
	}

	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
			@RequestParam(name = "sort", required = false, defaultValue = "id.tabla") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "nombre", required = false) String search) {
		logger.info(Strings.scnt(limit, offset, sort, order));

		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		Page<MsTablas> result = null;
		result = service.findBySearch(pageRequest);
		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String adding(@Validated @ModelAttribute("entity") MsTablas entity, BindingResult result, Model omodel ) {
		//result ya posee los errores cargados
		if (result.hasErrors()) {
			return "carga/mstablas/mstablas_add";
		}
		try {
			service.save(entity);
			return "redirect:/mstablas/";
		} catch (Exception e) {
			logger.error(e.getMessage());
			//Al asignar null aplica el error como cabecera
			result.rejectValue(null, "Error al generar un registro");
			return "carga/mstablas/mstablas_add";
		}
	}

	@RequestMapping("/edit/{tabla}/{codigo}/{subcodigo}")
	public ModelAndView edit(@PathVariable("tabla") Integer tabla, @PathVariable("codigo") Integer codigo, @PathVariable("subcodigo") Integer subcodigo ) {
		MsTablasPK id = new MsTablasPK(tabla, codigo, subcodigo);
		MsTablas entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/mstablas/mstablas_add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);
		return modelAndView;
	}

	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("carga/mstablas/mstablas_add");
		MsTablas entity = new MsTablas();
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}

	@RequestMapping(value = "/remove/{tabla}/{codigo}/{subcodigo}")
	public @ResponseBody ModelAndView remove(@PathVariable("tabla") Integer tabla, @PathVariable("codigo") Integer codigo, @PathVariable("subcodigo") Integer subcodigo ) {
		MsTablasPK id = new MsTablasPK(tabla, codigo, subcodigo);
		MsTablas entity = service.findById(id);
		entity = entity == null ? new MsTablas() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/mstablas/mstablas_add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}
	
	

	/***
	 * Funcion para retornar vista de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{tabla}/{codigo}/{subcodigo}")
	public ModelAndView show(@PathVariable("tabla") Integer tabla, @PathVariable("codigo") Integer codigo, @PathVariable("subcodigo") Integer subcodigo ) {
		MsTablasPK id = new MsTablasPK(tabla, codigo, subcodigo);
		MsTablas entity = service.findById(id);
		entity = entity == null ? new MsTablas() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/mstablas/mstablas_add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", false);
		
		return modelAndView;
	}
	
	/**
	 * Funcion para eliminar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/eliminar/{tabla}/{codigo}/{subcodigo}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("tabla") Integer tabla, @PathVariable("codigo") Integer codigo, @PathVariable("subcodigo") Integer subcodigo ) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
			MsTablasPK id = new MsTablasPK(tabla, codigo, subcodigo);
			service.remove(id);
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}
	
	@RequestMapping("/drag_drop")
	public ModelAndView dragAndDrop() {
		ModelAndView modelAndView = new ModelAndView("carga/mstablas/drag_drop");
		MsTablas entity = new MsTablas();
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}	

	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public MsTablas getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new MsTablas();
		} else {
			return service.findById(id);
		}
	}
	
	@RequestMapping(value = "/finByTablaCodigoSubCodigo")
	public @ResponseBody PageResult finByTablaCodigoSubCodigo(@RequestParam(required = true) String tablaSubCodigo, 
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page, 
			@RequestParam(required = false) String search) {
		String[] split = tablaSubCodigo.split(",");
		Integer tabla = Integer.parseInt(split[0]);
		Integer codigo = split[1].equals("null") ? null : Integer.parseInt(split[1]);
		Integer subCodigo = split[2].equals("null") ? null : Integer.parseInt(split[2]);
		boolean notIn = false;
		//Se valida que se haya indicado el tipo de query
		if(split.length > 3 && split[3] != null) {
			notIn = Boolean.parseBoolean(split[3]);
		}
		if(search != null) {
			search = '%' + search.toUpperCase() + '%';
		}
		logger.info(Strings.scnt("Buscando datos para la tabla " + tabla ,"SubCodigo " + subCodigo  + "codigo ", codigo));
		logger.info(Strings.scnt("Criterio de busqueda " + search));
		// Obtengo el repository a partir del nombre de la entidad
		PageRequest pr = SpringUtils.getPageRequestByPage(10, page);
		Page<?> result = null;
		if(notIn) {
			result = service.pageByTablaCodigoNotSubCodigo(pr,tabla, codigo, subCodigo, search);
		}else {
			result = service.pageByTablaCodigoSubCodigo(pr,tabla, codigo, subCodigo, search);
		}
		return new PageResult(result.getContent(), result.getTotalElements());
	}
	
	@RequestMapping(value = "/finByTablaCodigoSubCodigoId")
	public @ResponseBody Object getById(@RequestParam(required = true) String tablaSubCodigo) {
		String id = tablaSubCodigo;
		logger.info(Strings.scnt("Criterio de busqueda " + id));
		String[] split = id.split(",");
		Integer tabla = Integer.parseInt(split[0]);
		Integer codigo = split[1].equals("null") ? null : Integer.parseInt(split[1]);
		Integer subCodigo = split[2].equals("null") ? null : Integer.parseInt(split[2]);
		return service.findByTablaCodigoSubCodigo(tabla, codigo, subCodigo);	
	}

	@RequestMapping(value = "/finByTablaCodigoSubCodigoDescripcion")
	public @ResponseBody Object getByDescripcion(@RequestParam(required = true) String tablaSubCodigo) {
		String id = tablaSubCodigo;
		logger.info(Strings.scnt("Criterio de busqueda " + id));
		String[] split = id.split(",");
		Integer tabla = Integer.parseInt(split[0]);
		String descripcion = split[1];
		Integer subCodigo = split[2].equals("null") ? null : Integer.parseInt(split[2]);
		return service.findByTablaDescripcionSubCodigo(tabla, descripcion, subCodigo);	
	}
	
	
	
}
