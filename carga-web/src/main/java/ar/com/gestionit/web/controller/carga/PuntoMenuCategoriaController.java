package ar.com.gestionit.web.controller.carga;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.seguridad.PuntoMenuCategoria;
import ar.com.gestionit.model.seguridad.PuntoMenuDetalle;
import ar.com.gestionit.service.seguridad.PuntoMenuService;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.PuntoMenuCategoriaValidator;

@Controller
@RequestMapping("/puntoMenuCategoria")
public class PuntoMenuCategoriaController {

	private static Logger logger = LogManager.getLogger(PuntoMenuCategoriaController.class);

	@Autowired
	PuntoMenuService service;
	
	@Autowired
	PuntoMenuCategoriaValidator validator;

	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/categoria_list");
		return modelAndView;
	}

	/***
	 * Funcion para retornar los datos sin limpiar los filtros de la session
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/filtros")
	public ModelAndView listByFiltros(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/categoria_list");
		return modelAndView;
	}	
	
	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @param nombre
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
			@RequestParam(name = "sort", required = false, defaultValue = "orden") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "catPuntoMenu", required = false) Long categoriaId,
			HttpServletRequest request) {
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		Page<PuntoMenuCategoria> result = null;
		result = service.findByCategoria(pageRequest, categoriaId);
		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") PuntoMenuCategoria entity, BindingResult result,
			HttpServletRequest request) {
		if (result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/categoria_add");
			modelAndView.addObject("create", true);
			return modelAndView;
		}
		try {
			service.save(entity);
			return new ModelAndView("redirect:/puntoMenuCategoria");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un Punto de Menu");
			ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/categoria_add");			
			return modelAndView;
		}
	}

	/***
	 * Funcion para de volver la pantalla de crear
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request) {
		PuntoMenuCategoria entity = new PuntoMenuCategoria();		
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/categoria_add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para de volver la pantalla de editar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, HttpServletRequest request) {
		PuntoMenuCategoria entity = service.findCategoriaById(id);
		entity = entity == null ? new PuntoMenuCategoria() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/categoria_add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);		
		return modelAndView;
	}
	
	/***
	 * Funcion para de volver la pantalla de eliminar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/remove/{id}")
	public ModelAndView remove(@PathVariable("id") Long id,HttpServletRequest request) {
		PuntoMenuCategoria entity = service.findCategoriaById(id);
		entity = entity == null ? new PuntoMenuCategoria() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/categoria_add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de borrar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/eliminar/{id}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("id") Long id, HttpServletRequest request) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
			service.remove(id);
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}
	
	/***
	 * Funcion para de volver la pantalla de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{id}")
	public ModelAndView show(@PathVariable("id") Long id, HttpServletRequest request) {
		PuntoMenuCategoria entity = service.findCategoriaById(id);
		entity = entity == null ? new PuntoMenuCategoria() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/categoria_add");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}
	

	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//define que el atributo es del tipo set utilizando el repositorio del objeto atributo 
		//Para castear un id a un objeto determinado = eventoEstado (es id) utiliza eventoEstadoRepository y lo castea a EventoEstado
		binder.setValidator(validator);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public PuntoMenuCategoria getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new PuntoMenuCategoria();
		} else {
			return service.findCategoriaById(id);
		}
	}
	
	
	/***
	 * Funcion de validar si la categoria a borrar tiene productos asociados
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/validarCategoriaPuntoMenu")
	public @ResponseBody boolean validarCategoriaProducto(@RequestParam(required = false) Long categoriaId, HttpServletRequest request) {
		List<PuntoMenuDetalle> puntoMenuDetalle = service.findAllPuntoMenuByCategoriaList(categoriaId);
		return puntoMenuDetalle.isEmpty();
	}

}
