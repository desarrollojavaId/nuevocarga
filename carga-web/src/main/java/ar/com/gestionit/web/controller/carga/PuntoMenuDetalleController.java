package ar.com.gestionit.web.controller.carga;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ar.com.gestionit.dao.seguridad.PuntoMenuRepository;
import ar.com.gestionit.model.seguridad.PuntoMenuCategoria;
import ar.com.gestionit.model.seguridad.PuntoMenuDetalle;
import ar.com.gestionit.service.seguridad.PuntoMenuService;
import ar.com.gestionit.service.seguridad.PuntoMenuUrlService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.dto.carga.PuntoMenuDto;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.PuntoMenuDetalleValidator;

@Controller
@RequestMapping("/puntoMenuDetalle")
public class PuntoMenuDetalleController {

	private static Logger logger = LogManager.getLogger(PuntoMenuDetalleController.class);
	private static final Gson GSON = new Gson();
	@Autowired
	PuntoMenuService service;
	
	@Autowired
	PuntoMenuUrlService serviceUrl;
	
	@Autowired
	PuntoMenuDetalleValidator validator;
	
	@Autowired
	PuntoMenuRepository puntoMenuRepository;
	
	/***
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/showPuntoMenu/{categoriaId}")
	public ModelAndView showPuntoMenu(@PathVariable(name="categoriaId", required = true) Long id) {
		List<PuntoMenuDetalle> result = service.findAllPuntoMenuByCategoriaList(id);
		List<PuntoMenuDto> lstDto = new ArrayList<PuntoMenuDto>();
		PuntoMenuDto puntoMenuDto = new PuntoMenuDto();
		PuntoMenuCategoria categoria = service.findCategoriaById(id);
		
		for(PuntoMenuDetalle detalle: result) {
			PuntoMenuDto dto = new PuntoMenuDto();
			dto.setOrden(detalle.getOrden());
			dto.setDescripcionPuntoMenu(detalle.getDescripcionPuntoMenu());
			dto.setId(detalle.getId());
			if(detalle.getPuntoMenuUrl()!=null) {
				dto.setPuntoMenuUrlId(detalle.getPuntoMenuUrl().getId());
			}
			lstDto.add(dto);
		}
		String json = GSON.toJson(lstDto);
		
		puntoMenuDto.setCamposDinamicos(json);
		puntoMenuDto.setPuntoMenuCategoria(id);		
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/puntoMenu_list");	
		modelAndView.addObject("entity", puntoMenuDto);
		modelAndView.addObject("categoria", categoria.getDescIdPuntoMenu());
		return modelAndView;
	}
	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @param nombre
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
			@RequestParam(name = "sort", required = false, defaultValue = "orden") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "categoriaPuntoMenuId", required = false) Long categoriaId,
			@RequestParam(name = "catPuntoMenuDetalle", required = false) Long detalleId,
			HttpServletRequest request) {
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		Page<PuntoMenuDetalle> result = null;
		
		Long idCategoria = (Long) SpringUtils.coalesce(categoriaId, request.getSession().getAttribute("puntoMenuCategoriaId"));
		
		result = service.findAllPuntoMenuByCategoriaDetalle(pageRequest, idCategoria, detalleId);

		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}
	
	
	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") PuntoMenuDto entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/categoriaPuntoMenu/puntoMenu_list");
		if (result.hasErrors()) {
			modelAndView.addObject("evtEjecutar", "error");
			//guardo los datos en el model 
			return modelAndView;
		}
		try {
			persistirPuntosMenu(entity);
			return new ModelAndView("redirect:/puntoMenuDetalle/showPuntoMenu/" + entity.getPuntoMenuCategoria());
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.err.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un Punto de menu");
			modelAndView.addObject("evtEjecutar", "error");
			return modelAndView;
		}
	}
	
	/**
	 * Funcion para guardar los puntos de menu referenciados a un Grupo
	 * @param entity
	 */
	private void persistirPuntosMenu(PuntoMenuDto entity) {
		
		// Se transforma el json recibido del formulario a una lista de tipo
		// PasoDTO
		String obj = entity.getCamposDinamicos();
		Type type = new TypeToken<ArrayList<PuntoMenuDto>>() {}.getType();
		List<PuntoMenuDto> lstDto = GSON.fromJson(obj, type);
		PuntoMenuCategoria categoria = service.findCategoriaById(entity.getPuntoMenuCategoria());
		for (PuntoMenuDto dto : lstDto) {
			PuntoMenuDetalle puntoMenuDto= new PuntoMenuDetalle();			
			if(dto.isBorrarRegistro()) {
				service.remove(dto.getId());
			}
			else {
				//Le agrego el id para que realice el update en los registros previamente existentes
				puntoMenuDto.setId(dto.getId());
				puntoMenuDto.setDescripcionPuntoMenu(dto.getDescripcionPuntoMenu());
				puntoMenuDto.setOrden(dto.getOrden());
				puntoMenuDto.setCategoriaPuntoMenuId(categoria);
				puntoMenuDto.setPuntoMenuUrl(serviceUrl.findById(dto.getPuntoMenuUrlId()));
				service.save(puntoMenuDto);	
			}	
			
		}
		
	}
	
	/****
	 * 
	 * @param entity
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/get")
	public @ResponseBody Object getById(@RequestParam(required = true) String id) {
		logger.info(Strings.scnt("Criterio de busqueda " + id));
		if(!"".equals(id) && id != null) {
			return service.findDetalleById(new Long(id));	
		}
		return id;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public PuntoMenuDto getEntity(@RequestParam(required = false) Long id) {
		return new PuntoMenuDto();
	}


}
