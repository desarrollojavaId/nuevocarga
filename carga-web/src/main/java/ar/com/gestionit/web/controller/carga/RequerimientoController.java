package ar.com.gestionit.web.controller.carga;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dao.carga.ContratoRepository;
import ar.com.gestionit.dao.carga.SectorRepository;
import ar.com.gestionit.dao.carga.UsuClienteRepository;
import ar.com.gestionit.dao.carga.UsuEmpleadoRepository;
import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.carga.Contrato;
import ar.com.gestionit.model.carga.MsTablas;
import ar.com.gestionit.model.carga.Requerimiento;
import ar.com.gestionit.model.carga.Sector;
import ar.com.gestionit.model.carga.UsuCliente;
import ar.com.gestionit.model.carga.UsuEmpleado;
import ar.com.gestionit.service.carga.MsTablasService;
import ar.com.gestionit.service.carga.RequerimientoService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.editor.GenericEditor;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.RequerimientoValidator;

@Controller
@RequestMapping("/requerimiento")
public class RequerimientoController {
	private static Logger logger = LogManager.getLogger(RequerimientoController.class);

	@Autowired
	RequerimientoService service;
	
	@Autowired
	RequerimientoValidator validator;
	
	@Autowired
	UsuEmpleadoRepository usuEmpleadoRepository;

	// Se elimina esta relación porque la relación natural es cliente --> contrato --> requerimiento 
	//@Autowired
	//ClienteRepository clienteRepository;

	@Autowired
	SectorRepository sectorRepository;

	@Autowired
	ContratoRepository contratoRepository;

	@Autowired
	UsuClienteRepository usuClienteRepository;
	
	@Autowired
	MsTablasService mstablasService;

	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("nussSession");
		session.removeAttribute("tituloSession");
		ModelAndView modelAndView = new ModelAndView("carga/requerimiento/requerimiento_list");
		modelAndView.addObject("tituloPantalla", "requerimiento");
		modelAndView.addObject("tomarFiltrosSession", false);
		return modelAndView;
	}

	/***
	 * Funcion para retornar los datos sin limpiar los filtros de la session
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/filtros")
	public ModelAndView listByFiltros(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/requerimiento/requerimiento_list");
		modelAndView.addObject("tituloPantalla", "requerimiento");
		modelAndView.addObject("tomarFiltrosSession", true);
		return modelAndView;
	}	
	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
			@RequestParam(name = "sort", required = false, defaultValue = "nuss") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "nuss", required = false) Long nuss,
			@RequestParam(name = "titulo", required = false) String titulo,
			@RequestParam(name = "mantenerfiltros", required = false) boolean mantenerfiltros,
			@RequestParam(name = "tomarFiltrosSession", required = false) boolean tomarFiltrosSession,
			HttpServletRequest request) {
		logger.info(Strings.scnt(limit, offset, sort, order));
		HttpSession session = request.getSession();
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		if(mantenerfiltros) {
			session.setAttribute("nussSession", nuss);
			session.setAttribute("tituloSession", titulo);
		}
		if(tomarFiltrosSession) {
			nuss = (Long) SpringUtils.coalesce(nuss, session.getAttribute("nuss"));
			titulo = (String) SpringUtils.coalesce(titulo, session.getAttribute("titulo"));
		}
		Page<Requerimiento> result = null;
		
		if(("").equals(titulo)) {
			titulo = null;
		}

		titulo = titulo == null ? null : "%" + titulo.toUpperCase() + "%";
		
		result = service.findBySearch(pageRequest, nuss, titulo);

		// obtiene la descripción de requerimiento.t04Estado
		MsTablas mstablas=null;
		int estado=0;
		String descEstado = "";
		for(Requerimiento requerimiento : result) {
			
			estado = requerimiento.getT04Estado();
			mstablas = mstablasService.findByTablaCodigoSubCodigo(4, estado, null);
			descEstado = estado + " - " + mstablas.getDescripcion(); 

			requerimiento.setDescEstado(descEstado);
	}	
		
		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") Requerimiento entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/requerimiento/requerimiento_add");
		
		if (result.hasErrors()) {
			modelAndView.addObject("create", true);
			return modelAndView;
		}
		try {
			service.save(entity);
			return new ModelAndView("redirect:/requerimiento/");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un requerimiento");
			modelAndView.addObject("evtEjecutar", "error");
			modelAndView.addObject("create", true);
			return modelAndView;
		}
	}
	
	/***
	 * Funcion para devolver la pantalla de crear
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request) {
		Requerimiento entity = new Requerimiento();		
		ModelAndView modelAndView = new ModelAndView("carga/requerimiento/requerimiento_add");
		entity.setTituloPantalla("add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para devolver la pantalla de editar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, HttpServletRequest request) {
		Requerimiento entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/requerimiento/requerimiento_add");
		entity = entity == null ? new Requerimiento() : entity;
		entity.setTituloPantalla("edit");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para de volver la pantalla de eliminar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/remove/{id}")
	public ModelAndView remove(@PathVariable("id") Long id,HttpServletRequest request) {
		Requerimiento entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/requerimiento/requerimiento_add");
		entity = entity == null ? new Requerimiento() : entity;
		entity.setTituloPantalla("del");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de borrar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/eliminar/{id}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("id") Long id, HttpServletRequest request) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
			service.remove(id);			
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}
	
	/***
	 * Funcion para de volver la pantalla de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{id}")
	public ModelAndView show(@PathVariable("id") Long id, HttpServletRequest request) {
		Requerimiento entity = service.findById(id);
		entity = entity == null ? new Requerimiento() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/requerimiento/requerimiento_add");
		entity.setTituloPantalla("show");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}
	
	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(UsuEmpleado.class, "usuEmpLider", new GenericEditor(usuEmpleadoRepository));
		//binder.registerCustomEditor(Cliente.class, "cliente", new GenericEditor(clienteRepository));
		binder.registerCustomEditor(Sector.class, "sector", new GenericEditor(sectorRepository));
		binder.registerCustomEditor(Contrato.class, "contrato", new GenericEditor(contratoRepository));
		binder.registerCustomEditor(UsuCliente.class, "usuCliente", new GenericEditor(usuClienteRepository));
		
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public Requerimiento getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new Requerimiento();
		} else {
			return service.findById(id);
		}
	}


}
