package ar.com.gestionit.web.controller.carga;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dao.carga.AreaRepository;
import ar.com.gestionit.dao.carga.UsuEmpleadoRepository;
import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.carga.Area;
import ar.com.gestionit.model.carga.Sector;
import ar.com.gestionit.model.carga.UsuEmpleado;
import ar.com.gestionit.service.carga.SectorService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.editor.GenericEditor;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.SectorValidator;

@Controller
@RequestMapping("/sector")
public class SectorController {
	private static Logger logger = LogManager.getLogger(SectorController.class);

	@Autowired
	SectorService service;
	
	@Autowired
	SectorValidator validator;
	
	@Autowired
	AreaRepository areaRepository;
	
	@Autowired
	UsuEmpleadoRepository usuEmpleadoRepository;

	
	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		HttpSession session = request.getSession();
		ModelAndView modelAndView = new ModelAndView("carga/sector/sector_list");
		session.removeAttribute("descripcionSession");
		modelAndView.addObject("tituloPantalla", "sector");
		modelAndView.addObject("tomarFiltrosSession", false);
		return modelAndView;
	}

	/***
	 * Funcion para retornar los datos sin limpiar los filtros de la session
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/filtros")
	public ModelAndView listByFiltros(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/sector/sector_list");
		modelAndView.addObject("tituloPantalla", "sector");
		modelAndView.addObject("tomarFiltrosSession", true);
		return modelAndView;
	}	
	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @param nombre
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
//			@RequestParam(name = "sort", required = false, defaultValue = "area.descripcion, descripcion") String sort,
			@RequestParam(name = "sort", required = false, defaultValue = "area.descripcion") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "areaDescripcion", required = false) String areaDescripcion,
			@RequestParam(name = "sectorDescripcion", required = false) String sectorDescripcion,
			@RequestParam(name = "mantenerfiltros", required = false) boolean mantenerfiltros,
			@RequestParam(name = "tomarFiltrosSession", required = false) boolean tomarFiltrosSession,
			HttpServletRequest request) {
		logger.info(Strings.scnt(limit, offset, sort, order));
		HttpSession session = request.getSession();
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		if(mantenerfiltros) {
			session.setAttribute("areaDescripcionSession", areaDescripcion);
			session.setAttribute("sectorDescripcionSession", sectorDescripcion);
		}
		if(tomarFiltrosSession) {
			areaDescripcion = (String) SpringUtils.coalesce(areaDescripcion, session.getAttribute("areaDescripcion"));
			sectorDescripcion = (String) SpringUtils.coalesce(sectorDescripcion, session.getAttribute("sectorDescripcion"));
		}
		Page<Sector> result = null;
		
		if(("").equals(areaDescripcion)) {
			areaDescripcion = null;
		}
		
		if(("").equals(sectorDescripcion)) {
			sectorDescripcion = null;
		}

		areaDescripcion = areaDescripcion == null ? null : "%" + areaDescripcion.toUpperCase() + "%";
		sectorDescripcion = sectorDescripcion == null ? null : "%" + sectorDescripcion.toUpperCase() + "%";
		
		result = service.findBySearch(pageRequest, areaDescripcion, sectorDescripcion);
		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") Sector entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/sector/sector_add");
		
		if (result.hasErrors()) {
			modelAndView.addObject("create", true);
			return modelAndView;
		}
		try {
			service.save(entity);
			return new ModelAndView("redirect:/sector/");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un sector");
			modelAndView.addObject("evtEjecutar", "error");
			modelAndView.addObject("create", true);
			return modelAndView;
		}
	}
	
	/***
	 * Funcion para devolver la pantalla de crear
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request) {
		Sector entity = new Sector();		
		ModelAndView modelAndView = new ModelAndView("carga/sector/sector_add");
		entity.setTituloPantalla("add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para devolver la pantalla de editar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, HttpServletRequest request) {
		Sector entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/sector/sector_add");
		entity = entity == null ? new Sector() : entity;
		entity.setTituloPantalla("edit");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para de volver la pantalla de eliminar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/remove/{id}")
	public ModelAndView remove(@PathVariable("id") Long id,HttpServletRequest request) {
		Sector entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/sector/sector_add");
		entity = entity == null ? new Sector() : entity;
		entity.setTituloPantalla("del");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de borrar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/eliminar/{id}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("id") Long id, HttpServletRequest request) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
			service.remove(id);			
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}
	
	/***
	 * Funcion para de volver la pantalla de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{id}")
	public ModelAndView show(@PathVariable("id") Long id, HttpServletRequest request) {
		Sector entity = service.findById(id);
		entity = entity == null ? new Sector() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/sector/sector_add");
		entity.setTituloPantalla("show");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}
	
/*	@RequestMapping(value = "/comboPaxed/get")
	public @ResponseBody Object getById(@RequestParam(name="nombreUsuario", required = false) String nombreUsuario) {
		logger.info(Strings.scnt("Criterio de busqueda " + nombreUsuario));
		return service.findByNumeroUsuario(nombreUsuario);
	}
*/
	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(Area.class, "area", new GenericEditor(areaRepository));
		binder.registerCustomEditor(UsuEmpleado.class, "usuEmpLider", new GenericEditor(usuEmpleadoRepository));
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public Sector getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new Sector();
		} else {
			return service.findById(id);
		}
	}


}