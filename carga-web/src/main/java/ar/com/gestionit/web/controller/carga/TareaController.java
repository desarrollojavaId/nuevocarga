package ar.com.gestionit.web.controller.carga;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dao.carga.AreaRepository;
import ar.com.gestionit.dao.carga.RequerimientoRepository;
import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.carga.Area;
import ar.com.gestionit.model.carga.Requerimiento;
import ar.com.gestionit.model.carga.Tarea;
import ar.com.gestionit.service.carga.ContratoService;
import ar.com.gestionit.service.carga.MsTablasService;
import ar.com.gestionit.service.carga.RequerimientoService;
import ar.com.gestionit.service.carga.TareaService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.editor.GenericEditor;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.TareaValidator;

@Controller
@RequestMapping("/tarea")
public class TareaController {
	private static Logger logger = LogManager.getLogger(TareaController.class);

	@Autowired
	TareaService service;
	
	@Autowired
	TareaValidator validator;
	
	@Autowired
	RequerimientoRepository requerimientoRepository;
	
	@Autowired
	RequerimientoService requerimientoService;

	@Autowired
	AreaRepository areaRepository;
	
	@Autowired
	ContratoService contratoService;
	
	@Autowired
	MsTablasService mstablasService;

	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("requerimientoId");
		session.removeAttribute("nussSession");
		session.removeAttribute("nussTituloSession");
		session.removeAttribute("nussEstadoSession");
		session.removeAttribute("nussClienteSession");
		session.removeAttribute("tituloSession");
		ModelAndView modelAndView = new ModelAndView("carga/tarea/tarea_list");
		modelAndView.addObject("tituloPantalla", "tarea");
		modelAndView.addObject("tomarFiltrosSession", false);
		return modelAndView;
	}

	/***
	 * Función para visualizar las tareas de un requerimiento
	 * 
	 * @return
	 * @param request
	 */

	@RequestMapping("/showByRequerimiento/{id}")
	public ModelAndView showByEvento(@PathVariable("id") Long id, HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		session.setAttribute("requerimientoId", id);

		Requerimiento requerimiento = requerimientoService.findById(id);
		Long nuss = requerimiento.getNuss();
		session.setAttribute("nussSession", nuss);
		session.setAttribute("nussTituloSession", requerimiento.getTitulo());

		String descEstado = "";
		descEstado = mstablasService.findByTablaCodigoSubCodigo(04, requerimiento.getT04Estado(), null).getDescripcion(); 
		session.setAttribute("nussEstadoSession", requerimiento.getT04Estado()  + " - " + descEstado);

		session.setAttribute("nussClienteSession", requerimiento.getContrato().getCliente().getRazonSocial());
		
		ModelAndView modelAndView = new ModelAndView("carga/tarea/tarea_list");
		modelAndView.addObject("setearFiltrosSession", true);
		modelAndView.addObject("tomarFiltrosSession", true);
		modelAndView.addObject("tituloPantalla", "tarea");

		return modelAndView;
		
	}

	/***
	 * Funcion para retornar los datos sin limpiar los filtros de la session
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/filtros")
	public ModelAndView listByFiltros(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/tarea/tarea_list");
		modelAndView.addObject("tituloPantalla", "tarea");
		modelAndView.addObject("tomarFiltrosSession", true);
		return modelAndView;
	}	
	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
			@RequestParam(name = "sort", required = false, defaultValue = "area.descripcion") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "nuss", required = false) Long nuss,
			@RequestParam(name = "nussEstado", required = false) String nussEstado,
			@RequestParam(name = "nussTitulo", required = false) String nussTitulo,
			@RequestParam(name = "nussCliente", required = false) String nussCliente,
			@RequestParam(name = "titulo", required = false) String titulo,
			@RequestParam(name = "setearFiltrosSession", required = false) String setearFiltrosSession,
			@RequestParam(name = "mantenerfiltros", required = false) boolean mantenerfiltros,
			@RequestParam(name = "tomarFiltrosSession", required = false) boolean tomarFiltrosSession,
			HttpServletRequest request) {
		logger.info(Strings.scnt(limit, offset, sort, order));
		HttpSession session = request.getSession();
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		Page<Tarea> result = null;



		//Si se mantienen los filtros en cambio de vista
		if(mantenerfiltros) {
			session.setAttribute("tituloSession", titulo);
		}

		if(tomarFiltrosSession) {
			titulo = (String) SpringUtils.coalesce(titulo, session.getAttribute("titulo"));
		}

		nuss = (Long) SpringUtils.coalesce(nuss, session.getAttribute("nussSession"));
		session.setAttribute("nussSession", nuss);
	
		nussEstado = (String) SpringUtils.coalesce(nussEstado, session.getAttribute("nussEstadoSession"));
		session.setAttribute("nussEstadoSession", nussEstado);
		
		nussTitulo = (String) SpringUtils.coalesce(nussTitulo, session.getAttribute("nussTituloSession"));
		session.setAttribute("nussTituloSession", nussTitulo);

		nussCliente = (String) SpringUtils.coalesce(nussCliente, session.getAttribute("nussClienteSession"));
		session.setAttribute("nussClienteSession", nussCliente);

		
		if(("").equals(titulo)) {
			titulo = null;
		}

		titulo = titulo == null ? null : "%" + titulo.toUpperCase() + "%";
		
		result = service.findBySearch(pageRequest, nuss, titulo);

		// obtiene la descripción de tarea.t14Estado
		int tipoTarea=0;
		int estado=0;
		String descTipoTarea = "";
		String descEstado = "";
		for(Tarea tarea : result) {
			
			tipoTarea = tarea.getT07TipoTarea();
			estado = tarea.getT14Estado();

			descTipoTarea = tipoTarea + " - " + 
			mstablasService.findByTablaCodigoSubCodigo(07, tipoTarea, null).getDescripcion(); 

			descEstado = estado + " - " + 
			mstablasService.findByTablaCodigoSubCodigo(14, estado, null).getDescripcion(); 

			tarea.setDescTipoTarea(descTipoTarea);
			tarea.setDescEstado(descEstado);
	}	
		
		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") Tarea entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/tarea/tarea_add");
		
		if (result.hasErrors()) {
			modelAndView.addObject("create", true);
			return modelAndView;
		}
		try {
			service.save(entity);
			return new ModelAndView("redirect:/tarea/filtros/");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un tarea");
			modelAndView.addObject("evtEjecutar", "error");
			modelAndView.addObject("create", true);
			return modelAndView;
		}
	}
	
	/***
	 * Funcion para devolver la pantalla de crear
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		Long requerimiento = (Long) session.getAttribute("requerimientoId");

		Tarea entity = new Tarea();		
		if(requerimiento != null) {
			entity.setRequerimiento(requerimientoService.findById(requerimiento));
		}
		
		
		ModelAndView modelAndView = new ModelAndView("carga/tarea/tarea_add");
		entity.setTituloPantalla("add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para devolver la pantalla de editar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, HttpServletRequest request) {
		Tarea entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/tarea/tarea_add");
		entity = entity == null ? new Tarea() : entity;
		entity.setTituloPantalla("edit");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para de volver la pantalla de eliminar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/remove/{id}")
	public ModelAndView remove(@PathVariable("id") Long id,HttpServletRequest request) {
		Tarea entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/tarea/tarea_add");
		entity = entity == null ? new Tarea() : entity;
		entity.setTituloPantalla("del");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de borrar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/eliminar/{id}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("id") Long id, HttpServletRequest request) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
			service.remove(id);			
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}
	
	/***
	 * Funcion para de volver la pantalla de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{id}")
	public ModelAndView show(@PathVariable("id") Long id, HttpServletRequest request) {
		Tarea entity = service.findById(id);
		entity = entity == null ? new Tarea() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/tarea/tarea_add");
		entity.setTituloPantalla("show");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}
	
	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(Requerimiento.class, "requerimiento", new GenericEditor(requerimientoRepository));
		binder.registerCustomEditor(Area.class, "area", new GenericEditor(areaRepository));
		
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public Tarea getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new Tarea();
		} else {
			return service.findById(id);
		}
	}


}
