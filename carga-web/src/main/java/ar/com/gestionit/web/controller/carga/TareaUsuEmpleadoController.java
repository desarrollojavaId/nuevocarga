package ar.com.gestionit.web.controller.carga;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dao.carga.TareaRepository;
import ar.com.gestionit.dao.carga.UsuEmpleadoRepository;
import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.carga.Tarea;
import ar.com.gestionit.model.carga.TareaUsuEmpleado;
import ar.com.gestionit.model.carga.UsuEmpleado;
import ar.com.gestionit.service.carga.MsTablasService;
import ar.com.gestionit.service.carga.TareaService;
import ar.com.gestionit.service.carga.TareaUsuEmpleadoService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.editor.GenericEditor;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.TareaUsuEmpleadoValidator;

@Controller
@RequestMapping("/tareaUsuEmpleado")
public class TareaUsuEmpleadoController {
	private static Logger logger = LogManager.getLogger(TareaUsuEmpleadoController.class);

	@Autowired
	TareaUsuEmpleadoService service;
	
	@Autowired
	TareaUsuEmpleadoValidator validator;
	
	@Autowired
	TareaRepository tareaRepository;
	
	@Autowired
	UsuEmpleadoRepository usuEmpleadoRepository;
	
	@Autowired
	TareaService tareaService;
	
	@Autowired
	MsTablasService mstablasService;

	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("tareaIdSession");
		session.removeAttribute("tareaTituloSession");
		ModelAndView modelAndView = new ModelAndView("carga/tareaUsuEmpleado/tareaUsuEmpleado_list");
		modelAndView.addObject("tituloPantalla", "tareaUsuEmpleado");
		modelAndView.addObject("tomarFiltrosSession", false);
		return modelAndView;
	}

	/***
	 * Función para visualizar los empleados de una tarea
	 * 
	 * @return
	 * @param request
	 */

	@RequestMapping("/showByTarea/{id}")
	public ModelAndView showByEvento(@PathVariable("id") Long id, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("tareaIdSession", id);

		Tarea tarea = tareaService.findById(id);
		session.setAttribute("tareaTituloSession", tarea.getTitulo());

		TareaUsuEmpleado entity = new TareaUsuEmpleado();
		entity.setTarea(tarea);

//		Tarea tarea = tareaService.findById(id);
		
		ModelAndView modelAndView = new ModelAndView("carga/tareaUsuEmpleado/tareaUsuEmpleado_list");
		modelAndView.addObject("setearFiltrosSession", true);
		modelAndView.addObject("tomarFiltrosSession", true);
		modelAndView.addObject("tituloPantalla", "tareaUsuEmpleado");
		modelAndView.addObject("entity", entity);


		return modelAndView;
		
	}

	/***
	 * Funcion para retornar los datos sin limpiar los filtros de la session
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/filtros")
	public ModelAndView listByFiltros(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/tareaUsuEmpleado/tareaUsuEmpleado_list");
		modelAndView.addObject("tituloPantalla", "tareaUsuEmpleado");
		modelAndView.addObject("tomarFiltrosSession", true);
		return modelAndView;
	}	
	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
			@RequestParam(name = "sort", required = false, defaultValue = "usuEmpleado.usuario.nombre") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "tareaId", required = false) Long tareaId,
			@RequestParam(name = "tareaTitulo", required = false) String tareaTitulo,
			@RequestParam(name = "setearFiltrosSession", required = false) String setearFiltrosSession,
			@RequestParam(name = "mantenerfiltros", required = false) boolean mantenerfiltros,
			@RequestParam(name = "tomarFiltrosSession", required = false) boolean tomarFiltrosSession,
			HttpServletRequest request) {
		logger.info(Strings.scnt(limit, offset, sort, order));
		HttpSession session = request.getSession();
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		Page<TareaUsuEmpleado> result = null;

		tareaId = (Long) SpringUtils.coalesce(tareaId, session.getAttribute("tareaIdSession"));
		session.setAttribute("tareaIdSession", tareaId);

		tareaTitulo = (String) SpringUtils.coalesce(tareaTitulo, session.getAttribute("tareaTituloSession"));
		session.setAttribute("tareaTituloSession", tareaTitulo);
		
		result = service.findBySearch(pageRequest, tareaId);
		
		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") TareaUsuEmpleado entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/tareaUsuEmpleado/tareaUsuEmpleado_add");
		
		if (result.hasErrors()) {
			modelAndView.addObject("create", true);
			return modelAndView;
		}
		try {
			service.save(entity);
			return new ModelAndView("redirect:/tareaUsuEmpleado/filtros/");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un Empleado");
			modelAndView.addObject("evtEjecutar", "error");
			modelAndView.addObject("create", true);
			return modelAndView;
		}
	}
	
	/***
	 * Funcion para devolver la pantalla de crear
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		Long tarea = (Long) session.getAttribute("tareaIdSession");

		TareaUsuEmpleado entity = new TareaUsuEmpleado();		
		if(tarea != null) {
			entity.setTarea(tareaService.findById(tarea));
		}
		
		
		ModelAndView modelAndView = new ModelAndView("carga/tareaUsuEmpleado/tareaUsuEmpleado_add");
		entity.setTituloPantalla("add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para devolver la pantalla de editar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, HttpServletRequest request) {
		TareaUsuEmpleado entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/tareaUsuEmpleado/tareaUsuEmpleado_add");
		entity = entity == null ? new TareaUsuEmpleado() : entity;
		entity.setTituloPantalla("edit");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para de volver la pantalla de eliminar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/remove/{id}")
	public ModelAndView remove(@PathVariable("id") Long id,HttpServletRequest request) {
		TareaUsuEmpleado entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/tareaUsuEmpleado/tareaUsuEmpleado_add");
		entity = entity == null ? new TareaUsuEmpleado() : entity;
		entity.setTituloPantalla("del");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de borrar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/eliminar/{id}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("id") Long id, HttpServletRequest request) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
			service.remove(id);			
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}
	
	/***
	 * Funcion para de volver la pantalla de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{id}")
	public ModelAndView show(@PathVariable("id") Long id, HttpServletRequest request) {
		TareaUsuEmpleado entity = service.findById(id);
		entity = entity == null ? new TareaUsuEmpleado() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/tareaUsuEmpleado/tareaUsuEmpleado_add");
		entity.setTituloPantalla("show");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}
	
	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(Tarea.class, "tarea", new GenericEditor(tareaRepository));
		binder.registerCustomEditor(UsuEmpleado.class, "usuEmpleado", new GenericEditor(usuEmpleadoRepository));
		
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public TareaUsuEmpleado getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new TareaUsuEmpleado();
		} else {
			return service.findById(id);
		}
	}


}
