package ar.com.gestionit.web.controller.carga;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dao.carga.ClienteRepository;
import ar.com.gestionit.dao.seguridad.UsuarioRepository;
import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.carga.Cliente;
import ar.com.gestionit.model.carga.UsuCliente;
import ar.com.gestionit.model.seguridad.Usuario;
import ar.com.gestionit.service.carga.UsuClienteService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.editor.GenericEditor;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.UsuClienteValidator;

@Controller
@RequestMapping("/usuCliente")
public class UsuClienteController {
	private static Logger logger = LogManager.getLogger(UsuClienteController.class);
	
	@Autowired
	UsuClienteService service;
	
	@Autowired
	UsuClienteValidator validator;
	
	@Autowired 
	UsuarioRepository usuarioRepository;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("nombreSession");
		session.removeAttribute("apellidoSession");
		ModelAndView modelAndView = new ModelAndView("carga/usuCliente/usuCliente_list");
		modelAndView.addObject("tituloPantalla", "usuCliente");
		modelAndView.addObject("tomarFiltrosSession", false);
		return modelAndView;
	}

	/***
	 * Funcion para retornar los datos sin limpiar los filtros de la session
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/filtros")
	public ModelAndView listByFiltros(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/usuCliente/usuCliente_list");
		modelAndView.addObject("tituloPantalla", "usuCliente");
		modelAndView.addObject("tomarFiltrosSession", true);
		return modelAndView;
	}	
	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @param nombre
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
			@RequestParam(name = "sort", required = false, defaultValue = "cliente.razonSocial") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "razonSocial", required = false) String razonSocial,
			@RequestParam(name = "nombre", required = false) String nombre,
			@RequestParam(name = "apellido", required = false) String apellido,
			@RequestParam(name = "mantenerfiltros", required = false) boolean mantenerfiltros,
			@RequestParam(name = "tomarFiltrosSession", required = false) boolean tomarFiltrosSession,
			HttpServletRequest request) {
		logger.info(Strings.scnt(limit, offset, sort, order));
		HttpSession session = request.getSession();
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		if(mantenerfiltros) {
			session.setAttribute("razonSocial", razonSocial);
			session.setAttribute("nombreSession", nombre);
			session.setAttribute("apellidoSession", apellido);
		}
		if(tomarFiltrosSession) {
			razonSocial = (String) SpringUtils.coalesce(razonSocial, session.getAttribute("razonSocialSession"));
			nombre = (String) SpringUtils.coalesce(nombre, session.getAttribute("nombreSession"));
			apellido = (String) SpringUtils.coalesce(apellido, session.getAttribute("apellidoSession"));
		}
		Page<UsuCliente> result = null;

		if(("").equals(razonSocial)) {
			razonSocial = null;
		}
		razonSocial = razonSocial == null ? null : "%" + razonSocial.toUpperCase() + "%";

		if(("").equals(nombre)) {
			nombre = null;
		}
		nombre = nombre == null ? null : "%" + nombre.toUpperCase() + "%";

		if(("").equals(apellido)) {
			apellido = null;
		}
		apellido = apellido == null ? null : "%" + apellido.toUpperCase() + "%";
		
		result = service.findBySearch(pageRequest, razonSocial, nombre, apellido);
		if(limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());	
		}else {
			return result.getContent();
		}
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") UsuCliente entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/usuCliente/usuCliente_add");
	
		if (result.hasErrors()) {
			modelAndView.addObject("create", true);
			return modelAndView;
		}
		
		try {
			service.save(entity);
			return new ModelAndView("redirect:/usuCliente/");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un usuario");
			modelAndView.addObject("evtEjecutar", "error");
			modelAndView.addObject("create", true);
			return modelAndView;
		}
	}
	
	/***
	 * Funcion para devolver la pantalla de crear
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request) {
		UsuCliente entity = new UsuCliente();		
		ModelAndView modelAndView = new ModelAndView("carga/usuCliente/usuCliente_add");
		entity.setTituloPantalla("add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para devolver la pantalla de editar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, HttpServletRequest request) {
		UsuCliente entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/usuCliente/usuCliente_add");
		entity = entity == null ? new UsuCliente() : entity;
		entity.setTituloPantalla("edit");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);
		return modelAndView;
	}
	
	/***
	 * Funcion para de volver la pantalla de eliminar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/remove/{id}")
	public ModelAndView remove(@PathVariable("id") Long id,HttpServletRequest request) {
		UsuCliente entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/usuCliente/usuCliente_add");
		entity = entity == null ? new UsuCliente() : entity;
		entity.setTituloPantalla("del");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de borrar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/eliminar/{id}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("id") Long id, HttpServletRequest request) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
//			UsuCliente usuCliente = service.findById(id);
			service.remove(id);			
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}
	
	/***
	 * Funcion para de volver la pantalla de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{id}")
	public ModelAndView show(@PathVariable("id") Long id, HttpServletRequest request) {
		UsuCliente entity = service.findById(id);
		entity = entity == null ? new UsuCliente() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/usuCliente/usuCliente_add");
		entity.setTituloPantalla("show");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public UsuCliente getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new UsuCliente();
		} else {
			return service.findById(id);
		}
	}
	
	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(Usuario.class, "usuario", new GenericEditor(usuarioRepository));
		binder.registerCustomEditor(Cliente.class, "cliente", new GenericEditor(clienteRepository));
		
	}
}