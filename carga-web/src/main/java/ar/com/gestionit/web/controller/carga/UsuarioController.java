package ar.com.gestionit.web.controller.carga;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.seguridad.Usuario;
import ar.com.gestionit.service.seguridad.UsuarioService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;
import ar.com.gestionit.web.validator.carga.UsuarioValidator;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	private static Logger logger = LogManager.getLogger(UsuarioController.class);

	@Autowired
	UsuarioService service;

	@Autowired
	UsuarioValidator validator;

	/***
	 * Funcion para retornar los datos
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("")
	public ModelAndView list(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("nombreUsuarioSession");
		session.removeAttribute("nombreSession");
		session.removeAttribute("apellidoSession");
		ModelAndView modelAndView = new ModelAndView("carga/usuario/usuario_list");
		modelAndView.addObject("tituloPantalla", "usuario");
		modelAndView.addObject("tomarFiltrosSession", false);
		return modelAndView;
	}

	/***
	 * Funcion para retornar los datos sin limpiar los filtros de la session
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/filtros")
	public ModelAndView listByFiltros(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/usuario/usuario_list");
		modelAndView.addObject("tituloPantalla", "usuario");
		modelAndView.addObject("tomarFiltrosSession", true);
		return modelAndView;
	}

	/**
	 * Funcion para retornar registros
	 * 
	 * @param limit
	 * @param offset
	 * @param sort
	 * @param order
	 * @param nombre
	 * @return
	 */
	@RequestMapping(value = "/records")
	public @ResponseBody Object records(@RequestParam(name = "limit", required = false) Integer limit,
			@RequestParam(name = "offset", required = false) Integer offset,
			@RequestParam(name = "sort", required = false, defaultValue = "nombreUsuario") String sort,
			@RequestParam(name = "order", required = false) String order,
			@RequestParam(name = "nombreUsuario", required = false) String nombreUsuario,
			@RequestParam(name = "nombre", required = false) String nombre,
			@RequestParam(name = "apellido", required = false) String apellido,
			@RequestParam(name = "mantenerfiltros", required = false) boolean mantenerfiltros,
			@RequestParam(name = "tomarFiltrosSession", required = false) boolean tomarFiltrosSession,
			HttpServletRequest request) {

		logger.info(Strings.scnt(limit, offset, sort, order));
		HttpSession session = request.getSession();
		PageRequest pageRequest = SpringUtils.getPageRequestByOffset(limit, offset, sort, order);
		Page<Usuario> result = null;

		if (mantenerfiltros) {
			session.setAttribute("nombreUsuarioSession", nombreUsuario);
			session.setAttribute("nombreSession", nombre);
			session.setAttribute("apellidoSession", apellido);
		}
		if (tomarFiltrosSession) {
			nombreUsuario = (String) SpringUtils.coalesce(nombreUsuario, session.getAttribute("nombreUsuarioSession"));
			nombre = (String) SpringUtils.coalesce(nombre, session.getAttribute("nombreSession"));
			apellido = (String) SpringUtils.coalesce(apellido, session.getAttribute("apellidoSession"));
		}
		if (("").equals(nombreUsuario)) {
			nombreUsuario = null;
		}
		nombreUsuario = nombreUsuario == null ? null : "%" + nombreUsuario.toUpperCase() + "%";

		if (("").equals(nombre)) {
			nombre = null;
		}
		nombre = nombre == null ? null : "%" + nombre.toUpperCase() + "%";

		if (("").equals(apellido)) {
			apellido = null;
		}
		apellido = apellido == null ? null : "%" + apellido.toUpperCase() + "%";

		result = service.findBySearch(pageRequest, nombreUsuario, nombre, apellido);
		if (limit != null) {
			return new PageResult(result.getContent(), result.getTotalElements());
		} else {
			return result.getContent();
		}
	}

	/**
	 * Funcion para guardar
	 * 
	 * @param entity
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView adding(@Validated @ModelAttribute("entity") Usuario entity, BindingResult result,
			HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("carga/usuario/usuario_add");

		if (result.hasErrors()) {
			modelAndView.addObject("create", true);
			return modelAndView;
		}
		try {
			service.save(entity);
			return new ModelAndView("redirect:/usuario/");
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			result.rejectValue(null, "Error al dar de alta un usuario");
			modelAndView.addObject("evtEjecutar", "error");
			modelAndView.addObject("create", true);
			return modelAndView;
		}
	}

	/***
	 * Funcion para devolver la pantalla de crear
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/add")
	public ModelAndView add(HttpServletRequest request) {
		Usuario entity = new Usuario();
		ModelAndView modelAndView = new ModelAndView("carga/usuario/usuario_add");
		entity.setTituloPantalla("add");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("create", true);
		return modelAndView;
	}

	/***
	 * Funcion para devolver la pantalla de editar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, HttpServletRequest request) {
		Usuario entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/usuario/usuario_add");
		entity = entity == null ? new Usuario() : entity;
		entity.setTituloPantalla("edit");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("edit", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de eliminar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/remove/{id}")
	public ModelAndView remove(@PathVariable("id") Long id, HttpServletRequest request) {
		Usuario entity = service.findById(id);
		ModelAndView modelAndView = new ModelAndView("carga/usuario/usuario_add");
		entity = entity == null ? new Usuario() : entity;
		entity.setTituloPantalla("del");
		modelAndView.addObject("entity", entity);
		modelAndView.addObject("borrar", true);
		return modelAndView;
	}

	/***
	 * Funcion para de volver la pantalla de borrar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/eliminar/{id}")
	public @ResponseBody AjaxResponseDto eliminar(@PathVariable("id") Long id, HttpServletRequest request) {
		AjaxResponseDto ajaxResponseDto = new AjaxResponseDto();
		try {
			service.remove(id);
			return ajaxResponseDto;
		} catch (Exception e) {
			logger.info(e.getMessage());
			System.out.println(e.getMessage());
			return ajaxResponseDto;
		}
	}

	/***
	 * Funcion para de volver la pantalla de consultar
	 * 
	 * @return
	 * @param request
	 */
	@RequestMapping("/show/{id}")
	public ModelAndView show(@PathVariable("id") Long id, HttpServletRequest request) {
		Usuario entity = service.findById(id);
		entity = entity == null ? new Usuario() : entity;
		ModelAndView modelAndView = new ModelAndView("carga/usuario/usuario_add");
		entity.setTituloPantalla("show");
		modelAndView.addObject("entity", entity);
		return modelAndView;
	}

	/***
	 * 
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ModelAttribute("entity")
	public Usuario getEntity(@RequestParam(required = false) Long id) {
		if (id == null) {
			return new Usuario();
		} else {
			return service.findById(id);
		}
	}

}