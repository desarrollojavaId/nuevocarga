package ar.com.gestionit.web.controller.common;

import org.springframework.http.HttpStatus;

public class AjaxResponseException extends RuntimeException {

	private static final long serialVersionUID = -6864088323509264936L;
	
	HttpStatus status;

	public AjaxResponseException(HttpStatus badRequest, String string) {
		super(string);
		this.status = badRequest;
	}

	public HttpStatus getStatus() {
		return status;
	}

}
