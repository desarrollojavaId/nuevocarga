package ar.com.gestionit.web.controller.common;

import java.util.Collection;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ar.com.gestionit.dao.PagedComboRepository;
import ar.com.gestionit.dto.AjaxResponseDto;
import ar.com.gestionit.model.carga.Contrato;
import ar.com.gestionit.model.carga.MsTablas;
import ar.com.gestionit.service.carga.MsTablasService;
import ar.com.gestionit.utils.Strings;
import ar.com.gestionit.web.grid.PageResult;
import ar.com.gestionit.web.utils.SpringUtils;

@Controller
@RequestMapping("/combo")
public class GenericComboController implements ApplicationContextAware {

	private static final String REPOSITORY = "Repository";

	private static Logger logger = LogManager.getLogger(GenericComboController.class);
	private ApplicationContext context;
	
	@Autowired
	MsTablasService mstablasService;

	@RequestMapping(value = "/{entity}/get")
	public @ResponseBody Object getById(@PathVariable String entity, @RequestParam(required = true) String id) {
		logger.info(Strings.scnt("Buscando datos de la entidad " + entity));
		logger.info(Strings.scnt("Criterio de busqueda " + id));
		// Obtengo el repository a partir del nombre de la entidad
		CrudRepository<?, Long> repository = getRepository(entity);
		
		if(id.indexOf(",") != -1){
			String[] ids = id.split(",");
			return findObjectByIdIn(repository, ids);
		} else {
			Long parsedId = Long.parseLong(id);
			Object objetoDelID = repository.findOne(parsedId);
			
			// Si el objeto obtenido con el ID es del tipo Contrato
			// obtiene la descripciónde la categoría - T03Categoria
			if (objetoDelID instanceof Contrato) {
				MsTablas mstablas=null;
				int categoria=0;
				String descCategoria = "";
				categoria = ((Contrato) objetoDelID).getT03Categoria();
				mstablas = mstablasService.findByTablaCodigoSubCodigo(3, categoria, null);
				descCategoria = mstablas.getDescripcion(); 

				((Contrato) objetoDelID).setDescCategoria(descCategoria);
			}
			
			return objetoDelID;
		}
	}
		
	@SuppressWarnings("unchecked")
	private Collection<?> findObjectByIdIn(CrudRepository<?, Long> repository, String[] ids) {
		Collection<Object> result = new HashSet<>();
		boolean invoked = false;
		try {
			result = (Collection<Object>) MethodUtils.invokeExactMethod(repository, "findByIdIn", ids);
			invoked = true;
		} catch (Exception e) {
		}
		if(!invoked){
			for (String id : ids) {
				Long parsedId = Long.parseLong(id);
				result.add(repository.findOne(parsedId));
			}
		}
		
		return result;
	}

	@RequestMapping(value = "/{entity}/paged/recordsByName")
	public @ResponseBody PageResult pagedRecordsByName(@PathVariable String entity, @RequestParam(name = "page", required = false, defaultValue = "0") Integer page, @RequestParam(required = false) String search) {
		logger.info(Strings.scnt("Buscando datos de la entidad " + entity));
		logger.info(Strings.scnt("Criterio de busqueda " + search));
		// Obtengo el repository a partir del nombre de la entidad
		PageRequest pr = SpringUtils.getPageRequestByPage(10, page);
		Page<?> result = null;
		PagedComboRepository<?, ?> repository = getRepository(entity);
		if (StringUtils.isEmpty(search)) {
			result = repository.findByOrderByNombre(pr);
		} else {
			String criteria = Strings.wrap(search.toLowerCase(), "%");
			result = repository.findByNombreLikeIgnoreCaseOrderByNombre(pr, criteria);
		}
		
		return new PageResult(result.getContent(), result.getTotalElements());
	}
	
	@RequestMapping(value = "/popup")
	public @ResponseBody ModelAndView getPopUp(@RequestParam(required = true) String popup) {
		logger.info(Strings.scnt("Buscando la vista popup para  " + popup));
		ModelAndView modelAndView = new ModelAndView("popups/" + popup);
		return modelAndView;
	}
	
	@SuppressWarnings("unchecked")
	private <J> J getRepository(String entity) {
		String repoName = entity + REPOSITORY;
		if (!context.containsBean(repoName)) {
			logger.warn("El nombre de entidad no tiene asociado un repository ::" + entity);
			throw new IllegalArgumentException("El nombre de entidad no tiene asociado un repository ::" + entity);
		}
		return (J) context.getBean(repoName);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;
	}
	
	@RequestMapping(value = "/limpiar_filtro")
	public @ResponseBody AjaxResponseDto limpiarFiltroSession(@RequestParam String nombreFiltro, HttpServletRequest request) {
		HttpSession session = request.getSession();
		String[] filtros = nombreFiltro.split(",");
		for(Integer i = 0 ; i < filtros.length ; i++) {
			session.removeAttribute(filtros[i]);	
		}
		return new AjaxResponseDto();
	}
}
