package ar.com.gestionit.web.dto.carga;

import ar.com.gestionit.model.seguridad.Grupo;

public class GruposPuntoMenuDto {

	private Long id;	
	private String camposDinamicos;
	private Grupo grupo;
	
	private Long puntoMenuCategoria;
	private String cabeceraNomAlternativo;
	private Long puntoMenu;
	private String puntoMenuNomAlternativo;
	
	private boolean borrarRegistro;
	
	private Long orden;
	private Long ordenDetalle;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getPuntoMenuCategoria() {
		return puntoMenuCategoria;
	}
	public void setPuntoMenuCategoria(Long puntoMenuCategoria) {
		this.puntoMenuCategoria = puntoMenuCategoria;
	}
	public Long getPuntoMenu() {
		return puntoMenu;
	}
	public void setPuntoMenu(Long puntoMenu) {
		this.puntoMenu = puntoMenu;
	}
	public String getCabeceraNomAlternativo() {
		return cabeceraNomAlternativo;
	}
	public void setCabeceraNomAlternativo(String cabeceraNomAlternativo) {
		this.cabeceraNomAlternativo = cabeceraNomAlternativo;
	}
	public String getPuntoMenuNomAlternativo() {
		return puntoMenuNomAlternativo;
	}
	public void setPuntoMenuNomAlternativo(String puntoMenuNomAlternativo) {
		this.puntoMenuNomAlternativo = puntoMenuNomAlternativo;
	}
	public boolean isBorrarRegistro() {
		return borrarRegistro;
	}
	public void setBorrarRegistro(boolean borrarRegistro) {
		this.borrarRegistro = borrarRegistro;
	}
	
	public String getCamposDinamicos() {
		return camposDinamicos;
	}
	public void setCamposDinamicos(String camposDinamicos) {
		this.camposDinamicos = camposDinamicos;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public Long getOrden() {
		return orden;
	}
	public void setOrden(Long orden) {
		this.orden = orden;
	}
	public Long getOrdenDetalle() {
		return ordenDetalle;
	}
	public void setOrdenDetalle(Long ordenDetalle) {
		this.ordenDetalle = ordenDetalle;
	}
	
}
