package ar.com.gestionit.web.dto.carga;

public class PuntoMenuDto {

	private Long id;	
	private String camposDinamicos;
	
	private Long puntoMenuUrlId;
	private String descripcionPuntoMenu;
	private Long puntoMenuCategoria;
	private Long orden;
	private boolean borrarRegistro;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCamposDinamicos() {
		return camposDinamicos;
	}
	public void setCamposDinamicos(String camposDinamicos) {
		this.camposDinamicos = camposDinamicos;
	}
	public boolean isBorrarRegistro() {
		return borrarRegistro;
	}
	public void setBorrarRegistro(boolean borrarRegistro) {
		this.borrarRegistro = borrarRegistro;
	}
	public Long getPuntoMenuCategoria() {
		return puntoMenuCategoria;
	}
	public void setPuntoMenuCategoria(Long puntoMenuCategoria) {
		this.puntoMenuCategoria = puntoMenuCategoria;
	}
	public Long getOrden() {
		return orden;
	}
	public void setOrden(Long orden) {
		this.orden = orden;
	}
	public String getDescripcionPuntoMenu() {
		return descripcionPuntoMenu;
	}
	public void setDescripcionPuntoMenu(String descripcionPuntoMenu) {
		this.descripcionPuntoMenu = descripcionPuntoMenu;
	}
	public Long getPuntoMenuUrlId() {
		return puntoMenuUrlId;
	}
	public void setPuntoMenuUrlId(Long puntoMenuUrlId) {
		this.puntoMenuUrlId = puntoMenuUrlId;
	}

	
}
