package ar.com.gestionit.web.editor;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;


public class GenericDecimalEditor extends PropertyEditorSupport {
	Logger logger = LogManager.getLogger(GenericDecimalEditor.class);


	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (StringUtils.isEmpty(text)) {
			this.setValue(null);
		} else {
			BigDecimal elements = transformCommaSeparated(text);
			this.setValue(elements);
		}
	}

	private BigDecimal transformCommaSeparated(String text) {
		BigDecimal result = new BigDecimal(text.replace(".", "").replace(",", "."));
		return result;
	}

	@Override
	public String getAsText() {
		if (this.getValue() != null && this.getValue() instanceof Long) {
			return this.getValue().toString();
		}
		if (this.getValue() != null && this.getValue() instanceof Collection) {
            try {
                StringBuilder stringBuilder = new StringBuilder();
                Collection<?> iterable = (Collection<?>) this.getValue();

			for (Iterator<?> iter = iterable.iterator(); iter.hasNext();) {
                stringBuilder.append(PropertyUtils.getProperty(iter.next(), "nombre").toString());
                if(iter.hasNext())
                    stringBuilder.append(",");
			}

            return stringBuilder.toString();
			} catch (Exception e) {
			}
		}

		return null;
	}
}
