package ar.com.gestionit.web.editor;

import java.beans.PropertyEditorSupport;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.data.repository.CrudRepository;
import org.springframework.util.StringUtils;

public class GenericEditor extends PropertyEditorSupport {

	private CrudRepository<?, Long> repository;

	public GenericEditor(CrudRepository<?, Long> repository) {
		setRepository(repository);
	}

	public void setRepository(CrudRepository<?, Long> repository) {
		this.repository = repository;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (StringUtils.isEmpty(text)) {
			this.setValue(null);
		} else {
			Object sc = repository.findOne(Long.parseLong(text));
			this.setValue(sc);
		}
	}

	@Override
	public String getAsText() {
		if (this.getValue() != null && this.getValue() instanceof Long) {
			return this.getValue().toString();
		}
		if (this.getValue() != null && PropertyUtils.isReadable(this.getValue(), "id")) {
			try {
				return PropertyUtils.getProperty(this.getValue(), "id").toString();
			} catch (Exception e) {
			}
		}

		return null;
	}
}
