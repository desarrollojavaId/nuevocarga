package ar.com.gestionit.web.editor;

import java.beans.PropertyEditorSupport;

import org.springframework.util.StringUtils;

public class GenericEnumConverter<T extends Enum<T>> extends PropertyEditorSupport {

	Class<T> type;

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (StringUtils.isEmpty(text)) {
			this.setValue(null);
		} else {
			Object sc = convert(text);
			this.setValue(sc);
		}
	}

	public GenericEnumConverter(Class<T> type) {
		this.type = type;
	}

	public Enum<T> convert(String text) {
		for (Enum<T> candidate : type.getEnumConstants()) {
			if (candidate.name().equalsIgnoreCase(text)) {
				return candidate;
			}
		}

		return null;
	}
}