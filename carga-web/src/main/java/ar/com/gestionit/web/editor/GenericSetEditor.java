package ar.com.gestionit.web.editor;

import java.beans.PropertyEditorSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.repository.CrudRepository;
import org.springframework.util.StringUtils;

import java.util.StringTokenizer;

public class GenericSetEditor extends PropertyEditorSupport {
	Logger logger = LogManager.getLogger(GenericSetEditor.class);

	private CrudRepository<?, Long> repository;

	public GenericSetEditor(CrudRepository<?, Long> repository) {
		setRepository(repository);
	}

	public void setRepository(CrudRepository<?, Long> repository) {
		this.repository = repository;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (StringUtils.isEmpty(text)) {
			this.setValue(Collections.emptySet());
		} else {
			Set<Object> elements = transformCommaSeparated(text);
			this.setValue(elements);
		}
	}

	private Set<Object> transformCommaSeparated(String text) {
		Set<Object> result = new HashSet<>();
		StringTokenizer st = new StringTokenizer(text, ",");
		while(st.hasMoreTokens()){
			try {
				long value = Long.parseLong(st.nextToken());
				result.add(repository.findOne(value));
			} catch (Exception e) {
				logger.catching(e);
			}
		}
		   
		
		return result;
	}

	@Override
	public String getAsText() {
		if (this.getValue() != null && this.getValue() instanceof Long) {
			return this.getValue().toString();
		}
		if (this.getValue() != null && this.getValue() instanceof Collection) {
            try {
                StringBuilder stringBuilder = new StringBuilder();
                Collection<?> iterable = (Collection<?>) this.getValue();

			for (Iterator<?> iter = iterable.iterator(); iter.hasNext();) {
                stringBuilder.append(PropertyUtils.getProperty(iter.next(), "id").toString());
                if(iter.hasNext())
                    stringBuilder.append(",");
			}

            return stringBuilder.toString();
			} catch (Exception e) {
			}
		}

		return null;
	}
}
