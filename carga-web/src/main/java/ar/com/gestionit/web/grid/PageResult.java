package ar.com.gestionit.web.grid;

public class PageResult {
	Iterable<?> rows;
	long total;
	
	public PageResult(Iterable<?> rows,long total){
		this.rows = rows;
		this.total = total;
	}

	public Iterable<?> getRows() {
		return rows;
	}

	public void setRows(Iterable<?> rows) {
		this.rows = rows;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}
	
}
