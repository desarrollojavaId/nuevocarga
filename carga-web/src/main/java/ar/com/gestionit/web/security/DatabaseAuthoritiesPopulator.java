package ar.com.gestionit.web.security;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;

import ar.com.gestionit.dao.seguridad.GrupoRepository;

public class DatabaseAuthoritiesPopulator extends DefaultLdapAuthoritiesPopulator {
	private static final Logger logger = LogManager.getLogger(DatabaseAuthoritiesPopulator.class);

	@Autowired
	GrupoRepository grupoRepository;

	private String ldapGroupAttribute = "memberOf";

	public DatabaseAuthoritiesPopulator(ContextSource contextSource, String groupSearchBase) {
		super(contextSource, groupSearchBase);
	}

	@Override
	public Set<GrantedAuthority> getAdditionalRoles(DirContextOperations user, String username) {
		int permissionCount = 0;
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		try {
			if(logger.isDebugEnabled())
				logger.debug("Obteniendo los permisos para el usuario {}?", username);
			String[] groups = user.getStringAttributes(ldapGroupAttribute);
			if (groups != null) {
				for (int i = 0; i < groups.length; i++) {
					// Obtengo el grupo eliminando el CN=
					String auth = groups[i].substring(0, groups[i].indexOf(",")).replace("CN=", "");
					authorities.add(new SimpleGrantedAuthority(auth));
					permissionCount ++;
				}
			}
		} catch (Exception e) {
			throw new AuthorizationServiceException("Obteniendo los permisos del usuario " + username, e);
		}

		if (permissionCount == 0) {
			// Se agrega por tkt 0002103
			throw new InsufficientAuthenticationException(String.format("Usuario %s sin permisos", username));
		}

		if(logger.isDebugEnabled())
			logger.debug("Devuelvo {}? permisos para el usuario {}?", permissionCount, username);
		return authorities;
	}	

	public String getLDAPGroupAttribute() {
		return ldapGroupAttribute;
	}

	public void setLDAPGroupAttribute(String ldapGroupAttribute) {
		this.ldapGroupAttribute = ldapGroupAttribute;
	}
}
