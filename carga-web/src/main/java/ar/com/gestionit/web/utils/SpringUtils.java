package ar.com.gestionit.web.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.util.StringUtils;

public class SpringUtils {

	public static PageRequest getPageRequestByOffset(Integer limit, Integer offset, String sort, String order) {
		PageRequest pageRequest = null;
		if (limit != null && offset != null) {
			if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
				pageRequest = new PageRequest(offset / limit, limit, Direction.fromString(order), sort);
			} else {
				pageRequest = new PageRequest(offset / limit, limit);
			}
		}else if(!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
			pageRequest = new PageRequest(0, Integer.MAX_VALUE, Direction.fromString(order), sort);
		}
		return pageRequest;
	}

	public static PageRequest getPageRequestByPage(Integer limit, Integer page, String sort, String order) {
		PageRequest pageRequest = null;
		if (limit != null && page != null) {
			if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
				pageRequest = new PageRequest(page - 1, limit, Direction.fromString(order), sort);
			} else {
				pageRequest = new PageRequest(page - 1, limit);
			}
		}
		return pageRequest;
	}

	public static PageRequest getPageRequestByPage(Integer limit, Integer page) {
		return getPageRequestByPage(limit, page, null, null);
	}

	/***
	 * Funcion para retornar el valor no null de dos objetos
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static <T> T coalesce(T a, T b) {
		return a == null ? b : a;
	}

	/***
	 * 
	 * @param date
	 * @return
	 */
	public static Date convertirFechaSinMinutos(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		date = calendar.getTime();
		return date;
	}

	/***
	 * Funcion para valida si la fecha esta en el primer dia del mes
	 * 
	 * @param date
	 * @return
	 */
	public static Boolean esDiaInicioMes(Date date) {
		Calendar comparador = Calendar.getInstance();
		comparador.setTime(date);
		return comparador.getActualMinimum(Calendar.DAY_OF_MONTH) == comparador.get(Calendar.DAY_OF_MONTH);
	}

	/***
	 * Funcion para validar si la fecha esta en el ultimo dia del mes
	 * 
	 * @param date
	 * @return
	 */
	public static Boolean esDiaFinMes(Date date) {
		Calendar comparador = Calendar.getInstance();
		comparador.setTime(date);
		return comparador.getActualMaximum(Calendar.DAY_OF_MONTH) == comparador.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Funcion para devolver el primer día del mes siguiente.
	 * 
	 * @param date
	 * @return
	 */
	public static Date getfirstDayOfTheMonthNext(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.MONTH, 1);
		return calendar.getTime();
	}

	/***
	 * Funcion para devolver el ultimo item de una lista
	 * 
	 * @param elements
	 * @return
	 */
	public static <T> T getLastElement(final Iterable<T> elements) {
		final Iterator<T> itr = elements.iterator();
		T lastElement = itr.next();

		while (itr.hasNext()) {
			lastElement = itr.next();
		}

		return lastElement;
	}

	/***
	 * Funcion para devolver el primer item de una lista
	 * 
	 * @param elements
	 * @return
	 */
	public static <T> T getFirstElement(final Iterable<T> elements) {
		if (elements == null) {
			return null;
		}
		return elements.iterator().next();
	}

}
