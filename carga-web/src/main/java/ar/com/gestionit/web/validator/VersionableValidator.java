package ar.com.gestionit.web.validator;

import java.io.Serializable;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.dao.VersionableRepository;
import ar.com.gestionit.interfaz.Versionable;

public class VersionableValidator implements Validator {

	@SuppressWarnings("rawtypes")
	private VersionableRepository repository;

	@SuppressWarnings("rawtypes")
	public VersionableValidator(VersionableRepository repository) {
		this.repository = repository;
	}

	@Override
	public boolean supports(Class<?> cls) {
		return Versionable.class.isAssignableFrom(cls);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void validate(Object bean, Errors errors) {
		try {
			Versionable entity = (Versionable) bean;

			Serializable id = (Serializable) PropertyUtils.getProperty(entity, "id");

			if (id == null) {
				return;
			}

			Serializable version = this.repository.findLastVersion(id);
			Serializable versionActual = (Serializable) PropertyUtils.getProperty(entity, "version");

			if (!version.equals(versionActual)) {
				errors.rejectValue("version", "No se puede moficiar el objeto, esta versión no es la ultima");
			}
		} catch (Exception e) {

		}

	}

}
