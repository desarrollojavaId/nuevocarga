package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.Area;
import ar.com.gestionit.service.carga.AreaService;


@Component
public class AreaValidator  implements Validator{
	@Autowired 
	private AreaService service;

	@Override
	public boolean supports(Class<?> cls) {
		return Area.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		
		Area entity = (Area) bean;
		if(entity.isValida()) {
			Long countDescripcion = 0L;
			 
			if ("".equals(entity.getDescripcion()) || entity.getDescripcion() == null) {
				errors.rejectValue("descripcion", "campo_obligatorio");
			}
			
			Long id = entity.getId() != null ? entity.getId() : new Long(0);
			
			if(id>0)
				countDescripcion = service.countExisteDescripcion(id, entity.getDescripcion());
			else
				countDescripcion = service.countExisteDescripcion(entity.getDescripcion());
			
			if(countDescripcion>0)
				errors.rejectValue("descripcion", "area.descripcion_existente");
		}
		
	}	
}
