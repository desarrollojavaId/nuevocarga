package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.Cliente;
import ar.com.gestionit.service.carga.ClienteService;


@Component
public class ClienteValidator  implements Validator{
	@Autowired 
	private ClienteService service;

	@Override
	public boolean supports(Class<?> cls) {
		return Cliente.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		
		Cliente entity = (Cliente) bean;
//		if(entity.isValida()) {
			Long countCuit = 0L;
/*			if ("".equals(entity.getRazonSocial()) || entity.getRazonSocial() == null) {
				errors.rejectValue("razonSocial", "campo_obligatorio");
			} 
			if (entity.getCuit() == null) {
				errors.rejectValue("cuit", "campo_obligatorio");
			}
			if ("".equals(entity.getNumeroCliente()) || entity.getNumeroCliente() == null) {
				errors.rejectValue("numeroCliente", "campo_obligatorio");
			}
*/
			Long id = entity.getId() != null ? entity.getId() : new Long(0);
			
			if(id>0)
				countCuit = service.countExisteCuit(id, entity.getCuit());
			else
			   countCuit = service.countExisteCuit(entity.getCuit());
			
			if(countCuit>0)
				errors.rejectValue("cuit", "cliente.cuit_existente");
		}
		
//	}	
}
