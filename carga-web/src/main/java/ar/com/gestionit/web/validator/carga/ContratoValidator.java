package ar.com.gestionit.web.validator.carga;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.Contrato;


@Component
public class ContratoValidator  implements Validator{
		
//	@Autowired 
//	private ContratoService service;
	
	@Override
	public boolean supports(Class<?> cls) {
		return Contrato.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		
		Contrato entity = (Contrato) bean;
		
//		if(entity.isValida()) {
//			Long countUsuario = 0L;
//			
//			
//			Long id = entity.getId() != null ? entity.getId() : new Long(0);
//			
//			if(id>0)
//				countUsuario = service.countExisteContratoId(id, entity.getUsuario().getId());
//			else
//				countUsuario = service.countExisteContrato(entity.getUsuario().getId());
//			
//			if(countUsuario>0)
//				errors.rejectValue("usuario", "contrato.usuario_existente");
//		}
		
		if(entity.getFecFinal().before(entity.getFecInicio())) {
			errors.rejectValue("fecInicio", "contrato.fecInicioMayorFecFinal");
		}
		if(entity.getFecCargaHorasHasta().before(entity.getFecCargaHorasDesde())) {
			errors.rejectValue("fecCargaHorasDesde", "contrato.fecDesdeMayorHasta");
		}
	}	
}
