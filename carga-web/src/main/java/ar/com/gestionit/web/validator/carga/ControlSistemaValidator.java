package ar.com.gestionit.web.validator.carga;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.ControlSistema;

@Component
public class ControlSistemaValidator implements Validator {

	@Override
	public boolean supports(Class<?> cls) {
		return ControlSistema.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		ControlSistema entity = (ControlSistema) bean;
		if ("".equals(entity.getNombreBanco()) || entity.getNombreBanco() == null) {
			errors.rejectValue("nombreBanco", "campo_obligatorio");
		}

		if (entity.getNumeroBanco() == null) {
			errors.rejectValue("numeroBanco", "campo_obligatorio");
		}

		if ("".equals(entity.getDireccionAvisoEmail()) || entity.getDireccionAvisoEmail() == null) {
			errors.rejectValue("direccionAvisoEmail", "campo_obligatorio");
		}

		if ("".equals(entity.getCodigoSucursalCentral()) || entity.getCodigoSucursalCentral() == null) {
			errors.rejectValue("codigoSucursalCentral", "campo_obligatorio");
		}
	}
}
