package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.service.seguridad.GrupoPuntoMenuService;
import ar.com.gestionit.web.dto.carga.GruposPuntoMenuDto;

@Component
public class GrupoPuntoMenuValidator implements Validator {

	@Autowired
	GrupoPuntoMenuService service;

	@Override
	public boolean supports(Class<?> cls) {
		return GruposPuntoMenuDto.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
	}
}
