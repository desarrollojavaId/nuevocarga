package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.seguridad.Grupo;
import ar.com.gestionit.service.seguridad.GrupoService;

@Component
public class GrupoValidator  implements Validator{

	
	@Autowired
	GrupoService service;
	
	@Override
	public boolean supports(Class<?> cls) {
		return Grupo.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		Grupo entity = (Grupo) bean;
		Long count = 0L;
		if (entity.getId() == null) {
			count = service.countExisteGrupo(entity.getNombre());
		}
		else {
			count = service.countExisteGrupoById(entity.getNombre(), entity.getId());
		}
		
		if (count != 0L) {
			errors.rejectValue(null, "grupo.existeNombre");
		}
		
	}	

}