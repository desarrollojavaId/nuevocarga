package ar.com.gestionit.web.validator.carga;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.MsTablas;

@Component
public class MsTablasValidator  implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return MsTablas.class.isAssignableFrom(clazz); 	
		
	}

	@Override
	public void validate(Object target, Errors errors) {
		MsTablas mstablas = (MsTablas) target;
		
		if(mstablas.getId().getTabla() == null){
			errors.rejectValue("tabla", "campo_obligatorio");	
		}
		if(mstablas.getId().getCodigo() == null){
			errors.rejectValue("codigo", "campo_obligatorio");	
		}
		if(mstablas.getId().getSubCodigo() == null){
			errors.rejectValue("subCodigo", "campo_obligatorio");	
		}
		
		if("".equals(mstablas.getAutomaticaMananual())){
			errors.rejectValue("automaticaMananual", "campo_obligatorio");	
		}
				
		if("".equals(mstablas.getDescripcion())){
			errors.rejectValue("descripcion", "campo_obligatorio");	
		}
		
	}

	
}
