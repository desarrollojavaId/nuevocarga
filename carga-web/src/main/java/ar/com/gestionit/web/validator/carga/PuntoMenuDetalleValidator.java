package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.seguridad.PuntoMenuDetalle;
import ar.com.gestionit.service.seguridad.PuntoMenuService;

@Component
public class PuntoMenuDetalleValidator implements Validator {

	@Autowired
	PuntoMenuService service;

	@Override
	public boolean supports(Class<?> cls) {
		return PuntoMenuDetalle.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		PuntoMenuDetalle entity = (PuntoMenuDetalle) bean;
		Long count = 0L;
		
		if("".equals(entity.getDescripcionPuntoMenu()) || entity.getDescripcionPuntoMenu() == null) {
			errors.rejectValue("descripcionCortaProducto", "campo_obligatorio");
		}
		if(entity.getOrden() == null) {
			errors.rejectValue("orden", "campo_obligatorio");
		}	
		
		if (entity.getId() == null) {
			count = service.findExistOrder(entity.getOrden());
		}
		else {
			count = service.findExistOrder(entity.getOrden(), entity.getId());
		}
		if(count>0) {
			errors.rejectValue("orden", "puntoMenu.ordenExistente");
		}
		
		
	}
}
