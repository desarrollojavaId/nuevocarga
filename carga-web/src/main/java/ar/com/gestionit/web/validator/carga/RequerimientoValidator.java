package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.Requerimiento;
import ar.com.gestionit.service.carga.RequerimientoService;


@Component
public class RequerimientoValidator  implements Validator{
	@Autowired 
	private RequerimientoService service;

	@Override
	public boolean supports(Class<?> cls) {
		return Requerimiento.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		
		Requerimiento entity = (Requerimiento) bean;
	//	if(entity.isValida()) {
			Long countNuss = 0L;
			 /*
			if ("".equals(entity.getDescripcion()) || entity.getDescripcion() == null) {
				errors.rejectValue("descripcion", "campo_obligatorio");
			}
			*/
			Long id = entity.getId() != null ? entity.getId() : new Long(0);
			
			if(id>0)
				countNuss = service.countExisteNuss(id, entity.getNuss());
			else
				countNuss = service.countExisteNuss(entity.getNuss());
			
			if(countNuss>0)
				errors.rejectValue("nuss", "requerimiento.nussExistente");		
	}	
}
