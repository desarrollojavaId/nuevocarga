package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.Sector;
import ar.com.gestionit.service.carga.SectorService;


@Component
public class SectorValidator  implements Validator{
	@Autowired 
	private SectorService service;

	@Override
	public boolean supports(Class<?> cls) {
		return Sector.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		
		Sector entity = (Sector) bean;
		if(entity.isValida()) {
			Long countDescripcion = 0L;
			 
			if ("".equals(entity.getDescripcion()) || entity.getDescripcion() == null) {
				errors.rejectValue("descripcion", "campo_obligatorio");
			}
			
			Long id = entity.getId() != null ? entity.getId() : new Long(0);
			
			if(id>0)
				countDescripcion = service.countExisteDescripcion(id, entity.getDescripcion());
			else
				countDescripcion = service.countExisteDescripcion(entity.getDescripcion());
			
			if(countDescripcion>0)
				errors.rejectValue("descripcion", "sector.descripcion_existente");
		}
		
	}	
}
