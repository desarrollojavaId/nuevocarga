package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.UsuCliente;
import ar.com.gestionit.service.carga.UsuClienteService;


@Component
public class UsuClienteValidator  implements Validator{
		
	@Autowired 
	private UsuClienteService service;
	
	@Override
	public boolean supports(Class<?> cls) {
		return UsuCliente.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		
		UsuCliente entity = (UsuCliente) bean;
		
//		if(entity.isValida()) {
			Long countUsuario = 0L;
			
			
			Long id = entity.getId() != null ? entity.getId() : new Long(0);
			
			if(id>0)
				countUsuario = service.countExisteUsuClienteId(id, entity.getUsuario().getId());
			else
				countUsuario = service.countExisteUsuCliente(entity.getUsuario().getId());
			
			if(countUsuario>0)
				errors.rejectValue("usuario", "usuCliente.usuario_existente");
//		}
		
	}	
}
