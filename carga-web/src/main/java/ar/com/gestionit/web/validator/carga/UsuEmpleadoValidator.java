package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.carga.UsuEmpleado;
import ar.com.gestionit.service.carga.UsuEmpleadoService;


@Component
public class UsuEmpleadoValidator  implements Validator{
		
	@Autowired 
	private UsuEmpleadoService service;
	
	@Override
	public boolean supports(Class<?> cls) {
		return UsuEmpleado.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		
		UsuEmpleado entity = (UsuEmpleado) bean;
		
//		if(entity.isValida()) {
			Long countUsuario = 0L;
			
			
			Long id = entity.getId() != null ? entity.getId() : new Long(0);
			
			if(id>0)
				countUsuario = service.countExisteUsuEmpleadoId(id, entity.getUsuario().getId());
			else
				countUsuario = service.countExisteUsuEmpleado(entity.getUsuario().getId());
			
			if(countUsuario>0)
				errors.rejectValue("usuario", "usuEmpleado.usuario_existente");
//		}
		
	}	
}
