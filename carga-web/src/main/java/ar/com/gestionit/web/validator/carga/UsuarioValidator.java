package ar.com.gestionit.web.validator.carga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ar.com.gestionit.model.seguridad.Usuario;
import ar.com.gestionit.service.seguridad.UsuarioService;


@Component
public class UsuarioValidator  implements Validator{
	@Autowired 
	private UsuarioService service;

	@Override
	public boolean supports(Class<?> cls) {
		return Usuario.class.isAssignableFrom(cls);
	}

	@Override
	public void validate(Object bean, Errors errors) {
		
		Usuario entity = (Usuario) bean;
//		if(entity.isValida()) {
			Long countNombreUsuario = 0L;
			Long countInicialesUsuario = 0L;
			 
			if ("".equals(entity.getNombreUsuario()) || entity.getNombreUsuario() == null) {
				errors.rejectValue("nombreUsuario", "campo_obligatorio");
			}
			
			Long id = entity.getId() != null ? entity.getId() : new Long(0);
			
			if(id>0)
				countNombreUsuario = service.countExisteNombreUsuario(id, entity.getNombreUsuario());
			else
			   countNombreUsuario = service.countExisteNombreUsuario(entity.getNombreUsuario());
			
			if(countNombreUsuario>0)
				errors.rejectValue("nombreUsuario", "usuario.nombreUsuario_existente");
			
			if(id>0)
				countInicialesUsuario = service.countExisteInicialesUsuario(id, entity.getIniciales());
			else
				countInicialesUsuario = service.countExisteInicialesUsuario(entity.getIniciales());
			
			if(countInicialesUsuario>0)
				errors.rejectValue("iniciales", "usuario.iniciales_existente");
//		}
		
	}	
}

