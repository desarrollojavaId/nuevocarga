<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="puntoMenu.categoria" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			window.actionEvents = {
				'click .edit' : function(e, value, row, index) {
					window.location = '<c:url value="/puntoMenuCategoria/edit/"/>' + row.id;
				},
				'click .remove' : function(e, value, row, index) {
					window.location = '<c:url value="/puntoMenuCategoria/remove/"/>' + row.id;
				},
				'click .ver' : function(e, value, row, index) {
					window.location = '<c:url value="/puntoMenuCategoria/show/"/>' + row.id ;
				},
				'click .detalle' : function(e, value, row, index) {
					window.location = '<c:url value="/puntoMenuDetalle/showPuntoMenu/"/>' + row.id ;
				}
			};
			$(function() {
				$("#btn_buscar").click(function() {
				$("#buscar_form").serialize();
					var path = '<c:url value="/puntoMenuCategoria/records"/>' + '?'+ $("#buscar_form").serialize() ;
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				
				$("#btn_limpiar").click(function() {
					$("#categoriaComision").val("")
					$("#descripcion").val("")
			        var path = '<c:url value="/puntoMenuCategoria/records"/>';
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				//Funcion para tomar el "enter" para aplicar los filtros
				filtrarEnter('btn_buscar');
				$("#catPuntoMenu").comboBoxPaged(puntoMenuUrl, puntoMenuIdUrl, funcionCatPuntoMenu);
			});
		</script>
		
		<!-- Campos de filtrado -->
		<div class="box box-primary">
		  <div class="box-header">
		  </div> 
		  <div class="box-body">
		    <form id="buscar_form" class="form-horizontal">
			   <div class="row">
				 <div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="puntoMenu.catPuntoMenu" />
	                </label>
	                <div class="col-sm-4">
	                   <input name="catPuntoMenu" id="catPuntoMenu" class="form-control" >
                    </div>  
				  </div>
				 </div>
				
			   </div>
			</form>
		  </div>
		  <div class="box-footer">
			<button type="button" id="btn_buscar" class="btn btn-info pull-right">
			  <spring:message code="buscar" />
			</button>

			<button type="button" id="btn_limpiar" class="btn btn-info pull-left">
			  <spring:message code="limpiar" />
			</button>
		  </div> 
		</div>	
		
		<!-- Detalle -->
		<div class="box box-primary">
		  <div id="toolbar" class="pull-right">
            <git:refresh />
            <a href="<c:url value="/puntoMenuCategoria/add"/>" class="btn btn-default">
              <i class="fa fa-plus"></i>
            </a>
          </div>
          <div class="box-header">
          </div> 
          <div class="box-body no-padding">
            <git:table dataUrl="/puntoMenuCategoria/records" id="table" checkbox="false"
                 checkboxHiddenSelector="#seleccion_actual" showSearch="false" toolbarSelector="#toolbar" ignoreColumnsExport="[2]">

               <th data-field="orden" data-sortable="true">
                  <spring:message code="puntoMenu.orden" />
               </th>
            
               <th data-field="descripcionPuntoMenu" data-sortable="true">
                  <spring:message code="puntoMenu.descPuntoMenu" />
               </th>

               <!-- Botones -->
               
               <th data-field="action" data-align="center" data-formatter="actionFormatterGridData" data-editar="true"
                   data-eliminar="true" data-show="true"  data-detalle="true" data-info="'<spring:message code="puntoMenu.puntoMenu" />'" data-events="actionEvents">
               </th>
            </git:table>
          </div>
          <div class="box-footer">
          </div>
       </div>
	</tiles:putAttribute>
</tiles:insertDefinition>