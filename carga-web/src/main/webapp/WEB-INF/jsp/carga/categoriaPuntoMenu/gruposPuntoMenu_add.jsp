<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="grupoPuntoMenu" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<script type="text/javascript">
		   //
		   var datos;
		   var idIndex;
		   var datosNuevos = false;
		   //Agrega nueva fila a la grilla
		   function agregarFila(element,idIndex){
			   var puntoMenuCategoria = "";
               //**Se comenta por los momentos todo lo referente al nombre alternativo de la cabecera menu			   
			   // var cabeceraNomAlternativo = "";
			   var puntoMenu;
			   var puntoMenuNomAlternativo = "";
			   var id;
			   if(element != null){
				 puntoMenuCategoria = element.puntoMenuCategoria;
	             //**Se comenta por los momentos todo lo referente al nombre alternativo de la cabecera menu				 
				 // cabeceraNomAlternativo = element.cabeceraNomAlternativo;
				 puntoMenu = element.puntoMenu;
				 puntoMenuNomAlternativo = element.puntoMenuNomAlternativo;
				 id= element.id;
			   }
				//Se arma un arreglo de objeto con las configuraciones de los inputs
               //**Se comenta por los momentos todo lo referente al nombre alternativo de la cabecera menu
				var nuevoItemTd = [ {
					id: idIndex + '-pmenuC',
					name : "puntoMenuCategoria",
					data : puntoMenuCategoria
				}/*, {
					id: idIndex + '-pmenuCAlt',
					name : "cabeceraNomAlternativo",
					data : cabeceraNomAlternativo
				}*/, {
					id: idIndex + '-pmenu',
					name : "puntoMenu",
					data : puntoMenu
				}, {
					id: idIndex + '-pmenuAlt',
					name : "puntoMenuNomAlternativo",
					data : puntoMenuNomAlternativo
				} , {
					id: idIndex + '-id',
					name : "id",
					data : id,
					type : "hidden"
				} ];
				grillaDinamica('tabla_dinamica', idIndex, nuevoItemTd);
				
				//Redefino los eventos de los componentes de la grilla
				//Combo de categor�a de puntos de menu
				$("#" + idIndex +"-pmenuC").comboBoxPaged(puntoMenuUrl,puntoMenuIdUrl,funcionCatPuntoMenu);
				$("#" + idIndex +"-pmenuC").change(function() {
					$("#" + idIndex +"-pmenu").val("");
					$("#" + idIndex +"-pmenuCAlt,#" + idIndex +"-pmenu").attr('readonly', false);
					$("#" + idIndex +"-pmenu").comboBoxPaged(puntoMenuDetalleUrl + $("#" + idIndex +"-pmenuC").val(),puntoMenuDetalleIdUrl,funcionCatPuntoMenu);
					refrescaCamposDinamicos('camposDinamicos', true, 'puntoMenu');
				});
				//mantengo actualizado los valores en el json para el set de datos a enviar
				//Se comenta por los momentos todo lo referente al nombre alternativo de la cabecera menu
				/*$("#" + idIndex +"-pmenu").change(function() {
					$("#" + idIndex +"-pmenuAlt").attr('readonly', false);
					refrescaCamposDinamicos('camposDinamicos', true, 'puntoMenu');					
				});*/
				
				//Le asigno a los dos campos el evento onchange para que refresque la tabla al cambiar el componente
				$("#" + idIndex +"-pmenuCAlt,#" + idIndex +"-pmenuAlt").change(function() {
					refrescaCamposDinamicos('camposDinamicos', true, 'puntoMenu');
				});
				
				$("#" + idIndex +"-pmenuAlt,#" + idIndex + "-pmenu").change(function() {
					refrescaCamposDinamicos('camposDinamicos', true, 'puntoMenu');
				});
				//se agrega el id para generar un registro vacio y convertir el input en combo
				if($("#" + idIndex +"-pmenuC").val() == ""){
					customSelect2(idIndex +"-pmenu");
					
				}
				//En caso que no sea la primera vez que accedas, busco el item 
				else{
					$("#" + idIndex +"-pmenu").comboBoxPaged(puntoMenuDetalleUrl + $("#" + idIndex +"-pmenuC").val(),puntoMenuDetalleIdUrl,funcionCatPuntoMenu);
				}

				$("#" + idIndex +"-pmenuAlt").attr('required', false);

				//Se comenta por los momentos todo lo referente al nombre alternativo de la cabecera menu
				// $("#" + idIndex +"-pmenuCAlt,#" + idIndex +"-pmenuAlt").attr('required', false);

			}
		   $(function() {
			    registerParsley();
				//
				$("#agregarNuevo").click(function(){
					datosNuevos = true;
					idIndex = $(".item").length + 1;
					agregarFila(null,idIndex);
					return false;
				});
				//
				if($("#camposDinamicos").val() != "" && $("#camposDinamicos").val() != null){
					var camposDinamico = $.parseJSON($("#camposDinamicos").val());
					$.each(camposDinamico,function(index, element) {
						idIndex = index + 1;
						agregarFila(element,idIndex);
					});	
				}
				$("#agregarNuevo").hide();
				$("#cboGrupo").comboBoxPaged(grupoUrl,grupoIdUrl);
				//Actualizo el campo oculto con el valor seleccionado en el combo de grupos
				$("#cboGrupo").change(function() {
					//var propiedadOrdenar = 'puntoMenuCategoria'; 
					$("#grupo").val($("#cboGrupo").val());
					if($("#grupo").val() != ""){
						$("#agregarNuevo").show()
					}
					else{
						$("#agregarNuevo").hide();
					}
							
					var path = '<c:url value="/grupoPuntoMenu/records"/>' + '?grupo='+ $("#grupo").val() ;
					$("#tabla_dinamica").find("tr:gt(0)").remove();
					$.get(path,function(data){
						$("#camposDinamicos").val(data);
						var camposAsociar = $.parseJSON(data);
						camposAsociar.sort(ordenar('orden', 'ordenDetalle'));
						$.each(camposAsociar,function(index, element) {
							idIndex = index + 1;
							agregarFila(element,idIndex);
						});	
					});
					
				});				
			    //Selecciono el valor
			    $("#cboGrupo").val($("#grupo").val());
			    $("#cboGrupo").change();
			});
		</script>
		<!-- Campos de cabecera -->
		<div class="box box-primary">
		<div class="box-header">
		</div> 
			<div class="box-body">			 
			  <div class="row">
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="grupoPuntoMenu.grupo" />
                    </label>
                    <div class="col-sm-4">
                       <input name="cboGrupo" id="cboGrupo" class="form-control" >
					</div>  
				  </div>
				</div>
				
			  </div>
			</div>
			<div class="box-footer">
			</div>
		</div>	
		
		<!-- Detalle -->
		<div class="box box-primary">
          <div class="box-header ">
            <a href="#" id="agregarNuevo" class="btn btn-default pull-right">
              <i class="fa fa-plus"></i>
            </a>
          </div> 
          <div class="box-body no-padding">
            <git:form action_url="/grupoPuntoMenu/save" parsley="true" id="edit_form" modelAttribute="entity">
                <form:hidden path="camposDinamicos" />
                <form:hidden path="grupo" />
                <div class="form-group">
	              <div class="col-sm-offset-2 col-sm-4" style="padding: 12px;">
		            <form:errors path="*" cssClass="alert alert-danger" element="div" role="alert" />
	              </div>
                </div>
				<table data-toggle="table">
				  <thead>
					<tr>
					  <th><spring:message code="grupoPuntoMenu.cabecera"/></th>
					  <!-- <th><spring:message code="grupoPuntoMenu.nombreAltCab"/></th> -->
					  <th><spring:message code="grupoPuntoMenu.item"/></th>
					  <th><spring:message code="grupoPuntoMenu.nombreAltItem"/></th>
					</tr>
				  </thead>
				  <tbody id="tabla_dinamica">
				    <tr>
				    </tr>
				  </tbody>
				</table>
		        <div class="box-footer">
		         <c:if test="${mostrarBackUrl}">
			      <a href="<c:url value="/grupo"/>" id="cancel_btn" class="btn btn-default"> 
			       <spring:message code="back" />
			      </a>
			     </c:if>	
		         <button type="button" id="submit_btn"  class="btn btn-info pull-right parsley-save">
				    <spring:message code="save" />
			      </button>			      		     
		        </div>				
			</git:form>
          </div>
       </div>
	</tiles:putAttribute>
</tiles:insertDefinition>