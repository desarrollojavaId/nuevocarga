<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="puntoMenu.detalle" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
				}
				else if("${borrar}" == "true"){
				    //Asigno a todos los campos como solo lectura
					readOnlyForShow('edit_form');
					$("#submit_btn").text('<spring:message code="eliminar" />')
					//
					$("#submit_btn").click(function(event){							
					   //Elimina la acci�n click pre definida del input
					   event.preventDefault();
					   //
					   remove(null, '<c:url value="/puntoMenuDetalle/eliminar/"/>','<c:url value="/puntoMenuDetalle/showPuntoMenu/"/>' + $("#categoriaPuntoMenuId").val() ,$("#id").val());					   
					 
					});
				}
				else{
					readOnlyForShow('edit_form');
					$("#submit_btn").hide();
				}
			});			
		</script>
		<git:form action_url="/puntoMenuDetalle/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<form:hidden path="categoriaPuntoMenuId" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						<spring:message code="${titulo}" />
					</h3>
				</div>

				<div class="box-body">
					<git:errors />

					<git:field label="puntoMenu.orden">
						<git:number nombre_campo="orden" decimal="false" path="true" require="true" />
					</git:field>

					<git:field label="puntoMenu.descPuntoMenu">
						<form:input path="descripcionPuntoMenu" required="true" cssClass="form-control"  data-parsley-maxlength="50" />
					</git:field>

					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>
					
					<div class="box-footer">
						<a href="<c:url value="/puntoMenuDetalle/showPuntoMenu/${entity.categoriaPuntoMenuId.id}"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>
