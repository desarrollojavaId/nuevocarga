<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="puntoMenu.detalle" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<script type="text/javascript">
		   //
		   var datos;
		   var idIndex;
		   var datosNuevos = false;
		   //Agrega nueva fila a la grilla
		   function agregarFila(element,idIndex){
			   var descripcionPuntoMenu = "";
			   var id;
			   var orden;
			   var puntoMenuUrlId = "";
			   
			   
				if(element != null){
					descripcionPuntoMenu = element.descripcionPuntoMenu;
					id= element.id;
					orden = element.orden;
					puntoMenuUrlId = element.puntoMenuUrlId;
				}else{
					orden = idIndex; 
				}
			  
				//Se arma un arreglo de objeto con las configuraciones de los inputs
				var nuevoItemTd = [ {
					id: idIndex + '-pmenuUrl',
					name : "puntoMenuUrlId",
					data : puntoMenuUrlId
				}, {
					id: idIndex + '-pmenu',
					name : "descripcionPuntoMenu",
					data : descripcionPuntoMenu
				} , {
					id: idIndex + '-ord',
					name : "orden",
					data : orden
				}  ,{
					id: idIndex + '-id',
					name : "id",
					data : id,
					type : "hidden"
				}
				
				];
				grillaDinamica('tabla_dinamica', idIndex, nuevoItemTd);
				
				//AGREGADO
				$("#"+ idIndex).append('<td width="10"><i class="btn btn-default fa fa-ellipsis-v"></i></td>');
				
				//Redefino los eventos de los componentes de la grilla
				//Combo de categor�a de puntos de menu
				$("#" + idIndex +"-pmenuUrl").comboBoxPaged(urlPuntoMenuUrl,urlPuntoMenuIdUrl,funcionPuntoMenuUrl);
				
				$("#" + idIndex +"-pmenuUrl").change(function() {
					refrescaCamposDinamicos("camposDinamicos",true,"puntoMenuUrlId");
				});
				$("#" + idIndex +"-ord").attr("readonly", true);
				
				$("#" + idIndex +"-pmenu").change(function() {
					refrescaCamposDinamicos("camposDinamicos",true,"puntoMenuUrlId");
				});
				
				
			}
		   $(function() {
			   registerParsley();
				//
				$("#agregarNuevo").click(function(){
					datosNuevos = true;
					idIndex = $(".item").length + 1;
					agregarFila(null,idIndex);
					return false;
				});
				
				//Actualizo el campo oculto con el valor seleccionado en el combo de grupos
				if($("#camposDinamicos").val() != "" && $("#camposDinamicos").val() != null){
					var camposAsociar = $.parseJSON($("#camposDinamicos").val());
					$.each(camposAsociar,function(index, element) {
						idIndex = index + 1;
						agregarFila(element,idIndex);
					});		
				}
			
			 //habilita el drag and drop en la grilla de pasos
			$("#tabla_dinamica").sortable({
				//Funcion que se activa antes de hacer el drop del item en suite de preuba
				stop:function(evt,ui){
				  var data_id = ui.item.attr('data-id');
					$('.item').each(function(index, element) {
						var idTems = element.getAttribute("data-id");
						$("#" + idTems + "-ord").val(index + 1);
					});
					refrescaCamposDinamicos("camposDinamicos",true,"puntoMenuUrlId");
				}
			}).disableSelection();
			$("#tabla_dinamica").droppable({});
			
			
			});
		</script>
		<!-- Campos de cabecera -->
		<div class="box box-primary">
		<div class="box-header">
		</div> 
			<div class="box-body">			 
			  <div class="row">
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="puntoMenu.catPuntoMenu" />
                    </label>
                    <div class="col-sm-4">
                       <input name="categoria" value="${categoria}" id="categoria" readonly="readonly" class="form-control" >
					</div>  
				  </div>
				</div>
				
			  </div>
			</div>
			<div class="box-footer">
			</div>
		</div>	
		
		<!-- Detalle -->
		<div class="box box-primary">
          <div class="box-header ">
            <a href="#" id="agregarNuevo" class="btn btn-default pull-right">
              <i class="fa fa-plus"></i>
            </a>
          </div> 
          <div class="box-body no-padding">
            <git:form action_url="/puntoMenuDetalle/save" parsley="true" id="edit_form" modelAttribute="entity">
                <form:hidden path="camposDinamicos" />
                <form:hidden path="puntoMenuCategoria" />
                <div class="form-group">
	              <div class="col-sm-offset-2 col-sm-4" style="padding: 12px;">
		            <form:errors path="*" cssClass="alert alert-danger" element="div" role="alert" />
	              </div>
                </div>
				<table data-toggle="table">
				  <thead>
					<tr>
					  <th><spring:message code="puntoMenu.Url"/></th>
					  <th><spring:message code="puntoMenu.nombrePuntoMenu" /></th>					  
					  <th><spring:message code="puntoMenu.orden"/></th>
					</tr>
				  </thead>
				  <tbody id="tabla_dinamica">
				    <tr>
				    </tr>
				  </tbody>
				</table>
		        <div class="box-footer">	
		         <button type="button" id="submit_btn"  class="btn btn-info pull-right parsley-save">
				    <spring:message code="save" />
			      </button>
			      <a href="<c:url value="/puntoMenuCategoria"/>" id="cancel_btn"
				class="btn btn-default"> <spring:message code="back" />
			</a>			      		     
		        </div>				
			</git:form>
          </div>
       </div>
	</tiles:putAttribute>
</tiles:insertDefinition>