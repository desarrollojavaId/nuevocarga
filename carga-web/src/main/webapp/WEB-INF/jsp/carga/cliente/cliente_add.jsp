<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title></git:title>
	
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				$("#cbuCuentaDebito").attr("maxlength", 22);
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
					if("${edit}" == "true" && $("#id").val() === ""){
						popup('<spring:message code="objecto_no_existe" />');
					}
				}
				else if("${borrar}" == "true"){
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						//Asigno a todos los campos como solo lectura
						readOnlyForShow('edit_form');
						$("#submit_btn").text('<spring:message code="Eliminar" />')
						//
						$("#submit_btn").click(function(event){							
							//Elimina la acci�n click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acci�n
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que ser� eliminado.
							remove(null, '<c:url value="/cliente/eliminar/"/>','<c:url value="/cliente/"/>',$("#id").val());
						});
					}
				}
				else{
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						readOnlyForShow('edit_form');					
						$("#submit_btn").hide();
					}
				}
				
			});			
		</script>
		<git:form action_url="/cliente/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					<git:errors />
					
					<git:field label="cliente.razonSocial">
						<form:input path="razonSocial" required="true" cssClass="form-control"  maxlength="100" />
					</git:field>
					
					<git:field label="cliente.cuit">
						<form:input path="cuit" required="true" data-parsley-cuit="true" maxlength="11" cssClass="form-control" data-parsley-cuit-message="Introduzca un CUIT v�lido"  />						
					</git:field>
					
					<git:field label="cliente.direccionReal">
						<form:input path="direccionReal" cssClass="form-control"  maxlength="100" />
					</git:field>
					
					<git:field label="cliente.direccionFiscal">
						<form:input path="direccionFiscal" cssClass="form-control"  maxlength="100" />
					</git:field>
					
					<git:field label="cliente.t02SituacionFiscal">
					     <git:mstabla tabla="002" subCodigo="null" codigo="null" nombre_campo="t02SituacionFiscal" path="t02SituacionFiscal" value="${entity.t02SituacionFiscal}" />
					</git:field>

					
					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>

					
					<div class="box-footer">
						<a href="<c:url value="/cliente/filtros"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

