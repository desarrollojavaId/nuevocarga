<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title isList="true"></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			var removeUrl = '<c:url value="/cliente/remove/"/>';
			window.actionEvents = {
				'click .edit' : function(e, value, row, index) {
					window.location = '<c:url value="/cliente/edit/"/>' + row.id;
				},
				'click .remove' : function(e, value, row, index) {
					window.location = '<c:url value="/cliente/remove/"/>' + row.id;
				},
				'click .ver' : function(e, value, row, index) {
					window.location = '<c:url value="/cliente/show/"/>' + row.id ;
				},
			};
			$(function() {
				$("#btn_buscar").click(function() {
				$("#edit_forb").serialize();
					var path = '<c:url value="/cliente/records"/>' + '?'+ $("#buscar_form").serialize() + "&mantenerfiltros=true" ;
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				
				$("#btn_limpiar").click(function() {
					limpiarFiltrosSession('razonSocialSession,cuitSession');
					$("#razonSocial").val("")
					$("#cuit").val("")
			        var path = '<c:url value="/cliente/records"/>';
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				//Funcion para tomar el "enter" para aplicar los filtros
				filtrarEnter('btn_buscar');
			});
		</script>
		
		<!-- Campos de filtrado -->
		<div class="box box-primary">
		<div class="box-header">
		</div> 
			<div class="box-body">
			  <form id="buscar_form" class="form-horizontal">
			  <div class="row">
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="cliente.cliente" />
                    </label>
                    <div class="col-sm-4">
                       <input name="razonSocial" id="razonSocial" value="${sessionScope.razonSocialSession}" class="form-control" >
					</div>  
				  </div>
				</div>
				
			  </div>
			  <div class="row">
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4"> 
				       <spring:message code="cliente.cuit" />
                    </label>
                    <div class="col-sm-8">
                       <input name="cuit" id="cuit" value="${sessionScope.cuitSession}" maxlength="11" class="form-control" >
					</div>  
				  </div>
				</div>
				
				
			  </div>	
			  </form>
			</div>
			<div class="box-footer">
			  <button type="button" id="btn_buscar" class="btn btn-info pull-right">
				<spring:message code="buscar" />
			  </button>

			  <button type="button" id="btn_limpiar" class="btn btn-info pull-left">
				<spring:message code="limpiar" />
			  </button>
			</div>
		</div>	
		
		<!-- Detalle -->
		<div class="box box-primary">
			<div id="toolbar" class="pull-right">
				<git:refresh />
				<a href="<c:url value="/cliente/add"/>" class="btn btn-default">
					<i class="fa fa-plus"></i>
				</a>
			</div>

			<div class="box-header">
			</div> 
			<div class="box-body no-padding">
				<git:table dataUrl="/cliente/records?tomarFiltrosSession=${tomarFiltrosSession}" id="table" checkbox="false"
					checkboxHiddenSelector="#seleccion_actual" showSearch="false"
					toolbarSelector="#toolbar" ignoreColumnsExport="[3]">
							<th data-field="razonSocial" data-sortable="true" >
								<spring:message code="cliente.cliente" />
							</th>
							<th data-field="cuit" data-sortable="true">
								<spring:message code="cliente.cuit" />
							</th>
							
							<!-- Botones -->
							<th data-field="action" data-align="center"
								data-formatter="actionFormatterGridData" data-editar="true"
								data-eliminar="true" data-show="true" data-events="actionEvents"></th>
				</git:table>
				
			</div>
			<div class="box-footer">
			
			</div>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>