<%@ page language="java" contentType="text/html; charset=ISO-8859-2" pageEncoding="ISO-8859-2" %>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
					if("${edit}" == "true" && $("#id").val() === ""){
						popup('<spring:message code="objecto_no_existe" />');
					}
				}
				else if("${borrar}" == "true"){
					//Detengo el submit del formulario (no deber�a forzarlo, pero no hab�a otra manera)
					$("#edit_form").submit(function(e){
						e.preventDefault();
					});
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						//Asigno a todos los campos como solo lectura
						readOnlyForShow('edit_form');
						$("#submit_btn").text('<spring:message code="Eliminar" />')
						
						$("#submit_btn").click(function(event){
							//Elimina la acci�n click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acci�n
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que ser� eliminado.
							remove(null, '<c:url value="/contrato/eliminar/"/>','<c:url value="/contrato/"/>',$("#id").val());
						});
						
						//
					}
				}
				else{
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						readOnlyForShow('edit_form');					
						$("#submit_btn").hide();
					}
				}
				
				
				customSelect2('renovable', [ {
			        "id": "SI",
			        "text": "SI"
			      }, {
			        "id": "NO",
			        "text": "NO"
			      } ]);

				$("#cliente").comboBoxPaged(clienteUrl, clienteIdUrl, funcionClienteCuit);
				
			});
			
		</script>
		<git:form action_url="/contrato/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					<git:errors />
					
					
					<git:field label="contrato.nombre">
						<form:input path="nombre" required="true" cssClass="form-control"  maxlength="100" />
					</git:field>
					
					<git:field label="contrato.descripcion">
						<form:input path="descripcion" required="true" cssClass="form-control"  maxlength="255" />
					</git:field>
										
					<git:field label="contrato.cliente">
						<form:input path="cliente" required="true" cssClass="form-control"/>					
					</git:field>
					
					<git:field label="contrato.contacto1">
						<form:input path="contacto1" cssClass="form-control"  maxlength="255" />
					</git:field>
					
					<git:field label="contrato.contacto2">
						<form:input path="contacto2" cssClass="form-control"  maxlength="255" />
					</git:field>
					
					<git:field label="contrato.t03Categoria">
						<git:mstabla tabla="003" require="true" codigo="null" subCodigo="null" nombre_campo="t03Categoria" path="t03Categoria" value="${entity.t03Categoria}" />
					</git:field>
					
					<git:field label="contrato.horasAbono">
						<git:number nombre_campo="horasAbono" path="true" decimal="false"  max="5" id_campo="horasAbono"/>					
					</git:field>
										
					<git:field label="contrato.renovable">
						<form:input path="renovable" required="true" cssClass="form-control"  maxlength="2" />
					</git:field>
										
					<git:field label="contrato.puntoAviso">
						<git:number nombre_campo="puntoAviso" path="true" decimal="false" max="5" id_campo="puntoAviso"/>					
					</git:field>
					
					<git:field label="contrato.fecInicio">
						<form:input type="date" path="fecInicio" id="fecInicio" required="true" cssClass="form-control"/>
						<div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="fecInicio" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>						
					</git:field>
					
					<git:field label="contrato.fecFinal">
						<form:input type="date" path="fecFinal" id="fecFinal" required="true" cssClass="form-control"/>
						<div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="fecFinal" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>						
					</git:field>
					
					<git:field label="contrato.fecCargaHorasDesde">
						<form:input type="date" path="fecCargaHorasDesde" id="fecCargaHorasDesde" required="true" cssClass="form-control"/>
						<div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="fecCargaHorasDesde" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>						
					</git:field>
					
					<git:field label="contrato.fecCargaHorasHasta">
						<form:input type="date" path="fecCargaHorasHasta" id="fecCargaHorasHasta" required="true" cssClass="form-control"/>
						<div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="fecCargaHorasHasta" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>						
					</git:field>
					
	
					
					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>

					
					<div class="box-footer">
						<a href="<c:url value="/contrato/filtros"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

