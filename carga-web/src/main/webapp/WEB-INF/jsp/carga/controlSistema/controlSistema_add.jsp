<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="controlSistema" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				registerParsley();
				$("#numeroBanco").attr("maxlength",10);
			});			
		</script>
		<git:form action_url="/controlSistema/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					
					<git:field label="controlSistema.numeroBanco">
					    <git:number nombre_campo="numeroBanco" path="true" require="true"/>
					    <div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="numeroBanco" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>
					</git:field>
					
					<git:field label="controlSistema.nombreBanco">
						<form:input path="nombreBanco" cssClass="form-control" required="true"  maxlength="28" />
					    <div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="nombreBanco" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>
					</git:field>
					
					<git:field label="controlSistema.direccionAvisoEmail">
						<form:input type="email" path="direccionAvisoEmail" required="true" maxlength="100" cssClass="form-control"/>
					    <div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="direccionAvisoEmail" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>
					</git:field>
					
					<git:field label="controlSistema.codigoSucursalCentral">
					    <git:number nombre_campo="codigoSucursalCentral" path="true" require="true"/>
					    <div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="codigoSucursalCentral" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>
					</git:field>

					<div class="box-footer">
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

