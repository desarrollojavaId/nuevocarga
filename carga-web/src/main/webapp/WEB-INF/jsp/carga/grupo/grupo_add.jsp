<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title></git:title>
	
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
				}
				else if("${borrar}" == "true"){
					//Asigno a todos los campos como solo lectura
					readOnlyForShow('edit_form');
					$("#submit_btn").text('<spring:message code="Eliminar" />')
					//
				//}else{					
					$("#submit_btn").click(function(event){							
							//Elimina la acci�n click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acci�n
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que ser� eliminado.
							remove(null, '<c:url value="/grupo/eliminar/"/>','<c:url value="/grupo/"/>',$("#id").val());
					});
				}
				else{
					readOnlyForShow('edit_form');					
					$("#submit_btn").hide();
				}
			});			
		</script>
		<git:form action_url="/grupo/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					<git:errors />
					
					<git:field label="grupo.nombre">
						<form:input path="nombre" required="true" cssClass="form-control"  maxlength="100" />
					</git:field>
					
					<git:field label="grupo.descripcion">
						<form:input path="descripcion" required="true" cssClass="form-control"  maxlength="100" />
					</git:field>
										
					<git:field label="usuario.t01Categoria">
						<git:mstabla tabla="001" require="true" codigo="null" subCodigo="null" nombre_campo="t01Categoria" path="t01Categoria" value="${entity.t01Categoria}" />
					</git:field>
					
					<git:field label="grupo.enabled">
						   <form:checkbox path="enabled"/>
					</git:field>
					
					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>

					
					<div class="box-footer">
						<a href="<c:url value="/grupo"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>