<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="${titulo}" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
				}
				else if("${borrar}" == "true"){
					//Asigno a todos los campos como solo lectura
					readOnlyForShow('edit_form');
					$("#submit_btn").text('<spring:message code="Eliminar" />')
					//
				//}else{					
					$("#submit_btn").click(function(event){							
							//Elimina la acci�n click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acci�n
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que ser� eliminado.
							remove(null, '<c:url value="/cliente/eliminar/"/>','<c:url value="/cliente/"/>',$("#id").val());
					});
				}
				else{
					readOnlyForShow('edit_form');					
					$("#submit_btn").hide();
				}
			});			
		</script>
		<git:form action_url="/cliente/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

