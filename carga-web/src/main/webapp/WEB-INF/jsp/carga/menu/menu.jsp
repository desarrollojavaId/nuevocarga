<%@ include file="/WEB-INF/jsp/include.jsp"%>

<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header"><spring:message code="app.menu"/></li>

			<c:forEach items="${lst}" var="itemCategoria">
					<ul class="sidebar-menu tree" data-widget="tree">
						<li class="treeview">
							  <a href="#">
					             <i class="fa fa-dashboard"></i> <span>${itemCategoria.getNombreCategoriaParaMostrar()}</span>
					            <span class="pull-right-container">
			  		               <i class="fa fa-angle-left pull-right"></i>
					            </span>
					          </a>
					          <ul class="treeview-menu" style="display: none;">
					            <c:forEach items="${itemCategoria.puntoMenuDetalles}" var="itemDetalle">
									 <li><a href="<c:url value="${itemDetalle.puntoMenuUrl.url}"/>"><i class="fa fa-circle-o"></i> ${itemDetalle.getNombreDetalleParaMostrar()}</a></li>
								</c:forEach>
					          </ul>
				        </li>	
					</ul>
			</c:forEach>
		
		</ul>
	</section>
	<!-- /.sidebar -->
				
		
</aside>