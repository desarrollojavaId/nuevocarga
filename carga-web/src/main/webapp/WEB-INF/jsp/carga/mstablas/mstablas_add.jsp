<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="mstablas" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			var fecha;
			$(function() {
				if("${edit}" == "true"){
					$("#tabla").attr('readonly',true)
					$("#codigo").attr('readonly',true)
					$("#subCodigo").attr('readonly',true)
					registerParsley();
				} else if("${create}" == "true"){					
					//te ejecuta el submit del formulario
					registerParsley();				
				} else if("${borrar}" == "true"){
					//Asigno a todos los campos como solo lectura
					readOnlyForShow('edit_form');
					$("#submit_btn").text('<spring:message code="Eliminar" />')
					//
					$("#submit_btn").click(function(event){							
						//Elimina la acci�n click pre definida del input
						event.preventDefault();
						//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
						//@param2=Url del controller que atiende la acci�n
						//@param3=Url donde va a redireccionar luego de ejecutar
						//@param4=Id del objeto que ser� eliminado.
						remove(null, '<c:url value="/mstablas/eliminar/"/>','<c:url value="/mstablas/"/>',$("#tabla").val() + "/" + $("#codigo").val() + "/" + $("#subCodigo").val());
					});
				}
				else{
					readOnlyForShow('edit_form');
					$("#submit_btn").hide();
				}
			});
		</script>
		<git:form action_url="/mstablas/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						<spring:message code="mstablas" />
					</h3>
				</div>

				<div class="box-body">
					<git:errors />
					
					<git:field label="mstablas.tabla" >
						<git:number id_campo="tabla" nombre_campo="id.tabla" path="true" require="true"></git:number>
					</git:field>
					<git:field label="mstablas.codigo" >						
						<git:number id_campo="codigo" nombre_campo="id.codigo" path="true" require="true"></git:number>
					</git:field>
					<git:field label="mstablas.subCodigo" >
						<git:number id_campo="subCodigo" nombre_campo="id.subCodigo" path="true" require="true"></git:number>
					</git:field>
					
					<git:field label="mstablas.descripcion" >
						<form:input path="descripcion" cssClass="form-control" required="required" data-parsley-maxlength="100" />
					</git:field>
					<git:field label="mstablas.descripcionReducida" >
						<form:input path="descripcionReducida" cssClass="form-control"  data-parsley-maxlength="30" />
					</git:field>
					<git:field label="mstablas.auxiliarUsoInterno1" >
						<form:input path="auxiliarUsoInterno1" cssClass="form-control"  data-parsley-maxlength="1" />
					</git:field>
					<git:field label="mstablas.auxiliarUsoInterno2" >
						<form:input path="auxiliarUsoInterno2" cssClass="form-control"  data-parsley-maxlength="20" />
					</git:field>
					<git:field label="mstablas.auxiliarUsoBanco" >
						<form:input path="auxiliarUsoBanco" cssClass="form-control"  data-parsley-maxlength="20" />
					</git:field>
					<git:field label="mstablas.automaticaMananual" >
						<form:input path="automaticaMananual" cssClass="form-control" required="required" data-parsley-maxlength="1" />
					</git:field>
					
					<div class="box-footer">
						<a href="<c:url value="/mstablas"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>
