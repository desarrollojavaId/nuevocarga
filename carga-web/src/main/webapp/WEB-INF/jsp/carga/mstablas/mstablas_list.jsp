<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="mstablas" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			var removeUrl = '<c:url value="/mstablas/remove/"/>';
			window.actionEvents = {
				'click .edit' : function(e, value, row, index) {
					window.location = '<c:url value="/mstablas/edit/"/>' + row.id.tabla + '/'  + row.id.codigo + '/' + row.id.subCodigo ;
				},
				'click .remove' : function(e, value, row, index) {
					window.location = '<c:url value="/mstablas/remove/"/>' + row.id.tabla + '/'  + row.id.codigo + '/' + row.id.subCodigo ;
				},
				'click .ver' : function(e, value, row, index) {
					window.location = '<c:url value="/mstablas/show/"/>' + row.id.tabla + '/'  + row.id.codigo + '/' + row.id.subCodigo ;
				}
			};
			function actionFormatterGridDataCustom(value, row, index) {
				console.log(row);
				var array = new Array();
				array.push('<!-- ' + row.id + ' -->');
				if (this.editar){
					array.push(dataEditar);
				}
				if (row.automaticaMananual !== "A"){
					array.push(dataEliminar);
				}
				if(this.show){
					array.push(dataShow);
				}
				return array.join('');
			}
			$(function() {
				var $table = $('#table');
			});
		</script>

		<div class="box box-primary">
			<div id="toolbar" class="pull-right">
				<git:refresh />
				<a href="<c:url value="/mstablas/add"/>" class="btn btn-default">
					<i class="fa fa-plus"></i>
				</a>
			</div>

			<div class="box-body no-padding">
				<git:table dataUrl="/mstablas/records" id="table" checkbox="false"
                 checkboxHiddenSelector="#seleccion_actual" showSearch="false" toolbarSelector="#toolbar" ignoreColumnsExport="[6]">

					<th data-field="id.tabla" data-sortable="true">
						<spring:message code="mstablas.tabla" />
					</th>

					<th data-field="descripcion" data-sortable="true">
						<spring:message code="mstablas.descripcion" />
					</th>
					<th data-field="descripcionReducida" data-sortable="true">
						<spring:message code="mstablas.descripcionReducida" />
					</th>
					<th data-field="auxiliarUsoInterno1" data-sortable="true">
						<spring:message code="mstablas.auxiliarUsoInterno1" />
					</th>
					<th data-field="auxiliarUsoInterno2" data-sortable="true">
						<spring:message code="mstablas.auxiliarUsoInterno2" />
					</th>
					<th data-field="automaticaMananual" data-sortable="true">
						<spring:message code="mstablas.automaticaMananual" />
					</th>
					<th data-field="action" data-align="center" data-formatter="actionFormatterGridDataCustom" data-editar="true" data-show="true"
						data-eliminar="true" data-events="actionEvents"></th>
                </git:table>
			</div>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>