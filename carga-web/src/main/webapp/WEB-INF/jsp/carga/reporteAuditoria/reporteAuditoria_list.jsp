<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="content-header">
		<h1>
			<spring:message code="consultaAuditoria" />
		</h1>
	</tiles:putAttribute>
	<tiles:putAttribute name="body">
<!--         <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script> -->

        
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css"> -->
		<script type="text/javascript">
		
			$(function() {
				$("#submit_btn").click(function(e) {
					$("#sinRegistro, #error, #campoObligatorioFechaD, #campoObligatorioFechaH").hide();
					e.preventDefault();
					var fecDesde = $("#fechaDesde").val();
					var fecHasta = $("#fechaHasta").val();
					var exportar = true;
					if(fecDesde == ""){
						$("#campoObligatorioFechaD").show();
						exportar = false;
					}else if(fecHasta == ""){
						$("#campoObligatorioFechaH").show();
						exportar = false;
					}else if(fecDesde != "" && fecHasta != ""){
						if(moment(fecHasta, "YYYY-MM-DD HH:mm").isBefore(moment(fecDesde, "YYYY-MM-DD HH:mm"))){
							popup('<spring:message code="fecHasMenFecDes" />');
							exportar = false;
						}
					}
					if(exportar){
						$("#fechaDesde").val(moment(fecDesde,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'));
						$("#fechaHasta").val(moment(fecHasta,'DD/MM/YYYY HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'));
						var filtros = $("#edit_form").serialize();
                        $("#fechaDesde").val(fecDesde);
                        $("#fechaHasta").val(fecHasta);
                        //                        
    					$.get('<c:url value="/reporteAuditoria/validarDatosExistente"/>' + '?' + filtros, function(data) {
							if(!data){
								window.location = '<c:url value="/reporteAuditoria/exportar"/>' + '?' + filtros ;	
							}else {
								$("#sinRegistro").show();			
							}
						})
					}
				});
				$("#sinRegistro, #error, #campoObligatorioFechaD, #campoObligatorioFechaH").hide();
				if("${sinRegistro}" === "true"){
					$("#sinRegistro").show();
				}else if("${error}" === "true"){
					$("#error").show();	
				}
				$("#tabla").change(function(){
					$("#campo").select2("val","");
					var campos = '459,' + $("#tabla").val() +',null,true';
					$("#campo").comboBoxPaged(mStablaUrl + campos,mStablaIdUrl,funcionMstablasSubCodigo);
					$("#campo").attr("readonly",false);
			    });
				
				$("#btn_limpiar").click(function(e) {
					$("#sinRegistro, #campoObligatorioFechaD, #campoObligatorioFechaH, #error").hide();
					$("#tipoTransaccion, #tabla, #campo").select2("val", "");
					$("#fechaDesde, #fechaHasta, #usuario").val("");
				});
				//Funcion para tomar el "enter" para aplicar los filtros
				filtrarEnter('submit_btn');
				 $('#fechaDesde').datetimepicker({
					   defaultDate: moment(moment().format('YYYY-MM-DD') + ' 00:00:00').format('YYYY-MM-DD HH:mm:ss'),
	                    locale: 'es'
	            });
				
				 $('#fechaHasta').datetimepicker({
					    defaultDate: moment(moment().format('YYYY-MM-DD') + ' 23:59:00').format('YYYY-MM-DD HH:mm:ss'),
	                    locale: 'es'
	            });
			});

		</script>

		<git:form action_url="/reporteAuditoria/exportar" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						<spring:message code="${titulo}" />
					</h3>
				</div>
				<div class="box-body">
                    <div class="col-sm-offset-2 col-sm-10" style="padding: 12px;" id="sinRegistro">
                       <h4  class="alert alert-danger" role="alert">
                         <spring:message code="consultaAuditoria.noencontrada" />
                       </h4>
	                </div>

	                <div class="col-sm-offset-2 col-sm-10" style="padding: 12px;" id="error">
                       <h4  class="alert alert-danger" role="alert">
                         <spring:message code="comisionCobrada.error" />
                       </h4>
	                </div>
	                
				   <git:field label="fecha.desde">
					  <input type="text" id="fechaDesde" name="fechaDesde" class="form-control"/>
					  <div class="col-sm-offset-2 col-sm-10" style="padding: 12px;" id="campoObligatorioFechaD">
                        <h4  class="alert alert-danger" role="alert">
                           <spring:message code="campo_obligatorio" />
                        </h4>
	                  </div>
					</git:field>
					
					<git:field label="fecha.hasta">
					  <input type="text" name="fechaHasta" id="fechaHasta" class="form-control"/>
					  <div class="col-sm-offset-2 col-sm-10" style="padding: 12px;" id="campoObligatorioFechaH">
                        <h4  class="alert alert-danger" role="alert">
                           <spring:message code="campo_obligatorio" />
                        </h4>
	                  </div>
					</git:field>	                

					<git:field label="consultaAuditoria.tipoTransaccion">
						<git:mstabla tabla="406" subCodigo="null" codigo="null" nombre_campo="tipoTransaccion" />
					</git:field>
                    				
					<git:field label="consultaAuditoria.tabla">
					    <git:mstabla tabla="459" subCodigo="0" codigo="null" nombre_campo="tabla"/>
					</git:field>
					
					<git:field label="consultaAuditoria.campo">
					    <git:mstabla tabla="459" subCodigo="null" codigo="null" readOnly="true" consultaNotIn="true" nombre_campo="campo"/>
					</git:field>
					
					<git:field label="consultaAuditoria.usuario">
						<input name="usuario" id="usuario" class="form-control" />
					</git:field>

			      </div>
				  <div class="box-footer">
				  
				  	<a type="button" id="btn_limpiar" class="btn btn-info pull-left parsley-save">
			            <spring:message code="limpiar" />
			        </a>
			        
					<a type="button" id="submit_btn"  class="btn btn-info pull-right parsley-save">
						<spring:message code="export" />
					</a>
				  </div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>