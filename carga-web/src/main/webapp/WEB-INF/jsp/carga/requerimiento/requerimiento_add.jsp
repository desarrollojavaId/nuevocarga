<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
					if("${edit}" == "true" && $("#id").val() === ""){
						popup('<spring:message code="objecto_no_existe" />');
					}
				}
				else if("${borrar}" == "true"){
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						//Asigno a todos los campos como solo lectura
						readOnlyForShow('edit_form');
						$("#submit_btn").text('<spring:message code="Eliminar" />')
						//
						$("#submit_btn").click(function(event){							
							//Elimina la acción click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acción
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que será eliminado.
							remove(null, '<c:url value="/requerimiento/eliminar/"/>','<c:url value="/requerimiento/"/>',$("#id").val());
						});
					}
				}
				else{
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						readOnlyForShow('edit_form');					
						$("#submit_btn").hide();
					}
				}
				
				//$("#area").comboBoxPaged(areaUrl,areaIdUrl, funcionArea);
				//$("#cliente").comboBoxPaged(clienteUrl, clienteIdUrl, funcionClienteCuit);
				$("#sector").comboBoxPaged(sectorUrl, sectorIdUrl, funcionSector);
				$("#contrato").comboBoxPaged(clienteContratoUrl, contratoIdUrl, funcionClienteContrato);
				$("#usuEmpLider").comboBoxPaged(usuEmpleadoLiderUrl, usuEmpleadoIdUrl, funcionUsuario);
				$("#usuCliente").comboBoxPaged(usuClienteUrl, usuClienteIdUrl, funcionUsuCliente);
				

			});			
		</script>
		<git:form action_url="/requerimiento/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					<git:errors />
					
					
					<git:field label="requerimiento.nuss">
					    <git:number nombre_campo="nuss" path="true" require="true" decimal="false" max="100000" /> 
					</git:field>
					
					<git:field label="requerimiento.idCliente">
						<form:input path="idCliente" required="true" cssClass="form-control"  maxlength="25" />
					</git:field>
					
					<git:field label="requerimiento.titulo">
						<form:input path="titulo" required="true" cssClass="form-control"  maxlength="100" />
					</git:field>
					<git:field label="requerimiento.descripcion">
						<form:input path="descripcion" required="true" cssClass="form-control"  maxlength="255" />
					</git:field>
					<git:field label="requerimiento.sector">
						<form:input path="sector" required="true" cssClass="form-control"  maxlength="25" />
					</git:field>
					<git:field label="requerimiento.contrato">
						<form:input path="contrato" required="true" cssClass="form-control"  maxlength="25" />
					</git:field>
					
					<git:field label="requerimiento.usuEmpLider">
						<form:input path="usuEmpLider" required="true" cssClass="form-control"  maxlength="25" />
					</git:field>
					<git:field label="requerimiento.usuCliente">
						<form:input path="usuCliente" required="true" cssClass="form-control"  maxlength="25" />
					</git:field>
					
					<git:field label="requerimiento.t06Sistema">
						<git:mstabla tabla="006" require="true" codigo="null" subCodigo="null" nombre_campo="t06Sistema" path="t06Sistema" value="${entity.t06Sistema}" />
					</git:field>
					
					<git:field label="requerimiento.t05TipoRequerimiento">
						<git:mstabla tabla="005" require="true" codigo="null" subCodigo="null" nombre_campo="t05TipoRequerimiento" path="t05TipoRequerimiento" value="${entity.t05TipoRequerimiento}" />
					</git:field>

					<git:field label="requerimiento.t04TipoSolicitud">
						<git:mstabla tabla="009" require="true" codigo="null" subCodigo="null" nombre_campo="t04TipoSolicitud" path="t04TipoSolicitud" value="${entity.t04TipoSolicitud}" />
					</git:field>
					
					<git:field label="requerimiento.t04Estado">
						<git:mstabla tabla="004" require="true" codigo="null" subCodigo="null" nombre_campo="t04Estado" path="t04Estado" value="${entity.t04Estado}" />
					</git:field>
					
					<git:field label="requerimiento.t04EstadoAnterior">
						<git:mstabla tabla="004" require="true" codigo="null" subCodigo="null" nombre_campo="t04EstadoAnterior" path="t04EstadoAnterior" value="${entity.t04EstadoAnterior}" />
					</git:field>
					
					<git:field label="requerimiento.esfuerzoEstimado">
					    <git:number nombre_campo="esfuerzoEstimado" path="true" require="false" min="1" max="8" /> 
					</git:field>

					<git:field label="requerimiento.esfuerzoAdicional">
					    <git:number nombre_campo="esfuerzoAdicional" path="true" require="false" min="1" max="8" /> 
					</git:field>

					<git:field label="requerimiento.esfuerzoEstimadoUT">
					    <git:number nombre_campo="esfuerzoEstimadoUT" path="true" require="false" min="1" max="8" /> 
					</git:field>

					<git:field label="requerimiento.esfuerzoAdicionalUT">
					    <git:number nombre_campo="esfuerzoAdicionalUT" path="true" require="false" min="1" max="8" /> 
					</git:field>
					
					<git:field label="requerimiento.fecSolicitud">
						<form:input type="date" path="fecSolicitud" id="fecSolicitud" required="true" cssClass="form-control"/>
						<div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="fecSolicitud" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>						
					</git:field>
					
					 
					 <git:field label="requerimiento.fecSolicitud">
						<form:input type="date" path="fecSolicitud" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecInicioImputacion">
						<form:input type="date" path="fecInicioImputacion" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecOriInicioEstimado">
						<form:input type="date" path="fecOriInicioEstimado" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecUltInicioEstimado">
						<form:input type="date" path="fecUltInicioEstimado" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecOriFinalEstimado">
						<form:input type="date" path="fecOriFinalEstimado" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecUltFinalEstimado">
						<form:input type="date" path="fecUltFinalEstimado" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecEstimadaFinCarga">
						<form:input type="date" path="fecEstimadaFinCarga" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecFinCarga">
						<form:input type="date" path="fecFinCarga" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecEntrega">
						<form:input type="date" path="fecEntrega" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="requerimiento.fecImplementación">
						<form:input type="date" path="fecImplementación" cssClass="form-control" readonly="false"/>
					 </git:field>					
					
					
					<git:field label="requerimiento.observaciones">
						<form:input path="observaciones" required="false" cssClass="form-control"  maxlength="255" />
					</git:field>
					<git:field label="requerimiento.tratamiento">
						<form:input path="tratamiento" required="false" cssClass="form-control"  maxlength="255" />
					</git:field>
					<git:field label="requerimiento.linkDocumentacion">
						<form:input path="linkDocumentacion" required="false" cssClass="form-control"  maxlength="255" />
					</git:field>
					<git:field label="requerimiento.nussConRetraso">
						<form:input path="nussConRetraso" required="false" cssClass="form-control"  maxlength="1" />
					</git:field>
					<git:field label="requerimiento.t08MotivoRetraso1">
						<git:mstabla tabla="008" require="false" codigo="null" subCodigo="null" nombre_campo="t08MotivoRetraso1" path="t08MotivoRetraso1" value="${entity.t08MotivoRetraso1}" />
					</git:field>
					<git:field label="requerimiento.t08MotivoRetraso2">
						<git:mstabla tabla="008" require="false" codigo="null" subCodigo="null" nombre_campo="t08MotivoRetraso2" path="t08MotivoRetraso2" value="${entity.t08MotivoRetraso2}" />
					</git:field>
					<git:field label="requerimiento.t08MotivoRetraso3">
						<git:mstabla tabla="008" require="false" codigo="null" subCodigo="null" nombre_campo="t08MotivoRetraso3" path="t08MotivoRetraso3" value="${entity.t08MotivoRetraso2}" />
					</git:field>
					
					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>

					
					<div class="box-footer">
						<a href="<c:url value="/requerimiento/filtros"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

