<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title isList="true"></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			var removeUrl = '<c:url value="/requerimiento/remove/"/>';
			window.actionEvents = {
				'click .edit' : function(e, value, row, index) {
					window.location = '<c:url value="/requerimiento/edit/"/>' + row.id;
				},
				'click .remove' : function(e, value, row, index) {
					window.location = '<c:url value="/requerimiento/remove/"/>' + row.id;
				},
				'click .ver' : function(e, value, row, index) {
					window.location = '<c:url value="/requerimiento/show/"/>' + row.id ;
				},
				'click .detalle' : function(e, value, row, index) {
					window.location = '<c:url value="/tarea/showByRequerimiento/"/>' + row.id ;
				}
			};
			$(function() {

				$("#btn_buscar").click(function() {

				$("#edit_forb").serialize();
					var path = '<c:url value="/requerimiento/records"/>' + '?'+ $("#buscar_form").serialize() + "&mantenerfiltros=true" ;
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				
				$("#btn_limpiar").click(function() {
					limpiarFiltrosSession('nussSession');
					limpiarFiltrosSession('tituloSession');
					$("#nuss").val("")
					$("#titulo").val("")
			        var path = '<c:url value="/requerimiento/records"/>';
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				//Funcion para tomar el "enter" para aplicar los filtros
				filtrarEnter('btn_buscar');
			});
		</script>
		
		<!-- 					 	-->
		<!-- Campos de filtrado		-->
		<!-- 					 	-->
		<div class="box box-primary">
		<div class="box-header">
		</div> 
			<div class="box-body">
			  <form id="buscar_form" class="form-horizontal">

			  <div class="row">
			  	
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="requerimiento.nuss" />
                    </label>
                    <div class="col-sm-4">
                         <input name="nuss" id="nuss" value="${sessionScope.nussSession}" class="form-control" >
					</div>  
				  </div>
				</div>

			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="requerimiento.titulo" />
                    </label>
                    <div class="col-sm-4">
                         <input name="titulo" id="titulo" value="${sessionScope.tituloSession}" class="form-control" >
					</div>  
				  </div>
				</div>

			  </div>

			  </form>
			</div>
			<div class="box-footer">
			  <button type="button" id="btn_buscar" class="btn btn-info pull-right">
				<spring:message code="buscar" />
			  </button>

			  <button type="button" id="btn_limpiar" class="btn btn-info pull-left">
				<spring:message code="limpiar" />
			  </button>
			</div>
		</div>	
		
		<!--				-->
		<!-- Grilla Detalle	-->
		<!--				-->
		<div class="box box-primary">
			<div id="toolbar" class="pull-right">
				<git:refresh />
				<a href="<c:url value="/requerimiento/add"/>" class="btn btn-default">
					<i class="fa fa-plus"></i>
				</a>
			</div>

			<div class="box-header">
			</div> 
			<div class="box-body no-padding">
				<git:table dataUrl="/requerimiento/records?tomarFiltrosSession=${tomarFiltrosSession}" id="table" checkbox="false"
					checkboxHiddenSelector="#seleccion_actual" showSearch="false"
					toolbarSelector="#toolbar" ignoreColumnsExport="[3]">
							<th data-field="nuss" data-sortable="true">
								<spring:message code="requerimiento.nuss" />
							</th>
							<th data-field="titulo" data-sortable="true">
								<spring:message code="requerimiento.titulo" />
							</th>
							<th data-field="descEstado" data-sortable="true">
								<spring:message code="requerimiento.descEstado" />
							</th>
														
							<!--			-->
							<!-- Botones	-->
							<!--			-->
							<th data-field="action" data-align="center"
								data-formatter="actionFormatterGridData" data-editar="true"
								data-eliminar="true" data-show="true" data-events="actionEvents"
								data-detalle="true" data-info="'<spring:message code="requerimiento.detalle" />'"></th>
				</git:table>
			</div>
			<div class="box-footer">
			</div>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>