<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
					if("${edit}" == "true" && $("#id").val() === ""){
						popup('<spring:message code="objecto_no_existe" />');
					}
				}
				else if("${borrar}" == "true"){
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						//Asigno a todos los campos como solo lectura
						readOnlyForShow('edit_form');
						$("#submit_btn").text('<spring:message code="Eliminar" />')
						//
						$("#submit_btn").click(function(event){							
							//Elimina la acci�n click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acci�n
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que ser� eliminado.
							remove(null, '<c:url value="/tarea/eliminar/"/>','<c:url value="/tarea/"/>',$("#id").val());
						});
					}
				}
				else{
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						readOnlyForShow('edit_form');					
						$("#submit_btn").hide();
					}
				}
				
				$("#area").comboBoxPaged(areaUrl,areaIdUrl, funcionArea);
				$("#requerimiento").comboBoxPaged(requerimientoUrl, requerimientoIdUrl, funcionRequerimiento);
				$("#requerimiento").attr("readonly","${sessionScope.requerimientoId}" != "");

				

			});			
		</script>
		<git:form action_url="/tarea/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					<git:errors />
					
					
					<git:field label="tarea.requerimiento">
					    <git:number nombre_campo="requerimiento" path="true" require="true" decimal="false" max="25" /> 
					</git:field>
					
					<git:field label="tarea.titulo">
						<form:input path="titulo" required="true" cssClass="form-control"  maxlength="100" />
					</git:field>

					<git:field label="tarea.descripcion">
						<form:input path="descripcion" required="true" cssClass="form-control"  maxlength="255" />
					</git:field>

					<git:field label="tarea.area">
						<form:input path="area" required="true" cssClass="form-control"  maxlength="25" />
					</git:field>
					
					<git:field label="tarea.t07TipoTarea">
						<git:mstabla tabla="007" require="true" codigo="null" subCodigo="null" nombre_campo="t07TipoTarea" path="t07TipoTarea" value="${entity.t07TipoTarea}" />
					</git:field>
					
					<git:field label="tarea.t14Estado">
						<git:mstabla tabla="014" require="true" codigo="null" subCodigo="null" nombre_campo="t14Estado" path="t14Estado" value="${entity.t14Estado}" />
					</git:field>
					
					<git:field label="tarea.esfuerzoEstimado">
					    <git:number nombre_campo="esfuerzoEstimado" path="true" require="false" min="1" max="8" /> 
					</git:field>

					<git:field label="tarea.esfuerzoEstimadoUT">
					    <git:number nombre_campo="esfuerzoEstimadoUT" path="true" require="false" min="1" max="8" /> 
					</git:field>
					 
					 <git:field label="tarea.fecIniEjecucion">
						<form:input type="date" path="fecIniEjecucion" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="tarea.fecIniTarea">
						<form:input type="date" path="fecIniTarea" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="tarea.fecFinEjecucion">
						<form:input type="date" path="fecFinEjecucion" cssClass="form-control" readonly="false"/>
					 </git:field>
					 
					 <git:field label="tarea.fecFinTarea">
						<form:input type="date" path="fecFinTarea" cssClass="form-control" readonly="false"/>
					 </git:field>
					
					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>

					
					<div class="box-footer">
						<a href="<c:url value="/tarea/filtros"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

