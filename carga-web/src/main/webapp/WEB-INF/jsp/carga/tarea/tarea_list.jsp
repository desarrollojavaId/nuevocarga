<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title isList="true"></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			var removeUrl = '<c:url value="/tarea/remove/"/>';
			window.actionEvents = {
				'click .edit' : function(e, value, row, index) {
					window.location = '<c:url value="/tarea/edit/"/>' + row.id;
				},
				'click .remove' : function(e, value, row, index) {
					window.location = '<c:url value="/tarea/remove/"/>' + row.id;
				},
				'click .ver' : function(e, value, row, index) {
					window.location = '<c:url value="/tarea/show/"/>' + row.id ;
				},
				'click .detalle' : function(e, value, row, index) {
					window.location = '<c:url value="/tareaUsuEmpleado/showByTarea/"/>' + row.id ;
				}
			};
			$(function() {
				
				$("#btn_buscar").click(function() {

				$("#edit_forb").serialize();
					var path = '<c:url value="/tarea/records"/>' + '?'+ $("#buscar_form").serialize() + "&mantenerfiltros=true" ;
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				
				$("#btn_limpiar").click(function() {
					limpiarFiltrosSession('tituloSession');
					$("#titulo").val("")
			        var path = '<c:url value="/tarea/records"/>';
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				//Funcion para tomar el "enter" para aplicar los filtros
				filtrarEnter('btn_buscar');

			});
		</script>
		
		<!-- 					 	-->
		<!-- Campos de filtrado		-->
		<!-- 					 	-->
		<div class="box box-primary">
		<div class="box-header">
		</div> 
			<div class="box-body">
			  <form id="buscar_form" class="form-horizontal">

			  <div class="row">
			  	
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="requerimiento.nuss" />
                    </label>
                    <div class="col-sm-4">
                         <input name="nuss" id="nuss" value="${sessionScope.nussSession}" class="form-control" readonly>
					</div>  
				  </div>
				</div>
			  				  				  	
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="requerimiento.requerimiento" />
                    </label>
                    <div class="col-sm-4">
                         <input name="nussTitulo" id="nussTitulo" value="${sessionScope.nussTituloSession}" class="form-control" readonly>
					</div>  
				  </div>
				</div>
			  	
			  </div>
			  	
			  <div class="row">
			  	
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="requerimiento.descEstado" />
                    </label>
                    <div class="col-sm-4">
                         <input name="nussEstado" id="nussEstado" value="${sessionScope.nussEstadoSession}" class="form-control" readonly>
					</div>  
				  </div>
				</div>
			  				  	
			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="requerimiento.cliente" />
                    </label>
                    <div class="col-sm-4">
                         <input name="nussCliente" id="nussCliente" value="${sessionScope.nussClienteSession}" class="form-control" readonly>
					</div>  
				  </div>
				</div>

			  </div>

			  <div class="row">

			  	<div class="col-sm-6">
				  <div class="form-group">
				    <label class="control-label col-sm-4">
				       <spring:message code="tarea.titulo" />
                    </label>
                    <div class="col-sm-4">
                         <input name="titulo" id="titulo" value="${sessionScope.tituloSession}" class="form-control" >
					</div>  
				  </div>
				</div>

			  </div>

			  </form>
			</div>
			<div class="box-footer">
			  <button type="button" id="btn_buscar" class="btn btn-info pull-right">
				<spring:message code="buscar" />
			  </button>

			  <button type="button" id="btn_limpiar" class="btn btn-info pull-left">
				<spring:message code="limpiar" />
			  </button>
			</div>
		</div>	
		
		<!--				-->
		<!-- Grilla Detalle	-->
		<!--				-->
		<div class="box box-primary">
			<div id="toolbar" class="pull-right">
				<git:refresh />
				<a href="<c:url value="/tarea/add"/>" class="btn btn-default">
					<i class="fa fa-plus"></i>
				</a>
			</div>

			<div class="box-header">
			</div> 
			<div class="box-body no-padding">
				<git:table dataUrl="/tarea/records?tomarFiltrosSession=${tomarFiltrosSession}" id="table" checkbox="false"
					checkboxHiddenSelector="#seleccion_actual" showSearch="false"
					toolbarSelector="#toolbar">
							<th data-field="areaDescripcion" data-sortable="true">
								<spring:message code="area.descripcion" />
							</th>
							<th data-field="titulo" data-sortable="true">
								<spring:message code="tarea.titulo" />
							</th>
							<th data-field="descTipoTarea" data-sortable="true">
								<spring:message code="tarea.t07TipoTarea" />
							</th>
							<th data-field="descEstado" data-sortable="true">
								<spring:message code="tarea.t14Estado" />
							</th>
							<th data-field="esfuerzoEstimado" data-sortable="true">
								<spring:message code="tarea.esfuerzoEstimadoMini" />
							</th>
														
							<!--			-->
							<!-- Botones	-->
							<!--			-->
							<th data-field="action" data-align="center"
								data-formatter="actionFormatterGridData" data-editar="true"
								data-eliminar="true" data-show="true" data-events="actionEvents"
								data-detalle="true" data-info="'<spring:message code="tarea.detalle" />'"></th>
								
								
				</git:table>
				
				<div class="box-footer">
					<a href="<c:url value="/requerimiento"/>" id="cancel_btn"
						class="btn btn-default"> <spring:message code="back" />
					</a>
				</div>
				
			</div>
			<div class="box-footer">
			</div>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>