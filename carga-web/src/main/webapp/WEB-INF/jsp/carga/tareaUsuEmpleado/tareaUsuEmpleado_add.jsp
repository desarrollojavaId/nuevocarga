<%@ page language="java" contentType="text/html; charset=ISO-8859-2" pageEncoding="ISO-8859-2" %>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
					if("${edit}" == "true" && $("#id").val() === ""){
						popup('<spring:message code="objecto_no_existe" />');
					}
				}
				else if("${borrar}" == "true"){
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						//Asigno a todos los campos como solo lectura
						readOnlyForShow('edit_form');
						$("#submit_btn").text('<spring:message code="Eliminar" />')
						//
						$("#submit_btn").click(function(event){							
							//Elimina la acci�n click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acci�n
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que ser� eliminado.
							remove(null, '<c:url value="/tareaUsuEmpleado/eliminar/"/>','<c:url value="/tarea/"/>',$("#id").val());
						});
					}
				}
				else{
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						readOnlyForShow('edit_form');					
						$("#submit_btn").hide();
					}
				}
				$("#tareaTitulo").attr("readonly","${sessionScope.tareaIdSession}" != "");
				$("#usuEmpleado").comboBoxPaged(usuEmpleadoUrl, usuEmpleadoIdUrl, funcionUsuario);


			});			
		</script>
		<git:form action_url="/tareaUsuEmpleado/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					<git:errors />

					<git:field label="">
						<form:input path="tarea" required="true" cssClass="form-control"  maxlength="100" type="hidden"/>
					</git:field>
					<git:field label="tareaUsuEmpleado.tarea">
						<form:input path="tareaTitulo" required="true" cssClass="form-control"  maxlength="100" />
					</git:field>
										
					<git:field label="usuEmpleado.empleado">
						<form:input path="usuEmpleado" required="true" cssClass="form-control" />
					</git:field>
					
					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>

					<div class="box-footer">
						<a href="<c:url value="/tareaUsuEmpleado/filtros"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
					
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

