<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title isList="true"></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			var removeUrl = '<c:url value="/tareaUsuEmpleado/remove/"/>';
			window.actionEvents = {
				'click .edit' : function(e, value, row, index) {
					window.location = '<c:url value="/tareaUsuEmpleado/edit/"/>' + row.id;
				},
				'click .remove' : function(e, value, row, index) {
					window.location = '<c:url value="/tareaUsuEmpleado/remove/"/>' + row.id;
				},
				'click .ver' : function(e, value, row, index) {
					window.location = '<c:url value="/tareaUsuEmpleado/show/"/>' + row.id ;
				}
			};
			$(function() {
				
				$("#btn_buscar").click(function() {

				$("#edit_forb").serialize();
					var path = '<c:url value="/tareaUsuEmpleado/records"/>' + '?'+ $("#buscar_form").serialize() + "&mantenerfiltros=true" ;
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				
				$("#btn_limpiar").click(function() {
			        var path = '<c:url value="/tareaUsuEmpleado/records"/>';
					$("#table").bootstrapTable('refresh', {
						url : path,
	                    query: {pageSize: 0 , offset: 0}
					});
				});
				//Funcion para tomar el "enter" para aplicar los filtros
				filtrarEnter('btn_buscar');

			});
		</script>
		
		<!-- 					 	-->
		<!-- Campos de filtrado		-->
		<!-- 					 	-->
		<div class="box box-primary">
		<div class="box-header">
		</div> 
		  <div class="box-body">
		    <form id="buscar_form" class="form-horizontal">
		    
				  <div class="row">
				  	
				  	<div class="col-sm-6">
					  <div class="form-group">
					    <label class="control-label col-sm-4">
					       <spring:message code="tareaUsuEmpleado.tarea" />
	                    </label>
	                    <div class="col-sm-4">
	                         <input name="tareaTitulo" id="tareaTitulo" value="${sessionScope.tareaTituloSession}" class="form-control" readonly>
						</div>  
					  </div>
					</div>
	
				  </div>				
			   
			</form>
		  </div>

			
			<div class="box-footer">
			  <button type="button" id="btn_buscar" class="btn btn-info pull-right">
				<spring:message code="buscar" />
			  </button>

			  <button type="button" id="btn_limpiar" class="btn btn-info pull-left">
				<spring:message code="limpiar" />
			  </button>
			</div>
		</div>	
		
		<!--				-->
		<!-- Grilla Detalle	-->
		<!--				-->
		<div class="box box-primary">
			<div id="toolbar" class="pull-right">
				<git:refresh />
				<a href="<c:url value="/tareaUsuEmpleado/add"/>" class="btn btn-default">
					<i class="fa fa-plus"></i>
				</a>
			</div>

			<div class="box-header">
			</div> 
			<div class="box-body no-padding">
				<git:table dataUrl="/tareaUsuEmpleado/records?tomarFiltrosSession=${tomarFiltrosSession}" id="table" checkbox="false"
					checkboxHiddenSelector="#seleccion_actual" showSearch="false"
					toolbarSelector="#toolbar">
							<th data-field="nombreApellido" data-sortable="true">
								<spring:message code="tareaUsuEmpleado.empleado" />
							</th>
							<th data-field="esLider" data-sortable="true">
								<spring:message code="usuEmpleado.esLider" />
							</th>
														
							<!--			-->
							<!-- Botones	-->
							<!--			-->
							<th data-field="action" data-align="center"
								data-formatter="actionFormatterGridData" data-editar="true"
								data-eliminar="true" data-show="true" data-events="actionEvents"></th>
				</git:table>
								
				<div class="box-footer">
					<a href="<c:url value="/tarea/showByRequerimiento/${entity.tarea.requerimiento.id}"/>" id="cancel_btn"
						class="btn btn-default"> <spring:message code="back" />
					</a>
				</div>
				
			</div>
			<div class="box-footer">
			</div>
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>