<%@ page language="java" contentType="text/html; charset=ISO-8859-2" pageEncoding="ISO-8859-2" %>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title></git:title>
	
	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
					if("${edit}" == "true" && $("#id").val() === ""){
						popup('<spring:message code="objecto_no_existe" />');
					}
				}
				else if("${borrar}" == "true"){
					//Detengo el submit del formulario (no deber�a forzarlo, pero no hab�a otra manera)
					$("#edit_form").submit(function(e){
						e.preventDefault();
					});
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						//Asigno a todos los campos como solo lectura
						readOnlyForShow('edit_form');
						$("#submit_btn").text('<spring:message code="Eliminar" />')
						
						$("#submit_btn").click(function(event){
							//Elimina la acci�n click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acci�n
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que ser� eliminado.
							remove(null, '<c:url value="/usuEmpleado/eliminar/"/>','<c:url value="/usuEmpleado/"/>',$("#id").val());
						});
						
						//
					}
				}
				else{
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						readOnlyForShow('edit_form');					
						$("#submit_btn").hide();
					}
				}
				
				
				customSelect2('acreditaHoras', [ {
			        "id": "SI",
			        "text": "SI"
			      }, {
			        "id": "NO",
			        "text": "NO"
			      } ]);
				
				customSelect2('registraIngreso', [ {
			        "id": "SI",
			        "text": "SI"
			      }, {
			        "id": "NO",
			        "text": "NO"
			      } ]);
						
				customSelect2('esLider', [ {
			        "id": "SI",
			        "text": "SI"
			      }, {
			        "id": "NO",
			        "text": "NO"
			      } ]);

				$("#usuario").comboBoxPaged(usuariosUrl,usuariosIdUrl, funcionUsuario);
				$("#usuEmpLider").comboBoxPaged(usuEmpleadoLiderUrl, usuEmpleadoIdUrl, funcionUsuario);
				$("#sector").comboBoxPaged(sectorUrl,sectorIdUrl, funcionSector);
				$("#cliente").comboBoxPaged(clienteUrl, clienteIdUrl, funcionClienteCuit);
				
			});			
		</script>
		<git:form action_url="/usuEmpleado/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					<git:errors />
					
					<git:field label="usuEmpleado.empleado">
						<form:input path="usuario" required="true" cssClass="form-control" />
					</git:field>
					
					<git:field label="usuEmpleado.acreditaHoras">
						<form:input path="acreditaHoras" required="true" cssClass="form-control"  maxlength="2" />
					</git:field>
					
					<git:field label="usuEmpleado.horasDiarias">
						<form:input path="horasDiarias" required="true" cssClass="form-control"  maxlength="2" />
					</git:field>
					
					<git:field label="usuEmpleado.registraIngreso">
						<form:input path="registraIngreso" required="true" cssClass="form-control"  maxlength="2" />
					</git:field>
					
					<git:field label="usuEmpleado.esLider">
						<form:input path="esLider" required="true" cssClass="form-control"  maxlength="2" />
					</git:field>
					
					<git:field label="usuEmpleado.usuEmpLider">
						<form:input path="usuEmpLider" cssClass="form-control" maxlength="100" />						
					</git:field>
					
					<git:field label="sector.sector">
						<form:input path="sector" cssClass="form-control" />					
					</git:field>
										
					<git:field label="cliente.cliente">
						<form:input path="cliente" cssClass="form-control"/>					
					</git:field>
					
					<git:field label="usuEmpleado.numeroLegajo">
						<git:number nombre_campo="numeroLegajo" path="true" decimal="false" id_campo="numeroLegajo"/>					
					</git:field>
					
					<git:field label="usuEmpleado.diasVacacionesLey">
						<git:number nombre_campo="diasVacacionesLey" path="true" decimal="false" max="2" id_campo="diasVacacionesLey"/>					
					</git:field>
					
					<git:field label="usuEmpleado.diasVacacionesAdic">
						<git:number nombre_campo="diasVacacionesAdic" path="true" decimal="false"  max="2" id_campo="diasVacacionesAdic"/>					
					</git:field>
					
					
					<git:field label="usuEmpleado.fecIngreso">
						<form:input type="date" path="fecIngreso" id="fecIngreso" required="true" cssClass="form-control"/>
						<div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		                    <form:errors path="fecIngreso" cssClass="alert alert-danger" element="div" role="alert" />
	                    </div>						
					</git:field>


					
					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>

					
					<div class="box-footer">
						<a href="<c:url value="/usuEmpleado/filtros"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

