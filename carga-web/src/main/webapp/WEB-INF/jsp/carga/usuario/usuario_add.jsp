<%@ include file="/WEB-INF/jsp/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<git:title></git:title>

	<tiles:putAttribute name="body">
		<script type="text/javascript">
			$(function() {
				if("${edit}" == "true" || "${create}" == "true"){
					registerParsley();
					if("${edit}" == "true" && $("#id").val() === ""){
						popup('<spring:message code="objecto_no_existe" />');
					}
				}
				else if("${borrar}" == "true"){
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						//Asigno a todos los campos como solo lectura
						readOnlyForShow('edit_form');
						$("#submit_btn").text('<spring:message code="Eliminar" />')
						//
						$("#submit_btn").click(function(event){							
							//Elimina la acci�n click pre definida del input
							event.preventDefault();
							//@param1=Tabla (como visualiza una pantalla antes de eliminar, va null)
							//@param2=Url del controller que atiende la acci�n
							//@param3=Url donde va a redireccionar luego de ejecutar
							//@param4=Id del objeto que ser� eliminado.
							remove(null, '<c:url value="/usuario/eliminar/"/>','<c:url value="/usuario/"/>',$("#id").val());
						});
					}
				}
				else{
					if($("#id").val() === ""){
						registerParsley();
						popup('<spring:message code="objecto_no_existe" />');
					}else {
						readOnlyForShow('edit_form');					
						$("#submit_btn").hide();
					}
				}
				$("#t01Categoria").change(function(){
					habilitarClave();					
				});
				//Calcula las iniciales del usuario
				$("#nombre").blur(function(){
					calcularIniciales();					
				});
				//Calcula las iniciales del usuario
				$("#apellido").blur(function(){
					calcularIniciales();					
				});
				//Calcula las iniciales del usuario
				$("#t01Categoria").change(function(){
					calcularIniciales();					
				});
				
				function habilitarClave(){
					if($("#t01Categoria").val() != 1){
						$("#divClave").hide();	
						$("#clave").attr('required', false);
					}
					else{
						$("#divClave").show();
						$("#clave").attr('required', true);
						
					}
				}
				
				//Calcula las iniciales del usuario
				function calcularIniciales(){
					var iniciales;
					var categoria = $("#t01Categoria").val();
					if($("#nombre").val() != "" && $("#apellido").val() != "" && $("#t01Categoria").val() != ""){
						iniciales = $("#nombre").val().trim().substring(0,1) + $("#apellido").val().trim().substring(0,1);
						//Si es externo le agrego un -EXT
						if($("#t01Categoria").val() == "1"){
							$("#iniciales").val(iniciales.toUpperCase() + "-EXT");
						}
						else{
							$("#iniciales").val(iniciales.toUpperCase());	
						}
						
					}
					
				}
				
				//Habilito o deshabilito el campo clave dependiendo el perfil del usuario 
				habilitarClave();

			});			
		</script>
		<git:form action_url="/usuario/save" parsley="true" id="edit_form"
			modelAttribute="entity">
			<!-- Guardo el id oculto para que viaje al controlador -->
			<form:hidden path="id" />
			<form:hidden path="tituloPantalla" />
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						
					</h3>
				</div>

				<div class="box-body">
					<git:errors />
					
					<git:field label="usuario.nombreUsuario">
						<form:input path="nombreUsuario" required="true" cssClass="form-control"  maxlength="50" />
					</git:field>
					<div id="divClave">
						<git:field label="usuario.clave">
							<form:input path="clave" required="true" cssClass="form-control"  maxlength="50" />
						</git:field>
					</div>
					<git:field label="usuario.nombre">
						<form:input path="nombre" required="true" cssClass="form-control" maxlength="50" />
					</git:field>
					
					<git:field label="usuario.apellido">
						<form:input path="apellido" required="true" cssClass="form-control" maxlength="50"  />						
					</git:field>
					
					<git:field label="usuario.dni">
					    <git:number nombre_campo="dni" path="true" require="true" min="6" max="8" /> 
					</git:field>
					
					<git:field label="usuario.t01Categoria">
						<git:mstabla tabla="001" require="true" codigo="null" subCodigo="null" nombre_campo="t01Categoria" path="t01Categoria" value="${entity.t01Categoria}" />
					</git:field>
					
					<git:field label="usuario.mail">
						<form:input path="mail" cssClass="form-control" maxlength="255" data-parsley-type="email" />						
					</git:field>
					
					<git:field label="usuario.telPrincipal">
						<form:input path="telPrincipal" cssClass="form-control" minlength="8" maxlength="100" />						
					</git:field>
					
					<git:field label="usuario.telSecundario">
						<form:input path="telSecundario" cssClass="form-control" minlength="8" maxlength="100" />						
					</git:field>
					<git:field label="usuario.iniciales">
						<form:input path="iniciales" required="true" cssClass="form-control"  maxlength="50" />
					</git:field>
					
					<c:choose>
                       <c:when test="${not empty entity.id}">
					       <git:field label="usuario.ultimaModificacion">
						     <form:input path="usuarioUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					       
					       <git:field label="fecha.ultimaModificacion">
						     <form:input path="fechaUltimaActualizacion" readonly="true" cssClass="form-control"/>
					       </git:field>
					   </c:when>    
                    </c:choose>

					
					<div class="box-footer">
						<a href="<c:url value="/usuario/filtros"/>" id="cancel_btn"
							class="btn btn-default"> <spring:message code="back" />
						</a>
						<button type="button" id="submit_btn"
							class="btn btn-info pull-right parsley-save">
							<spring:message code="save" />
						</button>
					</div>
				</div>
			</div>
		</git:form>
	</tiles:putAttribute>
</tiles:insertDefinition>

