<%@ include file="/WEB-INF/jsp/include.jsp"%>
<%@ page isErrorPage="true" import="java.io.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Error interno</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet"
	href="<c:url value="/public/css/bootstrap.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/public/css/font-awesome.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/public/css/ionicons.min.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/adminlte.css"/>">
<link rel="stylesheet"
	href="<c:url value="/public/css/skin-blue-light.css"/>">
<!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
<script src="<c:url value="/public/js/jquery-2.1.4.min.js"/>"></script>
<script src="<c:url value="/public/js/bootstrap.min.js"/>"></script>
<style type="text/css">
html, body, .container-table {
	height: 100%;
}

.container-table {
	display: table;
}

.vertical-center-row {
	display: table-cell;
	vertical-align: middle;
}
</style>
</head>
<body>
	<div class="container container-table">
		<div class="row vertical-center-row">
			<div class="error-page">
				<h2 class="headline text-red">${requestScope["javax.servlet.error.status_code"]}</h2>
				<div class="error-content">
					<h3>
						<i class="fa fa-warning text-red"></i> ${errorMessage}
					</h3>
					<p>
						Ha ocurrido un error, para retornar al home haga click <a href="<c:url value="/"/>">aqu&iacute;</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<pre>
				<% if (exception != null) { exception.printStackTrace(new PrintWriter(out)); } %>
			</pre>
		</div>
	</div>
</body>
</html>