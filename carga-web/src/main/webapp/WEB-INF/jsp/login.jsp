<%@ include file="/WEB-INF/jsp/include.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="/public/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<c:url value="/public/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/font-awesome.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/ionicons.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/adminlte.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/skin-blue-light.css"/>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<c:url value="/public/js/jquery-2.1.4.min.js"/>"></script>
    <script src="<c:url value="/public/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/public/js/app.js"/>"></script>
    <script src="<c:url value="/public/js/parsley.min.js"/>"></script>
</head>
<body class="login-page" style="min-height: 300px;">
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b><spring:message code="app.titulo"/></b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg"><spring:message code="login-box-msg"/></p>
        <form action="<c:url value="/login_do"/>" method='post' autocomplete="off">
            <c:if test="${param.error != null}">
                <div class="alert alert-warning">
                	<spring:message javaScriptEscape="true" code="login.error"/>
                </div>
            </c:if>
            <c:if test="${param.logout != null}">
                <div class="alert alert-success">
                	<spring:message javaScriptEscape="true" code="logout.success"/>
                </div>
            </c:if>
            <div class="form-group has-feedback">
                <input name="username" value="" type="text" class="form-control" placeholder="<spring:message code="login.username"/>"> <span
                    class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password" type="password" value="" class="form-control" placeholder="<spring:message code="login.password"/>"> <span
                    class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8"></div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat"><spring:message code="login.accept"/></button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>