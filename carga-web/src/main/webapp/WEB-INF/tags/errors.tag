<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10" style="padding: 12px;">
		<form:errors path="*" cssClass="alert alert-danger" element="div" role="alert" />
	</div>
</div>