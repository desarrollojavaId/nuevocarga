<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="label" required="false"%>
<%@ attribute name="labelCol" required="false"%>
<%@ attribute name="fieldCol" required="false"%>

<c:set var="labelCol" value="${(empty labelCol) ? 'col-sm-2' : labelCol}" />
<c:set var="fieldCol" value="${(empty fieldCol) ? 'col-sm-10' : fieldCol}" />

<div class="form-group">
	<label class="control-label ${labelCol}"> <c:if test="${not empty label}">
			<spring:message code="${label}" />
		</c:if>
	</label>
	<div class="${fieldCol}">
		<jsp:doBody />
	</div>
</div>