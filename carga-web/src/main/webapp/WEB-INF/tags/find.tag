<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

   
<%@ attribute name="nombrePopUp" required="true"%>
<%@ attribute name="titlePopUp" required="true"%>
<%@ attribute name="idCampo" required="true"%>
<%@ attribute name="idCampoOculto" required="true"%>
<%@ attribute name="datoAmostrar" required="true"%>
<%@ attribute name="require" required="false"%>
<%@ attribute name="concatenarId" required="false"%>
<%@ attribute name="campoDependencia" required="false"%>
<%@ attribute name="rowId" required="false"%>
<%@ attribute name="path" required="false"%>

<c:set var="nombrePopUp" value="${nombrePopUp}" />
<c:set var="titlePopUp" value="${titlePopUp}" />
<c:set var="idCampo" value="${idCampo}" />
<c:set var="idCampoOculto" value="${idCampoOculto}" />
<c:set var="datoAmostrar" value="${datoAmostrar}" />
<c:set var="require" value="${(empty require) ? 'false' : require}" />
<c:set var="concatenarId" value="${(empty concatenarId) ? 'true' : concatenarId}" />
<c:set var="campoDependencia" value="${(empty campoDependencia) ? '' : campoDependencia}" />
<c:set var="rowId" value="${(empty rowId) ? 'id' : rowId}" />

<c:choose>
	<c:when test="${(empty path)}">
		<c:set var="path" value="${idCampo}" />
	</c:when>
	<c:otherwise>
		<c:set var="path" value="${path}" />	
	</c:otherwise>
</c:choose>



<div class="input-group">
<%--    <input type="text" class="form-control" id="${idCampo}" readonly="readonly"> --%>
   <form:input path="${path}" id="${idCampo}"  class="form-control" readonly="true"/>
   
   <form:hidden path="${idCampoOculto}" name="${idCampoOculto}" id="${idCampoOculto}"/>
   <div class="input-group-btn">
     <button class="btn btn-outline-secondary" id="findButton" type="button"><i class="fa fa-search"></i></button>
   </div>
</div>

   
<script type="text/javascript">
	$(function() {
		$("#findButton").click(function(){
			    BootstrapDialog.show({
	              title: "${titlePopUp}",
	              message: $('<div></div>').load(comboPopupUrl + "${nombrePopUp}"),
	              onshown: function(dialogRef){
	            	  dialogRef.getModalContent().find('#tablePopUp').on('click-row.bs.table', function(e, row){
	            		  $("#"+ "${idCampoOculto}").val(row.${rowId});
	                	  if("${concatenarId}" == 'true'){
	                		  $("#"+ "${idCampo}").val($("#"+ "${idCampoOculto}").val() + " - " + row.${datoAmostrar});
	                		  <c:if test="${!(empty campoDependencia)}">
                			  	$("#"+ "${campoDependencia}").val($("#"+ "${idCampoOculto}").val() + " - " + row.${campoDependencia});
	                		  </c:if>
	                		 
	                	  }else{
	                		  console.log("#"+ "${idCampo}");
	                		  $("#"+ "${idCampo}").val(row.${datoAmostrar});
	                		  
	                		  <c:if test="${!(empty campoDependencia)}">
	                		  $("#"+ "${campoDependencia}").val(row.${campoDependencia});
	                		  </c:if>
	                		  
	                			  
	                		  	                		  
	                	  }
	            		  dialogRef.close();
	            	  }); 
	            	  
	            	  
// 	            	  on('click', 'tbody tr', function(event) {
// 	            		  
	            		  	            		  
// 	            	      });
	              },
	              buttons: [ {
	                  label: 'Cerrar',
	                  action: function(dialogo){
	                	  dialogo.close();
	                  }
	              }]
		        });
		});
		//
		
		//
		if("${require}" === "true"){
			$("#" + "${idCampo}").attr("required",true);
		}else{
			$("#" + "${idCampo}").attr("required",false);
		}

	});
</script>