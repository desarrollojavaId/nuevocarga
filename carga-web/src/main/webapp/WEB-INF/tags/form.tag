<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ attribute name="id" required="true"%>
<%@ attribute name="action_url" required="true"%>
<%@ attribute name="parsley" required="true"%>
<%@ attribute name="modelAttribute" required="true"%>
<%@ attribute name="method" required="false"%>
<%@ attribute name="cssClass" required="false"%>
<%@ attribute name="enctype" required="false"%>

<c:url var="form_url" value="${action_url}" />
<c:set var="method" value="${(empty method) ? 'post' : method}" />
<c:set var="cssClass"
	value="${(empty cssClass) ? 'form-horizontal' : cssClass}" />
<form:form id="${id}" action="${form_url}"
	modelAttribute="${modelAttribute}" cssClass="${cssClass} frm"
	data-parsley-validate="${parsley}" enctype="${enctype}">

	<jsp:doBody />
</form:form>