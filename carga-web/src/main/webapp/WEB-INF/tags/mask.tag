<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%@ attribute name="require" required="false"%>
<%@ attribute name="id_campo" required="false"%>
<%@ attribute name="pattern" required="true"%>
<%@ attribute name="title" required="false"%>


<c:set var="require" value="${(empty require) ? 'false' : require}" />
<c:set var="id_campo" value="${(empty id_campo) ? '_campo_mascara_id' : id_campo}" />
<c:set var="pattern" value="${pattern}" />
<c:set var="title" value="${(empty title) ? 'Campo invalido' : title}" />


<c:choose>
    <c:when test="${require}">
       <input id="${id_campo}" class="form-control" required="${require}" data-parsley-maxlength="100" pattern="${pattern}" title="${title}"/> 
    </c:when>    
    <c:otherwise>
       <input id="${id_campo}" class="form-control" data-parsley-maxlength="100" pattern="${pattern}" title="${title}"/>
    </c:otherwise>
</c:choose>



<script type="text/javascript">

</script>




