<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%@ attribute name="require" required="false"%>
<%@ attribute name="readOnly" required="false"%>
<%@ attribute name="consultaNotIn" required="false"%>
<%@ attribute name="funcionSubCodigo" required="false"%>
<%@ attribute name="nombre_campo" required="true"%>
<%@ attribute name="path" required="false"%>
<%@ attribute name="tabla" required="true"%>
<%@ attribute name="codigo" required="true"%>
<%@ attribute name="subCodigo" required="true"%>
<%@ attribute name="value" required="false"%>
<%@ attribute name="funcionCodigoDescripcion" required="false"%>


<c:set var="require" value="${(empty require) ? 'false' : require}" />
<c:set var="consultaNotIn" value="${(empty consultaNotIn) ? 'false' : consultaNotIn}" />
<c:set var="funcionSubCodigo" value="${(empty funcionSubCodigo) ? 'false' : funcionSubCodigo}" />
<c:set var="funcionCodigoDescripcion" value="${(empty funcionCodigoDescripcion) ? 'false' : funcionCodigoDescripcion}" />
<c:set var="readOnly" value="${(empty readOnly) ? 'false' : readOnly}" />
<c:set var="nombre_campo" value="${nombre_campo}" />
<c:set var="path"  value="${(empty path) ? '' : path}" />
<c:set var="tabla" value="${tabla}" />
<c:set var="codigo" value="${codigo}" />
<c:set var="subCodigo" value="${subCodigo}" />
<c:set var="value" value="${value}" />




<c:choose>
    <c:when test="${empty path}">
       <input name="${nombre_campo}" id="${nombre_campo}" class="form-control" value ="${value}" data-tabla="${tabla}" data-funcionCodigoDescripcion="${funcionCodigoDescripcion}" data-codigo="${codigo}" data-funcionSubCodigo="${funcionSubCodigo}" data-subCodigo="${subCodigo}"/> 
    </c:when>    
    <c:otherwise>
      <form:input path="${path}" name="${nombre_campo}" id="${nombre_campo}" class="form-control" data-tabla="${tabla}" data-codigo="${codigo}" data-funcionCodigoDescripcion="${funcionCodigoDescripcion}" data-funcionSubCodigo="${funcionSubCodigo}" data-subCodigo="${subCodigo}"/>    
    </c:otherwise>
</c:choose>


<script type="text/javascript">
    //
    var funcionMstablas = {
 	   allowClear : true,
	   toText : function(data) {
	      return data.id.codigo + " - " + data.descripcion;
	   }, toId: function(data) {
		  return data.id.codigo
	   }
    };
    
    var funcionMstablasSubCodigo = {
 	   allowClear : true,
       toText : function(data) {
          return data.id.subCodigo + " - " + data.descripcion;
    	}, toId: function(data) {
    	  return data.id.subCodigo
    	}
    };
    //
    var funcionMstablasDescripcion = {
       allowClear : true,
       toText : function(data) {
    	  return data.descripcion;
       }, toId: function(data) {
    	  return data.id.codigo
       }
    };
    //	    
    var funcionMstablasSubCodigoDescripcion = {
       allowClear : true,
       toText : function(data) {
          return data.descripcion;
       }, toId: function(data) {
          return data.id.subCodigo
       }
    };
    //
	$(function(){
	    var tabla = $("#" + "${nombre_campo}").data("tabla");
	    var codigo = $("#" + "${nombre_campo}").data("codigo") == undefined ? null : $("#" + "${nombre_campo}").data("codigo");
	    var subCodigo = $("#" + "${nombre_campo}").data("subcodigo") == undefined ? 0 : $("#" + "${nombre_campo}").data("subcodigo");
	    var consultaNotIn = $("#" + "${nombre_campo}").data("consultanotin")  == undefined ? false : $("#" + "${nombre_campo}").data("consultanotin");;
	    var datosConQueBuscar = tabla + ',' + codigo + ',' + subCodigo + ',' + consultaNotIn;
	    var funcionSubCodigo = $("#" + "${nombre_campo}").data("funcionsubcodigo")  == undefined ? false : $("#" + "${nombre_campo}").data("funcionsubcodigo");
	    var funcionCodigoDescripcion = $("#" + "${nombre_campo}").data("funcioncodigodescripcion")  == undefined ? false : $("#" + "${nombre_campo}").data("funcioncodigodescripcion");
	    var funcion;
	    //Se valida el tipo de funcion que manejara los datos a usar
	    if(funcionCodigoDescripcion){
	    	funcion = funcionSubCodigo ? funcionMstablasSubCodigoDescripcion : funcionMstablasDescripcion;
	    }else {
	    	funcion = funcionSubCodigo ? funcionMstablasSubCodigo : funcionMstablas;	
	    }
		if("${require}" === "true"){
			$("#" + "${nombre_campo}").attr("required",true);
		}else{
			$("#" + "${nombre_campo}").attr("required",false);
		}
		if("${readOnly}" === "true"){
			$("#" + "${nombre_campo}").attr("readonly",true);
		}
	    //
        $('#'+ "${nombre_campo}" ).comboBoxPaged(mStablaUrl + datosConQueBuscar,mStablaIdUrl,funcion);
	});
</script>