<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="/WEB-INF/jsp/include.jsp"%>

<%@ tag dynamic-attributes="dynattrs" %> 
<%@ attribute name="decimal" required="false"%>
<%@ attribute name="require" required="false"%>
<%@ attribute name="id_campo" required="false"%>
<%@ attribute name="nombre_campo" required="true"%>
<%@ attribute name="path" required="true"%>
<%@ attribute name="min" required="false"%>
<%@ attribute name="max" required="false"%>
<%@ attribute name="valor" required="false"%>
<%@ attribute name="blockChar" required="false" description="Bloquea el tipeo de cualquier valor no n�merico"%>


<c:set var="parsleyType" value="${(empty decimal) ? 'digits' : 'number'}" />
<c:set var="require" value="${(empty require) ? 'false' : require}" />
<c:set var="nombre_campo" value="${nombre_campo}" />
<c:set var="id_campo" value="${(empty id_campo) ? nombre_campo : id_campo}" />
<c:set var="path" value="${path}" />
<c:set var="min" value="${(empty max) ? '1' : min}" />
<c:set var="max" value="${(empty max) ? '100' : max}" />
<c:set var="blockChar" value="${(empty blockChar) ? 'false' : blockChar}" />

<c:set var="valor" value="${valor}" />
<script type="text/javascript">
$(function(){
	if("${blockChar}" == "true"){
		console.log("Paso");	
		$('#'+ "${id_campo}" ).numeric();
	}	
});


</script>

<c:choose>
    <c:when test="${path}">
       <form:input path="${nombre_campo}" name="${nombre_campo}" data-parsley-type="${parsleyType}" class="form-control" minlength="${min}" maxlength="${max}" /> 
    </c:when> 
    <c:when test="${path == 'false' && require == 'true'}">
       <input id="${id_campo}" class="form-control" maxlength="${max}" data-parsley-type="${parsleyType}" value="${valor}" required="${require}" data-parsley-type="number" name="${nombre_campo}" /> 
    </c:when>   
    <c:otherwise>
      <input id="${id_campo}" class="form-control" maxlength="${max}" data-parsley-type="${parsleyType}"  data-parsley-type="number" name="${nombre_campo}" />
    </c:otherwise>
</c:choose>