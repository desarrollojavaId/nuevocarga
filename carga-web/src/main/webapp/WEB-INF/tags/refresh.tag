<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ attribute name="tablesel" required="false"%>

<c:set var="tableselector" value="${(empty tablesel) ? '#table' : tablesel}" />				
				
<button class="btn btn-default" type="button" name="refresh" title="Refresh" onclick="$('${tableselector}').bootstrapTable('refresh')">
	<i class="fa fa-refresh"></i>
</button>