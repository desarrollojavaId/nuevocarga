<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ tag dynamic-attributes="dynattrs" %> 
<%@ attribute name="id" required="true"%>
<%@ attribute name="dataUrl" required="true"%>

<%@ attribute name="showSearch" required="false"%>
<%@ attribute name="toolbarSelector" required="false"%>
<%@ attribute name="rowPropertyId" required="false"%>
<%@ attribute name="checkbox" required="false"%>
<%@ attribute name="checkboxHiddenSelector" required="false"%>
<%@ attribute name="radiobox" required="false"%>
<%@ attribute name="radioboxHiddenSelector" required="false"%>
<%@ attribute name="datosAMostrarPopUp" required="false"%>
<%@ attribute name="campoDependeciaPopUp" required="false"%>
<%@ attribute name="ignoreColumnsExport" required="false"%>

<c:url var="tableUrl" value="${dataUrl}" />
<c:set var="rowPropertyId" value="${(empty rowPropertyId) ? 'id' : rowPropertyId}" />
<c:set var="search" value="${(empty showSearch) ? 'false' : showSearch}" />
<c:set var="toolbar" value="${(empty toolbarSelector) ? '' : toolbarSelector}" />
<c:set var="checklist" value="${(empty checkbox) ? false : checkbox}" />
<c:set var="radiolist" value="${(empty radiobox) ? false : radiobox}" />
<c:set var="radioboxHiddenSelector" value="${(empty radioboxHiddenSelector) ? '#seleccion_actual' : radioboxHiddenSelector}" />
<c:set var="datosAMostrarPopUp" value="${(empty datosAMostrarPopUp) ? ' ' : datosAMostrarPopUp}" />
<c:set var="campoDependeciaPopUp" value="${(empty campoDependeciaPopUp) ? 'false' : campoDependeciaPopUp}" />
<c:set var="ignoreColumnsExport" value="${(empty ignoreColumnsExport) ? [] : ignoreColumnsExport}" />

<c:if test="${checklist}">
	<c:set var="updateChecklistField">
		<c:if test="${not empty checkboxHiddenSelector}">
			$('${checkboxHiddenSelector}').val(JSON.stringify(selections_${id}));
		</c:if>
	</c:set>
	<script type="text/javascript">
		var selections_${id} = [];
		
		function ${id}_responseHandler(res) {
			$.each(res.rows, function(i, row) {
				row.state = $.inArray(row.${rowPropertyId}, selections_${id}) !== -1;
			});
			return res;
		}
		
		$(function(){
			$('#${id}').on('check.bs.table check-all.bs.table uncheck.bs.table uncheck-all.bs.table',
					function(e, rows) {
						var ids = $.map(!$.isArray(rows) ? [ rows ]
								: rows, function(row) {
							return row.${rowPropertyId};
						});
						func = $.inArray(e.type, [ 'check',
								'check-all' ]) > -1 ? 'union'
								: 'difference';
						selections_${id} = _[func](selections_${id}, ids);
						${updateChecklistField}
			});
		})
		
	</script>
</c:if>

<c:if test="${radiolist}">
    <input type="hidden" id="seleccion_actual">
    <c:if test="${not empty datosAMostrarPopUp}">
        <input type="hidden" id="${datosAMostrarPopUp}">
        <c:if test="${not empty campoDependeciaPopUp}">
           <input type="hidden" id="table_${campoDependeciaPopUp}">
        </c:if>
    </c:if>
	<c:set var="updateChecklistField">
		<c:if test="${not empty radioboxHiddenSelector}">
			$('${radioboxHiddenSelector}').val(selection_${id});
		</c:if>
	</c:set>
	<script type="text/javascript">
		var selection_${id} = null;
		function ${id}_responseHandler(res) {
			$.each(res.rows, function(i, row) {
				row.state = row.${rowPropertyId} === selection_${id};
			});
			return res;
		}
		
		$(function(){
			$('#${id}').on('check.bs.table',function(e, row) {
				 var id = row.${rowPropertyId};
				 selection_${id} = id;
				 console.log(id);
				 ${updateChecklistField}
				 if("${datosAMostrarPopUp}" != ""){
					 $("#"+ "${datosAMostrarPopUp}").val(row.${datosAMostrarPopUp});
				 }
		         //Se valida si el model de "Find" tiene un campo de dependencia a mostrar a parte del seleccionado
				 if("${campoDependeciaPopUp}" != "false"){
					 $("#table_"+ "${campoDependeciaPopUp}").val(row.${campoDependeciaPopUp});
				 }
				 
			});
		})
		
	</script>
</c:if>

<table id="${id}" data-toggle="table" data-url="${tableUrl}" data-toolbar="${toolbar}" data-toolbar-align="right" data-ignroleColumns="${ignoreColumnsExport}" data-search="${search}"
	data-search-align="left" data-side-pagination="server" data-show-export="true" data-pagination="true"
	<c:if test="${checklist or radiolist}">
		data-response-handler="${id}_responseHandler"
	</c:if>
	
	<c:forEach items="${dynattrs}" var="da"> 
		${da.key}="${da.value}"
	</c:forEach> 
	>
	<thead>
		<tr>
			<c:if test="${checklist}">
			<th data-field="state" data-checkbox="true"></th>
			</c:if>
			<c:if test="${radiolist}">
			<th data-field="state" data-radio="true"></th>
			</c:if>
			<jsp:doBody />
		</tr>
	</thead>
</table>
