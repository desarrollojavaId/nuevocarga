<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<%@ tag dynamic-attributes="dynattrs"%>
<%@ attribute name="isList" required="false"
	description="Define si es un titulo de lista"%>

<c:set var="isList" value="${(empty isList) ? false : isList}" />
<c:set var="entityTitulo" value="${(empty entity.tituloPantalla) ? 'sinTitulo' : entity.tituloPantalla}" />
<c:set var="tituloPantalla" value="${(empty tituloPantalla) ? 'sinTitulo' : tituloPantalla}" />

<tiles:putAttribute name="content-header">
	<script type="text/javascript">
		
	</script>

	<h1>
		<c:choose>
			<c:when test="${isList}">
				<spring:message code="${tituloPantalla}" />
			</c:when>
			<c:otherwise>
				<spring:message code="${entityTitulo}" />
			</c:otherwise>
		</c:choose>
	</h1>

</tiles:putAttribute>
