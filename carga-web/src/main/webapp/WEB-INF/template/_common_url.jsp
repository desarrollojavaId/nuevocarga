<%@ include file="/WEB-INF/jsp/include.jsp"%>
<script type="text/javascript">
	var listUrl = '<c:url value=""/>';
	
	var tipoModeloUrl = '<c:url value="/combo/tipoModelo/paged/recordsByName" />';
	var tipoModeloIdUrl = '<c:url value="/combo/tipoModelo/get" />?id=';
	
	var claseModeloUrl = '<c:url value="/combo/claseModelo/paged/recordsByName" />';
	var claseFechaUrl = '<c:url value="/fecha/obtener/feriados" />';
	var claseModeloIdUrl = '<c:url value="/combo/claseModelo/get" />?id=';
	
	var concentradorIdUrl = '<c:url value="/combo/concentrador/get" />?id=';
	var concentradorUrl = '<c:url value="/combo/concentrador/paged/recordsByName" />';
	
	var adherenteIdUrl = '<c:url value="/combo/adherente/get" />?id=';
	var adherenteUrl = '<c:url value="/combo/adherente/paged/recordsByName" />';

	var mStablaIdUrl = '<c:url value="/mstablas/finByTablaCodigoSubCodigoId"/>?tablaSubCodigo=';
	var mStablaUrl = '<c:url value="/mstablas/finByTablaCodigoSubCodigo"/>?tablaSubCodigo=';
	var mStablaDescripcionUrl = '<c:url value="/mstablas/finByTablaCodigoSubCodigoDescripcion"/>?tablaSubCodigo=';
	
	var convenioIdUrl = '<c:url value="/combo/convenio/get" />?id=';
	var convenioUrl = '<c:url value="/combo/convenio/paged/recordsByName" />';
	
	var subConvenioIdUrl = '<c:url value="/combo/subConvenio/get" />?id=';
	var subConvenioUrl = '<c:url value="/combo/subConvenio/paged/recordsByName" />';
	var subConvenioByConvenio = '<c:url value="/subConvenio/combo/showByConvenio" />?id='; 

	var canalDeIngresoIdUrl = '<c:url value="/canalDeIngreso/get" />?id=';
	var canalDeIngresoUrl = '<c:url value="/canalDeIngreso/paged/recordsByName" />';
	
	var formaDeCobroIdUrl = '<c:url value="/formaDeCobro/get" />?id=';
	var formaDeCobroByCanalDeIngresoUrl = '<c:url value="/formaDeCobro/paged/recordsByCanalDeIngreso"/>?canalDeIngresoId=';
	
	var comboPopupUrl = '<c:url value="/combo/popup" />?popup=';
	
	var limpirFiltrosUrl = '<c:url value="/combo/limpiar_filtro" />?nombreFiltro=';
	
	var comisionCategoriaUrl = '<c:url value="/comisionCategoria/records" />';

	var comisionCategoriaUrlFiltro = '<c:url value="/comisionCategoria/records" />?filtroMinimoAseg=1';

	var comisionCategoriaByPaqueteUrl = '<c:url value="/comisionCategoria/recordsByPaquete" />?paquete=';
	var comisionCategoriaIdUrl ='<c:url value="/comisionCategoria/get" />?id=';

	var comisionUrl = '<c:url value="/comisionDetalle/recordsCombo" />?categoriaComision=';
	var comisionIdUrl ='<c:url value="/comisionDetalle/get" />?id=';
	
	var eventoCategoriaUrl = '<c:url value="/eventoCategoria/records" />';
	var eventoCategoriaIdUrl = '<c:url value="/eventoCategoria/get" />?id=';
	var eventoCategoriaByProductoDetalleUrl = '<c:url value="/eventoCategoria/recordsByProducto" />?productoDetalle=';
	
	var eventoUrl = '<c:url value="/eventoDetalle/recordsCombo" />?categoriaEventoId=';
	var eventoByProductoAtributosUrl = '<c:url value="/eventoDetalle/recordsComboByProductoAtributos" />?categoriaEventoId=';
	var eventoIdUrl = '<c:url value="/eventoDetalle/get" />?id=';
	
	var productoCategoriaUrl = '<c:url value="/productoCategoria/records" />';
	var productoCategoriaIdUrl = '<c:url value="/productoCategoria/get" />?id=';
	
	var productoUrl = '<c:url value="/productoDetalle/records" />?categoriaId=';
	var productoIdUrl = '<c:url value="/productoDetalle/get" />?id=';
	var productoDetalleByClienteUrl = '<c:url value="/productoDetalle/records" />?categoriaId=';
	
	var paqueteTarifaUrl = '<c:url value="/paqueteDeTarifas/records" />';
	var paqueteTarifaByClienteUrl = '<c:url value="/paqueteDeTarifas/recordsByCliente" />?cliente=';
	var paqueteTarifaIdUrl = '<c:url value="/combo/paqueteDeTarifas/get" />?id=';

	var clienteUrl = '<c:url value="/combo/cliente/paged/recordsByName" />';
	var clienteIdUrl = '<c:url value="/combo/cliente/get" />?id=';

	var puntoMenuUrl = '<c:url value="/combo/puntoMenu/paged/recordsByName" />';
	var puntoMenuIdUrl = '<c:url value="/combo/puntoMenu/get" />?id=';
	
	var puntoMenuDetalleUrl = '<c:url value="/puntoMenuDetalle/records" />?categoriaPuntoMenuId=';
	var puntoMenuDetalleIdUrl = '<c:url value="/puntoMenuDetalle/get" />?id=';
	
	var grupoUrl = '<c:url value="/combo/grupo/paged/recordsByName" />';
	var grupoIdUrl = '<c:url value="/combo/grupo/get" />?id=';
	
	var urlPuntoMenuUrl = '<c:url value="/combo/puntoMenuUrl/paged/recordsByName" />';
	var urlPuntoMenuIdUrl = '<c:url value="/combo/puntoMenuUrl/get" />?id=';
	
	var atributoUrl = '<c:url value="/combo/atributo/paged/recordsByName" />';
	var atributoIdUrl = '<c:url value="/combo/atributo/get" />?id=';
	var atributoByProductodUrl = '<c:url value="/atributo/getByProducto" />?productoDetalle=';
	
	var atributoValorUrl = '<c:url value="/atributoValor/records" />?atributoId=';
	var atributoValorProductoUrl = '<c:url value="/atributoValor/recordsByProductoAtributo" />?atributoId=';
	var atributoValorIdUrl = '<c:url value="/atributoValor/get" />?id=';

	/////////////// _NUEVO CARGA_ //////////////////////////
	
// 	var usuariosUrl = '<c:url value="/usuario/getByNombreApellido" />';
	var usuariosUrl = '<c:url value="/combo/usuario/paged/recordsByName" />';
	var usuariosIdUrl = '<c:url value="/combo/usuario/get" />?id=';
	
	var usuEmpleadoUrl = '<c:url value="/combo/usuEmpleado/paged/recordsByName" />';
	var usuEmpleadoLiderUrl = '<c:url value="/usuEmpleado/findByLider" />?esLider=SI';
	var usuEmpleadoIdUrl = '<c:url value="/combo/usuEmpleado/get" />?id=';

	//var areaUrl = '<c:url value="/area/records" />';
	var areaUrl = '<c:url value="/combo/area/paged/recordsByName" />';
	var areaIdUrl = '<c:url value="/combo/area/get" />?id=';
	
	//var sectorUrl = '<c:url value="/sector/records" />';
	var sectorUrl = '<c:url value="/combo/sector/paged/recordsByName" />';
	var sectorIdUrl = '<c:url value="/combo/sector/get" />?id=';
	
	//var contratoUrl = '<c:url value="/contrato/records" />';
	var clienteContratoUrl = '<c:url value="/contrato/findClienteContrato" />';
	var contratoUrl = '<c:url value="/combo/contrato/paged/recordsByName" />';
	var contratoIdUrl = '<c:url value="/combo/contrato/get" />?id=';
	
	//var usuClienteUrl = '<c:url value="/usuCliente/records" />';
	var usuClienteUrl = '<c:url value="/combo/usuCliente/paged/recordsByName" />';
	var usuClienteIdUrl = '<c:url value="/combo/usuCliente/get" />?id=';

	var requerimientoUrl = '<c:url value="/combo/requerimiento/paged/recordsByName" />';
	var requerimientoIdUrl = '<c:url value="/combo/requerimiento/get" />?id=';
	
</script>