<%@ include file="/WEB-INF/jsp/include.jsp"%>
<script type="text/javascript">
      //
      var funcionPaqueteTarifa ={
         allowClear : true,
         toText : function(data) {
           return data.codPaqComis + "-" + data.descripcion;
         },
         toId: function(data) {
           return data.id;
         }
      };
      //
      var funcionCategoriaComision = {
        allowClear : true,
        toText : function(data) {
           return data.categoriaComision + "-" + data.descripcionComision;
        },
        toId: function(data) {
           return data.id;
        }
      };
      //
      var funcionComision = {
        allowClear : true,
        toText : function(data) {
           return data.codigoComision + "-" + data.descripcionComision;
        },
        toId: function(data) {
           return data.id;
        }
      };
      //
	  var funcionEventoCategoria = {
		allowClear : true,
		toText : function(data) {
		   return data.categoriaEvento + "-" + data.descripcionEvento;
		 },
		 toId: function(data) {
           return data.id;
		 }
	   };
	   //
	   var funcionEvento = {
		 allowClear : true,
		 toText : function(data) {
			return data.codigoEvento + "-" + data.descripcionEvento;
		 },
		 toId: function(data) {
			return data.id;
		 }
	   };
	   var funcionProducto = {
         allowClear : true,
         toText : function(data) {
           return data.descripcionProducto;
         },
         toId: function(data) {
           return data.id;
         }
       };
	   //
	   var funcionCliente = {
		 allowClear : true,
		 toText : function(data) {
		     return data.cuit.toString();
		 },
		 toId: function(data) {
		     return data.id;
		 }
	   };
	   
	   var funcionClienteCuit =  {
		 allowClear : true,
		 toText : function(data) {
		     return data.razonSocial + " - " + data.cuit.toString();
		 },
		 toId: function(data) {
		     return data.id;
		 }
	  };
	   
	   //
	   
	   var funcionCatPuntoMenu = {
         allowClear : true,
         toText : function(data) {
           return data.descripcionPuntoMenu;
         },
         toId: function(data) {
           return data.id;
         }
       };
	   
	   var funcionPuntoMenuUrl = {
         allowClear : true,
         toText : function(data) {
           return data.url;
         },
         toId: function(data) {
           return data.id;
         }
       };
	   
	   
	   var funcionAtributo = {
		 allowClear : true,
		 toText : function(data) {
		   return data.nombreAtributo;
		 },
		 toId: function(data) {
		    return data.id;
		 }
       };
	   
	   var funcionAtributoValor = {
		 allowClear : true,
		 toText : function(data) {
		    if(data.codigoAtributo != undefined ){
			  return data.codigoAtributo  + " - " + data.valorAtributo;	 
			}else {
			  return data.valorAtributo
			}
		 },
		   toId: function(data) {
		    return data.id;
		 }
	   };
	   
	   var funcionUsuario ={
		         allowClear : true,
		         toText : function(data) {
		           return data.nombre + ", " + data.apellido;
		         },
		         toId: function(data) {
		           return data.id;
		         }
		      };
	   
	   var funcionClienteContrato ={
		         allowClear : true,
		         toText : function(data) {
		           return data.cliente.razonSocial + " (" + data.descCategoria + ") " + data.nombre;
		         },
		         toId: function(data) {
		           return data.id;
		         }
		      };
	   
	   var funcionArea ={
		         allowClear : true,
		         toText : function(data) {
		           return data.descripcion;
		         },
		         toId: function(data) {
		           return data.id;
		         }
		      };
	   
	   var funcionSector ={
		         allowClear : true,
		         toText : function(data) {
		           //return data.descripcion + " - " + data.area.id;
		        	 return data.descripcion;
		         },
		         toId: function(data) {
		           return data.id;
		         }
		      };
	   
	   var funcionContrato ={
		         allowClear : true,
		         toText : function(data) {
		           //return data.descripcion + " - " + data.area.id;
		        	 return data.descripcion;
		         },
		         toId: function(data) {
		           return data.id;
		         }
		      };
	   
	   var funcionUsuCliente ={
		         allowClear : true,
		         toText : function(data) {
		           //return data.descripcion + " - " + data.area.id;
			           return data.nombre + ", " + data.apellido;
		         },
		         toId: function(data) {
		           return data.id;
		         }
		      };
	   
	   var funcionRequerimiento ={
		         allowClear : true,
		         toText : function(data) {
			           return data.nuss + " - " + data.titulo;
		         },
		         toId: function(data) {
		           return data.id;
		         }
		      };
	   
</script>


