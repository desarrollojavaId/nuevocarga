<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include.jsp"%>
<!DOCTYPE html>
<html>
<tiles:insertAttribute name="header" />
<body class="hold-transition skin-blue-light sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b><spring:message code="app.titulo.mini"/></b></span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><spring:message code="app.titulo"/></span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
					role="button"> <span class="sr-only"><spring:message code="app.togglenav"/></span>
				</a>
				<div class="navbar-custom-menu">
					<form id="logout_form" style="display: none;"
						action="<c:url value="/logout"/>" method="post">
					</form>
					<ul class="nav navbar-nav">
						<li><a href="#"
							onclick="javascript:$('#logout_form').submit();"><i
								class="fa fa-sign-out"></i></a></li>
					</ul>
				</div>
			</nav>
		</header>
		<tiles:insertAttribute name="menu" />
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<tiles:insertAttribute name="content-header" defaultValue="" />
			</section>
			<!-- Main content -->
			<section class="content">
				<tiles:insertAttribute name="body" />
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<tiles:insertAttribute name="footer" />
	</div>
	<!-- ./wrapper -->
</body>
</html>