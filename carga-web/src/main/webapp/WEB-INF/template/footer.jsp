<%@ include file="/WEB-INF/jsp/include.jsp"%>

<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b><spring:message code="title.version"/></b>
		<spring:message code="version" />
	</div>
	<strong><spring:message code="app.copyright"/></strong>
</footer>