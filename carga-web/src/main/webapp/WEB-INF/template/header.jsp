<%@ include file="/WEB-INF/jsp/include.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="<c:url value="/public/favicon.ico"/>" type="image/x-icon"/>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.5 -->

<link rel="stylesheet" href="<c:url value="/public/css/bootstrap.min.css"/>">

<!-- Font Awesome -->
<link rel="stylesheet" href="<c:url value="/public/css/font-awesome.min.css"/>">
<!-- Ionicons -->
<link rel="stylesheet" href="<c:url value="/public/css/ionicons.min.css"/>">
<!-- Theme style -->
<link rel="stylesheet" href="<c:url value="/public/css/adminlte.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/skin-blue-light.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/bootstrap-table.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/bootstrap-dialog.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/select2.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/select2-bootstrap.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/bootstrap-datepicker3.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/bootstrap-datetimepicker.min.css"/>">
<!-- <<<<<<< HEAD -->
<link rel="stylesheet" href="<c:url value="/public/css/style.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/jquery.contextMenu.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/public/css/ui.fancytree.min.css"/>" />
		
<!-- ======= -->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script src="<c:url value="/public/js/jquery-2.1.4.min.js"/>"></script>
<script src="<c:url value="/public/js/jquery-ui.min.js"/>"></script>
<script src="<c:url value="/public/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/public/js/moment.min.js"/>"></script>
<script src="<c:url value="/public/js/bootstrap-datepicker.min.js"/>"></script>
<script src="<c:url value="/public/js/bootstrap-datetimepicker.min.js"/>"></script>
<script src="<c:url value="/public/js/jquery-dateFormat.min.js"/>"></script>
<script src="<c:url value="/public/js/app.js"/>"></script>
<script src="<c:url value="/public/js/parsley.min.js"/>"></script>
<script src="<c:url value="/public/js/comparison.js"/>"></script>
<script src="<c:url value="/public/js/bootstrap-table.js"/>"></script>
<script src="<c:url value="/public/js/FileSaver.min.js"/>"></script>
<script src="<c:url value="/public/js/jspdf.min.js"/>"></script>
<script src="<c:url value="/public/js/jspdf.plugin.autotable.js"/>"></script>
<script src="<c:url value="/public/js/xlsx.core.min.js"/>"></script>
<script src="<c:url value="/public/js/bootstrap-table-export.js"/>"></script>
<script src="<c:url value="/public/js/tableExport.min.js"/>"></script>
<script src="<c:url value="/public/js/bootstrap-dialog.min.js"/>"></script>
<script src="<c:url value="/public/js/select2.js"/>"></script>
<script src="<c:url value="/public/js/bootstrap-datepicker.min.js"/>"></script>
<script src="<c:url value="/public/js/jquery.maskedinput.min.js"/>"></script>
<script src="<c:url value="/public/js/nano.js"/>"></script>
<script src="<c:url value="/public/js/common.js"/>"></script>
<script src="<c:url value="/public/js/lodash.min.js"/>"></script>
<script src="<c:url value="/public/js/jquery.git.comboBoxMail.js"/>"></script>
<script src="<c:url value="/public/js/jquery.git.comboBoxPaged.js"/>"></script>
<script src="<c:url value="/public/js/jquery.git.comboBoxMultiple.js"/>"></script>
<script src="<c:url value="/public/js/jquery.numeric.js"/>"></script>
<script src="<c:url value="/public/js/jquery.contextMenu.min.js"/>"></script>
<script src="<c:url value="/public/js/jquery.fancytree-all.min.js"/>"></script>
<%@ include file="/WEB-INF/template/_common_url.jsp" %>
<%@ include file="/WEB-INF/template/_common_url_functionCombo.jsp" %>

<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({autoclose: true, language: 'es'});
		// Desabilito el autocompletado en todos los formularios
		$('form,input,select,textarea').attr("autocomplete", "off");
		

		
		<c:if test="${pageContext.request.serverName != 'localhost'}">
			$(document).bind("contextmenu",function(e){
				return false;
			});
		</c:if>		
		
	});

</script>
</head>