create or replace TRIGGER Auditoria_MSBONIFICACION
   FOR UPDATE OR INSERT OR DELETE ON MSBONIFICACION
/* ----------------------------------------------------------------------------
     Se encarga de registrar en Logtransacciones,los cambios de valores 
     en los campos, por inserción, actualización o borrado
    ------------------------------------------------------------------------ */

Compound TRIGGER


V_Rowid ROWID; 
V_Tabla VARCHAR2(30) := 'MSBONIFICACION'; --#indicar nombre de tabla

TYPE Type_Rowids IS TABLE OF ROWID INDEX BY BINARY_INTEGER; 
M_Rowids Type_Rowids; 
M_Indice BINARY_INTEGER := 1; 
V_Current_Rowid ROWID;

-- Se lanzará después de cada fila actualizada
   AFTER EACH ROW IS
BEGIN
   IF Inserting THEN      
      V_Rowid := :New.Rowid;      
      M_Rowids(M_Indice) := V_Rowid;
      M_Indice := M_Indice + 1;   
   END IF;
   IF Updating THEN
      V_Rowid := :New.Rowid;      
      M_Rowids(M_Indice) := V_Rowid;
      M_Indice := M_Indice + 1;   
   END IF;
   IF Deleting THEN
      V_Rowid := :Old.Rowid;      
      M_Rowids(M_Indice) := V_Rowid;
      M_Indice := M_Indice + 1;   
   END IF;
EXCEPTION
   WHEN OTHERS THEN
      Dbms_Output.Put_Line('Error en AFTER EACH ROW');
   
END AFTER EACH ROW;

-- Se lanzará después de la sentencia
AFTER STATEMENT IS BEGIN
  IF M_Rowids.Count > 0 THEN 
     Process_Trigger.Set_Table_Name(V_Tabla); 
     Process_Trigger.Create_Column_Names;

      IF Inserting THEN
         FOR i IN M_Rowids.First .. M_Rowids.Last LOOP 
           V_Current_Rowid := M_Rowids(i);             
           Process_Trigger.Process_New_Values(V_Current_Rowid); 
           Process_Trigger.Insert_Log_Inserting; 
           Process_Trigger.Clean_Table_Columns;             
         END LOOP; 
      END IF;

      IF Updating THEN
         FOR i IN M_Rowids.First .. M_Rowids.Last LOOP            
           V_Current_Rowid := M_Rowids(i);          
           Process_Trigger.Process_Old_Values(V_Current_Rowid); 
           Process_Trigger.Process_New_Values(V_Current_Rowid); 
           Process_Trigger.Insert_Log_Updating; 
           Process_Trigger.Clean_Table_Columns;      
         END LOOP; 
      END IF;

      IF Deleting THEN
         FOR i IN M_Rowids.First .. M_Rowids.Last LOOP 
           V_Current_Rowid := M_Rowids(i);            
           Process_Trigger.Process_Old_Values(V_Current_Rowid); 
           Process_Trigger.Insert_Log_Deleting; 
           Process_Trigger.Clean_Table_Columns;
         END LOOP; 
      END IF;
      Process_Trigger.Delete_Table_Columns; 
  END IF; 
  EXCEPTION
     WHEN OTHERS THEN Dbms_Output.Put_Line('Error en AFTER STATEMENT');
  END
AFTER STATEMENT;

END;