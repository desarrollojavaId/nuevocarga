create or replace TRIGGER "MSEVENTOBANCO_TRG" AFTER INSERT ON MSEVENTOBANCO
FOR EACH ROW
  DECLARE
    msPaquete              NUMBER := 0;
    cantidadEventoVariable NUMBER := 1;
    msTarifaId             NUMBER;
    logProcesoId           NUMBER;
    movimientoCobrarId     NUMBER;
    msClientePaqueteId     NUMBER;
    modalidadDeCobro       VARCHAR(4);
    fecProceso             TIMESTAMP;
    procesar               VARCHAR(1);
    --
    --Cursor para buscar todas las comisiones relacionadas al evento que se disparo
    cursor eventoComisionCursor is 
    SELECT pre.*
	  FROM MSPRODUCTOEVENTOCOMISION pre 
     WHERE pre.MSPRODUCTOEVENTO_ID = :NEW.MSPRODUCTOEVENTO_ID;
     --
     eventoComisionCursorActual eventoComisionCursor%ROWTYPE;
  BEGIN
      --
      SELECT ID_SEQ_LOGPROCESO.NEXTVAL 
        into logProcesoId
        FROM DUAL;
      --
      SELECT cont.fecproceso
        INTO fecProceso
        FROM MSCONTROLSISTEMA  cont
       WHERE rownum  = 1 
       ORDER BY id;
      --
 	  INSERT INTO LOGPROCESOS
          (ID,NROPROCESO, PASOACCION, ESTADOPROCESO, GRABADODESDE, NODO, DATOSERROR,  FECHA,  USUARIO)
      VALUES(logProcesoId, 35, 'Inicio', 100, 'Trigger_MSEVENTOBANCO', 'Trigger_MSEVENTOBANCO', 0, :NEW.fecProceso, :NEW.USRPROCESO);
      --
      BEGIN
          SELECT distinct clip.MSPAQUETE_ID
                 ,clip.id
            INTO msPaquete
                 ,msClientePaqueteId
            FROM MSCLIENTE cli
            JOIN MSCLIENTEPAQUETE clip
              ON clip.MSCLIENTE_ID = cli.id
            JOIN MSPAQUETE mps
              ON mps.id = clip.MSPAQUETE_ID
            JOIN MSCLIENTEPAQUETEPRODUCTO clipr
              ON clipr.MSCLIENTEPAQUETE_ID = clip.ID
            JOIN MSTARIFA msta
              ON msta.MSPAQUETE_ID = mps.ID
           WHERE cli.NROCLIENTE = :NEW.NROCLIENTE
             AND clipr.IDPRODUCTOBANCO = :NEW.IDPRODUCTOBANCO
             AND clipr.MSPRODUCTO_ID = :NEW.CODIGOPRODUCTO
             AND mps.PRECARGADO = 0
             AND EXISTS (SELECT 1
                           FROM MSTARIFA mst
                          INNER JOIN MSPRODUCTOEVENTOCOMISION mspre
                             ON mspre.MSPRODUCTOEVENTO_ID = :NEW.MSPRODUCTOEVENTO_ID
                            AND mst.MSCOMISION_ID = MSPRE.MSCOMISION_ID
                          WHERE mst.MSPAQUETE_ID = mps.ID  
                            AND mst.FORMAFACTURACION = 'S');
       EXCEPTION
            WHEN NO_DATA_FOUND THEN
                BEGIN
                    SELECT distinct paq.id 
                           ,clip.id
                      INTO msPaquete
                           ,msClientePaqueteId
                      FROM MSPAQUETE paq
                      JOIN MSCLIENTEPAQUETE clip
                        ON clip.MSPAQUETE_ID = paq.id
                      JOIN MSCLIENTE cli
                        ON cli.id = clip.MSCLIENTE_ID
                      JOIN MSTARIFA tr
                        ON TR.MSPAQUETE_ID = clip.MSPAQUETE_ID
                     WHERE CODPAQCOMIS = 1
                       AND cli.NROCLIENTE = :NEW.NROCLIENTE
                       AND paq.PRECARGADO = 1;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                     msPaquete := 0;
                END;
      END;
      --
      IF msPaquete <> 0 THEN
          --
          WHILE :NEW.cantidadEvento >= cantidadEventoVariable LOOP
              --
              FOR employee_rec IN eventoComisionCursor LOOP
                  --
                  BEGIN
                      --
                      SELECT mst.id,
                             mst.MODALIDADCOBRO
                        INTO msTarifaId,
                             modalidadDeCobro
                        FROM MSTARIFA mst
                       WHERE mst.MSPAQUETE_ID = msPaquete
                         AND mst.MSCOMISION_ID = employee_rec.MSCOMISION_ID
                         AND mst.FECVIGENCIA <= fecProceso
                         AND mst.FECVENCIMIENTO >= fecProceso
                         AND mst.FORMAFACTURACION = 'S';
                      --
                      procesar := 'S';
                      --
                  EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                          procesar := 'N';
                  END;
                  --
                  IF procesar = 'S' THEN
                      --
                      update mstarifa 
                         set fecUltUso = fecProceso,
                             fecUltAct = :NEW.fecProceso,
                             usrUltAct = :NEW.USRPROCESO
                       where id = msTarifaId;
                      --    DBMS_OUTPUT.put_line (msTarifaId);
                      --
                      SELECT ID_SEQ_LOGPROCESO.NEXTVAL 
                        into logProcesoId
                        FROM DUAL;
                      --
                      INSERT INTO LOGPROCESOS
                            (ID,NROPROCESO, PASOACCION, ESTADOPROCESO, GRABADODESDE, NODO, DATOSERROR,  FECHA,  USUARIO)
                      VALUES(logProcesoId, 35, CONCAT('Calculo de comision para  Evento con MSEventosBanco.ID : ', :new.id) , 200, 'Trigger_MSEVENTOBANCO', 'Trigger_MSEVENTOBANCO', 0, :NEW.fecProceso, :NEW.USRPROCESO);
                      --
                      SELECT ID_SEQ_MOVIMIENTOACOBRAR.NEXTVAL 
                        into movimientoCobrarId
                        FROM DUAL;
                      --
                      INSERT INTO MSMOVIMIENTOACOBRAR
                          (ID, FECMOVIMIENTO, MODALIDADCOBRO, MSCLIENTEPAQUETE_ID, MSEVENTOBANCO_ID, MSTARIFA_ID)
                      VALUES(movimientoCobrarId, fecProceso , modalidadDeCobro, msClientePaqueteId, :NEW.id, msTarifaId);
                      --
                  END IF;
              --
              END LOOP;
              --
               cantidadEventoVariable := cantidadEventoVariable + 1;
              --
          END LOOP;
          --
      END IF;
      --
      SELECT ID_SEQ_LOGPROCESO.NEXTVAL 
        into logProcesoId
        FROM DUAL;
      --
      INSERT INTO LOGPROCESOS
          (ID,NROPROCESO, PASOACCION, ESTADOPROCESO, GRABADODESDE, NODO, DATOSERROR,  FECHA,  USUARIO)
      VALUES(logProcesoId, 35, 'Fin', 900, 'Trigger_MSEVENTOBANCO', 'Trigger_MSEVENTOBANCO', 0, :NEW.fecProceso, :NEW.USRPROCESO);
       --
  END;