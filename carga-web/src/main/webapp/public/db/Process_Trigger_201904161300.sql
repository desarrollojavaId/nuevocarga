create or replace PACKAGE Process_Trigger IS
/* ----------------------------------------------------------------------------   
   Se encarga de procesar y registrar en Logtransacciones
   (seg�n evento Insert, Update o Delete)
  ------------------------------------------------------------------------ */
   PROCEDURE Set_Table_Name(P_Table VARCHAR2);
   PROCEDURE Clean_Table_Columns;
   PROCEDURE Delete_Table_Columns;
   PROCEDURE Create_Column_Names;
   PROCEDURE Process_Old_Values(P_Rowid ROWID);
   PROCEDURE Process_New_Values(P_Rowid ROWID);
   PROCEDURE Insert_Log_Inserting;
   PROCEDURE Insert_Log_Updating;
   PROCEDURE Insert_Log_Deleting;

END;