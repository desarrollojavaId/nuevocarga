var dataEditar = '<a class="edit" href="javascript:void(0)" data-toggle="tooltip" title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;';
var dataEliminar = '<a class="remove" href="javascript:void(0)" data-toggle="tooltip" title="Eliminar"><i class="fa fa-trash"></i></a>&nbsp;';
var dataShow = '<a class="ver" href="javascript:void(0)" data-toggle="tooltip" title="Consultar"><i class="fa fa-search"></i></a>';
//Esta variable se crea para tener control de cuando se tiene que filtrar por enter
var permitirFiltrarEnter = true;

/***
 * Funcion para armar una grilla dinamica
 * @param idTabla id de la tabla al cual se le asginaran los td dinamicos
 * @param idIndex Index de la nueva fila
 * @param nuevoItemTd items agregar a la tabla
 * @param idCampo id del campo que tendra el arreglo de json
 * @param refrescarCamposDespueEliminar indicador de si despues de eliminar una fila de la grilla se vuelve a refrescar los datos
 * @returns
 */
function grillaDinamica(idTabla, index, nuevoItemTd, idCampo, refrescarCamposDespueEliminar) {
	var templateTr  = '<tr class="item"  id="{id}" data-id="{id}"></tr>';
	var templateTd  = '<td><input type="{type}" campoValido="true" class="form-control" id="{id}" name="{name}" value="{data}" required ></td>';
	var templateH   = '<input type="{type}" campoValido="true" id="{id}" name="{name}" value="{data}" >';
	var templateBtn = '<td class="text-center"><button type="button" class="btn btn-danger btn-xs remove-item"><i class="fa fa-times"';
	var nuevoItemTr = {
	   id : index 
	}
    //Se agrega la nueva TR a la tabla, accediendo a atraves del id del TBODY configurado en la vista
    $('#' + idTabla).append(nano(templateTr, nuevoItemTr));
	//Ya que puede ver multiples grillas dinamcias en una sola vista, se busca el nuevo TD del TBODY creado, para 
	//asignarle directamente los nuevos TD
	var trNuevo = $('#' + idTabla).find("tr[id=" + index + "]");
    //Por cada objeto agregar se recorre y se arma el template
    $.each(nuevoItemTd, function(i, item){
    	item.type === undefined ? "text" : item.type;
    	templateTd = item.type === undefined ? templateTd : templateH;
    	trNuevo.append(nano(templateTd, item))
    });
    //Al final se concatena el boton de eliminar a la fila
    trNuevo.append(templateBtn);
    //
	$('.remove-item').off().click(function(e) {
		var idRemove = $(this).parent('td').parent('tr').attr("id");
		borrarCamposDinamicos(idCampo,idRemove);
       //En accion elimino el item de la Tabla
	   $(this).parent('td').parent('tr').remove();
	   if ($('#tabla_pasos tr.item').length == 0) {
	       $('#tabla_pasos .no-item').slideDown(300);
	   };
	   //Se vuelve a recorrer la lista de items actuales para renombrar los id segun el index que posea esa grilla
		$('.item').each(function(index, element) {
			$("#" + $(this).attr("id") + '-id' ).attr("id", index + 1 + '-id'); 
			$(this).find('td input').map(function() {
				if($(this).attr("campoValido") === "true"){
					var idViejo = $(this).attr("id");
					var idNuevo = idViejo.substring(idViejo.indexOf("-"), idViejo.length);
					idNuevo = index + 1 + idNuevo;
					$(this).attr("id", idNuevo);
				}
			});
			$(this).attr("id", index + 1);
			$(this).attr("data-id", index + 1);
		});
		//
		if(refrescarCamposDespueEliminar){
			refrescaCamposDinamicos(idCampo);
		}
    });
}

/***
 * Funcion para armar json con los campos creados de la grilla dinamica
 * @param idCampo id del campo a setear los valores
 * @param orden indicador si se ordena el arreglo por una propiedad en esepcifico
 * @param propiedadOrdenar propiedad por la cual ordena
 * @returns
 */
function refrescaCamposDinamicos(idCampo, orden, propiedadOrdenar) {
	   if(idCampo === null || idCampo === undefined){
		  idCampo = 'camposDinamicos';
	   }
	   var object;
	   var datos = [];
		$('.item').each(function(index, element) {
			var idElementoActual = $(this).attr("id");
			var idBaseDatos = $("#"+ idElementoActual + "-id").val();
			
			if($("#"+ idElementoActual + "-codigo").val() != undefined)
			$("#"+ idElementoActual + "-codigo").val($("#"+ idElementoActual + "-codigo").val().replace(/\"/g, '')); 
			
			if($("#"+ idElementoActual + "-valor").val() != undefined)
			$("#"+ idElementoActual + "-valor").val($("#"+ idElementoActual + "-valor").val().replace(/\"/g, '')); 
			//alert("codigo: " +$("#"+ idElementoActual + "-codigo").val())
			//alert("valor: " +$("#"+ idElementoActual + "-valor").val())
			
			var nameBaseDatos = $("#"+ idElementoActual + "-id").attr("name");
			object = {}
			idBaseDatos = idBaseDatos === "" ? 0 : idBaseDatos;
			//Se concatena el id del registro de la base de datos
			object[nameBaseDatos] = idBaseDatos;
			//Se concatena un id para identificar el registro de la grilla, solo es para uso en el JSP
			object.idGrilla = index + 1;
			object.borrarRegistro = false;
			//Se arma el objeto en forma de json con todos los input y valor
			$(this).find('td input').map(function() {
				if($(this).attr("campoValido") === "true"){
					var name = $(this).attr("name");
					object[name] =  $(this).val();
					return object;
				}
			});
			//Se arma una arreglo con todo los objetos
			datos.push(object);
		});
		if(orden){
			//Se ordena el arreglo por la propiedad
			datos.sort(function (a, b) {
			    return a[propiedadOrdenar].localeCompare(b[propiedadOrdenar]);
			});
		}
		//Se recorre los campos ya seteados para validar si hay uno que este con la marca de eliminar
		//para setearlo y no perder esos datos y poder borrar ese registro en el controler
		if($("#" + idCampo).val() != ""){
			var camposActuales = $.parseJSON($("#" + idCampo).val());
			$.each(camposActuales,function(index, element) {
				if(element.borrarRegistro && element.id != "0"){
					datos.push(element);
				}
			});
		}
		datos = JSON.stringify(datos);
		$("#" + idCampo).val(datos);
};
/****
 * Funcion para borrar un campo de una grilla dinamica
 * @param idCampo id del campo para setear el arreglo de json 
 * @param idRemove id de la fila a eliminar
 * @returns
 */
function borrarCamposDinamicos(idCampo,idRemove){
	if(idCampo === null || idCampo === undefined){
	    idCampo = 'camposDinamicos';
	}
	if($("#" + idCampo).val() != ""){
		var camposActuales = $.parseJSON($("#" + idCampo).val());
		//Se busca el id del elemento generado por BD, sino existe quiere decir que se esta eliminando un registro "nuevo"
		//y se toma el id de la grilla para eliminar el elemento de la grilla
		idRemove = $("#" + idRemove + "-id").val() === "" ? idRemove : $("#" + idRemove + "-id").val();
		var elementos = [];
		$.each(camposActuales,function(index, element) {
			//Si el elemento es igual al que estoy eliminando y es un registro de BD se marca como true el borrar
			if($.isNumeric(element.id) && parseInt(element.id) === parseInt(idRemove)){
				element.borrarRegistro = true;
			}
			else if(!$.isNumeric(element.id) && element.id === idRemove){
				element.borrarRegistro = true;
			}
			//Solo se vuelve armar el JSON con los registros, los datos que vienen de BD y 
			//los registros diferentes al que se esta eliminando
			if(element.id != "0" || parseInt(element.idGrilla) !== parseInt(idRemove)){
				elementos.push(element);	
			}
		});
		elementos = JSON.stringify(elementos);
		$("#" + idCampo).val(elementos);
	}
}

//Genera un mensaje popup
function popup(message) {
	BootstrapDialog.show({
        message: message,
        buttons: [ {
            label:  'Aceptar',
            hotkey: 13,
            action: function(dialogo){
          	   dialogo.close();
            }
        } ],
        onshow : function(dialogo) {
        	permitirFiltrarEnter = false;
        },
        onhidden : function(dialogo) {
        	permitirFiltrarEnter = true;
        }
    });
}

function array_contains(array, obj) {
	if (array == null) {
		return false;
	}
	var i = array.length;
	while (i--) {
		if (array[i] === obj) {
			return true;
		}
	}
	return false;
}

function isEmpty(str) {
	return (!str || 0 === str.length);
}

function booleanFormatter(value, row, index) {
	return value == true || value == 'true' ? "S&iacute;" : "No";
}

function selectComboSiNo(idCampos){
	//
	var data =  [ {
        "id": "S",
        "text": "Si"
      },{
        "id": "N",
        "text": "No"
      } ];
	idCampos = idCampos.split(",");
	//
	$.each(idCampos,function(index, element) {
		$('#' + element).select2({
			data : data,
			allowClear : true,
			placeholder: "Seleccionar"
		});
	});
}

function cargarFeriados(e) {
	var year;
	if (typeof e.date === "undefined") {
		year = new Date(e.timeStamp).getFullYear();
	} else {
		year = e.date.getFullYear();
	}
	var picker = $(this);
	$.ajax({
		type : 'GET',
		url : claseFechaUrl + "/" + year,
		dataType : 'json',
		success : function(results) {
			picker.datepicker('setDatesDisabled', results);
		}
	})
	return true;
}

function remove(tableSelector, removeUrl,redirectUrl, id) {
	BootstrapDialog.confirm('Confirma que desea eliminar el item', function(
			result) {
		if (result) {
			$.getJSON(removeUrl + id, function(result) {
				if (result.error) {
					BootstrapDialog.alert(result.message);
				} else {
					if(tableSelector == null){
						window.location = redirectUrl;  
					}else{
						$(tableSelector).bootstrapTable('refresh', {
							silent : true
						});
					}
				}
			});
		}
	});
}

function registerParsley() {
	//
	if ($('.frm .parsley-save').size < 1) {
		return;
	}
	$('.frm .parsley-save').click(function() {
		$(this).prop('disabled', true);
		var targetForm = $('.frm');
		var parsley = targetForm.parsley().destroy();
		var parsley = targetForm.parsley({
			'excluded' : ':disabled'
		});
		parsley.validate();

		if (parsley.isValid()) {
			targetForm.submit();
		} else {
			$(this).prop('disabled', false);
		}
	});
}

/*
function registerParsley() {
	//
	var targetForm = $('.frm');
	var parsley = targetForm.parsley().destroy();
	var parsley = targetForm
			.parsley({
				errorsWrapper : false,
				excluded : 'input[type=button], input[type=submit], input[type=reset], input[type=hidden], :disabled',
				trigger : 'focusin, focusout, input',
				validationThreshold: 0
			});
	parsley.on('field:error', function(fieldInstance) {
		var messages = fieldInstance.getErrorsMessages();
		var field = fieldInstance.$element;
		field.tooltip('destroy');
		field.tooltip({
			animation: false,
			container: 'body',
			placement: 'auto',
			title: messages
		});
		//field.tooltip('enable');
		field.tooltip('show');
	});
	parsley.on('field:success', function(fieldInstance) {
		var field = fieldInstance.$element;
		field.tooltip('destroy');

	});
	if ($('.frm .parsley-save').size < 1) {
		return;
	}
	$('.frm .parsley-save').click(function() {
		$(this).prop('disabled', true);
		parsley.validate();
		if (parsley.isValid()) {
			targetForm.submit();
		} else {
			$(this).prop('disabled', false);
		}
	});
}
*/

function actionFormatterGridData(value, row, index) {
	var array = new Array();
	array.push('<!-- ' + row.id + ' -->');
	if(row.fechaBajaLog == undefined){
		if (this.editar)
			array.push(dataEditar);
		if (this.eliminar)
			array.push(dataEliminar);
		if(this.detalle){
			var info = this.info == undefined ? "" : this.info;
			array.push('<a class="detalle" href="javascript:void(0)" data-toggle="tooltip" title=' + info +' ><i class="fa fa-align-justify"></i></a>&nbsp;');
		}
		if(this.detalledos){
			var info = this.infodos == undefined ? "" : this.infodos;
			array.push('<a class="detalledos" href="javascript:void(0)" data-toggle="tooltip" title=' + info +' ><i class="fa fa-align-justify"></i></a>&nbsp;');
		}
		if(this.detalletres){
			var info = this.infotres == undefined ? "" : this.infotres;
			array.push('<a class="detalletres" href="javascript:void(0)" data-toggle="tooltip" title=' + info +' ><i class="fa fa-align-justify"></i></a>&nbsp;');
		}
	}
	if(this.show){
		array.push(dataShow);
	}
	return array.join('');
}

function dateFormatter(value,row,index){
	var fechaFormateada;
	if(value != undefined ){
		fechaFormateada = moment.unix(value/1000).utc().format('DD/MM/YYYY');
		// fechaFormateada = $.format.date(new Date(value), "dd/MM/yyyy");	
	}
	return fechaFormateada;
}

function limpiarFiltrosSession(nombreFiltros){
	$.get(limpirFiltrosUrl + nombreFiltros);
}

function comboBoxByParent(input, listUrl, idUrl, parentValue) {
	$(input).select2({
		placeholder : 'Seleccionar',
		allowClear : true,
		initSelection : function(element, callback) {
			var id = $(element).val();
			if (id !== "") {
				$.ajax(idUrl + id, {
					dataType : "json"
				}).done(function(data) {
					callback({
						text : data.code + ' - ' + data.name,
						id : data.id
					});
				});
			}
		},
		ajax : {
			url : listUrl,
			dataType : 'json',
			type : "GET",
			quietMillis : 50,
			data : function(term) {
				return {
					parent : parentValue,
					search : term
				};
			},
			results : function(data) {
				return {
					results : $.map(data, function(item) {
						return {
							text : item.code + ' - ' + item.name,
							id : item.id
						}
					})
				};
			}
		}
	});
}
//Agrega a todos los elementos del form el atributo readonly
function readOnlyForShow(id){
	$("#" + id + " input").attr('readonly',true);
	$("#" + id + " input").datepicker("destroy");
	$("#" + id + " input[type=radio]").prop("disabled",true);
}

/***
 * Funcion para ordenar un arreglo de objeto
 * @returns
 */

//
function ordenar(propiedadOrdenar, segundaPropiedadOrdenar){
	return function (campo1,campo2) {
		campo1 = campo1[propiedadOrdenar];
	    campo2 = campo2[propiedadOrdenar];
		var campo1Propiedad2 = campo1[propiedadOrdenar];
	    var campo2Propiedad2 = campo2[propiedadOrdenar];
	    if(segundaPropiedadOrdenar != null){
	    	campo1Propiedad2 = campo1[segundaPropiedadOrdenar];
		    campo2Propiedad2 = campo2[segundaPropiedadOrdenar];
	    }
	    if(campo1 > campo2 && campo1Propiedad2 > campo2Propiedad2) {
	       return 1;
	    } else if (campo1 < campo2 && campo1Propiedad2 < campo2Propiedad2) {
	       return -1;
	    }
	    return 0;	
	}
}

/****
 * Funcion para validar cuit
 * @returns
 */

function validarCuit(){
	window.Parsley.addValidator('cuit',
	    function (cuit, requirement) {
	        if (cuit.length != 11) {
	           return false;
	        } else {
	        	console.log('el cuit es : ' + cuit);
	           var mult = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
	           var total = 0;
	           for (var i = 0; i < mult.length; i++) {
	             total += parseInt(cuit[i]) * mult[i];
	           }
	           var mod = total % 11;
	           var digito = mod == 0 ? 0 : mod == 1 ? 9 : 11 - mod;
	           return digito == parseInt(cuit[10]);
	        }
	    }, 32);
}


/****
 * Funcion para validar CBU
 * @returns
 */

function validarCBU(){
	window.Parsley.addValidator('cbu',
	    function (cbu, requirement) {
	        if (cbu.length != 22) {
	           return false;
	        } else {
	        	var validarCodigoBanco;
	        	var codigo = cbu.substring(0,8);
	        	var diferencia = 0;
	        	if(codigo.length != 8){
	        		validarCodigoBanco = false;
	        	}else {
	        		var banco = codigo.substring(0,3);
	        		var digitoVerificador = codigo.charAt(3);
	        		var sucursal = codigo.substring(4,7);
	        		var digitoVerificador2 = codigo.charAt(7);
	        		var suma = banco.charAt(0) * 7 + banco.charAt(1) * 1 + banco.charAt(2) * 3 + digitoVerificador * 9 + sucursal.charAt(0) * 7 + sucursal.charAt(1) * 1 + sucursal.charAt(2) * 3;
	        		if (suma % 10 != 0) {
	        			diferencia = 10 - (suma % 10);
	        		}
	        		validarCodigoBanco =  diferencia == digitoVerificador2;
	        	}
	        	var cuentaValidar;
	        	var cuenta = cbu.substring(8,22)
	        	if(cuenta.length != 14){
	        		cuentaValidar = false;
	        	}else {
	        		var digitoVerificador = cuenta.charAt(13);
	        		var diferencia = 0;
	        		var suma = cuenta.charAt(0) * 3 + cuenta.charAt(1) * 9 + cuenta.charAt(2) * 7
    				+ cuenta.charAt(3) * 1 + cuenta.charAt(4) * 3 + cuenta.charAt(5) * 9
    				+ cuenta.charAt(6) * 7 + cuenta.charAt(7) * 1 + cuenta.charAt(8) * 3
    				+ cuenta.charAt(9) * 9 + cuenta.charAt(10) * 7 + cuenta.charAt(11) * 1
    				+ cuenta.charAt(12) * 3;
	        		if (suma % 10 != 0){
	        			diferencia = 10 - (suma % 10);
	        		}
	        		cuentaValidar = diferencia == digitoVerificador;
	        	}
	        	return validarCodigoBanco && cuentaValidar;
	        }
	    }, 32);
}

$(function() {
	//Se hace la validacion aqui de los campos fechas para que sea algo generico,
	$("input[type=date]").attr("max",'2999-12-31').attr("min", '1900-01-01');
	$("input[type=date]").attr("data-parsley-max-message","Fecha no permitida").attr("data-parsley-min-message","Fecha no permitida").attr("data-parsley-range-message","Fecha no permitida");
	//
	validarCuit();
	validarCBU();

	// Detiene la ejecución normal del submit de los formularios de filtros, 
	// para que NO se recargen las grillas cuando se presiona Enter
	// Esto es así proque los Enter funcionar cómo botón buscar
	$("#buscar_form").submit(function(e) {
		e.preventDefault();
	});
	
});

//
function customSelect2(idCampo, dataCustom, messageFormatNoMaches){
	if(dataCustom === undefined) {
		dataCustom = [];	
	}
	if(messageFormatNoMaches === undefined) {
		messageFormatNoMaches = "No se encontraron resultados"
	}
	if(idCampo != null) {
		$.each(idCampo.split(","), function( index, value ) {
			$('#' + value).select2({
				data : dataCustom,
				allowClear : true,
				placeholder: "Seleccionar",
				formatNoMatches: function(term) {
				  return messageFormatNoMaches;
				}
			});
		});
	}
}

//Funcion para tomar "Enter" como accion de filtrado
function filtrarEnter(idBoton){
	$(document).keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13' && permitirFiltrarEnter) {
        	$("#" + idBoton).click();
		}
	});
}

