(function($) {
	$.fn.comboBoxMail = function(options) {
		var opts = $.extend({}, $.fn.comboBoxMail.defaults, options);
		$(this).select2({
			createSearchChoice : function(term) {
				var value = term;
				if (validateEmail(value)) {
					return {
						id : value,
						text : value
					};
				}
				return null;
			},
			multiple : opts.multiple,
			data : [],
			initSelection : function(element, callback) {
				var data = $(element).val().split(',');
				var result = $.map(data, function(item) {
					return {
						id : item,
						text : item
					}
				});
				callback(result);
			}
		});
	}
	
	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	$.fn.comboBoxMail.defaults = {
		multiple : true
	};
})(jQuery);