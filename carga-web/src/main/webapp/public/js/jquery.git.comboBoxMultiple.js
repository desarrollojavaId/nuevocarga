(function($) {
	$.fn.comboBoxMultiple = function(listUrl, idUrl, options) {
		var opts = $.extend({}, $.fn.comboBoxMultiple.defaults, options);
		$(this).select2({
			placeholder : 'Seleccionar',
			allowClear : opts.allowClear,
			multiple : true,
			initSelection : function(element, callback) {
				var id = $(element).val();
				if (id !== "") {
					$.ajax(idUrl + id, {
						dataType : "json"
					}).done(function(data) {
						if ($.isArray(data)) {
							var result = $.map(data, function(item) {
								return {
									id : item.id,
									text : opts.toText(item),
									object : item,
								}
							});
							callback(result);
						} else {
							callback({
								text : opts.toText(data),
								id : data.id,
								data : data
							});
						}
					});
				}
			},
			ajax : {
				url : listUrl,
				dataType : 'json',
				type : "GET",
				quietMillis : 50,
				data : function(term, page) { // page is the one-based page
					// number
					return {
						search : term, // search term
						page : page
					};
				},
				results : function(data, page) {
					var more = (page * 10) < data.total; // whether or not
					// there
					return {
						results : $.map(data.rows, function(item) {
							return {
								object : item,
								text : opts.toText(item),
								id : item.id
							}
						}),
						more : more
					};
				}
			}
		});
	};

	$.fn.comboBoxMultiple.defaults = {
		allowClear : true,
		toText : function(data) {
			return data.nombre;
		}
	};

})(jQuery);