(function($) {
	$.fn.comboBoxPaged = function(listUrl, idUrl, options) {
		var opts = $.extend({}, $.fn.comboBoxPaged.defaults, options);
		$(this).select2({
			placeholder : 'Seleccionar',
			allowClear : opts.allowClear,
			initSelection : function(element, callback) {
				var id = "";
				if($(element).data("tabla") != undefined && ($(element).data("subcodigo") != undefined || $(element).data("subcodigo") == null) && $(element).val() !== ""){
					//Se valida si el combo pertenece a un subcodigo del mstablas
					if($(element).data("funcionsubcodigo")) {
						var codgio =  $(element).data("codigo") == undefined ? null : $(element).data("codigo"); 
						id = $(element).data("tabla") + ','+ codgio + ',' + $(element).val()	
					}else {
						id = $(element).data("tabla") + ','+ $(element).val() + ',' + $(element).data("subcodigo");
					}
				}else{
					id = $(element).val();
				}
				if (id !== "") {
					$.ajax(idUrl + id, {
						dataType : "json",
						async : false,
					}).done(function(data) {
						callback({
							object : data,
							text : opts.toText(data),
							id :  opts.toId(data)
						});
					});
				}
			},
			ajax : {
				url : listUrl,
				dataType : 'json',
				type : "GET",
				quietMillis : 50,
				data : function(term, page) { // page is the one-based page
					// number
					var offset = page > 1 ? (page - 1) * 10 : 0; 
					return {
						search : term, // search term
						page : page,
						limit: 10,
						offset : offset
					};
				},
				results : function(data, page) {
					var total = data.total === undefined ? data.length : data.total;
					var dataResult = data.rows === undefined ? data : data.rows;
					var more = (page * 10) < total; // whether or not
					// there
					return {
						results : $.map(dataResult, function(item) {
							return {
								object : item,
								text : opts.toText(item),
								id : opts.toId(item)
							}
						}),
						more : more
					};
				}
			}
		});
	};

	$.fn.comboBoxPaged.defaults = {
		allowClear : true,
		toText : function(data) {
			return data.nombre;
		},
		toId: function(data) {
			return data.id;
		}
	};

})(jQuery);