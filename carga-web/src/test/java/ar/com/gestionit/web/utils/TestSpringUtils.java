package ar.com.gestionit.web.utils;

import static org.junit.Assert.*;

import java.util.Date;

import org.jasypt.util.text.BasicTextEncryptor;
import org.junit.Test;

import ar.com.gestionit.util.DateUtil;

public class TestSpringUtils {

	@Test
	public void testEsDiaInicioMes() throws Exception {
		Date fecha = null;
		fecha = DateUtil.generateDate("1982", "11", "6");
		assertFalse(SpringUtils.esDiaInicioMes(fecha));
		fecha = DateUtil.generateDate("2019", "6", "1");
		assertTrue(SpringUtils.esDiaInicioMes(fecha));
		fecha = DateUtil.generateDate("2019", "9", "11");
		assertFalse(SpringUtils.esDiaInicioMes(fecha));
		fecha = DateUtil.generateDate("2019", "1", "1");
		assertTrue(SpringUtils.esDiaInicioMes(fecha));
	}

	@Test
	public void testEsDiaFinMes() {
		fail("Not yet implemented");
	}
	
	@Test
	public void encriptar() {
		String text = "Change1234";
		System.out.println("Text      = " + text);

		BasicTextEncryptor bte = new BasicTextEncryptor();
		bte.setPassword("sGeadkgRw43nfsk");

		String encrypted = bte.encrypt(text);
		System.out.println("Encrypted = " + encrypted);

		String original = bte.decrypt(encrypted);
		System.out.println("Original  = " + original);
	}

}
